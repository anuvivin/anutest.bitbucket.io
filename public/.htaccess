# Turn Off ETags
Header unset Etag
FileETag none

# Allow assets to be delivered by the CDN
Header set Access-Control-Allow-Origin "*"

<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews -Indexes
    </IfModule>

    RewriteEngine On

    RewriteCond %{HTTP_USER_AGENT} !^Amazon\ CloudFront$
    RewriteCond %{HTTP_HOST} !^$
    RewriteCond %{HTTP_HOST} !^www\. [NC]
    RewriteRule ^ https://www.%{HTTP_HOST}%{REQUEST_URI} [R=301,L]

    RewriteCond %{HTTP_USER_AGENT} !^Amazon\ CloudFront$
    RewriteCond %{HTTP:X-Forwarded-Proto} !https
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

    # Office 365 Autodiscover Redirect
    RewriteRule ^autodiscover(/(.*))? https://autodiscover-s.outlook.com/Autodiscover/Autodiscover.xml [NC,R=302,L]

    # Handle Authorization Header
    RewriteCond %{HTTP:Authorization} .
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} (.+)/$
    RewriteRule ^ %1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]

    # Old Site Redirects
    Redirect 301 "/sharia-board" /shariah-board
    Redirect 301 "/how-it-works" /islamic-home-finance-co-ownership-program
    Redirect 301 "/faq" /home-buying-questions
    Redirect 301 "/videos" /shariah-compliant-faq-video-library
    Redirect 301 "/prequalify" /my/prequalify
    Redirect 301 "/find-an-account-executive" /connect-with-account-executive
    Redirect 301 "/assistance" /assistance-and-repayment
    Redirect 301 "/glossary" /glossary-of-terms
    Redirect 301 "/privacy-and-declarations" /privacy-and-security
    Redirect 301 "/webinars" /islamic-home-finance-webinars
    Redirect 301 "/blog" /resources/insight-articles
    Redirect 301 "/resources" /resources/insight-articles
    Redirect 301 "/myaccount" /my-account
    Redirect 301 "/contactus" /contact-us
    Redirect 301 "/islamic-finance" /us-islamic-home-finance
    Redirect 301 "/get-pre-qualified/islamic-home-finance" /my/prequalify
    Redirect 301 "/get-pre-qualified/islamic-home-finance-2014" /my/prequalify
    Redirect 301 "/islamic-home-finance-webinars/islam-purchasing-home-in-america_gc" /islamic-home-finance-webinars
    Redirect 301 "/press-release/guidance-encouraged-by-amja-resolution" /press-releases/encouraged-by-amja
    Redirect 301 "/press-release/historic-islamic-finance-training-draws-imams-from-across-the-nation" /press-releases/pioneering-event
    Redirect 301 "/press-release/pioneering-event-is-a-collaborative-effort-between-major-american-muslim-groups" /press-releases/historic-islamic-finance-training
    Redirect 301 "/guidance-goes-green" /resources/the-story-of-guidance-residential-and-the-green-home-contest

</IfModule>

# Compress HTML, CSS, JavaScript, Text, XML and fonts
<IfModule mod_mime.c>
    AddType application/javascript          js
    AddType application/vnd.ms-fontobject   eot
    AddType application/x-font-ttf          ttf ttc
    AddType font/opentype                   otf
    AddType application/x-font-woff         woff
    AddType image/svg+xml                   svg svgz
    AddEncoding gzip                        svgz
</Ifmodule>
<IfModule mod_deflate.c>
  AddOutputFilterByType DEFLATE application/javascript
  AddOutputFilterByType DEFLATE application/rss+xml
  AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
  AddOutputFilterByType DEFLATE application/octet-stream
  AddOutputFilterByType DEFLATE application/x-font
  AddOutputFilterByType DEFLATE application/x-font-opentype
  AddOutputFilterByType DEFLATE application/x-font-otf
  AddOutputFilterByType DEFLATE application/x-font-truetype
  AddOutputFilterByType DEFLATE application/x-font-ttf
  AddOutputFilterByType DEFLATE application/x-font-woff
  AddOutputFilterByType DEFLATE application/x-font-woff2
  AddOutputFilterByType DEFLATE application/x-javascript
  AddOutputFilterByType DEFLATE application/xhtml+xml
  AddOutputFilterByType DEFLATE application/xml
  AddOutputFilterByType DEFLATE font/opentype
  AddOutputFilterByType DEFLATE font/otf
  AddOutputFilterByType DEFLATE font/ttf
  AddOutputFilterByType DEFLATE font/woff
  AddOutputFilterByType DEFLATE font/woff2
  AddOutputFilterByType DEFLATE image/svg+xml
  AddOutputFilterByType DEFLATE image/vnd.microsoft.icon
  AddOutputFilterByType DEFLATE image/x-icon
  AddOutputFilterByType DEFLATE text/css
  AddOutputFilterByType DEFLATE text/html
  AddOutputFilterByType DEFLATE text/javascript
  AddOutputFilterByType DEFLATE text/plain
  AddOutputFilterByType DEFLATE text/xml
</IfModule>

# Expires Caching
<IfModule mod_expires.c>
  ExpiresActive On
  # default
  ExpiresDefault "access plus 2 days"
  # images
  ExpiresByType image/jpg "access plus 1 year"
  ExpiresByType image/jpeg "access plus 1 year"
  ExpiresByType image/gif "access plus 1 year"
  ExpiresByType image/png "access plus 1 year"
  ExpiresByType image/svg+xml "access plus 1 year"
  ExpiresByType image/x-icon "access plus 1 year"
  ExpiresByType image/vnd.microsoft.icon "access plus 1 year"
  # css
  ExpiresByType text/css "access plus 1 month"
  # javascript
  ExpiresByType text/javascript "access plus 1 year"
  ExpiresByType text/x-javascript "access plus 1 year"
  ExpiresByType application/javascript "access plus 1 year"
  ExpiresByType application/x-javascript "access plus 1 year"
  # misc
  ExpiresByType application/x-shockwave-flash "access plus 1 month"
  ExpiresByType application/pdf "access plus 1 month"
</IfModule>
