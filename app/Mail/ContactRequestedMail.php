<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactRequestedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $reason;

    /**
     * @var string|null
     */
    protected $comments;

    /**
     * Create a new message instance.
     *
     * @param  string  $name
     * @param  string  $email
     * @param  string  $phone
     * @param  string  $reason
     * @param  string|null  $comments
     *
     * @return void
     */
    public function __construct($name, $email, $phone, $reason, $comments = null)
    {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->reason = $reason;
        $this->comments = $comments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.contact')
            ->with([
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'reason' => $this->reason,
                'comments' => $this->comments,
            ]);
    }
}
