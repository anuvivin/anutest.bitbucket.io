<?php

namespace App\Exceptions;

use Log;
use Request;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        // Short circuit exception reporting from mix() helper.
        if (starts_with($exception->getMessage(), 'Unable to locate Mix file')) {
            return;
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // Write any 404 urls to the log file to help identify missing urls during the first few weeks after go live. Can be removed after 4/1/18.
        if ($exception instanceof NotFoundHttpException) {
            Log::error('MISSING URL: '.Request::fullUrl());
        }

        return parent::render($request, $exception);
    }
}
