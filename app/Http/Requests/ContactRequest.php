<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return trans('contact.attributes');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason' => ['required', 'string', 'max:100'],
            'name' => ['required', 'string', 'max:50'],
            'email' => ['required', 'email'],
            // string containing 10+ digits
            'phone' => ['required', 'string', 'regex:/(?=(?:[^0-9]*[0-9]){10})/'],
            'comments' => ['nullable', 'string', 'max:300'],
        ];
    }
}
