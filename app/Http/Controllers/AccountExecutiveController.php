<?php

namespace App\Http\Controllers;

use App\Services\GiosService;

class AccountExecutiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Services\GiosService  $giosService
     * @param  string  $state
     * @return \Illuminate\Http\Response
     */
    public function index(GiosService $giosService, $state = null)
    {
        $state = strtolower((strlen($state) == 2 ? $giosService->convertStateAbbrevToName($state) : $state));
        $states = $giosService->getAccountExecutiveStates();
        $aes = $giosService->getAccountExecutiveList($state);

        return view('pages.accountExecutives.index')->with([
            'states' => $states,
            'selected_state' => (isset($states[$state]) ? $state : ''),
            'aes' => $aes,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Services\GiosService  $giosService
     * @param  string  $ae
     * @return \Illuminate\Http\Response
     */
    public function show(GiosService $giosService, $ae)
    {
        $ae_obj = $giosService->getAccountExecutiveDetails($ae);
        if (!$ae_obj) {
           return redirect()->route('account-executive.index');
        }
        $ae_obj->testimonials = $giosService->getAccountExecutiveTestimonials($ae);
        return view('pages.accountExecutives.show')->with([
            'ae' => $ae_obj,
        ]);
    }

}
