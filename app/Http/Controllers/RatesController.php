<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RatesService;

class RatesController extends Controller
{
    /**
     * Show the edit view for rates.
     *
     * @param  \App\Services\RatesService  $ratesService
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(RatesService $ratesService)
    {
        return response()->view('pages.rates.edit', [
            'rates' => $ratesService->getRates(),
            'save_result' => session()->get('save_result'),
        ]);
    }

    /**
     * Update the rates and redirect back the result.
     *
     * @param  \App\Services\RatesService  $ratesService
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RatesService $ratesService, Request $request)
    {
        $regex_rate = '/^[0-9]+\.[0-9]{3}$/';
        $regex_amount = '/^[\-]?[0-9]+$/';

        $this->validate($request, [
            'rates.guidance-residential.purchase_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.guidance-residential.purchase_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.guidance-residential.purchase_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.guidance-residential.purchase_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.guidance-residential.purchase_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.guidance-residential.refinance_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.guidance-residential.refinance_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.guidance-residential.refinance_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.guidance-residential.refinance_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.guidance-residential.refinance_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',

            'rates.quicken-loans.purchase_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.quicken-loans.purchase_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.quicken-loans.purchase_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.quicken-loans.purchase_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.quicken-loans.purchase_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.quicken-loans.refinance_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.quicken-loans.refinance_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.quicken-loans.refinance_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.quicken-loans.refinance_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.quicken-loans.refinance_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',

            'rates.citi.purchase_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.citi.purchase_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.citi.purchase_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.citi.purchase_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.citi.purchase_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.citi.refinance_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.citi.refinance_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.citi.refinance_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.citi.refinance_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.citi.refinance_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',

            'rates.wells-fargo.purchase_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.wells-fargo.purchase_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.wells-fargo.purchase_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.wells-fargo.purchase_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.wells-fargo.purchase_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.wells-fargo.refinance_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.wells-fargo.refinance_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.wells-fargo.refinance_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.wells-fargo.refinance_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.wells-fargo.refinance_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',

            'rates.boa.purchase_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.boa.purchase_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.boa.purchase_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.boa.purchase_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.boa.purchase_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.boa.refinance_profit_rate' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.boa.refinance_apr' => 'required|regex:/^[0-9]+\.[0-9]{3}$/',
            'rates.boa.refinance_discount_points' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.boa.refinance_origin_fee' => 'required|regex:/^[\-]?[0-9]+$/',
            'rates.boa.refinance_total_fee' => 'required|regex:/^[\-]?[0-9]+$/',
        ], [], [
            'rates.guidance-residential.purchase_profit_rate' => 'Purchase > Guidance > Profit Rate',
            'rates.guidance-residential.purchase_apr' => 'Purchase Guidance APR',
            'rates.guidance-residential.purchase_discount_points' => 'Purchase Guidance Discount Points',
            'rates.guidance-residential.purchase_origin_fee' => 'Purchase Guidance Origination Fee',
            'rates.guidance-residential.purchase_total_fee' => 'Purchase Guidance Total Fee',
            'rates.guidance-residential.refinance_profit_rate' => 'Refinance Guidance Profit Rate',
            'rates.guidance-residential.refinance_apr' => 'Refinance Guidance APR',
            'rates.guidance-residential.refinance_discount_points' => 'Refinance Guidance Discount Points',
            'rates.guidance-residential.refinance_origin_fee' => 'Refinance Guidance Origination Fee',
            'rates.guidance-residential.refinance_total_fee' => 'Refinance Guidance Total Fee',

            'rates.quicken-loans.purchase_profit_rate' => 'Purchase Quicken Profit Rate',
            'rates.quicken-loans.purchase_apr' => 'Purchase Quicken APR',
            'rates.quicken-loans.purchase_discount_points' => 'Purchase Quicken Discount Points',
            'rates.quicken-loans.purchase_origin_fee' => 'Purchase Quicken Origination Fee',
            'rates.quicken-loans.purchase_total_fee' => 'Purchase Quicken Total Fee',
            'rates.quicken-loans.refinance_profit_rate' => 'Refinance Quicken Profit Rate',
            'rates.quicken-loans.refinance_apr' => 'Refinance Quicken APR',
            'rates.quicken-loans.refinance_discount_points' => 'Refinance Quicken Discount Points',
            'rates.quicken-loans.refinance_origin_fee' => 'Refinance Quicken Origination Fee',
            'rates.quicken-loans.refinance_total_fee' => 'Refinance Quicken Total Fee',

            'rates.citi.purchase_profit_rate' => 'Purchase Citi Profit Rate',
            'rates.citi.purchase_apr' => 'Purchase Citi APR',
            'rates.citi.purchase_discount_points' => 'Purchase Citi Discount Points',
            'rates.citi.purchase_origin_fee' => 'Purchase Citi Origination Fee',
            'rates.citi.purchase_total_fee' => 'Purchase Citi Total Fee',
            'rates.citi.refinance_profit_rate' => 'Refinance Citi Profit Rate',
            'rates.citi.refinance_apr' => 'Refinance Citi APR',
            'rates.citi.refinance_discount_points' => 'Refinance Citi Discount Points',
            'rates.citi.refinance_origin_fee' => 'Refinance Citi Origination Fee',
            'rates.citi.refinance_total_fee' => 'Refinance Citi Total Fee',

            'rates.wells-fargo.purchase_profit_rate' => 'Purchase Wells Fargo Profit Rate',
            'rates.wells-fargo.purchase_apr' => 'Purchase Wells Fargo APR',
            'rates.wells-fargo.purchase_discount_points' => 'Purchase Wells Fargo Discount Points',
            'rates.wells-fargo.purchase_origin_fee' => 'Purchase Wells Fargo Origination Fee',
            'rates.wells-fargo.purchase_total_fee' => 'Purchase Wells Fargo Total Fee',
            'rates.wells-fargo.refinance_profit_rate' => 'Refinance Wells Fargo Profit Rate',
            'rates.wells-fargo.refinance_apr' => 'Refinance Wells Fargo APR',
            'rates.wells-fargo.refinance_discount_points' => 'Refinance Wells Fargo Discount Points',
            'rates.wells-fargo.refinance_origin_fee' => 'Refinance Wells Fargo Origination Fee',
            'rates.wells-fargo.refinance_total_fee' => 'Refinance Wells Fargo Total Fee',

            'rates.boa.purchase_profit_rate' => 'Purchase BoA Profit Rate',
            'rates.boa.purchase_apr' => 'Purchase BoA APR',
            'rates.boa.purchase_discount_points' => 'Purchase BoA Discount Points',
            'rates.boa.purchase_origin_fee' => 'Purchase BoA Origination Fee',
            'rates.boa.purchase_total_fee' => 'Purchase BoA Total Fee',
            'rates.boa.refinance_profit_rate' => 'Refinance BoA Profit Rate',
            'rates.boa.refinance_apr' => 'Refinance BoA APR',
            'rates.boa.refinance_discount_points' => 'Refinance BoA Discount Points',
            'rates.boa.refinance_origin_fee' => 'Refinance BoA Origination Fee',
            'rates.boa.refinance_total_fee' => 'Refinance BoA Total Fee',
        ]);

        $saveResult = $ratesService->updateRates($request->input('rates'));

        return redirect()->back()->with(['save_result' => $saveResult]);
    }

    /**
     * Show the rates view, defaulting to the purchase tab.
     *
     * @param  \App\Services\RatesService  $ratesService
     *
     * @return \Illuminate\Http\Response
     */
    public function getPurchaseRates(RatesService $ratesService)
    {
        return response()->view('pages.rates.list', [
            'rates' => $ratesService->getRates(),
            'rate_default_type' => 'purchase',
        ]);
    }

    /**
     * Show the rates view, defaulting to the refinance tab.
     *
     * @param  \App\Services\RatesService  $ratesService
     *
     * @return \Illuminate\Http\Response
     */
    public function getRefinanceRates(RatesService $ratesService)
    {
        return response()->view('pages.rates.list', [
            'rates' => $ratesService->getRates(),
            'rate_default_type' => 'refinance',
        ]);
    }

    /**
     * Return the current rates in json format.
     *
     * @param  \App\Services\RatesService  $ratesService
     *
     * @return \Illuminate\Http\Response
     */
    public function getCurrentRates(RatesService $ratesService)
    {
        return response()->json($ratesService->getRates());
    }

}
