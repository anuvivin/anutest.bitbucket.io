<?php

namespace App\Http\Controllers;

use Session;
use App\Http\Forms\ListingForm;
use App\Http\Requests\ListingRequest;

class ListYourHomeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ListingRequest  $request
     * @param  \App\Http\Forms\ListingForm  $form
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ListingRequest $request, ListingForm $form)
    {
        $success = $form->handle($request);

        $message = trans(sprintf('list-your-home.messages.%s', $success ? 'success' : 'error'));

        if ($request->ajax() || $request->isJson() || $request->wantsJson()) {
            return response()
                ->json([
                    'success' => $success,
                    'message' => $message,
                    'errors' => $form->errors(),
                ])
                ->setStatusCode(($success ? 201 : 500));
        }

        Session::flash(($success ? 'success' : 'error'), $message);

        return back();
    }
}
