<?php

namespace App\Http\Controllers;

use Session;
use App\Http\Forms\ConsultationForm;
use App\Http\Requests\ConsultationRequest;

class FreeConsultationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ConsultationRequest  $request
     * @param  \App\Http\Forms\ConsultationForm  $form
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ConsultationRequest $request, ConsultationForm $form)
    {
        $success = $form->handle($request);

        $message = trans(sprintf('consultation.messages.%s', $success ? 'success' : 'error'));

        if ($request->ajax() || $request->isJson() || $request->wantsJson()) {
            return response()
                ->json([
                    'success' => $success,
                    'message' => $message,
                    'errors' => $form->errors(),
                ])
                ->setStatusCode(($success ? 201 : 500));
        }

        Session::flash(($success ? 'success' : 'error'), $message);

        return back();
    }
}
