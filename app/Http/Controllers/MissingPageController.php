<?php

namespace App\Http\Controllers;

use App\Services\GiosService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MissingPageController extends Controller
{

    /**
     * Handle the missing url.
     *
     * @param  \App\Services\GiosService  $giosService
     * @param  string  $url
     *
     * @return \Illuminate\Http\Response
     */
    public function catch(GiosService $giosService, $url)
    {
        // 1. Check to see if the missing url is an AE page
        $foundAe = $giosService->isValidAe($url);
        if ($foundAe) {
            return redirect()->route('account-executive.show', ['ae' => $url], 301);
        }

        // As a last result, throw not found exception as expected.
        throw new NotFoundHttpException();
    }

}
