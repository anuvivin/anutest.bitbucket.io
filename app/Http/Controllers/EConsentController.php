<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\GiosService;

class EConsentController extends Controller {

    protected $giosService;

    public function __construct(GiosService $giosService) {
        $this->giosService = $giosService;
    }

    public function getEConsent(Request $request) {
        $confirmationcode = $request->input('confirmationcode', '');
        if(!empty($confirmationcode)){
            $details = json_decode($this->giosService->getEConsentData($confirmationcode));
            if(empty($details->data)){
                return view('econsent.error')->with('confirmationCode', $confirmationcode);
            }
            if(!empty($details->data->date_given)) {
                return view('econsent.thanks')->with('data', $details->data);
            }
            return view('econsent.econsent')->with('data', $details->data);
        }
        return view('econsent.error')->with('confirmationCode', $confirmationcode);
    }

    public function postEConsent(Request $request) {
        $details = json_decode($this->giosService->getEConsentData($request->input('confirmationcode', '')));
        if(!empty($details->data->date_given)) {
            return view('econsent.thanks')->with('data', $details->data);
        }
        if($request->input('i-consent-confirmation',false) === '1') {
            $data = json_decode($this->giosService->doEConsentConfirm($request->input('confirmationcode', '')));
            if($data->data === true) {
                //reload details to get confirmation date
                $details = json_decode($this->giosService->getEConsentData($request->input('confirmationcode', '')));
                return view('econsent.thanks')->with('data', $details->data);
            }
        }
        return view('econsent.econsent')->with('data', $details->data);
    }

}
