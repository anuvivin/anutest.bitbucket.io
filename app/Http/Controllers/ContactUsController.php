<?php

namespace App\Http\Controllers;

use Session;
use App\Http\Forms\ContactForm;
use App\Http\Requests\ContactRequest;

class ContactUsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ContactRequest  $request
     * @param  \App\Http\Forms\ContactForm  $form
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request, ContactForm $form)
    {
        $success = $form->handle($request);

        $message = trans(sprintf('contact.messages.%s', $success ? 'success' : 'error'));

        if ($request->ajax() || $request->isJson() || $request->wantsJson()) {
            return response()
                ->json([
                    'success' => $success,
                    'message' => $message,
                    'errors' => $form->errors(),
                ])
                ->setStatusCode(($success ? 201 : 500));
        }

        Session::flash(($success ? 'success' : 'error'), $message);

        return back();
    }
}
