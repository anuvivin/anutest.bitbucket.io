<?php

namespace App\Http\Forms;

use Exception;
use App\Services\GiosService;
use App\Http\Requests\ConsultationRequest;

class ConsultationForm extends AbstractForm
{
    /**
     * @var \App\Services\GiosService
     */
    protected $gios;

    /**
     * Construct an instance of ConsultationForm.
     *
     * @param  \App\Services\GiosService  $gios
     */
    public function __construct(GiosService $gios)
    {
        parent::__construct();

        $this->gios = $gios;
    }

    /**
     * Handle the ConsultationRequest.
     *
     * @param  \App\Http\Requests\ContactRequest  $request
     *
     * @return boolean
     */
    public function handle(ConsultationRequest $request)
    {
        try {
            $input = $this->sanitize($request->input());

            return $this->gios->sendConsultationRequest(
                $input->get('first_name'),
                $input->get('last_name'),
                $input->get('email'),
                $input->get('phone'),
                $input->get('state')
            );
        } catch (Exception $exc) {
            $this->errors->add('form', $exc->getMessage());
        }

        return false;
    }

    /**
     * Sanitize input.
     *
     * @param array $input
     *
     * @return \Illuminate\Support\Collection
     */
    protected function sanitize(array $input)
    {
        $phone = preg_replace('/[^0-9]/', '', $input['phone']);

        return collect(array_replace($input, [
            'phone' => sprintf('%d-%d-%d', substr($phone, 0, 3), substr($phone, 3, 3), substr($phone, 6, 4)),
        ]));
    }
}
