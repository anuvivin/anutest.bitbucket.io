<?php

namespace App\Http\Forms;

use Mail;
use Exception;
use App\Mail\ContactRequestedMail;
use App\Http\Requests\ContactRequest;

class ContactForm extends AbstractForm
{
    /**
     * Handle the ContactRequest.
     *
     * @param  \App\Http\Requests\ContactRequest  $request
     *
     * @return boolean
     */
    public function handle(ContactRequest $request)
    {
        try {
            $input = $this->sanitize($request->input());

            Mail::to(config('mail.recipients.contact'))
                ->send(new ContactRequestedMail(
                    $input->get('name'),
                    $input->get('email'),
                    $input->get('phone'),
                    $input->get('reason'),
                    $input->get('comments')
                ));
        } catch (Exception $exc) {
            $this->errors->add('form', $exc->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Sanitize input.
     *
     * @param array $input
     *
     * @return \Illuminate\Support\Collection
     */
    protected function sanitize(array $input)
    {
        $phone = preg_replace('/[^0-9]/', '', $input['phone']);

        return collect(array_replace($input, [
            'phone' => sprintf('%d-%d-%d', substr($phone, 0, 3), substr($phone, 3, 3), substr($phone, 6, 4)),
        ]));
    }
}