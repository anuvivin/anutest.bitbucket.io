<?php

namespace App\Http\Forms;

use Illuminate\Support\MessageBag;

class AbstractForm
{
    /**
     * @var \Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Construct an instance of a form.
     */
    public function __construct()
    {
        $this->errors = new MessageBag();
    }

    /**
     * Get a Collection of errors.
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }
}
