<?php

namespace App\Http\Forms;

use Mail;
use Exception;
use App\Mail\ListingRequestedMail;
use App\Http\Requests\ListingRequest;

class ListingForm extends AbstractForm
{
    public function handle(ListingRequest $request)
    {
        try {
            $input = $this->sanitize($request->input());

            Mail::to(config('mail.recipients.listing'))
                ->send(new ListingRequestedMail(
                    implode(' ', [$input->get('first_name'), $input->get('last_name')]),
                    $input->get('email'),
                    $input->get('phone'),
                    $input->get('city'),
                    $input->get('state')
                ));
        } catch (Exception $exc) {
            $this->errors->add('form', $exc->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Sanitize input.
     *
     * @param array $input
     *
     * @return \Illuminate\Support\Collection
     */
    protected function sanitize(array $input)
    {
        $phone = preg_replace('/[^0-9]/', '', $input['phone']);

        return collect(array_replace($input, [
            'phone' => sprintf('%d-%d-%d', substr($phone, 0, 3), substr($phone, 3, 3), substr($phone, 6, 4)),
        ]));
    }
}
