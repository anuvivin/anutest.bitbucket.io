<?php

namespace App\Services;

use Log;
use Exception;

class GiosService
{
    /**
     * @var array
     */
    protected $config;

    /**
     * Construct an instance of GiosService.
     */
    public function __construct()
    {
        $this->config = config('gios', []);
    }

    /**
     * Check to see if the AE is valid
     *
     * @param  string  $ae
     *
     * @return boolean
     */
    public function isValidAe($ae)
    {
         return !!$this->getAccountExecutiveDetails($ae);
    }

    /**
     * Get the list of all account executives or optionally for only a specific state.
     *
     * @param  string  $state
     *
     * @return array
     */
    public function getAccountExecutiveList($state = '')
    {
        $states = $this->getAccountExecutiveStateAbbreviations();

        if(isset($states[$state])) {
            $url = $this->url('external.php', array_filter([
                'access_token' => $this->config('token'),
                'action' => 'getAccountExecutiveList',
                'state' => $states[$state] ?: null
            ]));

            try {
                $response = @file_get_contents($url);

                if ($response === false) {
                    throw new Exception('No response received.');
                }

                $aes = json_decode($response);

                if (is_null($aes)) {
                    throw new Exception('Response could not be parsed.');
                }

                if ($aes->data) {
                    foreach ($aes->data as $ae) {
                        $ae->profile_image = $this->getAccountExecutiveProfileImageUrl($ae->profile_url);
                    }

                    return $aes->data;
                }
            } catch (Exception $exc) {
                Log::error(sprintf('[%s] %s', __METHOD__, $exc->getMessage()));
            }
        }

        return [];
    }

    /**
     * Get the profile details of a specific account executive.
     *
     * @param  string  $aeId
     *
     * @return object|false
     */
    public function getAccountExecutiveDetails($aeId)
    {
        $url = $this->url('external.php', [
            'access_token' => $this->config('token'),
            'action' => 'getAccountExecutiveDetails',
            'aeUniqueRef' => $aeId
        ]);

        try {
            $response = @file_get_contents($url);

            if ($response === false) {
                throw new Exception('No response received.');
            }

            $parsed = json_decode($response);

            if (is_null($parsed)) {
                throw new Exception('Response could not be parsed.');
            }

            if ($parsed->data && $parsed->data->user) {
                $ae = $parsed->data->user;
                $ae->profile_image = $this->getAccountExecutiveProfileImageUrl($ae->profile_url);
                return $ae;
            }
        } catch (Exception $exc) {
            Log::error(sprintf('[%s] %s', __METHOD__, $exc->getMessage()));
        }

        return false;
    }

    /**
     * Submit a request for a free consultation.
     *
     * @param  string  $firstName
     * @param  string  $lastName
     * @param  string  $email
     * @param  string  $phone
     * @param  string  $state
     *
     * @return boolean
     */
    public function sendConsultationRequest($firstName, $lastName, $email, $phone, $state)
    {
        $url = $this->url('external.php', [
            'access_token' => $this->config('token'),
            'action' => 'createConsultantLead',
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email_address' => $email,
            'phone_number' => $phone,
            'state' => $state,
        ]);

        try {
            $response = @file_get_contents($url);

            if ($response === false) {
                throw new Exception('No response received.');
            }

            $parsed = json_decode($response);

            if (is_null($parsed)) {
                throw new Exception('Response could not be parsed.');
            }

            $status = intval($parsed->data->status ?? null);

            if (empty($status) || $status < 200 || $status >= 300) {
                throw new Exception($parsed->data->errors->message ?? 'Invalid response received.');
            }

            $message = $parsed->data->message ?? null;

            if (!empty($message)) {
                Log::debug(sprintf('[%s] %s', __METHOD__, $message));
            } else {
                Log::warning(sprintf('[%s] %s', __METHOD__, 'Successful response received with no message.'));
            }

            return true;
        } catch (Exception $exc) {
            Log::error(sprintf('[%s] %s', __METHOD__, $exc->getMessage()));
        }

        return false;
    }

    /**
     * Get the testimonials for a specific account executive.
     *
     * @param  string  $ae
     *
     * @return array
     */
    public function getAccountExecutiveTestimonials($ae)
    {
        return config('testimonials.'.$ae);
    }

    /**
     * Get the list of licensed states and their long format names.
     *
     * @return array
     */
    public function getAccountExecutiveStates()
    {
        return [
            '' => 'All',
            'alabama' => 'Alabama',
            'arizona' => 'Arizona',
            'arkansas' => 'Arkansas',
            'california' => 'California',
            'colorado' => 'Colorado',
            'connecticut' => 'Connecticut',
            'delaware' => 'Delaware',
            'florida' => 'Florida',
            'georgia' => 'Georgia',
            'illinois' => 'Illinois',
            'kansas' => 'Kansas',
            'kentucky' => 'Kentucky',
            'massachussetts' => 'Massachussetts',
            'maryland' => 'Maryland',
            'michigan' => 'Michigan',
            'minnesota' => 'Minnesota',
            'north-carolina' => 'North Carolina',
            'new-jersey' => 'New Jersey',
            'new-york' => 'New York',
            'ohio' => 'Ohio',
            'oregon' => 'Oregon',
            'pennsylvania' => 'Pennsylvania',
            'rhode-island' => 'Rhode Island',
            'south-carolina' => 'South Carolina',
            'tennessee' => 'Tennessee',
            'texas' => 'Texas',
            'virginia' => 'Virginia',
            'washington' => 'Washington',
            'washington-dc' => 'Washington, DC',
            'wisconsin' => 'Wisconsin',
        ];
    }

    /**
     * Get the list of licensed states and their abbreviations.
     *
     * @return array
     */
    public function getAccountExecutiveStateAbbreviations()
    {
        return [
            '' => '',
            'alabama' => 'AL',
            'arizona' => 'AZ',
            'arkansas' => 'AR',
            'california' => 'CA',
            'colorado' => 'CO',
            'connecticut' => 'CT',
            'delaware' => 'DE',
            'florida' => 'FL',
            'georgia' => 'GA',
            'illinois' => 'IL',
            'kansas' => 'KS',
            'kentucky' => 'KY',
            'massachussetts' => 'MA',
            'maryland' => 'MD',
            'michigan' => 'MI',
            'minnesota' => 'MN',
            'north-carolina' => 'NC',
            'new-jersey' => 'NJ',
            'new-york' => 'NY',
            'ohio' => 'OH',
            'oregon' => 'OR',
            'pennsylvania' => 'PA',
            'rhode-island' => 'RI',
            'south-carolina' => 'SC',
            'tennessee' => 'TN',
            'texas' => 'TX',
            'virginia' => 'VA',
            'washington' => 'WA',
            'washington-dc' => 'DC',
            'wisconsin' => 'WI',
        ];
    }

    /**
     * Try to convert the given state from an abbreviation to the full name, otherwise just return the state unmodified.
     *
     * @param  string  $state
     *
     * @return string
     */
    public function convertStateAbbrevToName($state)
    {
        $states = array_flip(array_map('strtolower', $this->getAccountExecutiveStateAbbreviations()));
        return $states[strtolower($state)] ?? $state;
    }

    /**
     *
     * Get eConsent details
     *
     */
    public function getEConsentData($confirmation_code)
    {
        $url = $this->url('external.php', [
            'action' => 'getEConsentData',
            'confirmationcode' => $confirmation_code,
        ]);

        try {
            $response = @file_get_contents($url);

            if ($response === false) {
                throw new Exception('No response received.');
            }

            return $response;
        } catch (Exception $exc) {
            Log::error(sprintf('[%s] %s', __METHOD__, $exc->getMessage()));
        }

        return '';
    }

    /**
     *
     * process eConsent commit in gios
     *
     */
    public function doEConsentConfirm($confirmation_code)
    {
        $url = $this->url('external.php', [
            'action' => 'doEConsentCommit',
            'confirmationcode' => $confirmation_code,
        ]);

        try {
            $response = @file_get_contents($url);

            if ($response === false) {
                throw new Exception('No response received.');
            }

            return $response;
        } catch (Exception $exc) {
            Log::error(sprintf('[%s] %s', __METHOD__, $exc->getMessage()));
        }

        return '';
    }

    /**
     * Get the profile image url for a specific account executive.
     *
     * @param  string  $ae_profile_url
     *
     * @return string|false
     */
    private function getAccountExecutiveProfileImageUrl($ae_profile_url)
    {
        try {
            return mix_remote_throw('images/accountExecutives/'.$ae_profile_url.'.jpg');
        } catch (\Exception $e) {
            //
        }
        return false;
    }

    /**
     * Create a GiOS URL.
     *
     * @param  string  $path
     * @param  array  $parameters
     * @param  string  $fragment
     *
     * @return string
     */
    protected function url($path, $parameters = [], $fragment = null)
    {
        $url = sprintf('%s/%s', $this->config('url'), ltrim($path, '/'));

        if (!empty($parameters) && is_array($parameters)) {
            $url .= '?' . http_build_query($parameters);
        }

        if (!empty($fragment) && is_string($fragment)) {
            $url .= '#' . $fragment;
        }

        return $url;
    }

    /**
     * Retrieve a value from the gios config file.
     *
     * @param  string  $key
     * @param  mixed  $default
     *
     * @return mixed
     */
    protected function config($key, $default = null)
    {
        return array_get($this->config, $key, $default);
    }
}
