<?php
namespace App\Services;

use Storage;
use Throwable;
use Carbon\Carbon;

class RatesService
{
    /**
     * Update rates and save them to disk.
     *
     * @param  array  $newRates
     *
     * @return bool
     */
    function updateRates($newRates)
    {
        $rateDate = Carbon::now()->toIso8601String();

        $baseRates = [
            'guidance-residential' => [
                'provider_id' => 'guidance-residential',
                'provider_name' => 'Guidance Residential',
                'rate_date' => $rateDate,
            ],
            'quicken-loans' => [
                'provider_id' => 'quicken-loans',
                'provider_name' => 'Quicken Loans',
                'rate_date' => $rateDate,
            ],
            'citi' => [
                'provider_id' => 'citi',
                'provider_name' => 'Citi',
                'rate_date' => $rateDate,
            ],
            'wells-fargo' => [
                'provider_id' => 'wells-fargo',
                'provider_name' => 'Wells Fargo',
                'rate_date' => $rateDate,
            ],
            'boa' => [
                'provider_id' => 'boa',
                'provider_name' => 'BOA',
                'rate_date' => $rateDate,
            ],
        ];

        $newRates = array_merge_recursive($baseRates, $newRates);

        return Storage::disk('local')->put('rates.json', json_encode($newRates, JSON_PRETTY_PRINT));
    }

    /**
     * Read the current rates from disk.
     *
     * @return array|null
     */
    function getRates()
    {
        try {
            $rates = json_decode(Storage::disk('local')->get('rates.json'), true);
        } catch (Throwable $e) {
            return null;
        }

        return (json_last_error() === JSON_ERROR_NONE ? $rates : null);
    }

}
