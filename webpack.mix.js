let mix = require('laravel-mix');

mix.js([
        'resources/assets/js/app.js',
    ], 'public/js')
    .extract([
//        'vue',
        'jquery',
        'lodash',
        'axios',
        'bootstrap',
        'owl.carousel',
        'typed.js',
        'bootstrap-slider',
    ])
    .autoload({
        jquery: ['window.jQuery', '$', 'jQuery', 'jquery'],
    });

mix.stylus('resources/assets/stylus/app.styl', 'public/css');

mix.copy('resources/assets/fonts', 'public/fonts/vendor/googleFont');
mix.copy('resources/assets/images', 'public/images');
mix.copy('resources/assets/media', 'public/media');
mix.copy('resources/assets/docs', 'public/docs');

if (mix.inProduction()) {
    mix.version();
    mix.version('public/images');
}

//manually copy blog assets files to the public directory
mix.copy('resources/assets/blog', 'public');