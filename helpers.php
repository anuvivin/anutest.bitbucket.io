<?php

use Illuminate\Support\HtmlString;

if (!function_exists('mix_local')) {
    /**
     * Get the path to a local versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @param  bool  $secure
     * @return string
     *
     * @throws \Exception
     */
    function mix_local($path, $manifestDirectory = '', $secure = null)
    {
        // Wrap mix in asset call to take into account subdirectories.
        return asset(mix_fix($path, $manifestDirectory), $secure);
    }
}

if (!function_exists('mix_local_throw')) {
    /**
     * Get the path to a local versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @param  bool  $secure
     * @return string
     *
     * @throws \Exception
     */
    function mix_local_throw($path, $manifestDirectory = '', $secure = null)
    {
        // Wrap mix in asset call to take into account subdirectories.
        return asset(mix_fix($path, $manifestDirectory, true), $secure);
    }
}

if (!function_exists('mix_remote')) {
    /**
     * Get the path to a remote versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @return string
     *
     * @throws \Exception
     */
    function mix_remote($path, $manifestDirectory = '')
    {
        $cdn = config('services.cdn.url');

        if (!empty($cdn)) {
            return $cdn.mix_fix($path, $manifestDirectory);
        }

        return mix_local($path, $manifestDirectory);
    }
}

if (!function_exists('mix_remote_throw')) {
    /**
     * Get the path to a remote versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @return string
     *
     * @throws \Exception
     */
    function mix_remote_throw($path, $manifestDirectory = '')
    {
        $cdn = config('services.cdn.url');

        if (!empty($cdn)) {
            return $cdn.mix_fix($path, $manifestDirectory, true);
        }

        return mix_local_throw($path, $manifestDirectory);
    }
}

if (!function_exists('mix_fix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @return \Illuminate\Support\HtmlString
     *
     * @throws \Exception
     */
    function mix_fix($path, $manifestDirectory = '', $throwExceptions = false)
    {
        try {
            $mixPath = mix($path, $manifestDirectory);

            if ($mixPath instanceof HtmlString) {
                return $mixPath;
            }
        } catch (Exception $e) {
            //
        }

        $unversioned = public_path($path);

        if (!$throwExceptions && file_exists($unversioned)) {
            return '/'.trim($path, '/');
        }

        throw new InvalidArgumentException("File {$path} not defined in asset manifest and not found on file system.");
    }
}

