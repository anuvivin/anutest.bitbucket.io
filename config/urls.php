<?php

return [
    'blog' => [
        'articles' => '/resources/insight-articles',
        'events' => '/resources/upcoming-events',
    ],
    'customer-portal' => [
        'prequalify' => '/my/prequalify',
        'login' => '/my/login',
    ],
    'customer-care-net' => [
        'login' => 'https://guidanceresidential.customercarenet.com/ccn/guidanceresidential/myaccount.html#HOME-C',
    ],
    'grealty' => [
        'search' => 'http://www.guidancerealty.com/search/#/search',
        'find-an-agent' => 'http://www.guidancerealty.com/find-an-agent',
        'find-a-platinum-agent' => 'http://www.guidancerealty.com/find-an-agent?showStatus[]=platinum',
        'agent-signup' => 'http://www.guidancerealty.com/agent-select-level',
        'terms-of-use' => 'http://www.guidancerealty.com/terms-of-use',
    ],
    'apps' => [
        'apple' => 'https://itunes.apple.com/us/app/gios-for-homeowners/id1346767378?mt=8',
        'android' => 'https://play.google.com/store/apps/details?id=com.guidance.customerapp&hl=en',
    ],
    'social' => [
        'facebook' => 'https://www.facebook.com/GuidanceResidential/',
        'twitter' => 'https://twitter.com/GuidanceRes',
        'linkedin' => 'https://www.linkedin.com/company/guidance-residential',
        'youtube' => 'https://www.youtube.com/user/GuidanceMarketing',
    ],
    'nmls' => [
        'home' => 'http://www.nmlsconsumeraccess.org/',
        'guidance' => 'http://www.nmlsconsumeraccess.org/EntityDetails.aspx/COMPANY/2908',
    ],
];
