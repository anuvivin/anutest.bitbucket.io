<?php

return [
    'url' => env('GIOS_URL'),
    'token' => env('GIOS_TOKEN'),
];
