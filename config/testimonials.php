<?php

return [
    'aikramullah' => [
        [
          'valediction' => 'Have an excellent day.',
          'author' => 'Ahsan Mir',
          'body' => <<<BODY
Assalamalaikum,

I just wanted to take a moment and give you some feedback on my experience with guidance residential, especially with Aasim Ikramullah.

I had my home recently refinanced and have to the say the experience was amazing from beginning to end. Aasim, is a delight to work with and answered so many questions throughout the process and made it so easy to understand! He even followed up with me daily after the closing to make sure I was satisfied and if everything went well. What an amazing experience and I am so happy to have refinanced with Guidance. Aasim is a complete gem for your company and I'm honored to have had him as my first point of contact with Guidance.

I will for sure refer you to others if I get the opportunity.
BODY
        ],
        [
            'valediction' => 'Thank you again',
            'author' => 'Fatouma Y.',
            'body' => <<<BODY
Salam every one,

I will get my keys today. I want to thank you guys, for the GREAT JOB, and for being patient with me JazaÃ¯ koum allah kheir.

YOU ARE THE BEST TEAM, I got 100% of what I needed when it comes the customer service and advice. You went above and beyond my expectation.
BODY
        ],
    ],
    'aali' => [
        [
            'valediction' => '',
            'author' => 'Zahir F.',
            'body' => <<<BODY
Abdihakim Ali is very knowledgeable and the best. I dealt with a few people at Guidance Residential before, but the knowledge and professionalism that he has is better than anybody else.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Ahmed A.',
            'body' => <<<BODY
Abdihakim is doing an excellent job. Great doing business with him.
BODY
        ],
        [
            'valediction' => 'Thank you',
            'author' => 'Oluwafunmilayo A.',
            'body' => <<<BODY
I wanted to share my home buying experience which Abdihakim Ali facilitated for me. I was referred to him by a family friend; he told me I was in good hands and I am glad I choose him as my account executive. Being a first time homebuyer, everything was new to me and overwhelming, but Abdihakim walked me through everything including the different options available . He was always there to answer questions in a timely fashion and I rest assured that I was being well taken care of. I would definitely recommend Abdihakim and this company to friends and family especially my Muslim friends who are hesitant to go through the regular home financing. Keep up the good work.
BODY
        ],
        [
            'valediction' => 'Best regards',
            'author' => 'Mahmood',
            'body' => <<<BODY
I would like to compliment one of your excellent employees, Abdihakim Ali. He has been very knowledgeable and responsive during the entire process of my Islamic mortgage application. He has been very courteous and responded to my questions promptly. I have come to rely on Abdihakim as a trustworthy individual, which I appreciated a lot because this was my first home that I purchased and I wasn't familiar with the process. So having Abdihakim be such a great resource was very helpful. I will definitely recommend Abdihakim and Guidance Residential to my friends.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Asraful',
            'body' => <<<BODY
I wanted to let you know that Abdihakim Ali is a great person to work with, and he helped us secure the financing for our homw and the home purchase process with his excellent service. He went above and beyond to answer any question we had. I would rate him 10 out of 10 for his service. If you have any specific question for me feel free to reach out to me. Once again thanks a lot for the service.
BODY
        ],
        [
            'valediction' => 'Thank you',
            'author' => 'Mohamed & Ayan',
            'body' => <<<BODY
We wanted to share our home buying experience which Abdihakim Ali helped us with. We were referred to him by a family friend. We were very interested in Home Financing that was based on no riba. We needed someone that could explain this financing option and guide us through all the necessary steps. Abdihakim was very patient through everything including providing details of the financing program details as well as answering any and all of our questions all the way up to close and made necessary follow ups at crucial stages which we appreciated. I would definitely recommend Abdihakim and this company to friends and family interested in financing their home/s riba free. Miriam who partnered with Abdihakim in underwriting was also terrific- we appreciated the direct and straight forward request of underwriting documents from which allowed us to turn the requests back to her in a timely manner. We appreciate helping us making the homeownership dream a reality.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mahad',
            'body' => <<<BODY
I wanted to let you know how much I appreciated your help and support in getting the house of our dream. During the of process, you answered all my questions and treat like I 'm your best client. With will of Allah and your hard work, we got house, Thanks for professionalism and insight that you showed to me during the process. on behalf my family and I truly appreciate your help.
BODY
        ],
    ],
    'atufa' => [
        [
          'valediction' => '',
          'author' => 'Yonis Mohammed',
          'body' => <<<BODY
Hi Abdisa,

I just want to take this chance to thank you for all the help thought my journey of home purchase, from planning to buy a house to closing on a perfect home. All your advices, help and professionalism was priceless. Many times you have stretched yourself beyond my expectations. I also want to thank you for your serenity on my continuous nagging.

I look forward to working with you again on future investments!!!
BODY
        ],
        [
            'valediction' => 'I really appreciate him.',
            'author' => 'Filly Adem',
            'body' => <<<BODY
Acquiring house is a big decision in any one's life. I was thinking about it for years and my plan was to have it halal way. My discussion with my pious friend about Islamic finance was interesting, we believe they are good but still we don't feel 100% comfortable.

However last year, after reading Amja fatwa, I began thinking about it but didn't take any action. Once I called Abdisa Tufa for something else and he explained it well and started right away. He asks and responds on time, and facilitated things well.

His fast response and personal skill, helped me all the way from pre-approval, financing and closing. Abdisa is proactive, responsive, available and easy to work Account Executive.
BODY
        ],
        [
            'valediction' => 'Thanks',
            'author' => 'Danu H.',
            'body' => <<<BODY
Assalaamualaikum warahmatullahi wabarakatuh, Br. Abdisa:

Just wanted to share my refinance experience with you. I contacted Guidance after I got small flyer about "Islamic Mortgage" from Jummah prayer. I immediately searched on Google and Guidance was the first line of the search. After seeing the video about the product, I filled out the form and was contacted by Abdisa. My impression about Abdisa is: you're very polite, informative, and fast. You followed up my inquiry from the website and explained the product very clearly. Specifically for myself (a person who was doing Refinancing for the 1st time), you were willing to answers my questions in a clear way. Even right now, I still don't believe that we have done the Refinancing (with your help and guidance). The process was so seamless, hassle free where the profit margin was very competitive with the other financial institutions. All-in-all, I'm very satisfied with Abdisa's service, and certainly would recommend you to other folks who want to get rid of the RIBA.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Hussein A.',
            'body' => <<<BODY
Assalamualaikum Abdisa. It is nice to know you. You are a very helpful Person, you have been very responsive and knowledgeable. You really helped me a lot during this very long home financing process and I appreciate your kindness and best service you provided me. Thank you and all Guidance Residential team for helping me buy my first ever home in a halal way
BODY
        ],
        [
            'valediction' => 'Your happy customer',
            'author' => 'Ahmed H.',
            'body' => <<<BODY
Brother Abdisa, I want you to know how much I appreciate the excellent service you provided to me and my wife throughout the entire home buying process. Great service makes your customers feel that you care about developing a long-term relationship that means more than just making money. You were very prompt when it comes to responding to my calls, emails, and messages and you were truly available at all times for us regardless of the method of communication we used. I decided to go with the Guidance Residential after doing lots of research and you proved to me it was the right decision. I would love to recommend you and your company to anyone who needs to buy a house in future.
BODY
        ],
        [
            'valediction' => 'Kind regards',
            'author' => 'Ousman Ali',
            'body' => <<<BODY
Abdisa Tufa, I am super happy to let you know that I started moving to my new house. I would like to say few words about how grateful and satisfied I am with your excellent service. Guidance Residential helped me to attain my American dream of owning a home without any hassle. Above all, with abiding Islamic Shariah rule which strictly forbids interest in possession of a house. Abdisa, had it not been for your experience, day to day follow up and doing a lot of paperwork, I wouldn't have owned the house right now. I am very confident that you will contribute a lot more to our Muslim community who are desperate to own houses in line with Shariah rule.
BODY
        ],
        [
            'valediction' => 'Thanks',
            'author' => 'Waqas',
            'body' => <<<BODY
I recently bought my first home and worked with Abdisa Tufa of Guidance to get financing. When I started my application, I didn't know much about the home finance process, but I got a call from Mr. Tufa soon after I completed my application. On that call and many calls after that, Mr. Tufa helped me understand different steps and terms involved in the home finance process.

He worked hard to get me the best rate available and was always available to talk to and guide me during my home finance application process.
BODY
        ],
        [
            'valediction' => 'Thanks',
            'author' => 'Hassen',
            'body' => <<<BODY
I worked with Abdisa Tufa to purchase our first home and really can't say enough good things about him. He's very knowledgeable, approachable, personable, very patient and incredibly generous with his time (including morning, evenings and weekends over the course of several months). He answered every question and explained every form and every part of the process clearly and thoroughly. Buying a home at the moment is very challenging, but working with Abdisa made it easy for us. I highly recommend Abdisa and Guidance Residential to anyone in need of home finance.
BODY
        ],
        [
            'valediction' => 'Thanks',
            'author' => 'Syed Jafar',
            'body' => <<<BODY
السّلام عليكم ورحمة الله وبركاته

Brother Abdisa Tufa, Thank you so much for helping me out with the process of refinancing. You helped me out from beginning to the closing. I felt very comfortable with the whole process and I was utmost satisfied with the exceptional service. I give 5 stars to you and Guidance Residential.
BODY
        ],
    ],
    'aahmed' => [
        [
            'valediction' => 'Best Regards',
            'author' => 'Nasim',
            'body' => <<<BODY
Salam Br Ahmed,

Thank you so much. You are a great help in this process. It could not have been possible without you. I cannot express in words the help I get from you. I will contact you for the next property in a couple of months In Shall Allah.
BODY
        ],
    ],
    'amalik' => [
        [
            'valediction' => '',
            'author' => 'Obaid S.',
            'body' => <<<BODY
Ahsen Malik was outstanding! He demonstrated lots of patience in answering all my questions and addressing the fears. He responds very quickly to emails and questions.
BODY
        ],
    ],
    'agohar' => [
        [
            'valediction' => 'Thank you again!',
            'author' => 'Fahad',
            'body' => <<<BODY
Dear Brother Amer,

I like to personally thank you for all your efforts in getting the loan approved and closing done on my condo. It was not an easy task since I was out of town but I truly appreciate your professional demeanor and desire to help from the get-go. I know who to contact for my next home purchase and will pass out your name in my network of potential buyers.
BODY
        ],
        [
            'valediction' => 'Kind regards',
            'author' => 'Akhter S.',
            'body' => <<<BODY
Recently Amer Gohar assisted me in obtaining a home loan from Guidance Residential. I consider myself a very demanding customer and also being a professional in customer service industry my expectations are very high. I must say, Amer knows the art of providing excellent customer service and representing his company very well. Service like this is the very reason for me to continue sending my friends to Guidance Residential. Amer truly go above and beyond his duties. This is true for all of his interactions with him and with the closing company and builder. Particularly, the closing company had lots of good things to say about him and Guidance Residential. According to them "Amer has got PASSION towards his work". Please pass along my appreciation to Amer Gohar - He is definitely a credit to your organization.
BODY
        ],
        [
            'valediction' => 'Regards',
            'author' => 'Tauqer S.',
            'body' => <<<BODY
Alhamdulillah, the closing of the house was done this previous day and everything went smooth and normal. I'm glad to have found a brother like you. Your help and assistance truly exceeded my expectations. I'm sure that the entire community will benefit from your services. I really appreciated the many times that your team went out of the way to fulfill customer needs and deliver. I would say that throughout the whole process, everything was very professional, all promises delivered and a friendly atmosphere. I will definitely refer Guidance Residential to everyone and especially to someone like you.
BODY
        ],
        [
            'valediction' => 'Regards',
            'author' => 'Ali H.',
            'body' => <<<BODY
Firstly, I wanted to thank you for the excellent support you provided to us in completing the recent transaction, Alhamdulillah. Secondly, I just wanted to let you know that I just received the survey today and I intend to send it back to Guidance, Insha'Allah. Needless to say, we are more than happy with how things went with you and the whole transaction and we look forward to working with you in the future as well.
BODY
        ],
        [
            'valediction' => 'Thanks again, Amer.',
            'author' => 'Nick W.',
            'body' => <<<BODY
Thank you for everything, Amer. I really hope we get the pleasure of working with you many times more. You've been professional, patient, communicative and open, and we really appreciate it. Thank you so much and I hope you enjoy a very happy new year. And let's do more business together in the New Year!
BODY
        ],
        [
            'valediction' => 'Warmest Regards',
            'author' => 'Fahad Q.',
            'body' => <<<BODY
I hadn't gotten a chance to express my deepest thanks to you for all of your help during the purchase of our first home. You all made the process very painless and many of the unfavorable expectations that I was worried about did not come to fruition. I am truly grateful to you for that. Again many thanks to you for all of your help in making this dream a reality for us.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Abul A.',
            'body' => <<<BODY
Amer Gohar was very organized, timely, and courteous. He explained the process and made me aware of what to expect. He responded to questions and inquiries in a speedy manner. Very satisfied.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mohammed F.',
            'body' => <<<BODY
The experience with Guidance Residential was good! I like the quick responses from Amer...and his working closely to resolve unforeseen issues.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Adnan S',
            'body' => <<<BODY
Amer, our Account Executive, was very kind and professional to work with. He actively responded to our needs and concerns.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mohamed S.',
            'body' => <<<BODY
My Account Executive, Amer Gohar is an excellent person with knowledge about the product and will relieve your doubts and let you know the different options about home financing and rates.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Syed B.',
            'body' => <<<BODY
Amer was excellent to deal with and was very knowledgeable.
BODY
        ],
    ],
    'adjumagulov' => [
        [
            'valediction' => '',
            'author' => 'Mohamed D.',
            'body' => <<<BODY
Salam Brother Atabek, I wanted to say thanks again for all the help with converting my conventional mortgage, it is a big relief for me. May Allah reward you and your family. Lets stay in touch Inshaa Allah. Have a blessed rest of the weekend.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Avaz',
            'body' => <<<BODY
Thank you for all the help, brother. In sha Allah looking forward to cooperate with you in future. Definitely will recommend you to all my friends.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Farhan',
            'body' => <<<BODY
Hope you are fine. Alhamdolilah today I got key for me new home &amp; I really want to thank you for helping me at each stage, jazak Allah brother Atabek, may Allah bless you.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Kashif ',
            'body' => <<<BODY
I'd like to extend my thanks to you and everyone else at Guidance that helped with the purchasing process of my home. It has been a rocky road, but all that matters is that you guys helped us through to the finish line, and for that please accept my sincerest gratitude.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Kashif R.',
            'body' => <<<BODY
I had a exceptional experience getting my mortgage from Guidance. Before I bought a house sold by its owner all by myself, no agent involved,but with Guidance and the help from very experienced staff, it was piece of cake this time around. Alhamdulillah, I got my keys today just within a month from when I submitted my application. I got referred by many other of my brother from masjid, who had a great experience as well like me.

Special thanks to brother Salah El Ghazzali and Atabek Djumagulov. You are such professionals and dedicated resources for Guidance.
BODY
        ],
    ],
    'aakbar' => [
        [
            'valediction' => 'Thank you so very much!',
            'author' => 'Saqib W.',
            'body' => <<<BODY
I just want to take time to thank you for amazing service from Guidance for my refinance. I had a really great experience working with you guys. I want to specially thank brother Atif Akbar, for his tireless, prompt, and diligent efforts to make this a smooth transaction. I really appreciate him for all his efforts and great customer service.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Awais M.',
            'body' => <<<BODY
I just wanted to send across a note regarding Atif Akbar and our experience with him during the financing process. Overall, we were really satisfied with how Atif managed our account. Atif was always very proactive in terms on communication and made sure all our queries were addressed. He would walk us through multiple options to make sure we thought about the best possible option available to us in terms of financing.
BODY
        ],
        [
            'valediction' => 'Thanks',
            'author' => 'Asif Iqbal',
            'body' => <<<BODY
I wanted to take the time to let you know that I had a pleasant experience dealing with Atif Akbar for the purchase of my home through Guidance Residential. Atif was very thorough in explaining the whole process to me and he helped me step-by-step throughout the whole process. He never made me feel nervous and handled the process in a professional manner. Due to his diligence, we actually closed in less than 30 days. Atif was always available even at late hours of the night when I had a question for him. I am very satisfied with Guidance Residential Shariah Compliant contract and wish the company and Atif the best of success in the future.

I am also reaching out to my friends who are planning to buy new home and telling them about Guidance and the excellent service you provide.
BODY
        ],
        [
            'valediction' => 'Thanks!',
            'author' => 'Usman B.',
            'body' => <<<BODY
Salam -

Just wanted to reach out about my experience with Atif Akbar. My wife and I were looking into purchasing a home, and started the process and being first time buyers had a lot of questions and bit nervous about all the steps, but Atif was absolutely amazing. He was very responsive, easy to get hold of (email, calls, texts, on the weekends) and walked us through the whole process. We had a shorter closing date, and was quite stressful to get everything done in time and he helped coordinate all the different parties and we got the house of our dreams and couldn't be happier. Will definitely recommend Atif and Guidance to friends and family who were looking into Islamic financing in the future.
BODY
        ],
    ],
    'babdalla' => [
        [
            'valediction' => '',
            'author' => 'Mohammed S.',
            'body' => <<<BODY
The experience was great. My representative was extremely helpful. I love the service and I would recommend you to everyone I know in a heartbeat. In fact I already have asked a friend to try to refinance their house using Guidance Residential as I absolutely loved the overall experience.
BODY
        ],
    ],
    'ehossain' => [
        [
            'valediction' => 'Thank you',
            'author' => 'Abdo A.',
            'body' => <<<BODY
I want to take this opportunity and thank you for helping me obtain Islamic home financing the Sharia way. You are very professional at what you do. You returned my calls and answered my emails whenever I had a question. You always updated me throughout the application process and gave me advice which I found very helpful. This made the application process easier than what I thought it would be. Alhamdulillah, by using Guidance Residential and their Declining Balace Co-Ownership Program, I am now a proud homeowner, the Sharia way. May Allah bless you at what you do. Also, I will highly recommend you and Guidance Residential to other family members and friends that are interested in Islamic Financing.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Rasheedah N.',
            'body' => <<<BODY
I want to express my heartfelt thanks and appreciation for your relentless efforts to help us acquire our new home. Through much prayer and constant outcry to our Lord, we are starting to enjoy the fruits of our labor. For that, I am extremely grateful and thankful. Additionally, your gift from Irada, Islamic wall art was a surprise and a welcomed addition to our new home. It is in the entry way of our home for all to see, and it made our home complete. Once again, may Allah reward you tremendously for your efforts in this life and the next.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mohammed A.',
            'body' => <<<BODY
Everything was smooth experience. On a personal level it is amazing to have a home and to allowing me finance the halal way is a dream come true. It wouldn't have been possible without the extra effort that some people did personally in Guidance Residential. May Allah accept our intentions and increase barakah in your company.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Adnan M.',
            'body' => <<<BODY
Very smooth transaction. My Account Executive, Emran went out of his way to make sure everything went smoothly.
BODY
        ],
    ],
    'hkhan' => [
        [
            'valediction' => 'Thanks again.',
            'author' => 'Mohammed S.',
            'body' => <<<BODY
Salaam, I would like to sincerely thank you for your gift and your assistance in the process of purchasing my home. You were very helpful throughout the process, especially taking my calls and texts in the evenings. I am impressed with the professionalism of Guidance and will certainly recommend them to friends and colleagues.
BODY
        ],
    ],
    'hali' => [
        [
            'valediction' => '',
            'author' => 'Azhar P',
            'body' => <<<BODY
Thank you so much Brother Hamza Ali - you were friendly and always there when I needed help.
BODY
        ],
        [
            'valediction' => 'Sincerely',
            'author' => 'Shahida & Abu',
            'body' => <<<BODY
Assalamu alaikum brother Hamza,

We wanted to drop you a note in order to express our thanks for helping us secure a mortgage. We literally wouldn't have been able to do all of this without the hard work you did for us to ensure we could finally celebrate our first home.

Thank you so much brother. You were so great to deal with and got us an amazing rate. We really appreciated your efforts through the whole process. And particularly appreciated you keeping us in the loop throughout. We've just finished moving everything in now. And alhamduliallah, we are very happy with the outcome.

We both look forward to keeping you up to date with the progress as we build, as well as working with you again on our future purchases inshallah. Thank you so much for all that you do!

I'll sure try to recommend my friends and relatives who are shopping for homes to you.

Again, thanks for your help.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mashal',
            'body' => <<<BODY
Dear Mr. Ali,

I wanted to write to you to thank you for your fantastic service. You were extremely professional and patient throughout this whole process and I greatly appreciate all that you have done for us. Seeing as this was my first home buying experience, I was extremely satisfied with how quick and easily we were able to secure financing and close on the house. You showed great patience and consideration and the time and effort you put into us is greatly appreciated. You were always available to give advice and update us in a timely manner which is so important to us as the new buyer. You made it so easy and assured us along the way. You are great at doing your job and I will definitely recommend your services to anyone else looking to secure home financing. Thank you again for all of your efforts and patience and we hope to deal with you again in the future InshAllah.
BODY
        ],
    ],
    'hghafour' => [
        [
            'valediction' => 'Thank you very much. Allah Bless you.',
            'author' => 'Safiya P.',
            'body' => <<<BODY
I want to personally thank you Hana, for looking out for our benefits throughout the Refinance process, and always explaining with patience all documents and figures. You did take great efforts to put our concerns to rest, answer all inquiries promptly and make it easy as possible. We greatly appreciate your personal touch and feedback.
BODY
        ],
    ],
    'hsyed' => [
        [
            'valediction' => '',
            'author' => 'Aghiad B.',
            'body' => <<<BODY
Syed was great and responsive account executive. I hope to work with him again in the future.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Shoeb K.',
            'body' => <<<BODY
My experience with Guidance Residential was wonderful and I never had any complaints, issues, concerns or problems. Keep up the good work!
BODY
        ],
    ],
    'hkhokhar' => [
        [
            'valediction' => '',
            'author' => 'Muhammad I.',
            'body' => <<<BODY
The whole process went so smooth and was carried out with a high degree of professionalism. I hope you keep it like this forever.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Zaman S.',
            'body' => <<<BODY
The process is simple and smooth. Staff are cooperative and efficient and they met the target timeline well and explained everything and every step throughout the entire process.
BODY
        ],
    ],
    'mgorod' => [
        [
            'valediction' => '',
            'author' => 'Faisal',
            'body' => <<<BODY
I would like to express our deep gratitude for the service we received at Guidance Residential during home financing. Thank you Mahamud for your continued advising, guiding in the process. We appreciate your prompt responses to answer our questions including weekends and late after hours. Great to have a professional team like yours by our side during.Thank you again for your service.
BODY
        ],
    ],
    'mrahaman' => [
        [
            'valediction' => '',
            'author' => 'Jawad M.',
            'body' => <<<BODY
Brother Mohammad Rahaman is the best. I was a first time home buyer but he was with me every step of the process. He was always there to clear my concerns and always helped me more than expected. I cannot thank him enough, I pray to Allah SWT to grant him the best rewards.
BODY
        ],
    ],
    'mamkor' => [
        [
            'valediction' => '',
            'author' => 'Aysha R.',
            'body' => <<<BODY
I'd like to thank the Guidance team, especially Account Executive Mohammed Amkor. He did a great job and helped us a lot to make our loan process easy.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Jafar S.',
            'body' => <<<BODY
Mohammed Amkor was an excellent Account Executive. He responded all my queries and satisfied all my needs.
BODY
        ],
    ],
    'mkhalil' => [
        [
          'valediction' => 'Regards',
          'author' => 'Hesham',
          'body' => <<<BODY
Dear Guidance Team,

I would like to thank you all for all your efforts during this process from pre-approval to final approval. Overall my experience was very positive, smooth and fast.

I really appreciate your timely and prompt responses, detailed information and clarifications, and your overall professionality. Also, the platform was nice and provided clear next steps at each phase.

Hope you all have a great weekend!
BODY
        ],
        [
            'valediction' => '',
            'author' => 'M. Mohammed & R. Ali',
            'body' => <<<BODY
We had a wonderful experience with Guidance Residential. Mohammed Khalil helped us and guided us during our transaction. Khalil is extremely knowledgeable about the product and very helpful. Whenever we had a concern, he was always there to help. With his instant availability and product knowledge he was able to resolve our issue immediately. We felt very comfortable with the whole process and were very satisfied. Khalil has provided wonderful help and professional services.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Amer & Olla A.',
            'body' => <<<BODY
We had a great experience with Guidance Residential. Mohammed Khalil our Account Executive guided us during this journey. Brother Khalil is knowledgeable about the product, always reachable, and very helpful. With his instant availability he was able to resolve our issue immediately and Al-Hamdolillah we closed earlier than we were required to. We felt very comfortable with the whole process. Thank you all Guidance team; Mohamed, Faye, Fredy, and Tarek.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Dega A.',
            'body' => <<<BODY
Thank you Guidance Residential for helping us through our loan process in Islamic financing. Special thanks to Mohammed Khalil for being kind and courteous and the best customer service. May Allah bless you and your family.
BODY
        ],
        [
            'valediction' => 'Assalam-o-Alaikum',
            'author' => 'Azfar',
            'body' => <<<BODY
We have completed the refinance process yesterday. I appreciate and thank your for all your work.
BODY
        ],
    ],
    'msalam' => [
        [
            'valediction' => '',
            'author' => 'Yasoob M.',
            'body' => <<<BODY
Mohammed Salam was highly professional and knowledgeable. The entire process of home loan from Guidance Residential was easy and smooth, especially the closing.
BODY
        ],
    ],
    'mboulhanna' => [
        [
            'valediction' => '',
            'author' => 'Ahmed Howeedy',
            'body' => <<<BODY
Jazakullaho Khair for all your efforts in helping us with the new home. It was such a blessing today while signing the closing documents, to have "Bismillahil Rahmanil Raheem" on the top of the documents, with the documents explaining the Shariah complaint loans, the co-ownership etc. Alhamdolilah, during the closing we were explaining to the agent from the title company how our deen encourages us not to deal in usury, and that there are financial institutions that allow us to still buy homes in ways complaint with Gods religion. Alhamdolilah. May Allah reward all of you at Guidance.
BODY
        ],
    ],
    'mbarakat' => [
        [
            'valediction' => 'Kind Regards',
            'author' => 'Kashif',
            'body' => <<<BODY
Good Morning Team,

We are able to close our transaction with the help of everyone. :D

I would like to take this moment to thank you all for you support, cooperation, guidance, professionalism and responsiveness during this process. You are such a great team to work with. Really appreciate your work.

Once again thank you all.
BODY
        ],
    ],
    'mqtaish' => [
        [
            'valediction' => '',
            'author' => 'Amr E.',
            'body' => <<<BODY
This was my first home purchase. Munther Qtaish was the Account Executive in California who helped me and he did an excellent job. He was always reachable, generous and candid with his time and sincere advice, trust-worthy and professional. As you would expect, occasionally miscommunication may happen between the different parties involved (escrow, lender, agent..etc.), you need an agent that will stay on top of the process to course-correct if needed.
BODY
        ],
        [
            'valediction' => 'Jazakum Allah khair for taking the time to read this.',
            'author' => 'Abdullah M.',
            'body' => <<<BODY
I wanted to just give you some feedback from my recent experience with Guidance. I first learned about Guidance from an event put on at our local masjid/community center in Santa Clara by Munther Qtaish. My initial expectation was (sad to say) this was another event put on by a "Muslim Organization" (i.e.: not starting on time, not professional, etc). I was sorely mistaken. From the minute I walked in I was impressed with how professional the room was set up - well dressed information tables in the back, snacks and coffee, very professional presentation and slide deck. I learned a ton and every time I felt myself having a question, I found it was answered by the presentation. I left the presentation not only excited about the Islamic financing possibility but excited to pursue an opportunity with a professional Muslim organization. From there I worked with Munther to get my pre-qual and get started shopping for houses and submitting necessary documents. Munther was extremely helpful and prompt to reply to any questions I had. He was also more than happy to explain (multiple times, on some occasions) any issues I didn't understand. I felt very comfortable working with him the entire time and never felt "on my own".

I had heard horror stories about the underwriting process from various friends and I was very nervous for it. However, working with Munther was seamless. I found myself thinking "that's it?" when I was informed that the underwriter was satisfied, alhamdulillah. I was inspired to write this email because I felt that Munther went above and beyond to make our process easy and I wanted to make sure he was recognized for it! Our loan was funded almost 2 weeks ago and we are preparing to move into our new house. We could not be more excited! Moreover, we are very happy knowing we did everything we could to get sharia compliant financing and are very grateful for the opportunity to work with Guidance. We will definitely be telling our friends about our experience!
BODY
        ],
        [
            'valediction' => 'Thanks!',
            'author' => 'Mohamad T.',
            'body' => <<<BODY
I would like to thank Munther for all the amazing work he did. I know my application wasn't the easiest, as I am new to this thing!
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Darlena Price',
            'body' => <<<BODY
Thank you so much for all your help in re-financing our home. I sincerely appreciate your advice, helpfulness, and immediate responses to our inquiries. As I am sure you are aware, the home loan process is stressful and your attention and consideration helped relieve that stress to bring the process to a successful conclusion. We appreciate your help in this matter.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Talal Darwich',
            'body' => <<<BODY
Salam dear brother Munther,

Here is my testimony.

I have been working with Munther for a year now.

I have found him very honest and responsive and detailed professional person.

He will get into all the details and explains them over and over until the customer is really satisfied. Very responsive and accommodating.

One great point also is honesty.

I was really comfortable that he is working on my case and I did not have to worry or do a lot of research to get everything in place.

He will take care of everything from his side.

I really recommend working with him.

I already ask my brother to work with him (but unfortunately Guidance does not work in his state). I also told 2 friends of mine to work with him.

One already bought a house through Munther and one just started to plan for the purchase of a new home.

Overall,

Munther is the right person to finance or refinance your new/existing home!
BODY
        ],
    ],
    'neid' => [
        [
            'valediction' => '',
            'author' => 'Faisal',
            'body' => <<<BODY
I signed the closing documents of my townhouse last month without any problems. I was in touch with Nabil for my home financing and he gave me wonderful service. It was not only his service at Guidance Residential, but he also gave me good suggestions for inspectors, home insurance companies, and many other things. I really appreciate that and wanted to thank you for the services.
BODY
        ],
        [
            'valediction' => 'Thank you',
            'author' => 'Faisal H.',
            'body' => <<<BODY
It has been a pleasure to work with Mr. Nabil. He did an excellent job in arranging the financing for our dream house. Most of all I appreciated the prompt responses. He answered all phone calls and emails. We are very impressed with his knowledge on financing. He is very skilled with great communication and is pleasant to converse with. He is an exceptional employee and a true asset to your organization. His service was very good; I have referred several friends to him. If I'm ever in the market for another loan, we will go straight to Mr. Nabil.
BODY
        ],
        [
            'valediction' => 'Best regards',
            'author' => 'Rachid and Zineb',
            'body' => <<<BODY
It's with pleasure to say I greatly appreciate all your efforts we received from you, Mr. Nabil. You were prompt, very active and proactive in the process of obtaining our house. You were very professional and always available through emails and phone calls.

Keep up the hard work and thank you so much.
BODY
        ],
        [
            'valediction' => 'Salaams',
            'author' => 'Zehra T.',
            'body' => <<<BODY
I want to express my sincere thanks to you for assisting and expediting all the steps that culminated in the successful closing for the refinance of our home. I appreciate your efficiency and competence greatly and will be happy to refer you to others. May Allah SWT give you increased blessings in your career!
BODY
        ],
        [
            'valediction' => 'With best regards',
            'author' => 'Nasir U.',
            'body' => <<<BODY
I would like inform that since yesterday my family has been living in our new home. I would like take the opportunity to thank the Guidance team and also express my gratitude to all of you for your professional advice and help in making the process of purchasing our home very smooth and on time.

BODY
        ],
        [
            'valediction' => 'Have a nice day',
            'author' => 'Jamal M.',
            'body' => <<<BODY
My wife and I are thankful and appreciate all of you, for putting down so much effort, time and patience to get our house. Once again thank you for this. Without all the generous help and effort, we would have not achieved this.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Dr. Sheikh',
            'body' => <<<BODY
We closed the house as planned yesterday. I wanted to take this moment to thank both of you. This is my second official transaction with Nabil and third one with Clarissa. I have a lot of respect for both of you for your professionalism and service. I have told about you to anyone who has asked and I am sure some of them may have contacted you. Hopefully we will end up staying in this house much longer than we did in the last one, Insha'Allah.
BODY
        ],
        [
            'valediction' => 'Thanks again!',
            'author' => 'Summar O.',
            'body' => <<<BODY
Thank you so much for all of your hard work and dedication. I really appreciate everything you did to ensure I got my loan and closed on my house. You were all so quick to respond to my countless phone calls and emails and answer all of my questions, I am very appreciative. I will definitely recommend you and Guidance to any of my friends and family in the future.
BODY
        ],
        [
            'valediction' => 'Regards',
            'author' => 'Mujeeb',
            'body' => <<<BODY
I have closed on my house on August 25th and moved in on August 30th.

The purpose of this letter is to give my personal thanks to you and the team that was working on my application. I would like to thank you all and appreciate all the effort that you have put through to get the process completed smoothly.

The team has worked tirelessly to get me through the process. I can say that because I had emails coming in at 11:00 PM and the same person sending it again at 7:00AM. That means a lot. A lot of my friends had to appreciate the speed and the smoothness of my process.

The team was not only working hard but was also very patient with me. I made numerous nervous enquiries via phone calls, text messages, emails. Every time I had the same pleasant response from Guidance team, which was not only accurate but was very prompt. Home buying is a very nervous experience but the team put me at ease and helped alleviate a lot of that feeling.

I definitely consider myself lucky to have worked with this team.

I once again would like to thank each one of you individually. You all are a great asset to the profession you are in and Guidance is lucky to have you all as part of their organization.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Urooj K.',
            'body' => <<<BODY
Recently, we successfully close our refinance document, and it was the culmination of the efforts from different people who helped me with this. I would like to thank Nabil for all your help and patience with innumerable emails and phone calls over the due course. Please extend my gratitude to other team members as well at Guidance Residential. Hope to reach you again for any of my future needs.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Ahmed M.',
            'body' => <<<BODY
It was a pleasure to work with you on the entire journey. We had strict timelines and we completed all the documents much before they were due. I cannot stress the importance of having such a great team that works collaboratively. I will be using your names as reference in case someone needs a new home or wants to refinance their existing home. Once more, a great job!
BODY
        ],
        [
            'valediction' => 'Jzk',
            'author' => 'Nizam',
            'body' => <<<BODY
Just wanted to inform you that the closing went well, AlhamduLillah. Thank you and your team for all the support. Glad to have connected with GR.
BODY
        ],
        [
            'valediction' => 'Jazzak Allah Khair!',
            'author' => '',
            'body' => <<<BODY
Salamu Alaikum Nabil,

I am writing to thank you for all the support and help you provided to us during our home loan application. We appreciate all the help and we ask that Allah bless you accordingly.
BODY
        ],
    ],
    'nmaataoui' => [
        [
            'valediction' => 'Thank you again',
            'author' => 'Toni\'s Own Cookies',
            'body' => <<<BODY
Please allow me to thank you for your professionalism, attention to details and above all your unparalleled ethical and moral character. This has been the smoothest, headache-free business transaction I have ever been involved in. Please stay the course; you are an asset to our community. Looking forward to our next deal, Inshallah!
BODY
        ],
        [
            'valediction' => 'Regards',
            'author' => 'Dr. AI',
            'body' => <<<BODY
Nabil, I want to thank you for your support during the process and for your professionalism and honesty. It was a pleasure to work with you.
BODY
        ],
        [
            'valediction' => 'Wassalam',
            'author' => 'Riaz',
            'body' => <<<BODY
Brother Nabil,

Thank you for your help in facilitating our home buying process. As first time buyers, we greatly appreciated your help and patience in dealing with us and explaining everything clearly. I will certainly recommend Guidance Residential and particularly you to my friends as I was referred by one of your customers.
BODY
        ],
        [
            'valediction' => 'Thank you so much',
            'author' => 'Juma K.',
            'body' => <<<BODY
Hi Nabil, my family and I want to thank you for your support. We are so happy that we finally are able to achieve our goal of buying a house, and you played a big role in achieving that goal.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Yasir R.',
            'body' => <<<BODY
It's been a couple of weeks since we moved into the new home. It is great alhamdolilah and everyday as I remember Allah blessings, the thought to thank Allah for the blessing of a easy mortgage process comes to mind and I especially feel thankful for having worked with you. Brother Nabil, you made the process very easy for us especially when this was our first home purchase. We panicked a few times, but you remained patient and explained what was going on. Every time there were minor bumps, we got worried but you always just took care of them. Everyone always told us how hard the mortgage process is but we did not experience any such thing. This is a testament to Guidance but more so I attribute it to you, your personality, positive and very professional attitude and sincere desire to help us get our first home in the US. May Allah reward you for your efforts and give barakat to you.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mynuddin S.',
            'body' => <<<BODY
Nabil Maataoui and the folks at the loan processing department did an excellent job throughout my loan process. I was constantly updated about the process and was communicated with very effectively and securely.
BODY
        ],
        [
            'valediction' => 'Thanks again',
            'author' => 'Lynda & Hakim',
            'body' => <<<BODY
Thanks for all your hard work and patience in helping us to buy our first home. As a first time buyer, we were completely in the dark about the entire process but you were very explicit to clarify for us all process in our first meeting. We were especially impressed by your efficiency, effectiveness, and teamwork. Most importantly we appreciated being kept informed and reassured via telephone and E-mail that things were proceeding smoothly.We would not hesitate to recommend your services to anyone in search of home.
BODY
        ],
    ],
    'nabdurahman' => [
        [
            'valediction' => '',
            'author' => 'Nabila Q.',
            'body' => <<<BODY
Nejat Abdurahman was very professional and answered all my questions. I would highly recommend her to anyone looking to find a home.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Nuru Y. (Annandale, VA)',
            'body' => <<<BODY
I had financed my first house through Guidance about 16 years ago, and my experience was not that great. When I refinanced my current house, I happened to meet Nejat Abdurahman who is highly professional that easily guided me through the process. She made the process very seamless and enjoyable. I am truly satisfied with her service and the care provided.  I highly appreciate Nejat for her knowledge, expertise and will not hesitate to recommend her.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'A. A (Katy, TX)',
            'body' => <<<BODY
I used Guidance to finance a new home in 2015. The entire process was very smooth. I was concerned that I would have to pay a premium for halal mortgage but their profit rate was very reasonable. My agent, Nejat Abdurahman, was very professional throughout the entire process. She is very knowledgeable and was always available and willing to answer my questions.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mohammed D. (Knoxville, TN)',
            'body' => <<<BODY
I was looking for a peace of mind and did an extensive research and ended up buying my current house through Guidance Residential. I worked with Najat Abdurahman and that was one of the smoothest transactions I've ever had. She walked me through the process and answered every question I had a timely and professional manner. With Guidance you get peace of mind Sharia wise and high quality staff of Najat's caliber. I absolutely recommend their services.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'M. B. (Troy, MI)',
            'body' => <<<BODY
Nejat Abdurahman, Account Executive, provided a fabulous service during refinancing of my home in 2014. I called Guidance and was lucky that she picked up the phone. Nejat's knowledge of the process was key in closing the deal smoothly and with minimal stress. I highly recommend her for any home financing needs with Guidance Residential. I'm sure she is an asset to the company.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mostafa A. Eden (Prairie, MN)',
            'body' => <<<BODY
I financed my first home with Guidance and it was a very smooth and efficient process, they hold your hand along the way and they explain every step in the process in a very detailed manner. I have also refinanced this same house with them and had the pleasure to have Nejat Abdurahman manning my refinance and she was very professional and very responsive, she was always there overtime I had a questions and she handled the refinance process pretty well and that's why she was the first person to contact when I decided to purchase investment properties. Great company and outstanding team.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Shahzad F. (Troy, MI)',
            'body' => <<<BODY
Excellent Service by Nejat Abdurahman. Would highly recommend Guidance Financial.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Saad S. (Mason, OH)',
            'body' => <<<BODY
Excellent service. Expert professionals, better than any bank. Nejat Abdulrahman is truly effective, pleasant and highly recommended. Refinance with relaxed mind and comfortable heart.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Unjum P. (Gaithersburg, MD)',
            'body' => <<<BODY
Alhamdulillah, we had a very good experience when we worked with Nejat Abdurrahman two years ago refinancing our house loan through Guidance. She helped us educating about Islamic Finance and answered all of our questions whenever needed. It was a very smooth process and there was no hidden charges or sudden fees throughout the process. We highly recommend Nejat to anyone who wants to finance their house through Guidance for the first time or through refinancing.
BODY
        ],
        [
            'valediction' => 'Thank you Guidance!',
            'author' => 'Eled S. (Wylie, TX)',
            'body' => <<<BODY
Alhamdulillah! Finally, I can sleep. My husband and I were both worried since we found out that buying a house with interest is haram. We refinanced our house two years ago through Guidance with help of Ms. Nejat Abdurahman from Virginia. She was a blessing to work with and it wasn't difficult for us to put my trust on her. She was very straight and honest with us thru out the process. Even though we are in Texas, she was always there for us even after hours which made the process very pleasant. I'm so glad that we can finally finance the Shariah way. I would recommend my relatives and friends to Ms. Nejat Abdurahman.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Ammar A. (Dublin, OH)',
            'body' => <<<BODY
Guidance residential was my first mortgage company I ever worked with, and it was a great experience. Agent Nejat was always available, answering any questions I had, worked with me on speeding up my application process so I can meet my deadlines. Guidance residential was recommended to me by my friend, and I have recommended it to my friends as well.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'I. Paredes. (Pittsburg, PA)',
            'body' => <<<BODY
Guidance Residential has helped me and my whole family to secure a home in our time of need. I couldn't be more grateful for their fair and generous lending rates, at a time when most other institution were engaging in predatory lending practices. My family had been victims of outrageously deceptive contracts with previous lenders that was designed to take advantage of vulnerable home owners. We were forced to seek out other alternatives that provided more flexibility and stability for my family, which led us to Guidance Residential. Their entire business is based on Islamic principles and values, and not so much capitalistic greed. If I should ever need lending services in the future, I know that I can depend on Guidance Residential to assist me and my family.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'M. Yahya. (Pembroke Pines, FL)',
            'body' => <<<BODY
Nejat Abdurahman helped me re-finance my property 3 times. She is very detail oriented and helpful with all paper work. She makes sure that her client saves money in closing cost. I have called her 100 times to clarify minor details and found her responses very helpful. I would recommend anyone looking for the 'Best' mortgage deals to contact her. It has been very nice experience working with her.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'M. Haque. (Port St. Lucie, FL)',
            'body' => <<<BODY
I didn't think that it will happen! Will I be able to buy the house my wife like? It was a miracle to me. I got the brochure of Guidance Residential from The Asian Food fair of Palm Beach Florida, I checked online and found out how exciting it is to work with and get a loan from GR with the help of one of their best Executive Ms. Nejat. I will never forgot how friendly, helpful she was.
BODY
        ],
        [
            'valediction' => 'Thank you Sister Nejat.',
            'author' => 'A. Khan. (Cypress, TX)',
            'body' => <<<BODY
I feel myself fortunate that I got in touch with Sister Nejat Abdurahman at Guidance. My experience with Guidance and especially with Sister Nejat was a memorable one. She was very professional, highly customer service oriented, quick responder and expert in her field. Her work ethics were very impressive. As a customer of mortgage financing, we always have a lot of questions with our rep, Sister Nejat was very responsive and always extremely respectful, polite and courteous. Amazingly she gave us her cell number also and we have access to her 24x7, which is very rare in this business. During entire process she was very prompt in keeping us updated on the status of process, which was a great help and gave us peace of mind. Guidance Residential is blessed to have an individual like Sister Nejat Abdurahman on their staff. She is a major asset to Guidance Residential and I think the company can use her to train other staff members for transferring her high level of skills. I would like to recommend every Muslim Brothers and Sisters to Sister Nejat Abdurahman.
BODY
        ],
    ],
    'nkhan' => [
        [
            'valediction' => '',
            'author' => 'Umma A.',
            'body' => <<<BODY
I worked with Noorjahan Khan for my refinance in the spring of 2017. The process of a refinance can be mind numbing if one is unaware the length of time, paperwork and jargon. I felt that Noorjahan was always available to answer my questions and did so in a kind and patient manner. Her correspondence was prompt and efficient and she always had answers for me in non-finance terms that I could understand. I look forward to working with her again for the home mortgage needs.
BODY
        ],
        [
            'valediction' => 'Jazakallahu khayr. Wassalamu alaykum',
            'author' => 'Shahid M',
            'body' => <<<BODY
Alhamdulillah. We moved in and enjoying the new home!

Please express my appreciation to all of your team, for staying on top of this, and working through some of the tricky areas of coordination with the builder. The team at Guidance Residential, kept us on-track and made very good use of every opportunity to follow-up and to make sure all parts of the process are addressed, helping avoid delays. A special thanks to Sr. Miriam Hassan for working diligently on all the outstanding documentation and gathering the necessary evidence. In a space, where the options are very competitive, Guidance Residential is doing a great service to the Muslim community, by offering this alternative. On a related note, working this loan via Guidance has been an interesting avenue for Da'wah as well, as one of the staff members in the builder's sales office was very interested to learn the details of this "faith-based financing model" (as he put it), and was pleased to know that there are players in this area. alhamdulillah. Inshaallah, we will be in touch.
BODY
        ],
    ],
    'rkhan' => [
        [
            'valediction' => 'Sincerely',
            'author' => 'Eesa Latif',
            'body' => <<<BODY
Rabia was a big reason the process to buy my first home felt like I was the most important customer they ever had. She was always busy, but never did I feel unattended for at any step of the way. InshaAllah I really hope to meet Rabia in person some day because she was pivotal in completing the process from start to end, and I honestly don't think I would have been able to do the process on my own. The more you learn about the process of purchasing a home, the more you realize the importance of having a good Account Executive who is there in your corner making sure every document, price agreement, and detail in and out of escrow is completed on its way in time to close.

Thank you again Rabia! May God bless you.
BODY
        ],
    ],
    'rasif' => [
        [
            'valediction' => '',
            'author' => 'Dr Mian H.',
            'body' => <<<BODY
Dear Rana Asif sb. Salaam.

Today morning before and after Fajr prayer I prayed for you and your family. I requested to Allah Please bless and forgive and reward lots of happiness to Rana Asif sb and his family as he helped me in obtaining a blessed home from his full attention and care. Allah have gifted me this house through his beloved person Rana Asif. You are permanent part of my prayers. I shall always remember everything you have done for me.
BODY
        ],
        [
            'valediction' => 'Thank you',
            'author' => 'Hazem H.',
            'body' => <<<BODY
Dear Rana,

Thank you very much for helping us with the purchase of our new home. House closing yesterday went very smooth. Thanks for explaining all steps and terms in details. I could not have asked for a better person to handle my case! You were always available regardless of what time of the day I contact you. You were always caring and on top of things. I profusely appreciate that and will be glad to refer my friends to work with you and Guidance Residential.
BODY
        ],
    ],
    'rserang' => [
        [
            'valediction' => '',
            'author' => 'Abidah H.',
            'body' => <<<BODY
I am your new client in Guidance. With the help of Rizwana, we, my husband and I, just closed the loan in August. She has been very helpful during the transactions: No pressure, gave details information to my request as best of her knowledge. She replied to my calls and emails promptly. Rizwana even worked after hours. I was pleasure doing business with her. She is an asset to your company.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Feryal K.',
            'body' => <<<BODY
Just wanted to share feedback on my experience of working with Rizwana and her team. Over all it was a great experience, especially in view of the fact that it was entirely remote. I had to switch financing at the last minute to Guidance and Rizwana was super accommodating in getting the paperwork over to my realtor and the seller in a hurry. I was concerned that an alternative model for financing may be problematic in working with the system here, but Rizwana and her team were extremely professional and diligent about working with both my realtor and the seller. Would thoroughly recommend Guidance to others!
BODY
        ],
        [
            'valediction' => 'Kind regards',
            'author' => 'Mutahir K.',
            'body' => <<<BODY
Assalam o Alaikym wa rehmatuAllah. I just bought a house in Elk Grove CA last week with help of Guidance Residential. I just wanted to say that process was very efficient and smooth only because of your contact loan person, Mrs Rizwana Serang. This house was a new construction but she remained in touch with us throughout this long period. She was available anytime with a phone, email, text or fax. She will respond right away and I sometimes wondered that whether she ever sleeps or gets some rest at all. I am very impressed by her professionalism and would highly recommend her for any promotion or job of higher responsibility. Please feel free to contact me anytime and thanks so much to Guidance Residential team with special obligation to recognize devoted, loyal and highly professional personnel like Mrs Rizwana Serang !
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Yasin H.',
            'body' => <<<BODY
My name is Yasin. I just bought my first home with Guidance Residential and I couldn't be anymore happier. The Staff of Guidance is exceptionally trained and very well experienced. They guided me through the entire process and kept me well informed. I want to specially thank Rizwana Serang who helped me every step of the way. She made the whole process easy to understand and explained all the materials very clearly. she was very responsive to answer all of my questions and concerns. Rizwana kept me very well informed and helped me understand the complicated terms and regulations of home buying. I was very fortunate to have her and i recommend her for anyone trying to go through this home buying journey for the first time. Thank you Rizwana and thank you Guidance team. This was a very first home buying experience for me and i am very fortunate to have done it with Guidance Residential.
BODY
        ],
    ],
    'salameddin' => [
        [
          'valediction' => 'Best regards',
          'author' => 'Abdirahman',
          'body' => <<<BODY
Asalaamu Alaikum brother Saad and sister Miriam,
We signed the documents yesterday and looking forward to moving to our new home.
Thank you guys for your help.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Afzal H.',
            'body' => <<<BODY
I would like to take moment and say a BIG Thanks and JazakAllah Khair to Brother Saad, Sister Mariam, Sister Omama and other person involved in my mortgaging process from Guidance Residential. This is our first home and complete process was tiring for me and my family, you guys made this process easy with your simplicity and always "ready to help" attitude. Brother Saad you are one of the knowledgeable and decent gentleman I ever met, May Allah Azzawajal increase your imaan and bless you and your family from his bounties. Ameen.

Sister Miriam - JazakAllah Khair for your quick and to the point work, it really made our life easy, especially after reading so many painful stories online. May Allah Subhan Wattala bless you and your family. Jzk Sis Omama to you as well, I can't talk enough about how impressed the title company was with you. They are a fan of Guidance Residential because of your pro-activeness.

This journey is just beginning.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Zakaria Abdelalim',
            'body' => <<<BODY
Salamo Alikum

Saad

I would like to thank you for working with me in buying my first home/ Condo. During this process you have answered all of my questions and replied to all my concerns via phone or email. You helped me in all aspects to complete the Guidance Residential, LLC Islamic finance application. During difficult situations you helped me by explaining the next steps and changes required to go forward with the process. I really appreciate all of what you did for me and wish you the best in continuing a good and reliable service to other brothers and sisters.

Abdelhakim

skafi aanta neo

I also thank you for answering all my questions and concerns during the process of purchasing my first home before and after.

wish you both the best in your career and I am referring all my friends in the Seattle area to you guys Jazakumu allah khair wa salamo alikum
BODY
        ],
    ],
    'selghazzali' => [
        [
            'valediction' => 'Thank you',
            'author' => 'Khalifa A.',
            'body' => <<<BODY
Salah was extremely nice, knowledgeable, and cooperative. He made the process easy for me. I'm really appreciative of Salah's service and I'm happy to have purchased my home through Guidance Residential.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Habeeb A.',
            'body' => <<<BODY
Thank you, Brother Salah! You were very professional and helpful during the whole process. Thank you again! And thank you Guidance Residential for providing us this opportunity of Islamic finance! It feels great to be in a country where we have these options.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Qased H.',
            'body' => <<<BODY
Overall it was great experience. I was treated like I was family. I wouldn't know how to better Guidance, because to me you were perfect!
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Arman M.',
            'body' => <<<BODY
Great customer service, Salah El-Ghazzali. You really made me feel and know that you cared about my concerns and wanted the best for me.
BODY
        ],
    ],
    'svasani' => [
        [
            'valediction' => 'Sincerely',
            'author' => 'Harrier F.',
            'body' => <<<BODY
I have to say I was nervous about going with an "unconventional" lender and received words of caution from my realtor and her agency as well because of it. However, my convictions led me to give it a try and hope for the best. Now that we are nearing the end of this process, I wanted to thank you for your diligence, being readily available, quick with responding to e-mails as well as thorough with your answers and offering a competitive loan package. This combination of services makes me feel confident that I made the right choice and your high level of service has made what can be an over-whelming process and commitment to be manageable and pleasant.

Wishing you and your team continued success!
BODY
        ],
        [
            'valediction' => 'Thank you',
            'author' => 'Sohail',
            'body' => <<<BODY
I was exploring ways to buy (and finance) a house in compliance with Sharia law and found Guidance Residential as an option. I was lucky enough to meet Salim. He explained to me in detail how the process works and how the contract and terms are setup. Salim is professional, courteous, and extremely communicative, still whenever I have any questions he is very prompt in responding. He is very patient in his explanations and his hands-on approach really worked well for me. Throughout the entire process he was incredibly responsive to all of my correspondence and a pleasure to work with. He worked very closely with my real estate agent, involving me only when absolutely needed. He worked really hard to make the process look easy and that's why I would highly recommend him and Guidance to anyone who is interested in buying a home.
BODY
        ],
        [
            'valediction' => 'Best Regards',
            'author' => 'Walid M.',
            'body' => <<<BODY
My wife, Maya and I, were happy to have you serve our needs for our home financing. We really appreciate all the help and support you professionally provided us in a timely and professional way.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mudassarah and Arifa C.',
            'body' => <<<BODY
Thank you for making this whole experience run as smoothly as possible and with all your help. Best wishes in all your future endeavors, Inshallah.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mike R. Pittsburg, CA',
            'body' => <<<BODY
Alhamdulillah! Finally I can sleep. My wife and were both worried since we found out that buying a house with interest is haram even though some Imams okayed it. We recently refinanced our house through Guidance with help of Mr. Salim Vasani. He was always there for us even after hours which made the process very pleasant. I'm so glad that we can finally finance the Shariah way and of course I would recommend my relatives and friends to Mr. Salim Vasani. Thank you Guidance!
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Said M. Corona, CA',
            'body' => <<<BODY
I bought my last house in Southern California using Guidance Residential loans. The overall experience was positive but the most impressive stand out was Salim Vasani, whom I was introduced to by accident through the Guidance online network. Salim was very instrumental at every step of the way from the preapproval to the needed documentation and clarity of terms. Salim was there whenever I needed and when others fell short. I strongly recommend Guidance and Salim, in particular, as a loan officer. Keep up the good work Salim.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Hasina H., CA',
            'body' => <<<BODY
I recently purchased a home through Guidance Residential. I had a great experience working with brother Salim Vasani, whom I found very knowledgeable and courteous. He was always accessible at the time of need.

I always felt the warm welcoming team work spirit of Guidance Residential team from the beginning to closing. If I plan to buy another home insha Allah, I will contact brother Salim Vasani of Guidance Residential without hesitation.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Syed S.',
            'body' => <<<BODY
This is my first home in US. I had dreams but was not sure of an easy way to avoid interest. That's when I heard about Guidance Residential from my friends. The executives are very friendly and they took more than an hour and answered all my questions. The entire life cycle of process was seamless and smooth. I worked with Salman Vasani, he is friendly and professional. I recommend Salim Vasani and Guidance Residential. Thank you Guidance for the "Guidance".
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Hassan M.',
            'body' => <<<BODY
We just bought a house in Northern California this year and used Guidance Residential for our financing needs since we were looking for a Shariah compliant option. We dealt primarily with Mr. Salim Vasani at Guidance and wanted to share my thoughts on the whole process and experience. Buying a house is an extremely trying process specially so in Northern California given the huge demand and lack of inventory and exorbitant real estate prices in this part of the country! We made many offers until we were finally able to secure the house that worked for us.

Throughout the process from initial approval to escrow and closing, Mr. Salim was very helpful and took the time to explain the process to us and take care of all the fine points and what needed to be done. It was a pleasure dealing with him. He was sensitive to our needs and did his best to make sure the process was successful given our constraints and that everything went as smoothly as possible. Since we were overseas during part of the process we even bothered him at odd hours and I found him to be patient and a pleasure to deal with.

I am thankful to him and all the Guidance Staff for all their effort and wish them well.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Shafi',
            'body' => <<<BODY
I recently bought a home through Guidance. In past I heard some bad rumors and average Guidance customer service as well as delays with regards to closing, but Brother Salim totally changed my perception. I had wonderful experience working with him. He, mashAllah, provided excellent guidance and help in educating me about various shariah financing models in a easy to understand manner. He is very knowledgeable, transparent, polite, easily accessible. He is very swift in responding to not only email messages but also text and calls. As a fist time buyer I had zillions of questions, he was always very polite, calm and composed and clarifying my doubts providing me an unbiased advise. May Allah SWT reward him immensely in both the lives Ameen.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Iyad H.',
            'body' => <<<BODY
Jazaka Allahu Khayran for your prompt courteous service. I will surely refer others who might need Islamic financing to you.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Akhom C.',
            'body' => <<<BODY
I just recently purchased a house in Vancouver WA, thru Guidance Residential. My loan executive was Salim Vasani. He is very knowledgable and pleasant to work with. He was available to answer my phone call day and night. The process went painlessly smooth. With his advise, I saved thousands of dollars with this transaction. I would recommend my friends and families to use Salim Vasani service and Guidance Residentail mortgage institution for their future home Purchase.
BODY
        ],
    ],
    'skabir' => [
        [
            'valediction' => 'Best Regards',
            'author' => 'Satir A.',
            'body' => <<<BODY
Alhamdulilah - we finished the closing today. I would like to personally thank you guys for your "Guidance" through this loan process and making it very simple. I am truly a happy customer. Hopefully we can do this again with my other property in the near future. Thank you.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Faheem Y.',
            'body' => <<<BODY
Working with Sami Kabir for last 6 years and have successfully completed multiple transactions with him. Transactions included both refinance and purchase of new homes. He has worked with me to lay out all the best options available that suit my needs and financial goals. I was guided through the entire process from initiating an application to close. He is very professional and efficient. You cannot go wrong with Sami Kabir and I would not want to work with anyone else.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mohammed H.',
            'body' => <<<BODY
Mr. Sami Kabir is very competent and dedicated person who displays good initiative and provides excellent day-to-day guidance during my loan application process.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Badrul C.',
            'body' => <<<BODY
I'm very happy with the transaction and with Sami's guidance. I was able to purchase multiple houses and I'm very thankful to him. He's knowledgeable and I will continue doing business with him.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Zia H.',
            'body' => <<<BODY
Sami guided me through my home mortgage process. He was there for me from start to finish. He is very knowledgeable at what he does. Any issues that came up during the process, he was there to help me resolve them. I highly recommend anyone that is thinking about buying a home to go and speak to Sami first. He will help you prepare for the process and he will see how much of a home you can afford.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Porlin K.',
            'body' => <<<BODY
Sami was patient with helping with the refinancing process. My dad and I have tried refinancing through Bank of America with no results. Sami was patient and guided us through the process of refinancing my home. I am happy to have worked with him. I recommend Sami.
BODY
        ],
        [
            'valediction' => 'Thank you',
            'author' => 'Haroon N.',
            'body' => <<<BODY
I am writing to you to thank Guidance Residential for their assistance in my most recent home purchase. Guidance has helped make a 30+ year dream come true for my parents and I. We will forever be grateful. I will recommend Guidance Residential to all my friends and family (regardless of their deen). It makes me truly happy to see an Islamic company provide the kind of service that I've received.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Mohammad Nadeem Khan',
            'body' => <<<BODY
I would like to thank each of you for the refinance. Thank you once again for the smooth closing.
BODY
        ],
    ],
    'sshekha' => [
        [
            'valediction' => 'With peace',
            'author' => 'A. Gaffoor',
            'body' => <<<BODY
Salaam and Good Morning,

About two weeks have passed since successfully closing on the Legacy Lane house (all praises to God) and we did not want the window of opportunity to escape us without properly extending our gratitude for your hard work in the pursuit of acquiring our home.

On behalf of Wardah and myself (and pending Baby Gaffoor), please accept our sincerest thanks for your patience, diligence, and persistence through this process. We genuinely appreciate each of your contributions towards making the acquisition of our home a reality.

May God brighten your lives with His generous favors and endow you with His choicest of blessings.
BODY
        ],
        [
            'valediction' => 'Best Regards',
            'author' => 'Mazhar ul haque Syed',
            'body' => <<<BODY
Brother Shabeer,

I would like to thank you for your enormous support to make the whole process easy, fast and smooth. I cannot say more than this, that you work so loyally, like its your own business. I appreciate the clarity, simplicity you and guidance residential have in the process. Special thanks for fulfilling the promises about the low rate and discounts as exactly you promised in the first communication.

I would definitely recommend and already recommending guidance for whomever I know.
BODY
        ],
        [
            'valediction' => 'Best Regards',
            'author' => 'Naina M.',
            'body' => <<<BODY
Dear Brother Shabeer, Alhamdullilah, by the immense Mercy of Allah (swt) we are in our new home and enjoying the snow here today in Cumming, Georgia. :) Our family thanks you and Guidance for making our home a reality in a Sharia way, Alhamdullilah. I appreciate all your diligence and guidance during this life time project of mine. You really did a transparent, honest and trustworthy job in all the dealings during this process. We really appreciate all your due diligence, assertiveness and clarity of thought as this home really means a lot to us. We appreciate the entire Guidance team. May Allah guide us all in his Straight path of Success. Jazakallah Khair Brother.
BODY
        ],
        [
            'valediction' => '',
            'author' => '',
            'body' => <<<BODY
Thank you all for helping make this a smooth and convenient process, Shabeer. Truly appreciate all the effort and hard work that you all put to make this happen!

Wishing you all the best, and look forward to great success for all.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Emrah S.',
            'body' => <<<BODY
We have finally moved into our house! Everything is coming out nicely, thanks to you!
BODY
        ],
    ],
    'trasheed' => [
        [
            'valediction' => '',
            'author' => 'Hassan M.',
            'body' => <<<BODY
The basic reason that drew me to Guidance Residential is the halal option it offers to home financing. And, it's halal as far as halal can go in a non Islamic economy and society. I'm happy I chose Guidance Residential.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Omer A.',
            'body' => <<<BODY
Taher was very responsive and professional - he helped guide me through the process.
BODY
        ],
    ],
    'tnisar' => [
        [
            'valediction' => '',
            'author' => 'Scott W.',
            'body' => <<<BODY
I had a wonderful experience with Guidance Residential to refinance my home at a lower rate. I also used Guidance Residential with my original mortgage and I'm satisfied with them.
BODY
        ],
        [
            'valediction' => 'Jazak Allah-u Khayran',
            'author' => 'Altijani H.',
            'body' => <<<BODY
Alhamdolillah, we are now in the new house that we bought with the help of Allah SWT and then with your assistance in closing this deal.

At this moment, my family and I would like to thank you for the many details that you provided, and for being always there. In particular, you have been very knowledgeable about the Sharia Compliant home owner finance, and you have communicated that to us directly and through different opportunities. In addition, you have been available to us to respond to our communications during late evening hours or weekends, by phone, or by email. That made things really easy to us. I remember when I called one late evening and I told you that I need to change the entire finance structure from 25% closing to 20% closing. I expected you to shout and get irritated, because we were only few days to close. Instead, you were calm and took care of it next morning, and we had to delay the closing by only one week.

I also like that you were keeping every communication professional and private. You helped me to navigate through your online system of emails, which ensured that our communication is secured and private. I have a previous experience with another Halal Financing mortgage, and my communication with them was not satisfactory. You showed me how important it is to have a reliable and available person at the other end of the wire to facilitate the process. It is the biggest purchase a person may have, and it was very good with Guidance, alhamdolillah.

At the end, I want to thank Allah SWT again for putting you on our way, and that the process went very smoothly. I will definitely recommend Guidance to others, and would highly recommend you as the point of contact, if I may :)

You will hear from me soon insha Allah, once we sell our house in NC. We will need to put that money into our account with you to reduce the monthly payment. Please keep tuned to that.
BODY
        ],
    ],
    'zmohammed' => [
        [
            'valediction' => 'Sincerely',
            'author' => 'Fausto C.',
            'body' => <<<BODY
Thank you Zeeshan for helping me through the process. Buying a house isn't easy, much less my first home. You were very helpful answering all my questions, guiding me, and finding solutions. You demonstrated true concern and wanting what was best for me. I didn't feel pressured or sold into anything. I could tell you worked hard and kept it professional through out the process. I really appreciate your business, I'm glad you were my lender agent, great job! Thank you!
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Abdulaziz D.',
            'body' => <<<BODY
Earlier this year me and my wife decided that we wanted to purchase a house. After renting, I was hesitant to buy because I did not want to incorporate Interest in my life. I did some research and found out about Guidance Residential. I spoke to Mr Zeeshan on the phone for an hour where he explained to me how their home financing works. I got a quote, compared it to other conventional debt quotes and was very surprised that they were so competitive - I was not paying more for Islamic financing. After I found the house that I wanted we had an issue with the seller where she needed to finish the sale in a period requiring that everything should be completed quickly by the deadline. Mr Zeeshan was very prompt with asking for documents and making sure that everything got done in time. Since it was my first home purchase, I had to be walked through every step and educated but he made sure that everything was very clear and that I had full information. He was always available to answer questions and at closing, he was working extra hard communicating with all parties. I really appreciate the way he handled the sale process and I highly recommend him for anyone wishing to purchase a house.
BODY
        ],
        [
            'valediction' => 'Regards',
            'author' => 'Masud S.',
            'body' => <<<BODY
I really appreciate your time and all your help. Thank you for all the suggestions and advice throughout the process. I felt I had someone to guide me - and you indeed did. May Allah reward you for your work. I look forward to keeping a good relationship with you insha'allah and have mentioned Guidance to my relative. I will keep in touch with you if I need any help or suggestions.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Omar T.',
            'body' => <<<BODY
After traveling, we are headed back to the US this week and are excited to be going to our new home. Without your assistance this would not have been possible and we appreciate what you have - with Allah's will and grace -helped us achieve. You showed excellent character throughout this long process and we are grateful that you were our account executive. May Allah reward you and bless your family. Any time we need Guidance we will be in touch with you. Our warmest regards and sincere appreciation.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Omar T.',
            'body' => <<<BODY
Jazakum Allahu Khair for assisting us in owning a home based on riba-free Islamic financing. You helped us make our home ownership dream a reality!
BODY
        ],
        [
            'valediction' => 'Best Regards',
            'author' => 'Emad A.',
            'body' => <<<BODY
I would like to extend my appreciation and gratitude to Allah SW. then to your hard work you provided me through my <strong>two</strong> investment closings in December 2016. I am so pleased with the amount of ownership you took on the processes and timeline. With a few hurdles we had &amp; 2 federal holidays, you still managed to meet the 45 days promise of closing. Jazak Allah Kheer for all your work &amp; efforts. Our community in North Dallas needs these characteristics.
BODY
        ],
        [
            'valediction' => '',
            'author' => 'Emad A.',
            'body' => <<<BODY
Zeeshan was an excellent Account Executive and dealt with my 2 closings like they were his own.
BODY
        ],
        [
            'valediction' => 'Best wishes',
            'author' => 'Saleem G.',
            'body' => <<<BODY
Just wanted to take time and thank you for the amazing and exceptional experience I had working with you through the overall loan process through Guidance Residential. You always had detailed information for all the questions I had and thorough knowledge of overall processes, including next steps. Also, from the loan application process to closing, my wife and I felt very comfortable to not to worry about any unknowns. Again, we appreciated all your help.
BODY
        ],
        [
            'valediction' => 'Thanks again',
            'author' => 'Fahad D.',
            'body' => <<<BODY
Thanks for all the great help you provided, you were amazing and thanks for being patient with me. May Allah give you reward for everything.
BODY
        ],
    ],
    'fafzal' => [
        [
            'valediction' => 'Regards',
            'author' => 'Serkan',
            'body' => <<<BODY
Thanks Farhan,

Yes, Alhamdulillah, everything went smoothly.

It was a long journey on my end, however you were always there when I needed help whether it is small or big. We finally came to end smoothly. Jazakallah for your hard work and effort. Thank you for your company to create an opportunity for people like us to make decisions suitable to our lifestyle and beliefs. Otherwise home ownership would not be option for us.

I have been and will definitely let all my friends know about your service.

I will keep you posted.
BODY
        ],
        [
            'valediction' => 'Thanks for all your help!',
            'author' => 'Najam',
            'body' => <<<BODY
Walaikum Assalam Farhan,

Yes, Alhamdulillah, everything went smoothly.

I must say that it was a pleasure working with you throughout this process.You have earned great respect in the way you handled everything and the approach you have towards your job.

I will absolutely refer your name to anyone who needs help with home financing.

Keep up the great work you are doing!
BODY
        ],
    ],
    'ialtufaili' => [
        [
            'valediction' => '',
            'author' => 'Shakeel Memon',
            'body' => <<<BODY
Salam sister Ihab Altufaili,

Ihab sister you have been truly wonderful throughout this whole guidance finance process of buying a house. You have picked up every phone call and answered all my questions I've had no idea about the process. You've made it very simple especially because this was my first time going through a loan process like this and completed this whole loan process within 2 months. For that I wanna say thank you, also thank you for being so responsive and friendly like family.
BODY
        ],
    ],
];
