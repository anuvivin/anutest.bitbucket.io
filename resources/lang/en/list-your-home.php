<?php

return [
    'attributes' => [
        'city' => 'City',
        'email' => 'Email Address',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'phone' => 'Phone Number',
        'state' => 'State',
    ],
    'messages' => [
        'success' => 'Your home listing request was submitted successfully!',
        'error' => 'Your home listing request could not be completed. Please try again later.',
    ],
];
