<?php

return [
    'attributes' => [
        'email' => 'Email Address',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'name' => 'Name',
        'state' => 'State',
    ],
    'messages' => [
        'success' => 'Your free consultation request was submitted successfully!',
        'error' => 'Your free consultation request could not be completed. Please try again later.',
    ],
];
