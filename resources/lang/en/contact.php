<?php

return [
    'attributes' => [
        'comments' => 'Comments',
        'email' => 'Email Address',
        'name' => 'Name',
        'phone' => 'Phone Number',
        'reason' => 'Reason',
    ],
    'messages' => [
        'success' => 'Your contact request was submitted successfully!',
        'error' => 'Your contact request could not be completed. Please try again later.',
    ],
];
