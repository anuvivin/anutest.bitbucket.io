export default () => {
	$('#contactUsModal').on('show.bs.modal', function (e) {
		$('#contactUsModal .desc-text.contactUs-error').removeClass('active');
	});

	$('#contactUsForm').on('submit', function(e){
		var form = $(e.currentTarget);
		var submit = form.find('button[type="submit"],input[type="submit"]');

		e.preventDefault();

		if (submit.prop('disabled')) {
			return;
		}

		var spinner = $('#contactUsModal .modal-spinner');

		submit.prop('disabled', true);
		spinner.removeClass('hidden');

		var route = form.prop('action');
		var method = form.prop('method');

		var input = {
			reason: form.find('[name="reason"]').val(),
			name: form.find('[name="name"]').val(),
			email: form.find('[name="email"]').val(),
			phone: form.find('[name="phone"]').val(),
			comments: form.find('[name="comments"]').val()
		};

		$.ajax({
			url: route,
			method: method,
			dataType: 'json',
			data: input,
			error: function (response) {
				$('#contactUsModal .desc-text.contactUs-error').addClass('active');
			},
			success: function (response) {
				form[0].reset();
				$('#contactUsModal .form-content').addClass('inactive');
				$('#contactUsModal #contactUsSuccessMessage').addClass('active');
				setTimeout(() => {
					$('#contactUsModal').modal('hide').data( 'bs.modal', null );
				}, 3000);
			},
			complete: function (response) {
				submit.prop('disabled', false);
				spinner.addClass('hidden');
			}
		});
	});
}
