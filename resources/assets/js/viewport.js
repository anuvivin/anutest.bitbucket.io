var initialized = false;
var $document = $(document);
var $window = $(window);

var offset = {
	x: 0,
	y: 0
};

var size = {
	width: 0,
	height: 0
};

var update = function() {
	offset.x = $window.scrollLeft();
	offset.y = $window.scrollTop();
	size.width = $window.width();
	size.height = $window.height();
};

export default {
	init: function() {
		if (initialized) {
			return this;
		}

		$window.on('scroll', update).scroll();
		$window.on('resize', _.throttle(update, 250));

		return this;
	},
	getPosition: function() {
		return offset;
	},
	getSize: function() {
		return size;
	},
	getBounds: function() {
		return {
			left: offset.x,
			right: offset.x + size.width,
			top: offset.y,
			bottom: offset.y + size.height
		};
	},
	getCenter: function() {
		let bounds = this.getBounds();
		let size = this.getSize();

		return {
			x: bounds.left + (size.width / 2),
			y: bounds.top + (size.height / 2)
		};
	},
	getQuadrant: function($element) {
		var offset = $element.offset();

		var size = {
			width: $element.outerWidth(),
			height: $element.outerHeight()
		};

		var center = {
			x: offset.left + (size.width / 2),
			y: offset.top + (size.height / 2)
		};

		var viewCenter = this.getCenter();

		if (center.x >= viewCenter.x) {
			return (center.y >= viewCenter.y) ? 2 : 1;
		} else {
			return (center.y >= viewCenter.y) ? 3 : 4;
		}
	}
};
