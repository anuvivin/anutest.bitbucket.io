export default () => {
	$('#listYourHomeModal').on('show.bs.modal', function (e) {
		$('#listYourHomeModal .desc-text.list-your-home-error').removeClass('active');
	});

	$('#listYourHomeForm').on('submit', function(e){
		var form = $(e.currentTarget);
		var submit = form.find('button[type="submit"],input[type="submit"]');

		e.preventDefault();

		if (submit.prop('disabled')) {
			return;
		}

		var spinner = $('#listYourHomeModal .modal-spinner');

		submit.prop('disabled', true);
		spinner.removeClass('hidden');

		var route = form.prop('action');
		var method = form.prop('method');

		var input = {
			first_name: form.find('[name="first_name"]').val(),
			last_name: form.find('[name="last_name"]').val(),
			email: form.find('[name="email"]').val(),
			phone: form.find('[name="phone"]').val(),
			city: form.find('[name="city"]').val(),
			state: form.find('[name="state"]').val()
		};

		$.ajax({
			url: route,
			method: method,
			data: input,
			dataType: 'json',
			error: function(response) {
				$('#listYourHomeModal .desc-text.list-your-home-error').addClass('active');
			},
			success: function(response) {
				form[0].reset();
				$('#listYourHomeModal .content').addClass('inactive');
				$('#listYourHomeModal #listyourHomeSuccessMessage').addClass('active');
				setTimeout(() => {
					$('#listYourHomeModal').modal('hide').data( 'bs.modal', null );
				}, 3000);
			},
			complete: function (response) {
				submit.prop('disabled', false);
				spinner.addClass('hidden');
			}
		});
	});
}
