export default () => {
	$('#scheduleConsultationModal').on('show.bs.modal', function (e) {
		$('#scheduleConsultationModal .desc-text.schedule-error').removeClass('active');
	});

	$('#scheduleConsultationForm').on('submit', function(e){
		var form = $(e.currentTarget);
		var submit = form.find('button[type="submit"],input[type="submit"]');

		e.preventDefault();

		if (submit.prop('disabled')) {
			return;
		}

		var spinner = $('#scheduleConsultationModal .modal-spinner');

		submit.prop('disabled', true);
		spinner.removeClass('hidden');

		var route = form.prop('action');
		var method = form.prop('method');

		var input = {
			first_name: form.find('[name="first_name"]').val(),
			last_name: form.find('[name="last_name"]').val(),
			email: form.find('[name="email"]').val(),
			phone: form.find('[name="phone"]').val(),
			state: form.find('[name="state"]').val()
		};

		$.ajax({
			url: route,
			method: method,
			dataType: 'json',
			data: input,
			error: function (response) {
				$('#scheduleConsultationModal .desc-text.schedule-error').addClass('active');
			},
			success: function (response) {
				var pixel = $('#scheduleSuccessMessage .pixel');

				if (pixel.length > 0) {
					pixel.first().append('<img src="' + pixel.data('src') + '" alt=""/>');
				}

				form[0].reset();
				$('#scheduleConsultationModal .content').addClass('inactive');
				$('#scheduleConsultationModal #scheduleSuccessMessage').addClass('active');
				setTimeout(()=> {
					$('#scheduleConsultationModal').modal('hide').data( 'bs.modal', null );
				}, 3000);
			},
			complete: function (response) {
				submit.prop('disabled', false);
				spinner.addClass('hidden');
			}
		});
	});
}
