export default ()=> {
  const FIRST_COOKIE_NAME = 'webinarBannerShown',
        SECOND_COOKIE_NAME = 'webinarBannerDate',
        EVENT_DATE = 'May 2, 2018 20:00:00 EDT',
        DISPLAY_TEXT = 'Interested in a Free Live Webinar with an Islamic Finance Expert?',
        EXP_DAYS = 30,
        DELETE_COOKIE_EXP_DATE = 'expires=Thu, 01 Jan 1970 00:00:00 UTC',
        COOKIE_PATH = 'path=/',
        WAIT_TIME = 60,
        USER_TIME_TIMEOUT = 6000,
        TIMER_TIME_TIMEOUT = 60000,
        ENABLE_ON_EXIT = false;
  let webinarModal = $('#webinarModal');

  if(modalExistsOnPage() && eventStillValid()){
    initializeModalProcess();
  //bind event to window, show modal when user is about to leave and then disable cookies
    if (ENABLE_ON_EXIT) {
      $('html').bind('mouseleave', ()=> {
        if(getCookie(FIRST_COOKIE_NAME) == 'false'){
          initializeWebinarModal();
          $('html').unbind('mouseleave');
        }
      });
    }
  }

  function initializeWebinarModal(){
    displayTimer();
    disableCookies();
    showModal();
    updateTimer();
  }

  function showModal(){
    webinarModal.modal('show');
  }

  function updateTimer(){
    setTimeout(()=> {
     if(webinarModal.data('bs.modal').isShown){
       displayTimer();
       setTimeout(updateTimer(), TIMER_TIME_TIMEOUT);
     }
    }, TIMER_TIME_TIMEOUT);
  }

  function displayTimer(){
    const distance = new Date(EVENT_DATE).getTime() - Date.now();

    let days = Math.floor(distance / (1000 * 60 * 60 * 24)),
        hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
        minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

    webinarModal.find('#webBannerDays').html(days);
    webinarModal.find('#webBannerHours').html(hours);
    webinarModal.find('#webBannerMinutes').html(minutes);
  }

  function checkElapsedTime(){
    if(!getCookie(SECOND_COOKIE_NAME)){ return;}
    let createDate = new Date(getCookie(SECOND_COOKIE_NAME)),
        nowTime = Date.now(),
        elapsedTime= (nowTime - createDate.getTime())/1000;
    if(elapsedTime >= WAIT_TIME){
      changeModalDisplayText();
      initializeWebinarModal();
    }
    else{
      setTimeout(checkElapsedTime.bind(this), USER_TIME_TIMEOUT);
    }
  }

  function changeModalDisplayText(){
    webinarModal.find('#webinarModalMainText').html(DISPLAY_TEXT);
  }

  function modalExistsOnPage(){
    return Object.getOwnPropertyNames(webinarModal).length > 0;
  }

  function eventStillValid(){
    return new Date(EVENT_DATE) > new Date();
  }

  function disableCookies(){
    document.cookie = `${SECOND_COOKIE_NAME}=;${DELETE_COOKIE_EXP_DATE};${COOKIE_PATH}`;
    document.cookie = `${FIRST_COOKIE_NAME}=true;${setExpirationDate()};${COOKIE_PATH}`;
  }

  function initializeModalProcess(){
    if(!getCookie(FIRST_COOKIE_NAME)){
      setCookies();
      setTimeout(checkElapsedTime(), USER_TIME_TIMEOUT);
    }
    else{
      if(getCookie(FIRST_COOKIE_NAME) == 'false'){
        setTimeout(checkElapsedTime(), USER_TIME_TIMEOUT);
      }
    }
  }

  function setCookies() {
    let create = new Date();
    document.cookie = `${FIRST_COOKIE_NAME}=false;create=${create};${setExpirationDate()};${COOKIE_PATH}`;
    document.cookie = `${SECOND_COOKIE_NAME}=${create};${setExpirationDate()};${COOKIE_PATH}`;
  }

  function setExpirationDate(){
    let expDate = new Date();
    expDate.setTime(expDate.getTime() + (EXP_DAYS * 24 * 60 * 60 * 1000));
    return `expires=${expDate.toUTCString()}`;
  }

  function getCookie(name) {
    let match = document.cookie.match(new RegExp(name + '=([^;]+)'));
    return match ? match[1] : null;
  }
}
