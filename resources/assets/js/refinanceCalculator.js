//import {formatDollars} from './formatDollars.js';
export default ()=>{
    //Refinance calculators code
    $('#refinanceCalculatorButton').on('click', e=> {
      let inputs =   $('#refinanceCalculator :input');
      let inputValues = {};
      for (let x in inputs) {
        if (inputs.hasOwnProperty(x)) {
          if ($(inputs[x])[0].id) {
            inputValues[$(inputs[x])[0].id] = parseInt($(inputs[x])[0].value);
          }
        }
      }
      print15YearRefinanceResults(getRefinanceData(15,3.875,inputValues));
      print30YearRefinanceResults(getRefinanceData(30,4.125,inputValues));
      $(e.currentTarget).html('Update Result');
      $('.result-box.refinanceResultWrapper').addClass('active');
    });

    //refinance calculator getResult method
    function getRefinanceData(term, rate, {refinanceAmount, currentMonPay, remainingMonths}){
      let result = {};
      result['term'] = term;
      result['rate'] = rate;
      let months = term * 12;
      result['newMonthlypay'] = Math.round(computeMonthlyPayment(refinanceAmount, months, rate));
      result['saveOnPymt'] =  Math.round(currentMonPay - result.newMonthlypay);
      result['totalAmt'] = Math.round(result.newMonthlypay * months);
      let remainingPay = remainingMonths * currentMonPay;
      result['totpayReduce'] = Math.abs(Math.round(remainingPay - result.totalAmt));

      return result;
     }

     function formatDollars(num) {
       return '$' + num.toLocaleString();
     }

    function computeMonthlyPayment(amount, months, rate){
      let pmtAmount = 0;
      if(rate == 0) {
          pmtAmount = amount / months;
      }
      else
      {
        if (rate >= 1.0) {
            rate = rate / 100.0;
        }
        rate /= 12;
        let pow = 1;
        for (let j = 0; j < months; j++){
          pow = pow * (1 + rate);
        }
        pmtAmount = (amount * pow * rate) / (pow - 1);
      }
      return pmtAmount;
    }

    //15 year refinance print method
     function print15YearRefinanceResults(result){
       $('#refNewMonthlyPayment .number').html(formatDollars(result.newMonthlypay));
       if(result.saveOnPymt<0) {
         $('#refPaymentIncrease .text').html('Monthly Payment Increased By:');
         $('#refPaymentIncrease .number').html(formatDollars(Math.abs(result.saveOnPymt)));
       }
       else
       {
         $('#refPaymentIncrease .number').html(formatDollars(Math.abs(result.saveOnPymt)));
       }
       $('#refTotalPayment .number').html(formatDollars(result.totalAmt));
       $('#refIncTotalPayment .number').html(formatDollars(result.totpayReduce));
     }

     //30 year refinance print method
      function print30YearRefinanceResults(result){
        $('#refNewMonthlyPayment30 .number').html(formatDollars(result.newMonthlypay));
        if(result.saveOnPymt<0) {
          $('#refPaymentIncrease30 .text').html('Monthly Payment Increased By:');
          $('#refPaymentIncrease30 .number').html(formatDollars(Math.abs(result.saveOnPymt)));
        }
        else
        {
          $('#refPaymentIncrease30 .number').html(formatDollars(Math.abs(result.saveOnPymt)));
        }
        $('#refTotalPayment30 .number').html(formatDollars(result.totalAmt));
        $('#refIncTotalPayment30 .number').html(formatDollars(result.totpayReduce));
      }

}
