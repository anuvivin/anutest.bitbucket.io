//import {formatDollars} from './formatDollars.js';
export default ()=>{
    //Rent Buy calculators code

  $('#rentBuyCalculatorButton').on('click', e=> {

    let inputs =   $('#rentBuyCalculator :input');
    let inputValues = {};
    for (let x in inputs) {
      if (inputs.hasOwnProperty(x)) {
        if ($(inputs[x])[0].id) {
          inputValues[$(inputs[x])[0].id] = parseInt($(inputs[x])[0].value);
        }
      }
    }
    console.log(inputValues);
    console.log(calculate(inputValues));
    printRentBuyResults(calculate(inputValues));
    $(e.currentTarget).html('Update Result');
    $('.result-box.rentBuyResultWrapper').addClass('active');
  });

  function calculate({targetMonthlyRent, targetHomeValue, targetDownPayment, stayExpectation,targetIncomeTax})
      {

      //assumptions
        let mortgageTerm = 30;
        let profitRate = 4.125;
        let AnnualPropTax = 1.02;
        let HomeOwnerIns =  0.5;
        let yrRentIns = 1.32;
        let closingCostRate = 4;
        let RentApprec = 2.3;
        let homeApprec = 2;
        let inflationRate = 2;
        let earnIntRate = 3;//dynamiv value change later on

        //Inflation
        inflationRate = inflationRate/100;
        inflationRate = inflationRate + 1;

        //interest for home
        let ContractAmt = targetHomeValue - targetDownPayment;
        let noMonths = mortgageTerm * 12;
       //Mortgage Monthly payment
        let monthlyPmt = computeMonthlyPayment(ContractAmt, noMonths, profitRate);
        //calculate home Appreciation for first year:
        homeApprec = homeApprec / 100;
        homeApprec = homeApprec + 1;
        let homeApprecAmt = Math.round(targetHomeValue * homeApprec * 100) / 100;


        //Closing Cost
        let closingCost = closingCostRate/100;
        closingCost = closingCost * targetHomeValue;

        let investIntPort = 0;
        let investPrinc =  targetDownPayment + closingCost;

        //  calculate Earned Interst
        earnIntRate /= 100;
        earnIntRate /= 12;

        let inflation = 1;
        let count = 0;

        //Rent Ins
        yrRentIns = yrRentIns/100;
        let moRentInsRate = yrRentIns/12;
        let totRentInsurance = 0;

        //StayMonths
        let stayMonths = stayExpectation * 12;
        RentApprec =RentApprec/100;

        //assign
        let totRent = 0;
        let totRentPmts = 0;
        let totRenInsAmt = 0;
        let prin = ContractAmt;
        let HomeIntAmt = 0;
        let totalinvestInsPort = 0;
        let totalmortgagepay = 0;
        while(count < stayMonths) {

            if(count > 0 && count % 12 == 0) {
                //After One Year changes in inflation and Rent
                homeApprecAmt = homeApprecAmt * homeApprec;
                targetMonthlyRent = Math.round(targetMonthlyRent *(RentApprec+ inflationRate) * 100) / 100;
                inflation = inflation * inflationRate;
            }
            let moRentIns = moRentInsRate*targetMonthlyRent;
            //Total Rent
            totRent = totRent + targetMonthlyRent + moRentIns;
            totRentPmts = totRentPmts + targetMonthlyRent;
            totRenInsAmt = totRenInsAmt + moRentIns;

            if(count < noMonths) {
                let tempProfitRate = profitRate/(12  * 100);
                let intPort = Math.round(prin * tempProfitRate);
                //Total interest cost payed for home loan
                HomeIntAmt = HomeIntAmt + intPort;
                //Total principle cost reduced form home loan
                let prinPort = monthlyPmt -  intPort;
                //principle+interest for stay years
                totalmortgagepay +=  monthlyPmt;
                prin = prin - prinPort;
            }
            //Earned Interest for Down Payment
            investIntPort = Math.round(earnIntRate * investPrinc * 100) / 100;
            //Total Intrest;
            totalinvestInsPort = totalinvestInsPort + investIntPort;
            //total interest + targetDownPayment
            investPrinc = investPrinc + investIntPort;

            count = count + 1;
        }
        //totRentIns = Math.round(totRent-totRentPmts);


        //Property tax
        AnnualPropTax = AnnualPropTax/100;
        let propTax = AnnualPropTax*targetHomeValue;
        let totPropTax = Math.round(propTax * stayExpectation * inflation * 100) / 100;

        //Home OwnerIns
        HomeOwnerIns = HomeOwnerIns / 100;
        let totHomeOwnerInsCost = Math.round(HomeOwnerIns * targetHomeValue * stayExpectation * inflation * 100) / 100;
        //Cost Of Ownership
        let totOwnership = Math.round(totalmortgagepay) + Math.round(totPropTax) + Math.round(totHomeOwnerInsCost);

        //Oppurtunitycost
        let investEarn = investPrinc - targetDownPayment - closingCost;
        let totRentOppCost = investEarn;

        let totHomeApprec = Math.round(homeApprecAmt- targetHomeValue);
        let totTaxDeduct = HomeIntAmt + totPropTax;
        targetIncomeTax = targetIncomeTax / 100;
        let totTaxSave = Math.round(targetIncomeTax  * totTaxDeduct  * 100) / 100;
        let totBuyOppCost = Math.round(totHomeApprec) + Math.round(totTaxSave);

        //netCost
        let netRentCost = Math.round(totRent) - Math.round(totRentOppCost);
        let netBuyCost =  Math.round(totOwnership) - Math.round(totBuyOppCost);
        let netResult = "";
        if(netRentCost < netBuyCost){
            netResult = `You will save <span class="golden">${formatDollars(netBuyCost - netRentCost)}</span> if you rent instead of buy.`;
        }else{
            netResult = `You will save <span class="golden">${formatDollars(netRentCost - netBuyCost)}</span> if you buy instead of rent.`;
        }
        let result = {};

        result['type'] = "rentvsbuy";
        result['totRentCost'] = formatDollars(Math.round(totRent));
        result['totCostOfOwn'] = {
                                'MortgagePay':formatDollars(Math.round(totalmortgagepay)),
                                'PropTaxes' :formatDollars(Math.round(totPropTax)),
                                'homeIns' :formatDollars(Math.round(totHomeOwnerInsCost)),
                                'total' :formatDollars(Math.round(totOwnership))
                              };
        result['rentOppCost'] = formatDollars(Math.round(investEarn));
        result['buyOppCost'] = {
                              'taxToSave': formatDollars(Math.round(totTaxSave)),
                              'homeApprec' :formatDollars(totHomeApprec),
                              'total': formatDollars(Math.round(totBuyOppCost))
                            };
        result['netCost']= {'rent' : netRentCost,
                              'buy' : netBuyCost,
                              'result': netResult
                            };

        return result;
      }

      function computeMonthlyPayment(amount, months, rate){
        let pmtAmount = 0;
        if(rate == 0) {
            pmtAmount = amount / months;
        }
        else
        {
          if (rate >= 1.0) {
              rate = rate / 100.0;
          }
          rate /= 12;
          let pow = 1;
          for (let j = 0; j < months; j++){
            pow = pow * (1 + rate);
          }
          pmtAmount = (amount * pow * rate) / (pow - 1);
        }
        return pmtAmount;
      }
  function formatDollars(num) {
    return '$' + num.toLocaleString();
  }

  function printRentBuyResults(result){
    $('#rentTotalRentCost').html(result.totRentCost);
    $('#rentTotalRentIns .number').html(result.totRentCost);
    $('#rentTotalRentCostAmount .number').html(result.totRentCost);
    $('#rentcostHomeOwnsership').html(result.totCostOfOwn.total);
    $('#rentcombinedMonthlyPay .number').html(result.totCostOfOwn.MortgagePay);
    $('#rentPropertyTaxes .number').html(result.totCostOfOwn.PropTaxes);
    $('#rentHomeownerIns .number').html(result.totCostOfOwn.homeIns);
    $('#rentTotalHomeownership .number').html(result.totCostOfOwn.total);
    $('#rentOpportunityCost').html(result.rentOppCost);
    $('#rentInterestEarned .number').html(result.rentOppCost);
    $('#rentTotalOpportunityCost .number').html(result.rentOppCost);
    $('#rentOpportunityCostTwo').html(result.buyOppCost.total);
    $('#rentTaxSavings .number').html(result.buyOppCost.taxToSave);
    $('#rentHomeAppreciation .number').html(result.buyOppCost.homeApprec);
    $('#rentTotalOpportunityCostTwo .number').html(result.buyOppCost.total);
    $('#rentNetCostRenting').html(formatDollars(result.netCost.rent));
    $('#rentNetCostBuying').html(formatDollars(result.netCost.buy));
    $('#rentSaveAmount').html(result.netCost.result);
    $('#rentRentingCost .number').html(formatDollars(result.netCost.rent));
    $('#rentBuyingCost .number').html(formatDollars(result.netCost.buy));
    if (result.netCost.rent > result.netCost.buy) {
      $(".bar-box.rentBuy").css("flex-direction","column-reverse");
      $('#rentRentingBar').css('width', "100%");
      $('#rentBuyingBar').css('width',(result.netCost.buy/result.netCost.rent*100)+'%');
    }
    else{
      $(".bar-box.rentBuy").css("flex-direction","column");
      $('#rentBuyingBar').css('width', "100%");
      $('#rentRentingBar').css('width',(result.netCost.rent/result.netCost.buy*100)+'%');
    }
  }
}
