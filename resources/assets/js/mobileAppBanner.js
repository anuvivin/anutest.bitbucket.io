import MobileDetect from 'mobile-detect';
export default ()=> {
  $('.mobile-app-banner .fa-close').on('click',()=> {
    $('.mobile-app-banner').removeClass('active');
  });
  //detect if the device is mobile or tablet
   let md = new MobileDetect(window.navigator.userAgent);
   if (md.mobile()) {
    let isAndroid = md.os()== 'AndroidOS';
    let isApple  = md.os()== 'iOS';
    if (isAndroid || isApple) {
      if (!checkCookie()) {
        setCookie('appNotificationBanner','v1', 30);
         $('.mobile-app-banner').addClass('active');
      }
      if (isAndroid) {
        $('.mobile-app-banner .android-logo').addClass('active');
        $('.mobile-app-banner .title-link').attr('href', "https://play.google.com/store/apps/details?id=com.amazon.mShop.android.shopping&hl=en");
      }
      if (isApple) {
        $('.mobile-app-banner .apple-logo').addClass('active');
        $('.mobile-app-banner .title-link').attr('href', "https://itunes.apple.com/us/app/amazon-prime-video/id545519333?mt=8");
      }
    }
   }
   //
   function setCookie(name, value, expirationDays) {
        let expDate = new Date();
        expDate.setTime(expDate.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
        let expires = 'expires='+expDate.toUTCString();
        document.cookie = name + '=' + value + ';' + expires + ';path=/';
    }

    function getCookie(cookieName) {
        let name = cookieName + '=';
        let cookieArray = document.cookie.split(';');
        for (var i = 0; i < cookieArray.length; i++) {
          let cookie = cookieArray[i];
          if (cookie.indexOf(name) !== -1) {
            return true;
          }
        }
        return false;
    }

    function checkCookie() {
      return getCookie('appNotificationBanner');
    }
}
