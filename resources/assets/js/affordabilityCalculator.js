//import {formatDollars} from './formatDollars.js';
export default ()=> {
  //Affordability calculators code
  $('#affordabilityCalculatorButton').on('click', e=> {
    let inputs =   $('#affordabilityCalculator :input');
    let inputValues = {};
    for (let x in inputs) {
      if (inputs.hasOwnProperty(x)) {
        if ($(inputs[x])[0].id) {
          inputValues[$(inputs[x])[0].id] = parseInt($(inputs[x])[0].value);
        }
      }
    }
    let calculation = calculate(inputValues);
    if(calculation){
      $('#affordabilityError').removeClass('active');
      printAffordabilityValues(calculation);
      $(e.currentTarget).html('Update Result');
      $('.result-box.affordabilityResultWrapper').addClass('active');
    }
    else{
      $('#affordabilityError').html('Your monthly debt to income ratio is too high. Please reduce your Monthly Expenses and try again.');
      $('#affordabilityError').addClass('active');
    }
  });

  function formatDollars(num) {
    return '$' + num.toLocaleString();
  }

  function calculate({affMonthlyIncome, affMonthlyExpense, affDownPayment}) {
      //assumptions
      let profitrate = 4.125;
      let term       = 360;
      let homeins    = 0.3;//home Insurance%
      let proptax    = 1.25;//property tax%
      let DTI        = 0.45;

      let interestrate = getMortgageInterestRate(profitrate);
      let totmonthlypay = getTotalMonthlyPayment(affMonthlyIncome, affMonthlyExpense, DTI);

      let taxes = 0;
      let monthlypi = getMonthlyPrincAndIntr(totmonthlypay,taxes);
      let loanamt = getLoanAmount(monthlypi, interestrate, term);
      taxes = getTaxAmount(loanamt, homeins, proptax);
      //Add taxes
      monthlypi = Math.round(getMonthlyPrincAndIntr(totmonthlypay,taxes));
      loanamt = getLoanAmount(monthlypi, interestrate, term);
      let homevalue = affDownPayment + loanamt;
      if (totmonthlypay <= 0 || loanamt <= 0 || homevalue <= 0) {
          return false;
      }
      let affDownPaymentpercent = getDownPmtPercentage(affDownPayment, homevalue);
      let result = {
        'homevalue':  homevalue,
        'monthlypi': monthlypi
      }
      return result;
    }
    /*
     * Calculate Mortgage InterestRate
     */
   function getMortgageInterestRate(rate)
    {
        return rate / (12 * 100);
    }

    /*
     * Calculate TotalMonthlyPayment
    */
   function getTotalMonthlyPayment(income, expense, DTI)
    {
        return income * DTI - expense;
    }
    /*
     * Calculate MonthlyPrincipleand Interest
    */
   function getMonthlyPrincAndIntr(totmonthlypay,taxes)
    {
        return totmonthlypay - taxes;
    }

    /*
     * Calculate LoanAmount
    */
   function getLoanAmount(monthlypi, rate, term)
    {
        return Math.round(monthlypi * (1 - Math.pow(1 + rate, term * -1)) / rate);
    }
    /*
     * Calculate affDownPayment Percentage
    */
   function getDownPmtPercentage(affDownPayment, homevalue) {

        return Math.round(100 * affDownPayment / homevalue);
    }
    /*
     * Calculate Taxamount
    */
   function getTaxAmount(amt, homeins, proptax)
    {
        homeins = homeins/(12 * 100);
        proptax = proptax/(12 * 100);
		let monthlyhomeins = (homeins*amt);
        let monthlyproptax = (proptax*amt);
        return (monthlyhomeins+monthlyproptax);
    }

    function printAffordabilityValues(result)
    {
      $('#affHomeValue .number').html(formatDollars(result.homevalue));
      $('#affMontlyPay .number').html(formatDollars(result.monthlypi));
    }

	// open link by URL fragment and scroll to it
	$(document).ready(() => {
		let fragment = window.location.hash;

		if (typeof fragment !== 'string' || fragment.length === 0) {
			return;
		}

		let $sectionButton = $('a.accordion-item[href="%s"]'.replace('%s', fragment));

		$sectionButton.click();

		$('html, body').animate({
			scrollTop: $sectionButton.offset().top
		}, 750);
	});
 }
