
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Typed from 'typed.js';
import Slider from 'bootstrap-slider';
import refinanceCalculator from './refinanceCalculator.js';
import rentBuyCalculator from './rentBuyCalculator.js';
import affordabilityCalculator from './affordabilityCalculator.js';
import scheduleConsultation from './scheduleConsultation.js';
import listYourHome from './listYourHome.js';
import contactUs from './contactUs.js';
import mobileAppBanner from './mobileAppBanner.js';
import viewport from './viewport.js';
import webinarModal from './webinarModal.js';

$(document).ready(()=> {
  window.viewport = viewport.init();
  $('[data-toggle="popover"]').popover();

  $(document).on('click', e => {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
      //the 'is' for buttons that trigger popups
      //the 'has' for icons within a button that triggers a popup
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
      }
    });
  });

  if (document.getElementById('stateMap')) {
    let stateMap = document.getElementById('stateMap');
    let parent = stateMap.parentNode;
    let src = stateMap.src;

    /*
     * By default the bootstrap.js file adds the X-CSRF-TOKEN header to all ajax requests. This
     * X-CSRF-TOKEN header breaks requests to our cdn, and since the below image is pulled from the cdn,
     * we must temporarily remove the header for ONLY this request and add it back when we are done.
     */
    let csrf = $.ajaxSettings.headers["X-CSRF-TOKEN"];
    delete $.ajaxSettings.headers["X-CSRF-TOKEN"];

    $.ajax({
      url: src,
      success: function (data, status) {
        if ('documentElement' in data) {
          let svg = data.documentElement;
          parent.replaceChild(data.documentElement, stateMap);
          svg.setAttribute('id', 'stateMap');
          svgLoad();
        }
      }
    });

    $.ajaxSettings.headers["X-CSRF-TOKEN"] = csrf;
  }

  function svgLoad(){
    $('.location-point').on('click', x=>{
      x.stopPropagation();
      $('.managers').css('display', 'none')
      $('.office-location').css('display', 'none');
      let $officeLocation = $(`.office-location.${x.currentTarget.id}`);
      $officeLocation.css('display', 'block');
      configureViewport($officeLocation,x);
    });

    $('rect, path.st2').on('click', x=>{
      x.stopPropagation();
      $('.office-location').css('display', 'none');
      $('.managers').css('display', 'none');
      let $managers = $(`.managers.${x.currentTarget.id}`);
      $managers.css('display', 'block');
      configureViewport($managers,x);
    });

    function configureViewport(divElement,x){
      if (viewport.getSize().width < 900) {
        let center = viewport.getCenter();

        divElement.css({
            top: center.y - (divElement.outerHeight() / 2),
            left: center.x - (divElement.outerWidth() / 2)
        });
      } else {
        divElement.css({top: x.pageY, left: x.pageX});

        let quadrant = viewport.getQuadrant(divElement);

        if (quadrant <= 2) {
          divElement.css({left: divElement.offset().left - divElement.outerWidth()});
          // repeat because it may resize itself
          divElement.css({left: x.pageX - divElement.outerWidth()});
        }
        if (quadrant === 2 || quadrant === 3) {
          divElement.css({top: divElement.offset().top - divElement.outerHeight()});
          // repeat because it may resize itself
          divElement.css({top: x.pageY - divElement.outerHeight()});
        }
      }
    }

    $(document).on('click', x=> {
      $('.office-location').css('display', 'none');
      $('.managers').css('display', 'none');
    });
  }
  //media gallery carousel expansion on click code
  $('.owl-carousel.media-gallery .item').on('click',x=> {
    console.log(x.currentTarget);
    let galleryName = x.currentTarget.classList['1'];

    //$(carouselClass).trigger('destroy.owl.carousel');
    $(`.owl-carousel.${galleryName}`).owlCarousel({
      margin:10,
      nav:true,
      navText: ['PREV', 'NEXT'],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          },
          1300: {
            items: 1
          },
          1600: {
            items: 1
          }
      }
    });
    let expansionGallery = $(`.owl-carousel.media-gallery-expand.${galleryName} img`);
    let imageSource = $(x.currentTarget)[0].firstElementChild.src;
    for (let x in expansionGallery) {
      if (expansionGallery.hasOwnProperty(x)) {
        if ($(expansionGallery[x])[0].src === imageSource) {
          $(`.owl-carousel.media-gallery-expand.${galleryName}`).trigger('to.owl.carousel', x)
        }
      }
    }
    $(`.modal.${galleryName}`).modal('show');
  });

  setTimeout(function() {
      //owl-slider js code
        $('.owl-carousel.scholars-rulings').owlCarousel({
          loop: true,
          margin: 10,
          nav: true,
          navText: ['PREV', 'NEXT'],
          autoplay: true,
          autoplayTimeout: 2000,
          responsive:{
              0:{
                items: 1
              },
              600:{
                items: 2
              },
              1200: {
                items: 3
              }
          }
        });

        $('.owl-carousel.channels').owlCarousel({
          loop: true,
          margin:30,
          autoplay: true,
          autoplayTimeout:2000,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:2
              },
              1000:{
                  items:3
              },
              1300: {
                items: 4
              },
              1600: {
                items: 4
              }
          }
        });

        $('.owl-carousel.channels-homepage').owlCarousel({
          loop: true,
          margin: 0,
          autowidth: true,
          autoplay: true,
          autoplayTimeout:2000,
          responsive: {
              0:{
                items:2
              },
              600: {
                items:3
              },
              1000: {
                items:4
              },
              1300: {
                items: 5
              },
              1600: {
                items: 5
              }
          }
        });

        $('.owl-carousel.media-gallery').owlCarousel({
          loop: true,
          margin: 10,
          nav:true,
          navText: ['PREV', 'NEXT'],
          autoplay: true,
          autoplayTimeout:2000,
          responsive:{
              0: {
                items:1
              },
              600:{
                items:2
              },
              1000: {
                items:3
              },
              1300: {
                items: 3
              },
              1600: {
                items: 3
              }
          }
        });
    }, 500);

  //js code for all range sliders
   if (document.getElementById('estimationCalculator')) {
     let affCombinedMonthly = new Slider('#affMonthlyIncome', {
       tooltip: 'always',
       'step': '1000',
       formatter: function(value) {
           return '$' + value.toLocaleString();
       }
     });

     let affTotalMonthly = new Slider('#affMonthlyExpense', {
       tooltip: 'always',
       'step': '1000',
       formatter: function(value) {
         return '$' + value.toLocaleString();
       }
     });

     let affDownPayment = new Slider('#affDownPayment', {
       tooltip: 'always',
       'step': '1000',
       formatter: function(value) {
         return '$' + value.toLocaleString();
       }
     });
     let targetMonthlyRent = new Slider('#targetMonthlyRent', {
       tooltip: 'always',
       'step': '1000',
       formatter: function(value) {
           return '$' + value.toLocaleString();
       }
     });

     let targetHomeValue = new Slider('#targetHomeValue', {
       tooltip: 'always',
       'step': '1000',
       formatter: function(value) {
         return '$' + value.toLocaleString();
       }
     });

     let targetDownPayment = new Slider('#targetDownPayment', {
       tooltip: 'always',
       'step': '1000',
       formatter: function(value) {
         return '$' + value.toLocaleString();
       }
     });

     let stayExpectation = new Slider('#stayExpectation', {
       tooltip: 'always',
       formatter: function(value) {
         return value + 'YR';
       }
     });

     let targetIncomeTax = new Slider('#targetIncomeTax', {
       tooltip: 'always',
       formatter: function(value) {
         return value + '%';
       }
     });

     let outstandingBalance = new Slider('#refinanceAmount', {
       tooltip: 'always',
       'step': '1000',
       formatter: function(value) {
         return '$' + value.toLocaleString();
       }
     });

     let currentMonthlyPayment = new Slider('#currentMonPay', {
       tooltip: 'always',
       'step': '500',
       formatter: function(value) {
         return '$' + value.toLocaleString();
       }
     });

     let remainingMonthlyPayments = new Slider('#remainingMonths', {
       tooltip: 'always',
       formatter: function(value) {
         return  value;
       }
     });
   }
  //when clicked anywhere else on the page, if menu is open, close the menu
  $(document).click(()=> {
    if ($('.drop-item.tier-1').hasClass('active') || $('.drop-item.no-sub').hasClass('active')) {
      toggleDropdown();
    }
  });
  //remove home-video image after video loads
  $('#homeVideo').on("canplay", ()=> {
    let checkForIE = checkIfIE();
    $('#homeVideoImage').remove();
    $('#homeVideo').css("display","block");
      setVideoParentHeight();
      document.getElementsByTagName("BODY")[0].onresize = function() {setVideoParentHeight()};
    //function to set video's parent element height for IE
    function setVideoParentHeight() {
      let height = $(window).height()-100;
      $('.container.home .home-video').css('height', height);
      if (!checkForIE) {
        $('#homeVideo').css('height', height);
      }
    }
    //function to check if the browser is internet explorer
    function checkIfIE() {
      let ua = window.navigator.userAgent;
      let msie = ua.indexOf("MSIE ");
      return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));// If Internet Explorer, return true
    }
  });
  //tab steps js code
  $('.tab-step-item').on('click' , e => {
    e.stopPropagation();
    $('.tab-step-item').removeClass('selected');
    $(e.currentTarget).addClass('selected');
    let stepName = `.tab-step-container .content.${$(e.currentTarget).find('.step-num')[0].id}`;
    $('.tab-step-container .content').removeClass('active');
    $(stepName).addClass('active');
  });

  //header js code
  function toggleDropdown(target){
    $('#headerOne .drop-item.tier-1').removeClass('active');
    $('#headerOne .drop-item.no-sub').removeClass('active');
    if (target) {
      $(target).addClass('active');
    }
  }
  $('#headerOne .drop-item.tier-1').on('click' , e => {
    e.stopPropagation();
    toggleDropdown(e.currentTarget);
  });

  $('#headerOne .drop-item.no-sub').on('click' , e => {
    e.stopPropagation();
    toggleDropdown(e.currentTarget);
  });

  //mobile menu js code
  var mobileMenu = $('.mobile-menu');
  var mobileIcon = $('.mobile-icon');
  $('.mobile-menu-wrapper').click( e=> {
    mobileMenu.addClass('active');
    mobileIcon.addClass('hide');
  });

  $('.close-mobile-icon').click( e=> {
    mobileMenu.removeClass('active');
    mobileIcon.removeClass('hide');
    e.stopPropagation();
  });

  //video player js code
  $('.video').on('click', e => {
    e.stopPropagation();
    let $this = $(e.currentTarget);
    let videoId = $this.data('video-id');

    $this.find('.video-play').remove();
    $this.find('.video-thumbnail').remove();
    let iframe = document.createElement("iframe");
    let embed = "https://www.youtube.com/embed/" + videoId + "?autoplay=1&rel=0";
    iframe.setAttribute("src", embed);
    iframe.setAttribute("width", '1000px');
    iframe.setAttribute("frameborder", "0");
    iframe.setAttribute("allowfullscreen", "1");
    $this.append(iframe);
  });

  //Tab button js code
  $('.tab-buttons .main-button').on('click', e=> {
    e.stopPropagation();
    let activeClass = `.tab-content.${e.currentTarget.name}`;
    $('.tab-buttons .main-button').removeClass('active');
    $(e.currentTarget).addClass('active');
    $('.tab-content').removeClass('active');
    $(activeClass).addClass('active');
  });

  //Homepage typed effect javascript code
  if (document.getElementById('mainTypedText')) {
   var typedTextInitStr = $('#mainTypedText').html().trim();
   $('#mainTypedText').empty();
   var typed = new Typed('#mainTypedText', {
     strings: [
       typedTextInitStr,
       "Over $5.0 Billion Funded",
       "Serving in 29 U.S. States",
       "Celebrating 15 Years",
       "20,000+ Families Served",
       "How can we serve you today?"
     ],
     typeSpeed: 50,
     backSpeed: 20,
     backDelay: 1500,
     loop: true
   });
 }

 //History steps js code
 $('.years-timeline li').on('click', e=> {
   e.stopPropagation();
   let target = e.currentTarget;
   $('.timeline-list li').removeClass('active');
   $(target).addClass('active');
   let year = $(target).find('.text')[0].textContent;
   let activeYearClass =  `.history-timeline  .year.${year}`;
   $('.history-timeline .year').removeClass('active');
   $(activeYearClass).addClass('active');
 });
  //calculators code
    refinanceCalculator();
    rentBuyCalculator();
    affordabilityCalculator();
  //schedule consultation from code
    scheduleConsultation();
 //any view component bindings can go here
    contactUs();
  //Mobile app banner js code
    mobileAppBanner();
  //List your home modal js code
    listYourHome();
  //Webinar modal js code
    webinarModal();
});
