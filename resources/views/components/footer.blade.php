<div class="app-footer">
  <ul class="social-media-icons">
    <li><a href="{{ config('urls.social.facebook') }}" class="media-icon fa fa-facebook-square" target="_blank"></a></li>
    <li><a href="{{ config('urls.social.twitter') }}" class="media-icon fa fa-twitter-square" target="_blank"></a></li>
    <li><a href="{{ config('urls.social.linkedin') }}" class="media-icon fa fa-linkedin-square" target="_blank"></a></li>
    <li><a href="{{ config('urls.social.youtube') }}" class="media-icon fa fa-youtube-square" target="_blank"></a></li>
  </ul>
  <ul class="site-links">
    <li class="site-link"><a href="{{ route('privacySecurity') }}">Privacy and Security</a></li> <span class="slash">/</span>
    <li class="site-link"><a href="{{ route('licensingRegistrations') }}">Licensing and registration</a></li><span class="slash">/</span>
    <li class="site-link"><a href="{{ route('assistanceRepayment') }}">Assistance and Repayment options</a></li>
  </ul>
  <ul class="corporate-links">
    <li class="corporate-link no-border"><img class="eh_logo" src="{{ mix_remote('images/Equal_Housing_Opportunity_PNG.png') }}" alt="Equal Housing"></li>
    <li class="corporate-link no-border"><a href="{{ config('urls.nmls.home') }}" target="_blank"><img src="{{ mix_remote('images/footer_home_logo.png') }}" width="114" height="18" alt="NMLS Consumer Access"></a></li>
    <li class="corporate-link"><a href="{{ config('urls.nmls.guidance') }}" target="_blank">NMLS# 2908</a></li><span class="slash">|</span>
    <li class="corporate-link">&copy; Guidance Residential, LLC {{ date('Y') }}</li><span class="slash">|</span>
    <li class="corporate-link no-border">All Rights Reserved</li>
  </ul>
  <ul class="company-address">
    <li>11107 Sunset Hills Road, Suite 100 &amp; 200, Reston, VA 20190</li><span class="slash">|</span>
    <li class="no-border"><a href="tel:18664843262">1.866.GUIDANCE</a></li>
  </ul>
  <div class="approval-msg">
    Website Approved by the NYS Department of Financial Services
  </div>
</div>
