<!-- Webinar Modal -->
<div class="modal fade" id="webinarModal" tabindex="-1" role="dialog" aria-labelledby="webinarModalLabel" aria-hidden="true">
  <div class="modal-dialog webinar-banner blue-modal" role="document">
    <div class="modal-content full">
      <div class="modal-body">
        <div class="app-circle">
          <div class="fa fa-laptop">
            <span class="fa fa-play"></span>
          </div>
        </div>
        <div class="close-icon golden" data-dismiss="modal">X</div>
        <div class="content">
          <div class="main-heading">
            <h3 id="webinarModalMainText">
              <span class="golden">Before you leave, </span>are you interested in a Free Live Webinar with an Islamic Finance Expert?
            </h3>
            <h3 class="golden">
              Our next Expert will be Speaking Soon!
            </h3>
          </div>
          <div class="timer-widget">
            <h3 class="golden">
              Wednesday, May 2, 2018
            </h3>
            <div class="countdown">
              <div class="time-spot">
                <div id="webBannerDays" class="time-num"></div>
                <div class="timer-title">
                  DAYS
                </div>
              </div>
              <span class="time-tick">:</span>
              <div class="time-spot">
                <div id="webBannerHours" class="time-num"></div>
                <div class="timer-title">
                  HOURS
                </div>
              </div>
              <span class="time-tick">:</span>
              <div class="time-spot">
                <div id="webBannerMinutes" class="time-num"></div>
                <div class="timer-title">
                  MINUTES
                </div>
              </div>
            </div>
          </div>
          <a type="submit" href="http://www.guidanceresidentialpages.com/guidance-residential-guidance-college-webinar?referral={{ $refLink or '#'}}" class="main-button gold" target="_blank">Learn More &amp; Reserve my spot</a>
        </div>
      </div>
    </div>
  </div>
</div>
