<!-- Contact-Us Modal -->
<div class="modal fade" id="contactUsModal" tabindex="-1" role="dialog" aria-labelledby="contactUsModalLabel" aria-hidden="true">
  <div class="modal-dialog contact-us" role="document">
    <div class="modal-content full">
      <div class="modal-body">
        <div class="content contactUs">
          <div class="info-content">
            <div class="main-heading left">
              <h1 class="title">
                Customer Service
              </h1>
              <hr class="seperate-line sub"></hr>
              <div class="desc-text">
                <h3 class="title three bold">For more information on a pending application:</h3>
                <div class="steps">
                  <div class="step">
                    <a class="app-circle" href="tel:1-866-484-3262">
                      <span class="fa fa-phone"></span>
                    </a>
                    <a class="text" href="tel:1-866-484-3262">1.866.484.3262</a>
                  </div>
                  <div class="step">
                    <div class="app-circle">
                      <span class="fa fa-calendar"></span>
                    </div>
                    <span class="text">Monday - Friday, 9:00 AM - 6:00 PM EST</span>
                  </div>
                </div>
              </div>
              <div class="desc-text">
                <h3 class="title three bold">For account information for an existing contract:</h3>
                <div class="steps">
                  <div class="step">
                    <a class="app-circle" href="tel:1-877-402-0537">
                      <span class="fa fa-phone"></span>
                    </a>
                    <a class="text" href="tel:1-877-402-0537">1.877.402.0537</a>
                  </div>
                  <div class="step">
                    <div class="app-circle">
                      <span class="fa fa-calendar"></span>
                    </div>
                    <span class="text">Monday - Friday, 9:00 AM - 6:00 PM EST</span>
                  </div>
                </div>
                <div class="steps">
                  <div class="step">
                    <div class="app-circle">
                      <span class="fa fa-building"></span>
                    </div>
                    <div class="text">
                      Guidance Residential, LLC <br>
                      P.O. Box 20005 <br>
                      Owensboro, KY 42304-0005
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-content">
            <div class="main-heading left">
              <h1 class="title">
                Email US
              </h1>
              <hr class="seperate-line sub"></hr>
            </div>
            <div class="desc-text contactUs-error">
              An error occured while processing your request. Please try again later*
            </div>
            <form action="{{ route('contact-us.store') }}" method="POST" autocomplete="on" id="contactUsForm">
              {{ csrf_field() }}
              <select required name="reason">
                <option value="">Select a Reason for Contacting Us</option>
                <option value="Status of Application">Status of Application</option>
                <option value="Monthly Payment Inquiry">Monthly Payment Inquiry</option>
                <option value="Feedback on Our Service">Feedback on Our Service</option>
                <option value="Media Inquiry">Media Inquiry</option>
                <option value="Other">Other</option>
              </select>
              <input required type="text" name="name" placeholder="Name*" maxlength="50"/>
              <input required type="email" name="email" placeholder="Email Address*" maxlength="256"/>
              <input required type="tel" name="phone" placeholder="Phone Number*" pattern="(\([0-9]{3}\)|[0-9]{3})[- ]?[0-9]{3}[- ]?[0-9]{4}"/>
              <textarea name="comments" rows="4" cols="50" maxlength="300" placeholder="Enter Your Comments..."></textarea>
              <button type="submit" class="main-button gold">Submit</button>
            </form>
          </div>
          <div id="contactUsSuccessMessage">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-check-circle-o"></span>
              </div>
              <h3 class="title three bold golden">
                Thank you for submitting your information. We will contact you soon.
              </h3>
            </div>
          </div>
        </div>
      </div>
      <div class="hidden modal-spinner"><span class="spinner"></span></div>
    </div>
  </div>
</div>
