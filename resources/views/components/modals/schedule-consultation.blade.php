<!-- Schedule-consultation Modal -->
<div class="modal fade" id="scheduleConsultationModal" tabindex="-1" role="dialog" aria-labelledby="scheduleConsultationModalLabel" aria-hidden="true">
  <div class="modal-dialog schedule-consultation blue-modal" role="document">
    <div class="modal-content full">
      <div class="modal-body">
        <div class="content">
          <div class="main-heading">
            <h3 class="title three bold golden">
              Schedule a free consultation
            </h3>
            <hr class="seperate-line"></hr>
          </div>
          <div class="desc-text">
            Fill out the form to schedule a free financing consultation with one of our expert licensed Account Executives
          </div>
          <div class="desc-text schedule-error">
            An error occured while processing your request. Please try again later.
          </div>
          <form action="{{ route('free-consultation.store') }}" method="POST" autocomplete="on" id="scheduleConsultationForm">
            {{ csrf_field() }}
            <input required type="text" name="first_name" placeholder="First Name*" maxlength="50"/>
            <input required type="text" name="last_name" placeholder="Last name*" maxlength="50"/>
            <input required type="email" name="email" placeholder="Email Address*" maxlength="256"/>
            <input required type="tel" name="phone" placeholder="Phone Number*" pattern="(\([0-9]{3}\)|[0-9]{3})[- ]?[0-9]{3}[- ]?[0-9]{4}" maxlength="15"/>
            <select required name="state">
              <option value="">-Select State-</option>
              <option value="AL">Alabama</option>
              <option value="AK">Alaska</option>
              <option value="AZ">Arizona</option>
              <option value="AR">Arkansas</option>
              <option value="CA">California</option>
              <option value="CO">Colorado</option>
              <option value="CT">Connecticut</option>
              <option value="DE">Delaware</option>
              <option value="DC">District Of Columbia</option>
              <option value="FL">Florida</option>
              <option value="GA">Georgia</option>
              <option value="HI">Hawaii</option>
              <option value="ID">Idaho</option>
              <option value="IL">Illinois</option>
              <option value="IN">Indiana</option>
              <option value="IA">Iowa</option>
              <option value="KS">Kansas</option>
              <option value="KY">Kentucky</option>
              <option value="LA">Louisiana</option>
              <option value="ME">Maine</option>
              <option value="MD">Maryland</option>
              <option value="MA">Massachusetts</option>
              <option value="MI">Michigan</option>
              <option value="MN">Minnesota</option>
              <option value="MS">Mississippi</option>
              <option value="MO">Missouri</option>
              <option value="MT">Montana</option>
              <option value="NE">Nebraska</option>
              <option value="NV">Nevada</option>
              <option value="NH">New Hampshire</option>
              <option value="NJ">New Jersey</option>
              <option value="NM">New Mexico</option>
              <option value="NY">New York</option>
              <option value="NC">North Carolina</option>
              <option value="ND">North Dakota</option>
              <option value="OH">Ohio</option>
              <option value="OK">Oklahoma</option>
              <option value="OR">Oregon</option>
              <option value="PA">Pennsylvania</option>
              <option value="RI">Rhode Island</option>
              <option value="SC">South Carolina</option>
              <option value="SD">South Dakota</option>
              <option value="TN">Tennessee</option>
              <option value="TX">Texas</option>
              <option value="UT">Utah</option>
              <option value="VT">Vermont</option>
              <option value="VA">Virginia</option>
              <option value="WA">Washington</option>
              <option value="WV">West Virginia</option>
              <option value="WI">Wisconsin</option>
              <option value="WY">Wyoming</option>
            </select>
            <button type="submit" class="main-button gold flex-button">Schedule an appointment</button>
          </form>
          <p class="desc-text">
            We dislike spam as much as you do.<br/>
            Your information is safe with us.
          </p>
        </div>
        <div id="scheduleSuccessMessage">
          <div class="main-heading">
            <div class="app-circle">
              <span class="fa fa-check-circle-o"></span>
              @if (config('app.env') == 'production')
              <span class="hidden pixel" data-src="https://5212.xg4ken.com/media/redir.php?track=1&token=94d50dc9-7924-41b7-998c-c7700891e97b&type=free_consultation&val=0.0&orderId=&promoCode=&valueCurrency=USD&GCID=&kw=&product="></span>
              @endif
            </div>
            <h3 class="title three bold golden">
              Thank you for submitting your information for a free consultation. An Account Executive will contact you soon.
            </h3>
          </div>
        </div>
      </div>
      <div class="hidden modal-spinner"><span class="spinner"></span></div>
    </div>
  </div>
</div>