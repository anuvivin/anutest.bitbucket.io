<div class="container-fluid">
  <div class="mobile-app-banner" style="display: none">
    <span class="fa fa-close"></span>
    <a class="title-link">
      <div class="title three">
        Download our <span class="lowercase">gi</span>OS app
      </div>
    </a>
    <div class="logo apple-logo">
      <a href="{{ config('urls.apps.apple') }}"><img src="{{ mix_remote('images/appDownload/appleLogo.svg') }}" alt="Apple store Image" class='app-store-img'></a>
    </div>
    <div class="logo android-logo">
      <a href="{{ config('urls.apps.android') }}"><img src="{{ mix_remote('images/appDownload/google_play.png') }}" alt="Google Play Image" class='app-store-img android'></a>
    </div>
  </div>
  <div class="header-main" id="header">
    <div class="mobile-menu-wrapper">
      <span class="mobile-icon fa fa-navicon"></span>
      <div class="mobile-menu" id="mobileMenu">
        <div class="close-mobile">
            <span class="close-mobile-icon fa fa-close"></span>
          </div>
        <div class="list-group">
          <a href="#exploreCollapse" class="list-group-item  tier-1" data-toggle="collapse">
            <span class="fa fa-bars"></span>Explore guidance <span class="fa open-close-icon"></span>
          </a>
          <div class="list-group collapse" id="exploreCollapse">

            <a href="#aboutCollapse" class="list-group-item tier-2" data-toggle="collapse">
              About Us <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="aboutCollapse">
              <a href="{{ route('ceoMessage') }}" class="list-group-item tier-3">Message from the CEO</a>
              <a href="{{ route('ourHistory') }}" class="list-group-item tier-3">Our History</a>
              <a href="{{ route('ourCompany') }}" class="list-group-item tier-3">What We Do</a>
              <a href="{{ route('ourTeam') }}" class="list-group-item tier-3">Our Team</a>
              <a href="{{ route('shariahBoard') }}" class="list-group-item tier-3">Shariah Board</a>
              <a href="{{ route('operationalStates') }}" class="list-group-item tier-3">Operational States</a>
            </div>

            <a href="#programCollapse" class="list-group-item tier-2" data-toggle="collapse">
              Our Program <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="programCollapse">
              <a href="{{ route('whyGuidance') }}" class="list-group-item tier-3">Why guidance</a>
              <a href="{{ route('coOwnership') }}" class="list-group-item tier-3">Co-ownership Model</a>
              <a href="{{ route('scholarsRulings') }}" class="list-group-item tier-3">Scholar's Rulings(Fatwa)</a>
              <a href="{{ route('endorsements') }}" class="list-group-item tier-3">Endorsements</a>
              <a href="{{ route('testimonials') }}" class="list-group-item tier-3">Testimonials</a>
              <a href="{{ route('guidanceRealty') }}" class="list-group-item tier-3">Realty Program</a>
            </div>

            <a href="#newsCollapse" class="list-group-item tier-2" data-toggle="collapse">
              News and Events <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="newsCollapse">
              <a href="{{ config('urls.blog.events') }}" class="list-group-item tier-3">Upcoming Events</a>
              <a href="{{ route('guidanceInNews') }}" class="list-group-item tier-3">Guidance in the News</a>
              <a href="{{ route('awards') }}" class="list-group-item tier-3">Awards and Recognitions</a>
              <a href="{{ route('givingBack') }}" class="list-group-item tier-3">Corporate Philanthropy</a>
              <a href="{{ route('pressReleases') }}" class="list-group-item tier-3">Press Releases</a>
              <a href="{{ route('mediaGallery') }}" class="list-group-item tier-3">Photo Gallery</a>
            </div>

            <a href="#knowledgeCollapse" class="list-group-item tier-2" data-toggle="collapse">
              Knowledge Center <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="knowledgeCollapse">
              <a href="{{ route('videoLibrary') }}" class="list-group-item tier-3">Video Library</a>
              <a href="{{ route('whitePapers') }}" class="list-group-item tier-3">White Papers</a>
              <a href="{{ route('webinars') }}" class="list-group-item tier-3">Webinars</a>
              <a href="{{ route('eBooks') }}" class="list-group-item tier-3">E-Books</a>
              <a href="{{ route('podcasts') }}" class="list-group-item tier-3">Podcasts</a>
              <a href="{{ config('urls.blog.articles') }}" class="list-group-item tier-3">Insights &amp; Articles</a>
            </div>
          </div>
          <!-- second menu main item -->
          <a href="#buyingCollapse" class="list-group-item  tier-1" data-toggle="collapse">
            <span class="fa fa-home"></span>Buying <span class="fa open-close-icon"></span>
          </a>
          <div class="list-group collapse" id="buyingCollapse">

            <a href="#homeBuyCollapse" class="list-group-item tier-2" data-toggle="collapse">
              Buying Reasons <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="homeBuyCollapse">
              <a href="{{ route('homeBuyingReasons') }}#firstTimeHomeBuyer" class="list-group-item tier-3">First Time Homebuyer</a>
              <a href="{{ route('homeBuyingReasons') }}#buyToRentOut" class="list-group-item tier-3">Investment Property</a>
              <a href="{{ route('homeBuyingReasons') }}#vacationHomes" class="list-group-item tier-3">Vacation Homes</a>
            </div>

            <a href="#howWorksCollapse" class="list-group-item tier-2" data-toggle="collapse">
              How It Works <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="howWorksCollapse">
              <a href="{{ route('homeBuyingProcess') }}" class="list-group-item tier-3">Home Buying Process</a>
              <a href="{{ route('financingOptions') }}" class="list-group-item tier-3">Financing Options</a>
              <a href="{{ route('buyingApplicationChecklist') }}" class="list-group-item tier-3">Application Checklist</a>
            </div>

            <a href="#homeResourceCollapse" class="list-group-item tier-2" data-toggle="collapse">
              Home Buying Resources <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="homeResourceCollapse">
              <a href="{{ route('purchaseRates') }}" class="list-group-item tier-3">Competitive Rates</a>
              <a href="{{ route('estimationCalculators') }}" class="list-group-item tier-3">Estimation Calculators</a>
              <a href="{{ route('homeBuyingQuestions') }}" class="list-group-item tier-3">Home Buying Questions</a>
            </div>

            <a href="#getStartedCollapse" class="list-group-item tier-2" data-toggle="collapse">
            Get Started<span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="getStartedCollapse">
              <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Header_Buying_GetStarted_Feb2018" target="_blank" class="list-group-item tier-3">Pre-qualify Online</a>
              <a href="{{ route('account-executive.index') }}" class="list-group-item tier-3">Connect with Account Executive</a>
              <a href="{{ config('urls.grealty.find-an-agent') }}" class="list-group-item tier-3">Find a Real Estate Agent</a>
            </div>
          </div>
          <!-- Third menu item -->
          <a href="#refinanceCollapse" class="list-group-item  tier-1" data-toggle="collapse">
            <span class="fa fa-home"></span>Refinancing <span class="fa open-close-icon"></span>
          </a>
          <div class="list-group collapse" id="refinanceCollapse">

            <a href="#refReasonsCollapse" class="list-group-item tier-2" data-toggle="collapse">
            Refinancing Reasons <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="refReasonsCollapse">
              <a href="{{ route('refinancingReasons') }}#ribaHomeOwnership" class="list-group-item tier-3">Riba-Free Homeownership</a>
              <a href="{{ route('refinancingReasons') }}#lowerMonthlyPayment" class="list-group-item tier-3">Lower Monthly Payment</a>
              <a href="{{ route('refinancingReasons') }}#reducePayOffTerm" class="list-group-item tier-3">Reduce Payoff Term</a>
            </div>

            <a href="#howWorksCollapseRef" class="list-group-item tier-2" data-toggle="collapse">
              How It Works <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="howWorksCollapseRef">
              <a href="{{ route('refinancingProcess') }}" class="list-group-item tier-3">Refinancing Process</a>
              <a href="{{ route('refinancingOptions') }}" class="list-group-item tier-3">Refinancing Options</a>
              <a href="{{ route('refinancingApplicationChecklist') }}" class="list-group-item tier-3">Application Checklist</a>
            </div>

            <a href="#refResourceCollapse" class="list-group-item tier-2" data-toggle="collapse">
              Refinancing Resources <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="refResourceCollapse">
              <a href="{{ route('refinanceRates') }}" class="list-group-item tier-3">Competitive Rates</a>
              <a href="{{ route('estimationCalculators') }}" class="list-group-item tier-3">Estimation Calculators</a>
              <a href="{{ route('refinancingQuestions') }}" class="list-group-item tier-3">Refinancing Questions</a>
            </div>

            <a href="#getStartedCollapseRef" class="list-group-item tier-2" data-toggle="collapse">
            Get Started<span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="getStartedCollapseRef">
              <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Mobile_Menu_Refinancing_GetStarted_Feb2018" target="_blank" class="list-group-item tier-3">Pre-qualify Online</a>
              <a href="{{ route('account-executive.index') }}" class="list-group-item tier-3">Connect With Account Executive</a>
            </div>
          </div>
          <!-- Forth menu item -->
          <a href="#guidRealtyCollapse" class="list-group-item  tier-1" data-toggle="collapse">
            <span class="fa fa-home"></span>Realty Program<span class="fa open-close-icon"></span>
          </a>
          <div class="list-group collapse" id="guidRealtyCollapse">

            <a href="#realEstateCollapse" class="list-group-item tier-2" data-toggle="collapse">
            Are You An Agent?<span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="realEstateCollapse">
              <a href="{{ route('getPreQualifiedRefferals') }}" class="list-group-item tier-3">Get Pre-Qualified Referrals</a>
              <a href="{{ route('getPreQualifiedRefferals') }}#how-it-works" class="list-group-item tier-3">How it Works</a>
              <a href="{{ route('signUpGetStarted') }}" class="list-group-item tier-3">Sign Up and Get Started</a>
            </div>

            <a href="#homeBuyersCollapse" class="list-group-item tier-2" data-toggle="collapse">
              Are You A Homebuyer? <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="homeBuyersCollapse">
              <a href="{{ config('urls.grealty.search') }}" class="list-group-item tier-3">Find Your Home</a>
              <a href="{{ config('urls.grealty.find-an-agent') }}" class="list-group-item tier-3">Find a Real Estate Agent</a>
              <a href="{{ route('guidanceRealty') }}" class="list-group-item tier-3">Save at Closing</a>
            </div>

            <a href="#homeSellersCollapse" class="list-group-item tier-2" data-toggle="collapse">
              Are You A Home Seller? <span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="homeSellersCollapse">
              <a href="{{ config('urls.grealty.find-a-platinum-agent') }}" class="list-group-item tier-3">Find a Real Estate Agent</a>
              <a data-toggle="modal" data-target="#listYourHomeModal" class="list-group-item tier-3">List Your Home</a>
              <a href="{{ route('sellWithGuidance') }}" class="list-group-item tier-3">Save When Selling</a>
            </div>
          </div>
          <!-- Fifth Menu item -->
          <a href="#contactUsCollapse" class="list-group-item  tier-1" data-toggle="collapse">
            <span class="fa fa-home"></span>Contact Us<span class="fa open-close-icon"></span>
          </a>
          <div class="list-group collapse" id="contactUsCollapse">
            <a data-toggle="modal" data-target="#contactUsModal" class="list-group-item tier-3">Call Us: 1-866-GUIDANCE</a>
            <a href="{{ route('appDownload') }}" class="list-group-item tier-3">Download Guidance App</a>
            <a class="list-group-item tier-3" data-toggle="modal" data-target="#scheduleConsultationModal">Schedule Free Consultation</a>
            <a href="{{ route('operationalStates') }}" class="list-group-item tier-3">Visit a Local Office</a>
            <a href="{{ route('careers') }}" class="list-group-item tier-3">Join Our Team</a>
            <a data-toggle="modal" data-target="#contactUsModal" class="list-group-item tier-3">Email Us</a>
          </div>

          <!-- Sixth Menu item -->
          <a href="#myAccCollapse" class="list-group-item  tier-1" data-toggle="collapse">
            <span class="fa fa-lock"></span>My Account<span class="fa open-close-icon"></span>
          </a>
          <div class="list-group collapse" id="myAccCollapse">
            <a href="#returningUsers" class="list-group-item tier-2" data-toggle="collapse">
              Applying for Financing<span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="returningUsers">
              <a href="{{ config('urls.customer-portal.login') }}" target="_blank" class="list-group-item tier-3">View Application Status</a>
              <a href="{{ config('urls.customer-portal.login') }}" target="_blank" class="list-group-item tier-3">Upload documents</a>
              <a href="{{ config('urls.customer-portal.login') }}" target="_blank" class="list-group-item tier-3">Manage Application status</a>
            </div>

            <a href="#existingCustomers" class="list-group-item tier-2" data-toggle="collapse">
              Existing Customers<span class="fa open-close-icon"></span>
            </a>
            <div class="list-group collapse" id="existingCustomers">
              <a href="{{ config('urls.customer-care-net.login') }}" target="_blank" class="list-group-item tier-3">Make Online Payment</a>
              <a href="{{ config('urls.customer-care-net.login') }}" target="_blank" class="list-group-item tier-3">View Account Statements</a>
              <a href="{{ config('urls.customer-care-net.login') }}" target="_blank" class="list-group-item tier-3">Update Billing Information</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="company-logo">
      <a href="{{ route('home') }}">
        <img src="{{ mix_remote('images/GR_LOGO.png') }}" alt="Logo">
      </a>
    </div>
    <ul class="dropdown">
      <li class="drop-item tier-1">
        <a href="#">Explore Guidance<span class="triangle-up"></span></a>
        <div class="dropdown-sub">
          <div class="sub-menu">
            <div class="menu-column">
              <a class="heading bold">About Us</a>
              <a href="{{ route('ceoMessage') }}" class="list-group-item">Message from the CEO</a>
              <a href="{{ route('ourHistory') }}" class="list-group-item">Our History</a>
              <a href="{{ route('ourCompany') }}" class="list-group-item">What We Do</a>
              <a href="{{ route('ourTeam') }}" class="list-group-item">Our Team</a>
              <a href="{{ route('shariahBoard') }}" class="list-group-item">Shariah Board</a>
              <a href="{{ route('operationalStates') }}" class="list-group-item">Operational States</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">Our Program</a>
              <a href="{{ route('whyGuidance') }}" class="list-group-item">Why Guidance</a>
              <a href="{{ route('coOwnership') }}" class="list-group-item">Co-ownership Model</a>
              <a href="{{ route('scholarsRulings') }}" class="list-group-item">Scholar's Rulings (Fatwa)</a>
              <a href="{{ route('endorsements') }}" class="list-group-item">Endorsements</a>
              <a href="{{ route('testimonials') }}" class="list-group-item">Testimonials</a>
              <a href="{{ route('guidanceRealty') }}" class="list-group-item">Realty Program</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">News And Events</a>
              <a href="{{ config('urls.blog.events') }}" class="list-group-item">Upcoming Events</a>
              <a href="{{ route('guidanceInNews') }}" class="list-group-item">Guidance in the News</a>
              <a href="{{ route('awards') }}" class="list-group-item">Awards and Recognitions</a>
              <a href="{{ route('givingBack') }}" class="list-group-item">Corporate Philanthropy</a>
              <a href="{{ route('pressReleases') }}" class="list-group-item">Press Releases</a>
              <a href="{{ route('mediaGallery') }}" class="list-group-item">Photo Gallery</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">Knowledge Center</a>
              <a href="{{ route('videoLibrary') }}" class="list-group-item">Video Library</a>
              <a href="{{ route('whitePapers') }}" class="list-group-item">White Papers</a>
              <a href="{{ route('webinars') }}" class="list-group-item">Webinars</a>
              <a href="{{ route('eBooks') }}" class="list-group-item">E-Books</a>
              <a href="{{ route('podcasts') }}" class="list-group-item">Podcasts</a>
              <a href="{{ config('urls.blog.articles') }}" class="list-group-item">Insights &amp; Articles</a>
            </div>
          </div>
          <div class="quick-links">
            <a class="heading bold">Quick Links:</a>
            <a href="{{ route('purchaseRates') }}" class="list-group-item">Current Rates</a>
            <a href="{{ config('urls.customer-portal.prequalify') }}" target="_blank" class="list-group-item">Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect with Account Executive</a>
            <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal">1.866.GUIDANCE</a>
          </div>
        </div>
      </li>
      <li class="drop-item tier-1">
        <a href="#">Buying<span class="triangle-up"></span></a>
        <div class="dropdown-sub">
          <div class="sub-menu">
            <div class="menu-column">
              <a class="heading bold">Buying Reasons</a>
              <a href="{{ route('homeBuyingReasons') }}#firstTimeHomeBuyer" class="list-group-item">First Time Homebuyer</a>
              <a href="{{ route('homeBuyingReasons') }}#buyToRentOut" class="list-group-item">Investment Property</a>
              <a href="{{ route('homeBuyingReasons') }}#vacationHomes" class="list-group-item">Vacation Homes</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">How It Works</a>
              <a href="{{ route('homeBuyingProcess') }}" class="list-group-item">Home Buying Process</a>
              <a href="{{ route('financingOptions') }}" class="list-group-item">Financing Options</a>
              <a href="{{ route('buyingApplicationChecklist') }}" class="list-group-item">Application Checklist</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">Buying Resources</a>
              <a href="{{ route('purchaseRates') }}" class="list-group-item">Competitive Rates</a>
              <a href="{{ route('estimationCalculators') }}" class="list-group-item">Estimation Calculators</a>
              <a href="{{ route('homeBuyingQuestions') }}" class="list-group-item">Home Buying Questions</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">Get Started</a>
              <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Mobile_Menu_Buying_GetStarted_Feb2018" target="_blank" class="list-group-item">Pre-Qualify Online</a>
              <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect with Account Executive</a>
              <a href="{{ config('urls.grealty.find-an-agent') }}" class="list-group-item">Find a Real Estate Agent</a>
            </div>
          </div>
          <div class="quick-links">
            <a class="heading bold">Quick Links:</a>
            <a href="{{ route('purchaseRates') }}" class="list-group-item">Current Rates</a>
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Header_Buying_BottomBar_Feb2018" target="_blank" class="list-group-item">Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect with Account Executive</a>
            <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal">1.866.GUIDANCE</a>
          </div>
        </div>
      </li>
      <li class="drop-item tier-1">
        <a href="#">Refinancing<span class="triangle-up"></span></a>
        <div class="dropdown-sub">
            <div class="sub-menu">
              <div class="menu-column">
                <a class="heading bold">Refinancing Reasons</a>
                <a href="{{ route('refinancingReasons') }}#ribaHomeOwnership" class="list-group-item">Riba-Free Homeownership</a>
                <a href="{{ route('refinancingReasons') }}#lowerMonthlyPayment" class="list-group-item">Lower Monthly Payment</a>
                <a href="{{ route('refinancingReasons') }}#reducePayOffTerm" class="list-group-item">Reduce Payoff Term</a>
              </div>
              <div class="menu-column">
                <a class="heading bold">How It Works</a>
                <a href="{{ route('refinancingProcess') }}" class="list-group-item">Refinancing Process</a>
                <a href="{{ route('refinancingOptions') }}" class="list-group-item">Refinancing Options</a>
                <a href="{{ route('refinancingApplicationChecklist') }}" class="list-group-item">Application Checklist</a>
              </div>
              <div class="menu-column">
                <a class="heading bold">Refinancing Resources</a>
                <a href="{{ route('refinanceRates') }}" class="list-group-item">Competitive Rates</a>
                <a href="{{ route('estimationCalculators') }}" class="list-group-item">Estimation Calculators</a>
                <a href="{{ route('refinancingQuestions') }}" class="list-group-item">Refinancing Questions</a>
              </div>
              <div class="menu-column">
                <a class="heading bold">Get Started</a>
                <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Header_Refinancing_GetStarted_Feb2018" target="_blank" class="list-group-item">Pre-Qualify Online</a>
                <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect With Account Executive</a>
              </div>
            </div>
            <div class="quick-links">
              <a class="heading bold">Quick Links:</a>
            <a href="{{ route('refinanceRates') }}" class="list-group-item">Current Rates</a>
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Header_Refinancing_BottomBar_Feb2018" target="_blank" class="list-group-item">Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect with Account Executive</a>
            <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal">1.866.GUIDANCE</a>
            </div>
        </div>
      </li>
      <li class="drop-item tier-1">
        <a href="#">Realty Program<span class="triangle-up"></span></a>
        <div class="dropdown-sub">
          <div class="sub-menu">
            <div class="menu-column">

            </div>
            <div class="menu-column">
              <a class="heading bold">Are You An Agent?</a>
              <a href="{{ route('getPreQualifiedRefferals') }}" class="list-group-item">Get Pre-Qualified Referrals</a>
              <a href="{{ route('getPreQualifiedRefferals') }}#how-it-works" class="list-group-item">How It Works</a>
              <a href="{{ route('signUpGetStarted') }}" class="list-group-item">Sign Up and Get Started</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">Are You A Homebuyer?</a>
              <a href="{{ config('urls.grealty.search') }}" class="list-group-item">Find Your Home</a>
              <a href="{{ config('urls.grealty.find-an-agent') }}" class="list-group-item">Find a Real Estate Agent</a>
              <a href="{{ route('guidanceRealty') }}" class="list-group-item">Save at Closing</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">Are You A Home Seller?</a>
              <a href="{{ config('urls.grealty.find-a-platinum-agent') }}" class="list-group-item">Find a Real Estate Agent</a>
              <a data-toggle="modal" data-target="#listYourHomeModal" class="list-group-item">List Your Home</a>
              <a href="{{ route('sellWithGuidance') }}" class="list-group-item">Save When Selling</a>
            </div>
          </div>
          <div class="quick-links">
            <a class="heading bold">Quick Links:</a>
            <a href="{{ route('purchaseRates') }}" class="list-group-item">Current Rates</a>
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Header_RealtyProgram_BottomBar_Feb2018" target="_blank" class="list-group-item">Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect with Account Executive</a>
            <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal">1.866.GUIDANCE</a>
          </div>
        </div>
      </li>
      <li class="drop-item no-sub">
        <a href="#">Contact Us</a>
        <div class="dropdown-sub">
          <div class="sub-menu">
            <div class="menu-column">
              <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal"><span class="fa fa-phone"></span>Call Us: 1.866.GUIDANCE</a>
              <a href="{{ route('operationalStates') }}" class="list-group-item"><span class="fa fa-building-o"></span>Visit a Local Office</a>
            </div>
            <div class="menu-column">
              <a href="{{ route('appDownload') }}" class="list-group-item"><span class="fa fa-mobile"></span>Download Our App</a>
              <a href="{{ route('careers') }}" class="list-group-item"><span class="fa fa-users"></span>Join our Team</a>
            </div>
            <div class="menu-column">
              <a class="list-group-item" data-toggle="modal" data-target="#scheduleConsultationModal"><span class="fa fa-user-circle"></span>Schedule Free Consultation</a>
              <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal"><span class="fa fa-envelope-o"></span>Email Us</a>
            </div>
          </div>
          <div class="quick-links">
            <a class="heading bold">Quick Links:</a>
            <a href="{{ route('purchaseRates') }}" class="list-group-item">Current Rates</a>
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Header_ContactUs_BottomBar_Feb2018" target="_blank" class="list-group-item">Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect with Account Executive</a>
            <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal">1.866.GUIDANCE</a>
          </div>
        </div>
      </li>
      <li class="drop-item no-sub">
        <a href="#">My Account</a>
        <div class="dropdown-sub">
          <div class="sub-menu">
            <div class="menu-column">
              <a class="heading bold">Applying for Financing</a>
              <a href="{{ config('urls.customer-portal.login') }}" target="_blank" class="list-group-item">View Application Status</a>
              <a href="{{ config('urls.customer-portal.login') }}" target="_blank" class="list-group-item">Upload Documents</a>
              <a href="{{ config('urls.customer-portal.login') }}" target="_blank" class="list-group-item">Manage Application Status</a>
            </div>
            <div class="menu-column">
              <a class="heading bold">Existing Customers</a>
              <a href="{{ config('urls.customer-care-net.login') }}" target="_blank" class="list-group-item">Make Online Payment</a>
              <a href="{{ config('urls.customer-care-net.login') }}" target="_blank" class="list-group-item">View Account Statements</a>
              <a href="{{ config('urls.customer-care-net.login') }}" target="_blank" class="list-group-item">Update Billing Information</a>
            </div>
          </div>
          <div class="quick-links">
            <a class="heading bold">Quick Links:</a>
            <a href="{{ route('purchaseRates') }}" class="list-group-item">Current Rates</a>
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Header_MyAccount_BottomBar_Feb2018" target="_blank" class="list-group-item">Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="list-group-item">Connect with Account Executive</a>
            <a class="list-group-item" data-toggle="modal" data-target="#contactUsModal">1.866.GUIDANCE</a>
          </div>
        </div>
      </li>
    </ul>
    <div class="bismillah-logo" data-toggle="modal" data-target="#contactUsModal">
      <img src="{{ mix_remote('images/bismillah.png') }}" alt="bismillah-logo">
    </div>

    @include('components.modals.contact-us')

    @include('components.modals.schedule-consultation')

    @include('components.modals.list-your-home')
  </div>
</div>
