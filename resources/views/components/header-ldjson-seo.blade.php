<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "FinancialService",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "11107 Sunset Hills Road, Suite 100 & 200",
    "addressLocality": "Reston",
    "addressRegion": "VA",
    "postalCode": "20190"
  },
  "name": "Guidance Residential",
  "image": "{{ mix_remote('images/GR_LOGO.png') }}",
  "description": "We are here to guide you through the homebuying process. Guidance Residential offers unique benefits that distinguish it from other home financing providers. These benefits provide a transparent, consumer friendly home financing option which is consistent with the Muslim faith's strict prohibition on riba (usury).",
  "url": "{{ config('app.url') }}",
  "telephone":"1.866.GUIDANCE",
  "priceRange":"$$$",
  "sameAs":
    [
      "https://www.facebook.com/GuidanceResidential/",
      "https://twitter.com/GuidanceRes",
      "https://www.linkedin.com/company/guidance-residential",
      "https://www.youtube.com/user/GuidanceMarketing"
    ]
}
</script>
