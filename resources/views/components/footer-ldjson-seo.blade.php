<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WPFooter",
    "copyrightYear" : "{{ date('Y') }}",
    "copyrightHolder": "Guidance Residential, LLC"
  }
</script>
