<!DOCTYPE html>
<html>
  <head>
    <meta content="origin-when-cross-origin" name="referrer">
    <meta name="format-detection" content="telephone=no">
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Guidance Residential - @yield('title', 'Unnamed Page')</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{ mix_remote('images/apple-touch-icon-60x60.jpg') }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ mix_remote('images/apple-touch-icon-76x76.jpg') }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ mix_remote('images/apple-touch-icon-120x120.jpg') }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ mix_remote('images/apple-touch-icon-152x152.jpg') }}" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ mix_remote('favicon.ico') }}">
    <link rel="stylesheet" type="text/css" href="{{ mix_remote('css/app.css') }}"/>
    <script type="text/javascript" src="{{ mix_remote('js/manifest.js') }}"></script>
    <script type="text/javascript" src="{{ mix_remote('js/vendor.js') }}"></script>
    <script type="text/javascript" src="{{ mix_remote('js/app.js') }}"></script>
    @yield('head')
    @if (App::environment('production'))
        @include('components.header-tracking-codes')
    @endif
  </head>
  <body>
    <header>
      @include('components.header')
      @include('components.header-ldjson-seo')
    </header>
    <main>
      @yield('content')
      @yield('ldjson-seo')
      @yield('mainFooter')
    </main>
    <footer>
    @include('components.footer')
    </footer>
    @yield('end')
    @include('components.footer-ldjson-seo')
    @if (App::environment('production'))
        @include('components.footer-tracking-codes')
    @endif
  </body>
</html>
