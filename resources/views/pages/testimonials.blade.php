@extends('layouts.default')

@section('title', 'Testimonials')

@section('content')
<div class="middle">
  <div class="container testimonials">
    <div class="main-heading">
      <h1 class="title">
        Testimonials
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Our commitment to service excellence along with competitive pricing has made Guidance Residential the #1 choice for homebuyers and refinancers in the U.S. Islamic Home Financing market.
      </p>
      <p class="desc-text">
        We have helped over 19,000 families achieve their home financing goals. Click on each region to read a selection of testimonials from our clients.
      </p>
    </div>
    <div class="testimonials-wrapper">
      <div class="testimonial-buttons tab-buttons">
        <a name="northeast" class="main-button active">Northeast</a>
        <a name="midatlantic" class="main-button">Mid-atlantic</a>
        <a name="southeast" class="main-button">Southeast</a>
        <a name="southwest" class="main-button">Southwest</a>
        <a name="midwest" class="main-button">Midwest</a>
        <a name="west" class="main-button">West</a>
      </div>

      <div class="testimonial-list tab-content northeast active center">
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Assalamu alaikum brother Hamza,

We wanted to drop you a note in order to express our thanks for helping us secure a mortgage. We literally wouldn't have been able to do all of this without the hard work you did for us to ensure we could finally celebrate our first home.

Thank you so much brother. You were so great to deal with and got us an amazing rate. We really appreciated your efforts through the whole process. And particularly appreciated you keeping us in the loop throughout. We've just finished moving everything in now. And alhamduliallah, we are very happy with the outcome.

We both look forward to keeping you up to date with the progress as we build, as well as working with you again on our future purchases inshallah. Thank you so much for all that you do!

I'll sure try to recommend my friends and relatives who are shopping for homes to you.

Again, thanks for your help."</p>
            <h4 class="title three">Shahida &amp; Abu</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Hamza Ali</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Good Morning Team,

We are able to close our transaction with the help of everyone. :D

I would like to take this moment to thank you all for you support, cooperation, guidance, professionalism and responsiveness during this process. You are such a great team to work with. Really appreciate your work.

Once again thank you all."</p>
            <h4 class="title three">Kashif</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Mourad Barakat</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Ahsen Malik was outstanding! He demonstrated lots of patience in answering all my questions and addressing the fears. He responds very quickly to emails and questions."</p>
            <h4 class="title three">Obaid S.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Ahsen Malik</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"It's been a couple of weeks since we moved into the new home. It is great alhamdolilah and everyday as I remember Allah blessings, the thought to thank Allah for the blessing of a easy mortgage process comes to mind and I especially feel thankful for having worked with you. Brother Nabil, you made the process very easy for us especially when this was our first home purchase. We panicked a few times, but you remained patient and explained what was going on. Every time there were minor bumps, we got worried but you always just took care of them. Everyone always told us how hard the mortgage process is but we did not experience any such thing. This is a testament to Guidance but more so I attribute it to you, your personality, positive and very professional attitude and sincere desire to help us get our first home in the US. May Allah reward you for your efforts and give barakat to you."</p>
            <h4 class="title three">Yasir R.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Nabil Maataoui</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Working with Sami Kabir for last 6 years and have successfully completed multiple transactions with him. Transactions included both refinance and purchase of new homes. He has worked with me to lay out all the best options available that suit my needs and financial goals. I was guided through the entire process from initiating an application to close. He is very professional and efficient. You cannot go wrong with Sami Kabir and I would not want to work with anyone else."</p>
            <h4 class="title three">Faheem Y.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Sami Kabir</h5>
          </div>
      </div>

      <div class="testimonial-list tab-content midatlantic center">
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"The experience was great. My representative was extremely helpful. I love the service and I would recommend you to everyone I know in a heartbeat. In fact I already have asked a friend to try to refinance their house using Guidance Residential as I absolutely loved the overall experience."</p>
            <h4 class="title three">Mohammed S.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Bassem Abdalla</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I had financed my first house through Guidance about 16 years ago, and my experience was not that great. When I refinanced my current house, I happened to meet Nejat Abdurahman who is highly professional that easily guided me through the process. She made the process very seamless and enjoyable. I am truly satisfied with her service and the care provided.  I highly appreciate Nejat for her knowledge, expertise and will not hesitate to recommend her."</p>
            <h4 class="title three">Nuru Y. (Annandale, VA)</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Nejat Abdurahman</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I feel myself fortunate that I got in touch with Sister Nejat Abdurahman at Guidance. My experience with Guidance and especially with Sister Nejat was a memorable one. She was very professional, highly customer service oriented, quick responder and expert in her field. Her work ethics were very impressive. As a customer of mortgage financing, we always have a lot of questions with our rep, Sister Nejat was very responsive and always extremely respectful, polite and courteous. Amazingly she gave us her cell number also and we have access to her 24x7, which is very rare in this business. During entire process she was very prompt in keeping us updated on the status of process, which was a great help and gave us peace of mind. Guidance Residential is blessed to have an individual like Sister Nejat Abdurahman on their staff. She is a major asset to Guidance Residential and I think the company can use her to train other staff members for transferring her high level of skills. I would like to recommend every Muslim Brothers and Sisters to Sister Nejat Abdurahman."</p>
            <h4 class="title three">A. Khan. (Cypress, TX)</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Nejat Abdurahman</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"The process is simple and smooth. Staff are cooperative and efficient and they met the target timeline well and explained everything and every step throughout the entire process."</p>
            <h4 class="title three">Zaman S.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Hassan Khokhar</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I had a wonderful experience with Guidance Residential to refinance my home at a lower rate. I also used Guidance Residential with my original mortgage and I'm satisfied with them."</p>
            <h4 class="title three">Scott W.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Tahir Nisar</h5>
          </div>
      </div>

      <div class="testimonial-list tab-content southeast center">
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Salaam and Good Morning,

About two weeks have passed since successfully closing on the Legacy Lane house (all praises to God) and we did not want the window of opportunity to escape us without properly extending our gratitude for your hard work in the pursuit of acquiring our home.

On behalf of Wardah and myself (and pending Baby Gaffoor), please accept our sincerest thanks for your patience, diligence, and persistence through this process. We genuinely appreciate each of your contributions towards making the acquisition of our home a reality.

May God brighten your lives with His generous favors and endow you with His choicest of blessings."</p>
            <h4 class="title three">A. Gaffoor</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Shabeer Shekha</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Salam Br Ahmed,

Thank you so much. You are a great help in this process. It could not have been possible without you. I cannot express in words the help I get from you. I will contact you for the next property in a couple of months In Shall Allah."</p>
            <h4 class="title three">Nasim</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Ahmed Ahmed</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Salam Brother Atabek, I wanted to say thanks again for all the help with converting my conventional mortgage, it is a big relief for me. May Allah reward you and your family. Lets stay in touch Inshaa Allah. Have a blessed rest of the weekend."</p>
            <h4 class="title three">Mohamed D.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Atabek Djumagulov</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Dear Rana,

Thank you very much for helping us with the purchase of our new home. House closing yesterday went very smooth. Thanks for explaining all steps and terms in details. I could not have asked for a better person to handle my case! You were always available regardless of what time of the day I contact you. You were always caring and on top of things. I profusely appreciate that and will be glad to refer my friends to work with you and Guidance Residential."</p>
            <h4 class="title three">Hazem H.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Rana Asif</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Great customer service, Salah El-Ghazzali. You really made me feel and know that you cared about my concerns and wanted the best for me."</p>
            <h4 class="title three">Arman M.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Salah Elghazzali</h5>
          </div>
      </div>

      <div class="testimonial-list tab-content southwest center">
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Recently Amer Gohar assisted me in obtaining a home loan from Guidance Residential. I consider myself a very demanding customer and also being a professional in customer service industry my expectations are very high. I must say, Amer knows the art of providing excellent customer service and representing his company very well. Service like this is the very reason for me to continue sending my friends to Guidance Residential. Amer truly go above and beyond his duties. This is true for all of his interactions with him and with the closing company and builder. Particularly, the closing company had lots of good things to say about him and Guidance Residential. According to them "Amer has got PASSION towards his work". Please pass along my appreciation to Amer Gohar - He is definitely a credit to your organization."</p>
            <h4 class="title three">Akhter S.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Amer Gohar</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Brother Mohammad Rahaman is the best. I was a first time home buyer but he was with me every step of the process. He was always there to clear my concerns and always helped me more than expected. I cannot thank him enough, I pray to Allah SWT to grant him the best rewards."</p>
            <h4 class="title three">Jawad M.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Mohammad Rahaman</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Thank you Zeeshan for helping me through the process. Buying a house isn't easy, much less my first home. You were very helpful answering all my questions, guiding me, and finding solutions. You demonstrated true concern and wanting what was best for me. I didn't feel pressured or sold into anything. I could tell you worked hard and kept it professional through out the process. I really appreciate your business, I'm glad you were my lender agent, great job! Thank you!"</p>
            <h4 class="title three">Fausto C.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Zeeshan Mohammed</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Alhamdulillah, the closing of the house was done this previous day and everything went smooth and normal. I'm glad to have found a brother like you. Your help and assistance truly exceeded my expectations. I'm sure that the entire community will benefit from your services. I really appreciated the many times that your team went out of the way to fulfill customer needs and deliver. I would say that throughout the whole process, everything was very professional, all promises delivered and a friendly atmosphere. I will definitely refer Guidance Residential to everyone and especially to someone like you."</p>
            <h4 class="title three">Tauqer S.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Amer Gohar</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Earlier this year me and my wife decided that we wanted to purchase a house. After renting, I was hesitant to buy because I did not want to incorporate Interest in my life. I did some research and found out about Guidance Residential. I spoke to Mr Zeeshan on the phone for an hour where he explained to me how their home financing works. I got a quote, compared it to other conventional debt quotes and was very surprised that they were so competitive - I was not paying more for Islamic financing. After I found the house that I wanted we had an issue with the seller where she needed to finish the sale in a period requiring that everything should be completed quickly by the deadline. Mr Zeeshan was very prompt with asking for documents and making sure that everything got done in time. Since it was my first home purchase, I had to be walked through every step and educated but he made sure that everything was very clear and that I had full information. He was always available to answer questions and at closing, he was working extra hard communicating with all parties. I really appreciate the way he handled the sale process and I highly recommend him for anyone wishing to purchase a house."</p>
            <h4 class="title three">Abdulaziz D.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Zeeshan Mohammed</h5>
          </div>
      </div>

      <div class="testimonial-list tab-content midwest center">
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I would like to express our deep gratitude for the service we received at Guidance Residential during home financing. Thank you Mahamud for your continued advising, guiding in the process. We appreciate your prompt responses to answer our questions including weekends and late after hours. Great to have a professional team like yours by our side during.Thank you again for your service."</p>
            <h4 class="title three">Faisal</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Mahamud Gorod</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Salaam, I would like to sincerely thank you for your gift and your assistance in the process of purchasing my home. You were very helpful throughout the process, especially taking my calls and texts in the evenings. I am impressed with the professionalism of Guidance and will certainly recommend them to friends and colleagues."</p>
            <h4 class="title three">Mohammed S.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Hamid Khan</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I signed the closing documents of my townhouse last month without any problems. I was in touch with Nabil for my home financing and he gave me wonderful service. It was not only his service at Guidance Residential, but he also gave me good suggestions for inspectors, home insurance companies, and many other things. I really appreciate that and wanted to thank you for the services."</p>
            <h4 class="title three">Faisal</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Nabil Eid</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"We had a great experience with Guidance Residential. Mohammed Khalil our Account Executive guided us during this journey. Brother Khalil is knowledgeable about the product, always reachable, and very helpful. With his instant availability he was able to resolve our issue immediately and Al-Hamdolillah we closed earlier than we were required to. We felt very comfortable with the whole process. Thank you all Guidance team; Mohamed, Faye, Fredy, and Tarek."</p>
            <h4 class="title three">Amer &amp; Olla A.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Mohammed Khalil</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I have closed on my house on August 25th and moved in on August 30th.

The purpose of this letter is to give my personal thanks to you and the team that was working on my application. I would like to thank you all and appreciate all the effort that you have put through to get the process completed smoothly.

The team has worked tirelessly to get me through the process. I can say that because I had emails coming in at 11:00 PM and the same person sending it again at 7:00AM. That means a lot. A lot of my friends had to appreciate the speed and the smoothness of my process.

The team was not only working hard but was also very patient with me. I made numerous nervous enquiries via phone calls, text messages, emails. Every time I had the same pleasant response from Guidance team, which was not only accurate but was very prompt. Home buying is a very nervous experience but the team put me at ease and helped alleviate a lot of that feeling.

I definitely consider myself lucky to have worked with this team.

I once again would like to thank each one of you individually. You all are a great asset to the profession you are in and Guidance is lucky to have you all as part of their organization."</p>
            <h4 class="title three">Mujeeb</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Nabil Eid</h5>
          </div>
      </div>

      <div class="testimonial-list tab-content west center">
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Assalamalaikum,

I just wanted to take a moment and give you some feedback on my experience with guidance residential, especially with Aasim Ikramullah.

I had my home recently refinanced and have to the say the experience was amazing from beginning to end. Aasim, is a delight to work with and answered so many questions throughout the process and made it so easy to understand! He even followed up with me daily after the closing to make sure I was satisfied and if everything went well. What an amazing experience and I am so happy to have refinanced with Guidance. Aasim is a complete gem for your company and I’m honored to have had him as my first point of contact with Guidance.

I will for sure refer you to others if I get the opportunity.

Have an excellent day."</p>
            <h4 class="title three">Ahsan Mir</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Aasim Ikramullah</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I wanted to share my home buying experience which Abdihakim Ali facilitated for me. I was referred to him by a family friend; he told me I was in good hands and I am glad I choose him as my account executive. Being a first time homebuyer, everything was new to me and overwhelming, but Abdihakim walked me through everything including the different options available . He was always there to answer questions in a timely fashion and I rest assured that I was being well taken care of. I would definitely recommend Abdihakim and this company to friends and family especially my Muslim friends who are hesitant to go through the regular home financing. Keep up the good work."</p>
            <h4 class="title three">Oluwafunmilayo A.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Abdihakim Ali</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Brother Abdisa, I want you to know how much I appreciate the excellent service you provided to me and my wife throughout the entire home buying process. Great service makes your customers feel that you care about developing a long-term relationship that means more than just making money. You were very prompt when it comes to responding to my calls, emails, and messages and you were truly available at all times for us regardless of the method of communication we used. I decided to go with the Guidance Residential after doing lots of research and you proved to me it was the right decision. I would love to recommend you and your company to anyone who needs to buy a house in future."</p>
            <h4 class="title three">Ahmed H.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Abdisa Tufa</h5>
          </div>
          <div class="testimonial content square">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"I just want to take time to thank you for amazing service from Guidance for my refinance. I had a really great experience working with you guys. I want to specially thank brother Atif Akbar, for his tireless, prompt, and diligent efforts to make this a smooth transaction. I really appreciate him for all his efforts and great customer service."</p>
            <h4 class="title three">Saqib W.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Atif Akbar</h5>
          </div>
          <div class="testimonial content rectangle">
            <div class="icon"><span class="fa fa-quote-left"></span></div>
            <p class="desc-text quote italic">"Everything was smooth experience. On a personal level it is amazing to have a home and to allowing me finance the halal way is a dream come true. It wouldn't have been possible without the extra effort that some people did personally in Guidance Residential. May Allah accept our intentions and increase barakah in your company."</p>
            <h4 class="title three">Mohammed A.</h4>
            <h5 class="desc-text italic">Buyer, Served By</h5>
            <h5 class="sub-text">Emran Hossain</h5>
          </div>
      </div>

    </div>
  </div>
  <div class="single-video-section">
    <div class="main-heading">
      <h1 class="title six">
        Touching Peoples lives
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        We are more than just a home financing provider.
      </p>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="jewLAIqYvDs">
        <img src="{{ mix_remote('images/testimonials/Testimonials.jpg') }}" alt="coOwnership video" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Testimonials'])
@endsection
