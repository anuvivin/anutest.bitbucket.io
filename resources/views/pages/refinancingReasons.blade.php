@extends('layouts.default')

@section('title', 'Refinancing Reasons')

@section('content')
<div class="middle">
  <div class="container refinancingReasons">
    <div class="main-heading">
      <h1 class="title">
        REFINANCING REASONS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance Residential offers refinancing options for anyone who desires to refinance to Shariah-compliant home financing, lower monthly payments or reduce the payoff term.
      </p>
    </div>
    <div class="split-list-container">
      <div class="item" id="reebaFreeHomeOwnership">
        <div class="image-container background-image"></div>
        <div class="description">
          <h3 class="title three">RIBA-FREE HOME OWNERSHIP</h3>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            We believe in the timeless values of faith, which is why we offer truly Shariah-compliant home financing services. Refinance with Guidance Residential to switch from a riba-
            (or interest) based mortgage to our riba-free Declining Balance Co-ownership Program. Complete the online Pre-Qualification form or connect with a licensed Account Executive
            to get started.
          </p>
          <div class="button-container">
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_RefinancingReasons_RibaFreeHomeOwnership_Feb2018" target="_blank" class="main-button gold solid" > <span class="fa fa-check-square-o"></span>Get pre-qualified </a>
            <a href="{{ route('account-executive.index') }}" class="main-button"><span class="fa fa-user-circle-o"></span>Find an Account Executive</a>
          </div>
        </div>
      </div>
      <div class="item" id="lowerMonthlyPayment">
        <div class="image-container background-image"></div>
        <div class="description">
          <h3 class="title three">LOWER MONTHLY PAYMENT</h3>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            Guidance Residential offers a Shariah-compliant, competitive refinancing option to lower your monthly payment. Complete the online Pre-Qualification form or connect with
            a licensed Account Executive to get started.
          </p>
          <div class="button-container">
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_RefinancingReasons_LowerMonthlyPayment_Feb2018" target="_blank" class="main-button gold solid" > <span class="fa fa-check-square-o"></span>Get pre-qualified </a>
            <a href="{{ route('account-executive.index') }}" class="main-button " > <span class="fa fa-user-circle-o"></span>Find an account Executive </a>
          </div>
        </div>
      </div>
      <div class="item" id="reducePayOffTerm">
        <div class="image-container background-image"></div>
        <div class="description">
          <h3 class="title three">REDUCE PAY OFF TERM</h3>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            You can reduce you current mortgage or home finance contract payoff term by refinancing with our Shariah-compliant home financing solution. Complete the online
            Pre-Qualification form or connect with a licensed Account Executive to get started.
          </p>
          <div class="button-container">
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_RefinancingReasons_ReducePayoffTerm_Feb2018" target="_blank" class="main-button gold solid" > <span class="fa fa-check-square-o"></span>Get pre-qualified </a>
            <a href="{{ route('account-executive.index') }}" class="main-button " > <span class="fa fa-user-circle-o"></span>Find an account Executive </a>
          </div>
        </div>
      </div>
      <div class="item" id="cashOutRefinancing">
        <div class="image-container background-image"></div>
        <div class="description">
          <h3 class="title three">CASH OUT REFINANCING</h3>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            If you are looking to get cash out from refinancing - whether it be for college tuition or another financial need - Guidance Residential offers Shariah-compliant refinancing
            options for you. Complete the online Pre-Qualification form or connect with a licensed Account Executive to get started. *May not be available in all states
          </p>
          <div class="button-container">
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_RefinancingReasons_CashOutRefinancing_Feb2018" target="_blank" class="main-button gold solid" > <span class="fa fa-check-square-o"></span>Get pre-qualified </a>
            <a href="{{ route('account-executive.index') }}" class="main-button " > <span class="fa fa-user-circle-o"></span>Find an account Executive </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "RIBA-FREE HOME OWNERSHIP",
    "url": "{{ Request::url() }}#ribaHomeOwnership",
    "description": "We believe in the timeless values of faith, which is why we offer truly Shariah-compliant home financing services. Refinance with Guidance Residential to switch from a riba- (or interest) based mortgage to our riba-free Declining Balance Co-ownership Program. Complete the online Pre-Qualification form or connect with a licensed Account Executive to get started."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "LOWER MONTHLY PAYMENT",
    "url": "{{ Request::url() }}#ribaHomeOwnership",
    "description": "Guidance Residential offers a Shariah-compliant, competitive refinancing option to lower your monthly payment. Complete the online Pre-Qualification form or connect with a licensed Account Executive to get started."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "REDUCE PAY OFF TERM",
    "url": "{{ Request::url() }}#ribaHomeOwnership",
    "description": "You can reduce you current mortgage or home finance contract payoff term by refinancing with our Shariah-compliant home financing solution. Complete the online Pre-Qualification form or connect with a licensed Account Executive to get started."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_RefinanceReasons'])
@endsection
