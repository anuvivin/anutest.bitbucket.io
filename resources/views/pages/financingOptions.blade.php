@extends('layouts.default')

@section('title', 'Financing Options')

@section('content')
  <div class="container financing-options">
    <div class="main-heading">
      <div class="title">
        FINANCING OPTIONS
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Learn about the financing options available to purchase your home.
      </p>
    </div>
    <div class="options-container">
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Product Type</h3>
          <div class="fa fa-calendar"></div>
          <div class="desc-text">
            <a href="{{ route('account-executive.index') }}" class="white-link">Connect with a licensed Account Executive to learn more about our financial products.</a>
          </div>
        </div>
        <div class="content">
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Fixed rates for 30, 20, and 15-year contracts</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Adjustable rates for 3/1, 5/1, 7/1, and 10/1 contracts</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">First-Time Homebuyer Program - allows as little as 3% down payment</span></p>
        </div>
      </div>
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Financing Type</h3>
          <div class="fa fa-gears"></div>
          <div class="desc-text">
            <a href="{{ route('account-executive.index') }}" class="white-link">Connect with a licensed Account Executive to learn more about our financing types.</a>
          </div>
        </div>
        <div class="content full">
          <div class="accordion-split-content full">
            <div class="accordion-content">
              <a href="#conforming" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="conforming">
                <span class="fa expand-icon"></span><h4 class="title four bold">conforming </h4>
              </a>
              <div class="text-content left collapse" id="conforming">
                <div class="text">
                  <p class="desc-text italic">
                    A loan that conforms to Government-sponsored enterprise (GSE) guidelines
                  </p>
                  <div class="table ">
                    <table>
                      <tr>
                        <td>Property Type</td>
                        <td>Maximum Base Conforming Home Financing Limits</td>
                      </tr>
                      <tr>
                        <td>1-Unit</td>
                        <td>$453,100</td>
                      </tr>
                      <tr>
                        <td>2-Unit</td>
                        <td>$580,150</td>
                      </tr>
                      <tr>
                        <td>3-Unit</td>
                        <td>$701,250</td>
                      </tr>
                      <tr>
                        <td>4-Unit</td>
                        <td>$871,450</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
              <a href="#conformingJumbo" class="accordion-item second" data-toggle="collapse"  aria-expanded="false" aria-controls="conformingJumbo">
                <span class="fa expand-icon"></span><h4 class="title four bold">Conforming Jumbo</h4>
              </a>
              <div class="text-content left collapse" id="conformingJumbo">
                <div class="text">
                  <div class="table ">
                    <table>
                      <tr>
                        <td>Property Type</td>
                        <td>Minimum and Maximum* Base Conforming Home Financing Limits</td>
                      </tr>
                      <tr>
                        <td>1-Unit</td>
                        <td>$453,100 or $679,650</td>
                      </tr>
                      <tr>
                        <td>2-Unit</td>
                        <td>$580,150 or $870,225</td>
                      </tr>
                      <tr>
                        <td>3-Unit</td>
                        <td>$701,250 or $1,051,875</td>
                      </tr>
                      <tr>
                        <td>4-Unit</td>
                        <td>$871,450 or $1,307,175</td>
                      </tr>
                    </table>
                  </div>
                  <p class="desc-text italic">
                    * These are the maximum potential home financing limits for designated high cost areas. Actual home financing limits are established for each county (or equivalent) and the home financing limits for specific high-cost areas may be lower. The original principal balance of a contract must not exceed the maximum home financing limit for the specific area in which the property is located.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Property Type</h3>
          <div class="fa fa-home"></div>
          <div class="desc-text">
            <a href="{{ route('account-executive.index') }}" class="white-link">Connect with a licensed Account Executive to learn more about property types.</a>
          </div>
        </div>
        <div class="content">
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Single Family Homes</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Condominiums</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Townhomes</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Planned Unit Developments</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Multi-unit (2 to 4 units)</span></p>
        </div>
      </div>
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Occupancy Type</h3>
          <div class="fa fa-building-o"></div>
          <div class="desc-text">
            <a href="{{ route('account-executive.index') }}" class="white-link">Connect with a licensed Account Executive to learn more about occupancy types.</a>
          </div>
        </div>
        <div class="content">
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Owner Occupied</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Second Home</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Non-owner Occupied (Investment Property)</span></p>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "FINANCING OPTIONS",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Learn about the financing options available to purchase your home."
  }
  </script>
@endsection
