@extends('layouts.default')

@section('title', 'Operational States')

@section('content')
<div class="middle">
  <div class="container operationalStates">
    <div class="main-heading">
      <div class="title">
        Operational States
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance Residential is licensed and operational in the following 29 U.S. states:
      </p>
      <p class="desc-text skyblue">
        Click on the map location pins to view office locations.
      </p>
    </div>
    <div class="state-map">
      <img src="{{ mix_remote('images/operationStates/map.svg') }}" id="stateMap" alt="">
    </div>
    <div class="managers connecticutState connecticutInitial">
      <h4 class="title four bold">Connecticut</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Nabil Maataoui</span><br>
         Phone: 857.259.3522
      </p>
    </div>
    <div class="managers massachusettsState massachusettsInitial">
      <h4 class="title four bold">Massachussets</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Nabil Maataoui</span><br>
         Phone: 857.259.3522
      </p>
    </div>
    <div class="managers newJerseyState newJerseyInitial">
      <h4 class="title four bold">New Jersey</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Omar Jasser</span><br>
         Phone: 201.268.5888
      </p>
    </div>
    <div class="managers newYorkState">
      <h4 class="title four bold">New York</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Sami Kabir</span><br>
         Phone: 347.894.5811
      </p>
    </div>
    <div class="managers pennsylvaniaState">
      <h4 class="title four bold">Pennsylvania</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Omar Jasser</span><br>
         Phone: 201.268.5888
      </p>
    </div>
    <div class="managers rhodeIslandState rhodeIslandInitial">
      <h4 class="title four bold">Rhode Island</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Nabil Maataoui</span><br>
         Phone: 857.259.3522
      </p>
    </div>
    <div class="managers californiaState">
      <h4 class="title four bold">CALIFORNIA</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Ali Khurrum</span><br>
         Phone: 213.377.5271
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Emran Hossain - Northern California</span><br>
         Phone: 408.650.8889
      </p>
      <p class="desc-text">
        <span class="bold">Aziz Molai - Southern California</span><br>
         Phone: 213.377.5662
      </p>
    </div>
    <div class="managers oregonState">
      <h4 class="title four bold">Oregon</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Ali Khurrum</span><br>
         Phone: 213.377.5271
      </p>
    </div>
    <div class="managers washingtonState">
      <h4 class="title four bold">WASHINGTON</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Ali Khurrum</span><br>
         Phone: 213.377.5271
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Abdihakim Ali</span><br>
         Phone: 206.981.2336
      </p>
    </div>
    <div class="managers delawareState delawareInitial">
      <h4 class="title four bold">Delaware</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Salman Ali</span><br>
         Phone: 703.885.0847
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Ejaz Ahmad</span><br>
         Phone: 703.885.0879
      </p>
    </div>
    <div class="managers kentuckyState">
      <h4 class="title four bold">Kentucky</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Salman Ali</span><br>
         Phone: 703.885.0847
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Nejat Abdurahman</span><br>
         Phone: 703.885.0851
      </p>
    </div>
    <div class="managers marylandState marylandInitial">
      <h4 class="title four bold">Maryland</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Salman Ali</span><br>
         Phone: 703.885.0847
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Ejaz Ahmad</span><br>
         Phone: 703.885.0879
      </p>
    </div>
    <div class="managers virginiaState">
      <h4 class="title four bold">Virginia</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Salman Ali</span><br>
         Phone: 703.885.0847
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Ejaz Ahmad - Central VA</span><br>
         Phone: 703.885.0879
      </p>
      <p class="desc-text">
        <span class="bold">Nejat Abdurahman - Northern VA, West Virginia</span><br>
         Phone: 703.885.0851
      </p>
    </div>
    <div class="managers dcState">
      <h4 class="title four bold">Whashington DC</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Salman Ali</span><br>
         Phone: 703.885.0847
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Nejat Abdurahman</span><br>
         Phone: 703.885.0851
      </p>
    </div>
    <div class="managers arizonaState">
      <h4 class="title four bold">Arizona</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Junaid Iqbal</span><br>
         Phone: 713.955.1733
      </p>
    </div>
    <div class="managers kansasState">
      <h4 class="title four bold">Kansas</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Junaid Iqbal</span><br>
         Phone: 713.955.1733
      </p>
    </div>
    <div class="managers texasState">
      <h4 class="title four bold">Texas</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Junaid Iqbal</span><br>
         Phone: 713.955.1733
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Fasih Siddiqui</span><br>
         Phone: 713.491.2644
      </p>
    </div>
    <div class="managers illinoisState">
      <h4 class="title four bold">ILLINOIS</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Faye Safi</span><br>
         Phone: 847.897.4974
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Nabil Eid</span><br>
         Phone: 773.942.2850
      </p>
    </div>
    <div class="managers michiganState">
      <h4 class="title four bold">Michigan</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Faye Safi</span><br>
         Phone: 847.897.4974
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Nabil Eid</span><br>
         Phone: 773.942.2850
      </p>
    </div>
    <div class="managers minnesotaState">
      <h4 class="title four bold">MINNESOTA</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Faye Safi</span><br>
         Phone: 847.897.4974
      </p>
    </div>
    <div class="managers ohioState">
      <h4 class="title four bold">Ohio</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Faye Safi</span><br>
         Phone: 847.897.4974
      </p>
    </div>
    <div class="managers wisconsinState">
      <h4 class="title four bold">Wisconsin</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Faye Safi</span><br>
         Phone: 847.897.4974
      </p>
    </div>
    <div class="managers alabamaState">
      <h4 class="title four bold">Alabama</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Shabeer Shekha</span><br>
         Phone: 954.372.2534
      </p>
    </div>
    <div class="managers arkansasState">
      <h4 class="title four bold">Arkansas</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
    </div>
    <div class="managers floridaState">
      <h4 class="title four bold">Florida</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Shabeer Shekha</span><br>
         Phone: 954.372.2534
      </p>
    </div>
    <div class="managers tenneseeState">
      <h4 class="title four bold">Tennessee</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
    </div>
    <div class="managers georgiaState">
      <h4 class="title four bold">Georgia</h4>
      <p class="desc-text">2180 Satellite Boulevard, Suite 400-98<br>Duluth, GA 30097</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Salah Elghazzali</span><br>
         Phone: 404.631.6755
      </p>
    </div>
    <div class="managers northCarolinaState">
      <h4 class="title four bold">North Carolina</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Salah Elghazzali</span><br>
         Phone: 404.631.6755
      </p>
    </div>
    <div class="managers southCarolinaState">
      <h4 class="title four bold">South Carolina</h4>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Salah Elghazzali</span><br>
         Phone: 404.631.6755
      </p>
    </div>
    <div class="managers coloradoState">
      <h4 class="title four bold">Coming Soon</h4>
    </div>
    <div class="managers westVirginiaState">
      <h4 class="title four bold">Coming Soon</h4>
    </div>
    <div class="managers mississippiState">
      <h4 class="title four bold">Coming Soon</h4>
    </div>
    <div class="office-location corporateLocation">
      <h4 class="title four bold">CORPORATE OFFICE</h4>
      <p class="desc-text">11107 Sunset Hills Road, Suite 100 &amp; 200<br>
        Reston, VA 20190
      </p>
      <h4 class="title four bold">RECEPTIONIST</h4>
      <p class="desc-text">
         Phone: 703.885.0850
      </p>
    </div>
    <div class="office-location virginiaLocation">
      <h4 class="title four bold">Virginia office</h4>
      <p class="desc-text">11107 Sunset Hills Road, Suite 200 <br>
        Reston, VA  20190
      </p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Salman Ali</span><br>
         Phone: 703.885.0847
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Ejaz Ahmad - Central VA</span><br>
         Phone: 703.885.0879
      </p>
      <p class="desc-text">
        <span class="bold">Nejat Abdurahman - Northern VA, West Virginia</span><br>
         Phone: 703.885.0851
      </p>
    </div>
    <div class="office-location washingtonLocation">
      <h4 class="title four bold">WASHINGTON OFFICE</h4>
      <p class="desc-text">16400 Southcenter Parkway, Suite 400<br>Tukwila, Washington 98188</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Ali Khurrum</span><br>
         Phone: 213.377.5271
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Abdihakim Ali</span><br>
         Phone: 206.981.2336
      </p>
    </div>
    <div class="office-location californiaLocation">
      <h4 class="title four bold">NORTHERN CALIFORNIA OFFICE (2)</h4>
      <p class="desc-text">621 Capitol Mall, Suite 2026<br> Sacramento, CA 95814</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Ali Khurrum</span><br>
         Phone: 213.377.5271
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Emran Hossain</span><br>
         Phone: 408.650.8889
      </p>
    </div>
    <div class="office-location northCaliforniaLocation">
      <h4 class="title four bold">NORTHERN CALIFORNIA OFFICE (1)</h4>
      <p class="desc-text">3000 Scott Boulevard, Suite 208<br> Santa Clara, CA 95054</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Ali Khurrum</span><br>
         Phone: 213.377.5271
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Emran Hossain</span><br>
         Phone: 408.650.8889
      </p>
    </div>
    <div class="office-location southCaliforniaLocation">
      <h4 class="title four bold">SOUTHERN CALIFORNIA OFFICE</h4>
      <p class="desc-text">2400 E. Katella Avenue, Suite 800<br>Anaheim, CA 92806</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Ali Khurrum</span><br>
         Phone: 213.377.5271
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Aziz Molai</span><br>
         Phone: 213.377.5662
      </p>
    </div>
    <div class="office-location dallasLocation">
      <h4 class="title four bold">Dallas office location</h4>
      <p class="desc-text">6150 Independence Parkway, Suite K Plano<br> Plano, TX 75023</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Junaid Iqbal</span><br>
         Phone: 713.955.1733
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Fasih Siddiqui</span><br>
         Phone: 713.491.2644
      </p>
    </div>
    <div class="office-location houstonLocation">
      <h4 class="title four bold">Houston office location</h4>
      <p class="desc-text">14090 Southwest Freeway, Suite 300<br>Sugar Land, Texas 77478</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Junaid Iqbal</span><br>
         Phone: 713.955.1733
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Fasih Siddiqui</span><br>
         Phone: 713.491.2644
      </p>
    </div>
    <div class="office-location duluthLocation">
      <h4 class="title four bold">Georgia office location</h4>
      <p class="desc-text">2180 Satellite Boulevard, Suite 400-98<br>Duluth, GA 30097</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Salah Elghazzali</span><br>
         Phone: 404.631.6755
      </p>
    </div>
    <div class="office-location tenneseeLocation">
      <h4 class="title four bold">Tennessee office location</h4>
      <p class="desc-text">101 Guinevere Court<br>Murfreesboro, TN 37127</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
    </div>
    <div class="office-location illinoisLocation">
      <h4 class="title four bold">ILLINOIS office location</h4>
      <p class="desc-text">100 E. Roosevelt Road, Suite 44-45<br>Villa Park, IL 60181</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Faye Safi</span><br>
         Phone: 847.897.4974
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Nabil Eid</span><br>
         Phone: 773.942.2850
      </p>
    </div>
    <div class="office-location minnesotaLocation">
      <h4 class="title four bold">MINNESOTA office location</h4>
      <p class="desc-text">6519 Nicollet Avenue South, Suite 302<br>Richfield, MN 55423</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Faye Safi</span><br>
         Phone: 847.897.4974
      </p>
    </div>
    <div class="office-location floridaLocation">
      <h4 class="title four bold">Florida office location</h4>
      <p class="desc-text">5323 Millenia Lakes Blvd, Suite 300 (Office 307)<br>Orlando, FL 32839</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Shabeer Shekha</span><br>
         Phone: 954.372.2534
      </p>
    </div>
    <div class="office-location floridaLocation_2">
      <h4 class="title four bold">Florida office location</h4>
      <p class="desc-text">8875 Hidden River Parkway, Suite 300<br>Tampa, FL 33637</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Shabeer Shekha</span><br>
         Phone: 954.372.2534
      </p>
    </div>
    <div class="office-location ncLocation">
      <h4 class="title four bold">North Carolina office location</h4>
      <p class="desc-text">4801 Glenwood Avenue, Suite 200<br>Raleigh, NC 27612</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Manager</h4>
      <p class="desc-text">
        <span class="bold">Salah Elghazzali</span><br>
         Phone: 404.631.6755
      </p>
    </div>
    <div class="office-location njLocation">
      <h4 class="title four bold">New Jersey office location</h4>
      <p class="desc-text">71 Route 46 West,<br>Elmwood Park, New Jersey 07407</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Omar Jasser - NJ, PA</span><br>
         Phone: 201.268.5888
      </p>
      <p class="desc-text">
        <span class="bold">Sami Kabir - NY</span><br>
         Phone: 347.894.5811
      </p>
      <p class="desc-text">
        <span class="bold">Nabil Maataoui - MA, CT, RI</span><br>
         Phone: 857.259.3522
      </p>
    </div>
    <div class="office-location newYorkLocation">
      <h4 class="title four bold">New York office location</h4>
      <p class="desc-text">171-21 Jamaica Avenue,<br>Jamaica, NY 11432</p>
      <h4 class="title four bold">Regional Manager</h4>
      <p class="desc-text">
        <span class="bold">Abdessamad Melouane</span><br>
         Phone: 201.879.0600
      </p>
      <h4 class="title four bold">Area Managers</h4>
      <p class="desc-text">
        <span class="bold">Omar Jasser - NJ, PA</span><br>
         Phone: 201.268.5888
      </p>
      <p class="desc-text">
        <span class="bold">Sami Kabir - NY</span><br>
         Phone: 347.894.5811
      </p>
      <p class="desc-text">
        <span class="bold">Nabil Maataoui - MA, CT, RI</span><br>
         Phone: 857.259.3522
      </p>
    </div>
    <div class="state-map-description">
      <p class="desc-text"><span class="icon square"></span> <span class="text">States Pending</span> </p>
      <p class="desc-text"><span class="icon fa fa-map-marker golden"></span> <span class="text">Office Location</span> </p>
      <p class="desc-text"><span class="icon fa fa-star golden"></span> <span class="text">Corporate Office Location</span> </p>
    </div>
    <div class="state-list">
      <div class="region">
        <div class="icon">
          <span class="fa fa-check-circle"></span>
        </div>
        <div class="states">
          <span class="heading">Northeast</span>
          <span>Connecticut</span>
          <span>Massachussets</span>
          <span>New Jersey</span>
          <span>New York</span>
          <span>Pennsylvania</span>
          <span>Rhode Island</span>
        </div>
      </div>
      <div class="region">
        <div class="icon">
          <span class="fa fa-check-circle"></span>
        </div>
        <div class="states">
          <span class="heading">Mid-Atlantic</span>
          <span>Delaware</span>
          <span>Kentucky</span>
          <span>Maryland</span>
          <span>Virginia</span>
          <span>Washington D.C.</span>
          <span>West Virginia</span>
        </div>
      </div>
      <div class="region">
        <div class="icon">
          <span class="fa fa-check-circle"></span>
        </div>
        <div class="states">
          <span class="heading">Midwest</span>
          <span>Illinois</span>
          <span>Michigan</span>
          <span>Minnesota</span>
          <span>Ohio</span>
          <span>Wisconsin</span>
        </div>
      </div>
      <div class="region">
        <div class="icon">
          <span class="fa fa-check-circle"></span>
        </div>
        <div class="states">
          <span class="heading">West Coast</span>
          <span>California</span>
          <span>Oregon</span>
          <span>Washington</span>
        </div>
      </div>
      <div class="region">
        <div class="icon">
          <span class="fa fa-check-circle"></span>
        </div>
        <div class="states">
          <span class="heading">Southwest</span>
          <span>Arizona</span>
          <span>Colorado</span>
          <span>Kansas</span>
          <span>Texas</span>
        </div>
      </div>
      <div class="region">
        <div class="icon">
          <span class="fa fa-check-circle"></span>
        </div>
        <div class="states">
          <span class="heading">Southeast</span>
          <span>Alabama</span>
          <span>Arkansas</span>
          <span>Florida</span>
          <span>Georgia</span>
          <span>Mississippi</span>
          <span>North Carolina</span>
          <span>South Carolina</span>
          <span>Tennessee</span>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org/",
    "@type": "Service",
    "serviceType": "FinanicialService",
    "areaServed":
    [
      {
        "@type": "State",
        "containedInPlace":"NORTHEAST",
        "name": ["Connecticut", "Massachussets", "New Jersey", "New York","Pennsylvania","Rhode Island"]
      },{
        "@type": "State",
        "containedInPlace":"MID-ATLANTIC",
        "name": ["Delaware","Kentucky","Maryland","Virginia","Washington D.C."]
      },{
        "@type": "State",
        "containedInPlace":"MIDWEST",
        "name": ["Illinois","Michigan","Minnesota","Ohio","Wisconsin"]
      },{
        "@type": "State",
        "containedInPlace":"WEST COAST",
        "name": ["California","Oregon","Washington"]
      },{
        "@type": "State",
        "containedInPlace":"SOUTHWEST",
        "name": ["Arizona","Kansas","Texas"]
      },{
        "@type": "State",
        "containedInPlace":"SOUTHEAST",
        "name": ["Alabama","Arkansas","Florida","Georgia","North Carolina","South Carolina","Tennessee"]
      }
    ]
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Operationalstates'])
@endsection
