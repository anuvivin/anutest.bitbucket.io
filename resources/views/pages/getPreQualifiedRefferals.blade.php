@extends('layouts.default')

@section('title', 'Get Pre-Qualified Refferals')

@section('content')
<div class="middle">
  <div class="container guidanceRealty">
    <div class="main-heading">
      <h1 class="title">
        GET PRE-QUALIFIED REFERRALS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        GuidanceRealty.com participating agents receive leads that Pre-Qualify for financing through Guidance Residential and are ready to purchase.
      </p>
    </div>
    <div class="image-container getPrequalifiesRefferals"></div>
  </div>
  <div class="how-it-works">
    <div class="main-heading">
      <h1 class="title six" id="how-it-works">
        How it works
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Learn about the GuidanceRealty.com participating agent program and sign up today to get started.
      </p>
    </div>
    <div class="realty-steps">
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-user-plus"></span>
        </div>
        <h4 class="title three">AGENT PROFILE</h4>
        <p class="desc-text">Once your information is verified, your profile will be showcased in select markets on our property search engine.</p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-random"></span>
        </div>
        <h4 class="title three">GET PRE-QUALIFIED LEADS</h4>
        <p class="desc-text">You will receive leads for homebuyers that Pre-Qualify online through Guidance Residential. </p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-suitcase"></span>
        </div>
        <h4 class="title three">BUYERS' INCENTIVES</h4>
        <p class="desc-text">Your client will receive an appraisal credit up to $500 at closing when he or she finances with Guidance Residential and purchases a home through you.</p>
      </div>
    </div>
    <div class="button-wrapper">
      <a href="{{ config('urls.grealty.agent-signup') }}" class="main-button gold solid"><span class="fa fa-power-off"></span><span>Get started</span></a>
      <a href="{{ mix_remote('docs/GR_Ebook_You_Should_Be_Closing_More_Jan2018.pdf') }}" class="main-button" target="_blank"><span>Download Agent's Guidebook</span><span class="fa fa-arrow-circle-o-down"></span></a>
    </div>
    <p class="desc-text italic">
      <span class="bold">New York consumers and agents:</span> Access to features of this website is restricted pending approval by the NY Department of Financial Services.
    </p>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "PRE-QUALIFIED REFERRALS",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "GuidanceRealty.com participating agents receive leads that Pre-Qualify for financing through Guidance Residential and are ready to purchase."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "HOW IT WORKS",
    "url": "{{ Request::url() }}#how-it-works",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Learn about the GuidanceRealty.com participating agent program and sign up today to get started. AGENT PROFILE: Once your information is verified, your profile will be showcased in select markets on our property search engine. GET PRE-QUALIFIED LEADS: You will receive leads for homebuyers that Pre-Qualify online through Guidance Residential. BUYERS INCENTIVES: Your client will receive an appraisal credit up to $500 at closing when he or she finances with Guidance Residential and purchases a home through you."
  }
  </script>
@endsection
