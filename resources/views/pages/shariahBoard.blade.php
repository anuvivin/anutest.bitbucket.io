@extends('layouts.default')

@section('title', 'Shariah Board')

@section('content')
  <div class="container ourteam">
    <div class="main-heading">
      <h1 class="title">
        INDEPENDENT SHARIAH BOARD
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Our independent Shariah board is comprised of distinguished Shariah scholars, many of whom serve or have served as members of the Accounting and Auditing Organization for Islamic Financial Institutions.
      </p>
    </div>
    <div class="team-list shariah">
      <div class="member full">
        <div class="icon" data-toggle="modal" data-target="#muhammadTaqiUsmani">
          <img src="{{ mix_remote('images/shariahboard/Justice_Muhammad_Taqi_Usmani.jpg') }}" alt="Muhammad Taqi Usmani" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="muhammadTaqiUsmani" tabindex="-1" role="dialog" aria-labelledby="muhammadTaqiUsmani" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Justice Muhammad Taqi Usmani
                    </h1>
                    <h4 class="sub-heading">CHAIRMAN, PAKISTAN</h4>
                    <span class="seperate-line"></span>
                    <p class="desc-text">
                      Justice Usmani is the chairman of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He has been a member of the Supreme Court of Pakistan since 1982. Presently, he is the vice president of Dar Al Uloom Karachi. Formerly, he was the deputy chairman of the Islamic Fiqh Academy, Jeddah. He is also chairman of the Shariah Supervisory Boards of Guidance Residential Financial Group, U.S.; Saudi American Bank, Jeddah; and Citi Islamic Investment Bank, Bahrain; vice chairman of the Shariah Supervisory Board of Abu Dhabi Islamic Bank; and member of the Shariah Supervisory Boards of Al-Baraka Group, Jeddah; First Islamic Investment Bank, Bahrain; and the Islamic Unit of the United Bank of Kuwait, among others. Justice Usmani holds Alimiyyah and Takhassus degrees from Darul Uloom, Karachi, an M.A. degree from Punjab University and an LLB from Karachi University.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#muhammadTaqiUsmani">
          <h3 class="title three">Justice Muhammad Taqi Usmani</h3>
          <h4 class="sub-heading">CHAIRMAN, PAKISTAN</h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#shaykhYusufTalal">
          <img src="{{ mix_remote('images/shariahboard/Shaykh_Yusuf_Talal_Delorenzo.jpg') }}" alt="Shaykh Yusuf Talal Delorenzo" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="shaykhYusufTalal" tabindex="-1" role="dialog" aria-labelledby="shaykhYusufTalal" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Shaykh Yusuf Talal Delorenzo
                    </h1>
                    <h4 class="sub-heading">SHARIAH BOARD MEMBER, USA</h4>
                    <span class="seperate-line"></span>
                    <p class="desc-text">
                      Shaykh DeLorenzo is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He is considered a leading authority on Islamic finance in the United States. He has translated over twenty books from Arabic, Persian and Urdu for publication, including a three-volume Compendium of Legal Rulings on the Operations of Islamic Banks. Shaykh DeLorenzo has also been a pioneer in internet education with a course entitled 'Principles of Islamic Investing.' He is a member of Shariah Boards of several Islamic financial institutions in the United States and abroad, including Dow Jones Islamic Markets and Guidance Residential Financial Group. Shaykh DeLorenzo has served as secretary of the Fiqh Council of North America and was also an advisor on Islamic education to the government of Pakistan. Following a university education in the United States, Shaykh DeLorenzo studied the classical Shariah Sciences with scholars in Pakistan.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#shaykhYusufTalal">
          <h3 class="title three">Shaykh Yusuf Talal Delorenzo</h3>
          <h4 class="sub-heading">SHARIAH BOARD MEMBER, USA</h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#shaykhNizam">
          <img src="{{ mix_remote('images/shariahboard/Shaykh_Nizam_Yaquby.jpg') }}" alt="Shaykh Nizam Yaquby" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="shaykhNizam" tabindex="-1" role="dialog" aria-labelledby="shaykhNizam" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Shaykh Nizam Yaquby
                    </h1>
                    <h4 class="sub-heading">SHARIAH BOARD MEMBER, BAHRAIN</h4>
                    <span class="seperate-line"></span>
                    <p class="desc-text">
                      Shaykh Nizam Yaquby is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He has contributed important original research on many aspects of modern Islamic finance, and is considered one of the world's leading experts in the field. He is a member of the Shariah supervisory boards for several Islamic financial institutions, including Guidance Residential Financial Group, Citi Islamic Investment Bank and the Abu Dhabi Islamic Bank. Since 1976, he has taught Tafsir, Hadith and Fiqh in Bahrain. He is also the author of several articles and publications on Islamic finance and other sciences in English and Arabic. Shaykh Yaquby received his M.Sc. in finance from McGill University in Montreal. He was educated in the classical Shariah sciences in his native Bahrain and in Makkah.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#shaykhNizam">
          <h3 class="title three">Shaykh Nizam Yaquby</h3>
          <h4 class="sub-heading">SHARIAH BOARD MEMBER, BAHRAIN</h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#mohamedElgari">
          <img src="{{ mix_remote('images/shariahboard/Dr_Mohamed_Elgari.jpg') }}" alt="Dr. Mohamed A. Elgari" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="mohamedElgari" tabindex="-1" role="dialog" aria-labelledby="mohamedElgari" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Dr. Mohamed A. Elgari
                    </h1>
                    <h4 class="sub-heading">SHARIAH BOARD MEMBER, SAUDI ARABIA</h4>
                    <span class="seperate-line"></span>
                    <p class="desc-text">
                      Dr. Elgari is professor of Islamic economics at King Abdulaziz University in Jeddah, Saudi Arabia and formerly director of its Center for Research in Islamic Economics. He is a member of the OIC Islamic Fiqh Academy and of the Academic Committee of the Islamic Development Bank. Dr. Elgari advises numerous Islamic banks and financial institutions worldwide. He authored several books and research papers on the subject of Islamic finance, including a widely used textbook on Islamic economics and frequently lectures on the subject. He is a member of the editorial Boards of the Harvard Law School's 'Harvard Series in Islamic Law' and of the 'Review of Islamic Economics.' Dr. Elgari holds a Ph.D. in economics from the University of California.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#mohamedElgari">
          <h3 class="title three">Dr. Mohamed A. Elgari</h3>
          <h4 class="sub-heading">SHARIAH BOARD MEMBER, SAUDI ARABIA</h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#abdulSattarAbuGhuddah">
          <img src="{{ mix_remote('images/shariahboard/Dr_Abdul_Sattar_Abu_Ghuddah.jpg') }}" alt="Abdul Sattar Abu Ghuddah" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="abdulSattarAbuGhuddah" tabindex="-1" role="dialog" aria-labelledby="abdulSattarAbuGhuddah" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Dr. Abdul Sattar Abu Ghuddah
                    </h1>
                    <h4 class="sub-heading">SHARIAH BOARD MEMBER, KUWAIT</h4>
                    <span class="seperate-line"></span>
                    <p class="desc-text">
                      Dr. Abdul Sattar Abu Ghuddah is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He is also a director of the Department of Financial Instruments at Al-Baraka Investment &amp; Development Company, a member of its Shariah Supervisory Board and Shariah Advisor to the parent company, Dallah Al-Baraka Group. He is an active member of the Islamic Fiqh Academy, Jeddah and the Accounting and Auditing Standards Boards of Islamic Financial Institutions. He has taught Fiqh and Islamic studies in Riyadh and Kuwait. From 1982-1990 he was a member of the Fatwa Board in the Ministry of Islamic Affairs in Kuwait. He formerly lectured in the faculty of law and Shariah at Kuwait University. Dr. Abu Ghuddah obtained B.A. degrees in Islamic Shariah and in Law from Damascus University. He then went on to earn his M.A. in Shariah and Hadith and his Ph.D. in Shariah and Comparative Fiqh from Al-Azhar University, Cairo.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#abdulSattarAbuGhuddah">
          <h3 class="title three">Dr. Abdul Sattar Abu Ghuddah</h3>
          <h4 class="sub-heading">SHARIAH BOARD MEMBER, KUWAIT</h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#imranAshrafUsmani">
          <img src="{{ mix_remote('images/shariahboard/Dr_Imran_Ashraf_Usmani.jpg') }}" alt="Imran Ashraf Usmani" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="imranAshrafUsmani" tabindex="-1" role="dialog" aria-labelledby="imranAshrafUsmani" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Dr. Imran Ashraf Usmani
                    </h1>
                    <h4 class="sub-heading">SHARIAH BOARD MEMBER, PAKISTAN</h4>
                    <span class="seperate-line"></span>
                    <p class="desc-text">
                      Dr. Usmani has been teaching Islamic sciences for the past fifteen years in Jamia Darul Uloom Karachi and has published several books and articles, including 'Meezan Bank's Guide to Islamic Banking.' Dr. Usmani also serves as a Shariah supervisory board member / Shariah advisor to State Bank of Pakistan, HSBC Amanah Finance, Credit Suisse, Lloyds TSB, Meezan Bank, Pak Kuwait Takaful Co and others. Dr. Usmani holds LLB, M.Phil. and Ph.D. degrees in Islamic finance from Karachi University as well as Alimiyyah and Takhassus degrees in Islamic jurisprudence from Jamia Darul Uloom Karachi.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#imranAshrafUsmani">
          <h3 class="title three">Dr. Imran Ashraf Usmani</h3>
          <h4 class="sub-heading">SHARIAH BOARD MEMBER, PAKISTAN</h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#mohamadDaudBakar">
          <img src="{{ mix_remote('images/shariahboard/Dr_Mohamad_Daud_Bakar.jpg') }}" alt="Dr. Mohamad Daud Bakar" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="mohamadDaudBakar" tabindex="-1" role="dialog" aria-labelledby="mohamadDaudBakar" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Dr. Mohamad Daud Bakar
                    </h1>
                    <h4 class="sub-heading">SHARIAH BOARD MEMBER, MALAYSIA</h4>
                    <span class="seperate-line"></span>
                    <p class="desc-text">
                      Dr. Mohamad Daud Bakar is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He is an associate professor in Islamic law and the deputy rector, Student Affairs and Discipline, at the International Islamic University, Malaysia. His areas of specialization include Islamic legal theory, Islamic banking and finance, Islamic law of Zakah and Islamic medical law. Besides his teaching and research assignments, Dr. Mohamad Daud provides Shariah consultancy, structuring and advisory services to various Islamic financial institutions. Dr. Mohamad Daud is currently a Shariah advisor to Guidance Residential Financial Group and a member of the Shariah supervisory board of esteemed organizations such as the Securities Commission of Malaysia, the Central Bank of Malaysia, Takaful Nasional Berhad, HSBC (Malaysia), Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI) in Bahrain, Keppel Insurance (Takaful) Ltd of Singapore, the Dow Jones Islamic Market Index and the Malaysian Rating Corporation Bhd. Dr. Mohamad Daud obtained his Bachelor of Shariah from the University of Kuwait in 1988 and was later conferred with a Ph.D. from the University of St. Andrews, Scotland.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#mohamadDaudBakar">
          <h3 class="title three">Dr. Mohamad Daud Bakar</h3>
          <h4 class="sub-heading">SHARIAH BOARD MEMBER, MALAYSIA</h4>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "Justice Muhammad Taqi Usmani",
    "jobTitle": "CHAIRMAN, PAKISTAN",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/shariahboard/Justice_Muhammad_Taqi_Usmani.jpg') }}",
    "description": "Justice Usmani is the chairman of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He has been a member of the Supreme Court of Pakistan since 1982. Presently, he is the vice president of Dar Al Uloom Karachi. Formerly, he was the deputy chairman of the Islamic Fiqh Academy, Jeddah. He is also chairman of the Shariah Supervisory Boards of Guidance Residential Financial Group, U.S.; Saudi American Bank, Jeddah; and Citi Islamic Investment Bank, Bahrain; vice chairman of the Shariah Supervisory Board of Abu Dhabi Islamic Bank; and member of the Shariah Supervisory Boards of Al-Baraka Group, Jeddah; First Islamic Investment Bank, Bahrain; and the Islamic Unit of the United Bank of Kuwait, among others. Justice Usmani holds Alimiyyah and Takhassus degrees from Darul Uloom, Karachi, an M.A. degree from Punjab University and an LLB from Karachi University."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "Shaykh Yusuf Talal Delorenzo",
    "jobTitle": "SHARIAH BOARD MEMBER, USA",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/shariahboard/Shaykh_Yusuf_Talal_Delorenzo.jpg') }}",
    "description": "Shaykh DeLorenzo is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He is considered a leading authority on Islamic finance in the United States. He has translated over twenty books from Arabic, Persian and Urdu for publication, including a three-volume Compendium of Legal Rulings on the Operations of Islamic Banks. Shaykh DeLorenzo has also been a pioneer in internet education with a course entitled 'Principles of Islamic Investing.' He is a member of Shariah Boards of several Islamic financial institutions in the United States and abroad, including Dow Jones Islamic Markets and Guidance Residential Financial Group. Shaykh DeLorenzo has served as secretary of the Fiqh Council of North America and was also an advisor on Islamic education to the government of Pakistan. Following a university education in the United States, Shaykh DeLorenzo studied the classical Shariah Sciences with scholars in Pakistan."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "SHAYKH NIZAM YAQUBY",
    "jobTitle": "SHARIAH BOARD MEMBER, BAHRAIN",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/shariahboard/Shaykh_Nizam_Yaquby.jpg') }}",
    "description": "Shaykh Nizam Yaquby is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He has contributed important original research on many aspects of modern Islamic finance, and is considered one of the world's leading experts in the field. He is a member of the Shariah supervisory boards for several Islamic financial institutions, including Guidance Residential Financial Group, Citi Islamic Investment Bank and the Abu Dhabi Islamic Bank. Since 1976, he has taught Tafsir, Hadith and Fiqh in Bahrain. He is also the author of several articles and publications on Islamic finance and other sciences in English and Arabic. Shaykh Yaquby received his M.Sc. in finance from McGill University in Montreal. He was educated in the classical Shariah sciences in his native Bahrain and in Makkah."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "DR. MOHAMED A. ELGARI",
    "jobTitle": "SHARIAH BOARD MEMBER, SAUDI ARABIA",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/shariahboard/Dr_Mohamed_Elgari.jpg') }}",
    "description": "Dr. Elgari is professor of Islamic economics at King Abdulaziz University in Jeddah, Saudi Arabia and formerly director of its Center for Research in Islamic Economics. He is a member of the OIC Islamic Fiqh Academy and of the Academic Committee of the Islamic Development Bank. Dr. Elgari advises numerous Islamic banks and financial institutions worldwide. He authored several books and research papers on the subject of Islamic finance, including a widely used textbook on Islamic economics and frequently lectures on the subject. He is a member of the editorial Boards of the Harvard Law School's 'Harvard Series in Islamic Law' and of the 'Review of Islamic Economics.' Dr. Elgari holds a Ph.D. in economics from the University of California."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "DR. ABDUL SATTAR ABU GHUDDAH",
    "jobTitle": "SHARIAH BOARD MEMBER, KUWAIT",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/shariahboard/Dr_Abdul_Sattar_Abu_Ghuddah.jpg') }}",
    "description": "Dr. Abdul Sattar Abu Ghuddah is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He is also a director of the Department of Financial Instruments at Al-Baraka Investment & Development Company, a member of its Shariah Supervisory Board and Shariah Advisor to the parent company, Dallah Al-Baraka Group. He is an active member of the Islamic Fiqh Academy, Jeddah and the Accounting and Auditing Standards Boards of Islamic Financial Institutions. He has taught Fiqh and Islamic studies in Riyadh and Kuwait. From 1982-1990 he was a member of the Fatwa Board in the Ministry of Islamic Affairs in Kuwait. He formerly lectured in the faculty of law and Shariah at Kuwait University. Dr. Abu Ghuddah obtained B.A. degrees in Islamic Shariah and in Law from Damascus University. He then went on to earn his M.A. in Shariah and Hadith and his Ph.D. in Shariah and Comparative Fiqh from Al-Azhar University, Cairo."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "DR. IMRAN ASHRAF USMANI",
    "jobTitle": "SHARIAH BOARD MEMBER, PAKISTAN",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/shariahboard/Dr_Imran_Ashraf_Usmani.jpg') }}",
    "description": "Dr. Usmani has been teaching Islamic sciences for the past fifteen years in Jamia Darul Uloom Karachi and has published several books and articles, including 'Meezan Bank's Guide to Islamic Banking.' Dr. Usmani also serves as a Shariah supervisory board member / Shariah advisor to State Bank of Pakistan, HSBC Amanah Finance, Credit Suisse, Lloyds TSB, Meezan Bank, Pak Kuwait Takaful Co and others. Dr. Usmani holds LLB, M.Phil. and Ph.D. degrees in Islamic finance from Karachi University as well as Alimiyyah and Takhassus degrees in Islamic jurisprudence from Jamia Darul Uloom Karachi."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "DR. MOHAMAD DAUD BAKAR",
    "jobTitle": "SHARIAH BOARD MEMBER, MALAYSIA",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/shariahboard/Dr_Mohamad_Daud_Bakar.jpg') }}",
    "description": "Dr. Mohamad Daud Bakar is a member of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI). He is an associate professor in Islamic law and the deputy rector, Student Affairs and Discipline, at the International Islamic University, Malaysia. His areas of specialization include Islamic legal theory, Islamic banking and finance, Islamic law of Zakah and Islamic medical law. Besides his teaching and research assignments, Dr. Mohamad Daud provides Shariah consultancy, structuring and advisory services to various Islamic financial institutions. Dr. Mohamad Daud is currently a Shariah advisor to Guidance Residential Financial Group and a member of the Shariah supervisory board of esteemed organizations such as the Securities Commission of Malaysia, the Central Bank of Malaysia, Takaful Nasional Berhad, HSBC (Malaysia), Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI) in Bahrain, Keppel Insurance (Takaful) Ltd of Singapore, the Dow Jones Islamic Market Index and the Malaysian Rating Corporation Bhd. Dr. Mohamad Daud obtained his Bachelor of Shariah from the University of Kuwait in 1988 and was later conferred with a Ph.D. from the University of St. Andrews, Scotland."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Shariahboard'])
@endsection
