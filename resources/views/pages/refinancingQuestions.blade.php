@extends('layouts.default')

@section('title', 'Refinancing Questions')

@section('content')
  <div class="container home-buying-questions">
    <div class="main-heading">
      <h1 class="title">
        REFINANCING QUESTIONS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Refinancing can be very complex, which is why we want to help simplify the process for you. Read our answers to some commonly asked questions about refinancing a home.
      </p>
    </div>
    <div class="accordion-split-content">
      <div class="description-content">
        <div class="main-heading">
          <div class="app-circle">
            <span class="fa fa-question-circle"></span>
          </div>
          <h3 class="title three bold">Frequently asked questions</h3>
          <span class="seperate-line"></span>
          <p class="desc-text">
            Click on each category to find answers to the frequently asked questions. If your question is not listed here, click on the chat icon at the bottom of the page to get a quick, real-time response. If we are not available live, do not hesitate to contact us by clicking on the Question Box in the bottom right corner of the page. We will respond as soon as possible.
          </p>
          <a href="{{ route('glossaryOfTerms')}}" class="main-button">Glossary of terms</a>
        </div>
      </div>
      <div class="accordion-content">
        <a href="#aboutGuidance" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="aboutGuidance">
          <span class="fa expand-icon"></span><h4 class="title four bold">ABOUT GUIDANCE RESIDENTIAL</h4>
        </a>
        <div class="text-content left collapse" id="aboutGuidance">
          <div class="text">
            <h5 class="title five bold">Q. What is Guidance Residential?</h5>
            <p class="desc-text">
              Guidance Residential is the largest provider of Shariah-compliant home financing in the U.S. Guidance Residential has provided over $4.6 billion in home financing to Muslim American homeowners over the
              last 15 years, and the company holds nearly 80% market share in the U.S. Shariah-compliant home financing industry. Headquartered in Reston, Virginia, the company was established in the D.C. Metro area in
              2001 and currently, the company is operational in 29 states. We are a member of Guidance Financial Group, an affiliated global financial group, which has pioneered the creation and development of
              Shariah-compliant investment products for institutional investors and financial intermediaries worldwide, as well as financial products and services that address the unmet needs of millions of Muslim
              consumers.
            </p>
            <h5 class="title five bold">Q. What types of financing does Guidance Residential offer?</h5>
            <div class="desc-text">
              <span class="bold">Purchase financing </span>
              <ul>
                <li><span class="list-text">Fixed and Adjustable rates for 30, 20, and 15-year contracts</span></li>
                <li><span class="list-text">Conforming and Conforming Jumbo financing</span></li>
                <li><span class="list-text">Single family, condominiums, townhomes, planned unit developments(PUDs) and 2 to 4 units financing</span></li>
                <li><span class="list-text">Owner occupied, secondhome and non-owner occupied (investment property) financing</span></li>
              </ul>
              <span class="bold">Refinancing</span>
              <ul>
                <li><span class="list-text">Fixed and Adjustable rates for 30, 20, and 15-year contracts</span></li>
                <li><span class="list-text">Conforming, Conforming Jumbo, Home Affordable Refinance Program and cash out refinancing</span></li>
                <li><span class="list-text">Single family, condominiums, townhomes, planned unit developments(PUDs) and 2 to 4 units financing</span></li>
                <li><span class="list-text">Owner occupied, second home and non-owner occupied (investment property) financing</span></li>
              </ul>
            </div>
          </div>
        </div>
        <a href="#shariahCompliantFinancing" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="shariahCompliantFinancing">
          <span class="fa expand-icon"></span><h4 class="title four bold">SHARIAH-COMPLIANT FINANCING</h4>
        </a>
        <div class="text-content left collapse" id="shariahCompliantFinancing">
          <div class="text">
            <h5 class="title five bold">Q. What do you mean by Shariah-compliant home financing?</h5>
            <p class="desc-text">
              Shariah-compliant home financing is a type of financing that does not violate the principles of Islamic law. There is a set of rules and injunctions from the Quran and Sunnah regarding what is permissible
              and not permissible when it comes to the day-to-day financial transactions of Muslims. Among the prohibitions are riba (usury / interest), gharar (speculation or contractual uncertainty) and maysir
              (gambling), for example. When you finance your home with a conventional mortgage, it requires the payment of interest (riba), which is prohibited in Islam and all monotheistic and even some non-monotheistic
               faiths. Guidance Residential's Shariah-compliant home financing program does not involve riba. The Declining Balance Co-ownership Program that Guidance Residential offers is a participatory financing
               program, which is based on equity partnership between Guidance and the customer. The following are the unique differentiating factors of Guidance Residential's program from a conventional mortgage.
            </p>
            <p class="desc-text">
              <span class="bold">Co-ownership</span><br>
              Guidance Residential and homebuyers each own a percentage of the property as co-owners. Homebuyers increase their share over a period of time through a monthly payment that results in a corresponding
               decrease in Guidance's share.
            </p>
            <p class="desc-text">
              <span class="bold">Risk sharing</span><br>
              The risk is shared if the property is lost in the case of a natural disaster, or a public service project initiated by the government forces you out of the property. In this situation the proceeds
              provided by insurance or government are shared based upon the percentage of ownership at the point of the loss. In a similar situation, conventional loan providers will apply the proceeds to pay off the loan without any allocation.
            </p>
            <p class="desc-text">
              <span class="bold">Riba-free</span><br>
              The Declining Balance Co-ownership Program does not involve payment of interest between a debtor and creditor. It is 100% riba-free.
            </p>
            <p class="desc-text">
              <span class="bold">No pre-payment penalty</span><br>
              There is no pre-payment penalty required by Guidance Residential when a homebuyer wants to pay ahead of the agreed schedule.
            </p>
            <p class="desc-text">
              <span class="bold">Capped late payment fees</span><br>
              In the event that late payments occur, Guidance Residential will only charge a capped fee equaling $50 or less, exclusively meant to cover the expenses involved in administering a late payment, rather than the conventional 5% penalty.
            </p>
            <p class="desc-text">
              <span class="bold">Non-recourse commitment</span><br>
              In the event of payment default, Guidance Residential does not pursue any of the homebuyer's other assets.
            </p>
            <h5 class="title five bold">Q. How does this program work?</h5>
            <p class="desc-text">
              Guidance Residential and the homebuyer engage in a co-ownership venture where each owns a percentage of the home, in proportion to the equity they each contributed. Homebuyers are required to make monthly
              payments to Guidance Residential over a predetermined period of time in order to completely buy out Guidance Residential's shares in the property, and own the property outright. The monthly payment consists
               of two portions; (1) an amount for the acquisition of a portion of Guidance Residential's ownership interest (Acquisition Payment) and (2) the other for the exclusive use of the entire property that
               includes Guidance's share (Profit Payment). The Acquisition Payment serves in buying Guidance Residential's shares of ownership over the predetermined period of time and the Profit Payment is akin to
                rent or use of property of another.
            </p>
            <p class="desc-text">
              For instance, if you are looking to buy a $300,000 home and you have $30,000 of your own money to contribute towards the purchase of the home, Guidance will contribute the remaining $270,000 in funds to purchase
              the home together. Guidance will purchase the property along with you and we will form a co-ownership agreement stating that we both own the property together. Then, we set up a 15, 20, or 30-year contract in
              which you make a monthly payment to Guidance Residential. A portion of that payment will go towards buying out Guidance Residential's ownership. The other portion of the monthly payment is for the exclusive
              use of the property. The utilization fee that we charge you will decrease every month as our ownership in the property decreases and the portion that goes to buying Guidance's ownership will increase by that
               same amount so that the monthly payment stays constant.
            </p>
            <p class="desc-text">
              In accordance with the terms of the co-ownership agreement, you can sell the property at any time and any profit from that sale will be 100% yours. For instance, if after 5 years, you sell this property for $350,000,
               that extra $50,000 will be all yours.
            </p>
            <h5 class="title five bold">Q. How is it different from regular loans?</h5>
            <p class="desc-text">
               Our Shariah-compliant home financing program does not constitute a debtor and creditor loan arrangement. Lending money to profit from any commercial or investment activity including the financing of
               real estate is not an acceptable method for commerce, according to Islam. Therefore, our home financing program has been modeled on the Islamic financing concept known as "Musharakah Mutanaqisa" or
               "Diminishing Partnership." The relationship between Guidance Residential and the homebuyer is that of co-owners in a property and not that of a borrower-lender. The initial financing provided by
                Guidance Residential is applied to acquire an interest in the property and not to provide a loan.
            </p>
            <p class="desc-text">
              <a href="{{route('videoLibrary')}}">Watch our FAQ video: What is the difference between Guidance's Shariah-compliant home financing program and a mortgage loan?</a>
            </p>
            <h5 class="title five bold">Q. What makes your program Halal?</h5>
            <p class="desc-text">
              The unique thing about Guidance Residential is that we brought together seven of the leading scholars in the world on Islamic finance. These scholars are all widely known for their expertise in Islamic
               legal jurisprudence in financial transactions and they come from across the globe. The Scholars help us offer a product that is both Shariah-complaint and work within each individual states' home
               protection laws. Our Shariah Supervisory Board has authored numerous fatwas on various aspects including one on the overall structure.
            </p>
            <p class="desc-text">
              <a href="{{route('videoLibrary')}}">Check out video library to find more about the program.</a>
            </p>
          </div>
        </div>
        <a href="#eligibility" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="eligibility">
          <span class="fa expand-icon"></span><h4 class="title four bold">Eligibility</h4>
        </a>
        <div class="text-content left collapse" id="eligibility">
          <div class="text">
            <h5 class="title five bold">Q. How can I know if I am eligible to apply?</h5>
            <p class="desc-text">
              You can get started by doing our online Pre-Qualification. It is a simple process to start your home financing journey. No credit check is required, and it only takes 10 minutes.
              <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_RefinancingQuestions_Eligibility_Feb2018" target="_blank">Visit this link to get Pre-Qualified.</a>
            </p>
          </div>
        </div>
        <a href="#refinancingOptions" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="refinancingOptions">
          <span class="fa expand-icon"></span><h4 class="title four bold">FINANCING OPTIONS</h4>
        </a>
        <div class="text-content left collapse" id="refinancingOptions">
          <div class="text">
            <h5 class="title five bold">Q. What types of refinancing do you offer?</h5>
            <p class="desc-text">
              Currently, Guidance Residential provides Shariah-compliant home refinancing for both primary properties (homes which are occupied by the owner) and non-owner occupied properties (also known as investment properties).
            </p>
            <p class="desc-text">
              <a href="{{ route('refinancingOptions')}}"> Visit this page to see details of the financing options we offer</a>
            </p>
          </div>
        </div>
        <a href="#refinanceProcess" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="refinanceProcess">
          <span class="fa expand-icon"></span><h4 class="title four bold">REFINANCE PROCESS</h4>
        </a>
        <div class="text-content left collapse" id="refinanceProcess">
          <div class="text">
            <h5 class="title five bold">Q. How can I get started? </h5>
            <p class="desc-text">
              You can get started by completing our online <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_RefinancingQuestions_RefinanceProcess_Feb2018" target="_blank">Pre-Qualification</a> form. It is a simple process that takes less 10 minutes to complete.
              You will need to provide us with general information that will be used to provide you with the best estimate and Pre-Qualification status.
            </p>
            <h5 class="title five bold">Q. How long does it take from application to closing? </h5>
            <p class="desc-text">
              The timeline varies case by case. On average, it takes 45 days to close a file from the day the application is taken. Timeline can be shortened to 30 days,
               dependent upon third party vendors' timeliness, customer's responsiveness and credit profile.
            </p>
          </div>
        </div>
        <a href="#preQualification" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="preQualification">
          <span class="fa expand-icon"></span><h4 class="title four bold">Pre-Qualification</h4>
        </a>
        <div class="text-content left collapse" id="preQualification">
          <div class="text">
            <h5 class="title five bold">Q. What is pre-qualification?</h5>
            <p class="desc-text">
              Pre-Qualification is the process that provides you estimates on your affordability based on the information you provide. It does not require a credit check and takes less than 10 minutes to complete.
            </p>
            <h5 class="title five bold">Q. What happens after I get Pre-Qualified?</h5>
            <p class="desc-text">
              After you complete the Pre-Qualification form, you will be assigned an expert Account Executive that will guide you through the home financing process. He or she will be able to answer any questions
              you may have to help get you pre-approved and receive conditional approval for your home purchase or to help you complete a refinance.
            </p>
          </div>
        </div>
        <a href="#applicationProcess" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="applicationProcess">
          <span class="fa expand-icon"></span><h4 class="title four bold">Application Process</h4>
        </a>
        <div class="text-content left collapse" id="applicationProcess">
          <div class="text">
            <h5 class="title five bold">Q. How does the application process work? </h5>
            <p class="desc-text">
              Once you Pre-Qualify with Guidance, your licensed Account Executive will follow up with you periodically to see if your purchase offer has been accepted.  You can also contact your Account Executive
               if your offer is accepted prior to hearing from him or her. In either situation, your Account Executive will begin the finance application process for you once your offer is accepted.
               This typically takes 10 minutes over the phone.  During the finance application process, you will need to provide a number of documents and signed paperwork before you can be approved for financing.
                This process takes an average of 45 days, but can be reduced (dependent upon third party vendors' timeliness, your credit profile and responsiveness).
            </p>
            <p class="desc-text">
              Please view the <a href="{{ route( 'refinancingApplicationChecklist' )}}">Application Checklist page</a> to know what documents will be needed to complete the application process.
            </p>
            <h5 class="title five bold">Q. What documentations are required to apply for financing?</h5>
            <p class="desc-text">
              <a href="{{ route( 'refinancingApplicationChecklist' )}}">Visit the Application Checklist page to view the documents needed to complete the application process.</a>
              Please note documentation requirements may vary based on individual situation.
            </p>
          </div>
        </div>
        <a href="#closingProcess" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="closingProcess">
          <span class="fa expand-icon"></span><h4 class="title four bold">Closing Process</h4>
        </a>
        <div class="text-content left collapse" id="closingProcess">
          <div class="text">
            <h5 class="title five bold">Q. How does the closing process work?</h5>
            <div class="desc-text">
              <span class="bold">Arrange for Closing Costs</span>
              <ul>
                <li>
                  <span class="list-text">
                    Your Account Executive will make sure that you are provided with a closing cost estimate, which is the amount you will need to bring to the closing in the form of a cashier's check or wire transfer. Avoid making large purchases and taking on new debt - such as furniture, home appliances or vehicle - so that you do not delay or stop the closing process.
                 </span>
                </li>
              </ul>
              <span class="bold">Sign the Closing Paperwork, Get the Keys and Move In!</span>
              <ul>
                <li>
                  <span class="list-text">
                    It's the big day! The closing itself and signing of paperwork typically takes 1-2 hours. Then enjoy your new home and find comfort in knowing that you became a homeowner without compromising your faith.
                  </span>
                </li>
              </ul>
            </div>
            <p class="desc-text">
              Visit the <a href="{{ route( 'refinancingProcess' )}}">Refinance Process</a> page to learn more.
            </p>
          </div>
        </div>
        <a href="#connectingAccountExecutive" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="connectingAccountExecutive">
          <span class="fa expand-icon"></span><h4 class="title four bold">CONNECTING WITH AN ACCOUNT EXECUTIVE</h4>
        </a>
        <div class="text-content left collapse" id="connectingAccountExecutive">
          <div class="text">
            <h5 class="title five bold">Q. How do I connect with an Account Executive?</h5>
            <p class="desc-text">
              After you complete Guidance's online Pre-Qualification, you will be assigned a licensed Account Executive who will verify the information you provided and assist you with the pre-approval and conditional approval process. Your Account Executive will follow up with you shortly after you Pre-Qualify; however, you will receive contact information if you wish to connect sooner.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT IS GUIDANCE RESIDENTIAL?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Guidance Residential is the largest provider of Shariah-compliant home financing in the U.S. Guidance Residential has provided over $4.6 billion in home financing to Muslim American homeowners over the last 15 years, and the company holds nearly 80% market share in the U.S. Shariah-compliant home financing industry. Headquartered in Reston, Virginia, the company was established in the D.C. Metro area in 2001 and currently, the company is operational in 29 states. We are a member of Guidance Financial Group, an affiliated global financial group, which has pioneered the creation and development of Shariah-compliant investment products for institutional investors and financial intermediaries worldwide, as well as financial products and services that address the unmet needs of millions of Muslim consumers."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT TYPES OF FINANCING DOES GUIDANCE RESIDENTIAL OFFER?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Purchase financing: Fixed and Adjustable rates for 30, 20, and 15-year contracts, Conforming and Conforming Jumbo financing, Single family, condominiums, townhomes, planned unit developments(PUDs) and 2 to 4 units financing, Owner occupied, secondhome and non-owner occupied (investment property) financing. Refinancing: Fixed and Adjustable rates for 30, 20, and 15-year contracts, Conforming, Conforming Jumbo, Home Affordable Refinance Program and cash out refinancing, Single family, condominiums, townhomes, planned unit developments(PUDs) and 2 to 4 units financing, Owner occupied, second home and non-owner occupied (investment property) financing"
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT DO YOU MEAN BY SHARIAH-COMPLIANT HOME FINANCING?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Shariah-compliant home financing is a type of financing that does not violate the principles of Islamic law. There is a set of rules and injunctions from the Quran and Sunnah regarding what is permissible and not permissible when it comes to the day-to-day financial transactions of Muslims. Among the prohibitions are riba (usury / interest), gharar (speculation or contractual uncertainty) and maysir (gambling), for example. When you finance your home with a conventional mortgage, it requires the payment of interest (riba), which is prohibited in Islam and all monotheistic and even some non-monotheistic faiths. Guidance Residentials Shariah-compliant home financing program does not involve riba. The Declining Balance Co-ownership Program that Guidance Residential offers is a participatory financing program, which is based on equity partnership between Guidance and the customer. The following are the unique differentiating factors of Guidance Residential's program from a conventional mortgage. Co-ownership: Guidance Residential and homebuyers each own a percentage of the property as co-owners. Homebuyers increase their share over a period of time through a monthly payment that results in a corresponding decrease in Guidances share. Risk sharing: The risk is shared if the property is lost in the case of a natural disaster, or a public service project initiated by the government forces you out of the property. In this situation the proceeds provided by insurance or government are shared based upon the percentage of ownership at the point of the loss. In a similar situation, conventional loan providers will apply the proceeds to pay off the loan without any allocation. Riba-free:  The Declining Balance Co-ownership Program does not involve payment of interest between a debtor and creditor. It is 100% riba-free. No pre-payment penalty: There is no pre-payment penalty required by Guidance Residential when a homebuyer wants to pay ahead of the agreed schedule. Capped late payment fees: In the event that late payments occur, Guidance Residential will only charge a capped fee equaling $50 or less, exclusively meant to cover the expenses involved in administering a late payment, rather than the conventional 5% penalty. Non-recourse commitment: In the event of payment default, Guidance Residential does not pursue any of the homebuyers other assets."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW DOES THIS PROGRAM WORK?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Guidance Residential and the homebuyer engage in a co-ownership venture where each owns a percentage of the home, in proportion to the equity they each contributed. Homebuyers are required to make monthly payments to Guidance Residential over a predetermined period of time in order to completely buy out Guidance Residentials shares in the property, and own the property outright. The monthly payment consists of two portions; (1) an amount for the acquisition of a portion of Guidance Residentials ownership interest (Acquisition Payment) and (2) the other for the exclusive use of the entire property that includes Guidances share (Profit Payment). The Acquisition Payment serves in buying Guidance Residentials shares of ownership over the predetermined period of time and the Profit Payment is akin to rent or use of property of another. For instance, if you are looking to buy a $300,000 home and you have $30,000 of your own money to contribute towards the purchase of the home, Guidance will contribute the remaining $270,000 in funds to purchase the home together. Guidance will purchase the property along with you and we will form a co-ownership agreement stating that we both own the property together. Then, we set up a 15, 20, or 30-year contract in which you make a monthly payment to Guidance Residential. A portion of that payment will go towards buying out Guidance Residentials ownership. The other portion of the monthly payment is for the exclusive use of the property. The utilization fee that we charge you will decrease every month as our ownership in the property decreases and the portion that goes to buying Guidances ownership will increase by that same amount so that the monthly payment stays constant. In accordance with the terms of the co-ownership agreement, you can sell the property at any time and any profit from that sale will be 100% yours. For instance, if after 5 years, you sell this property for $350,000, that extra $50,000 will be all yours."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW IS IT DIFFERENT FROM REGULAR LOANS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Our Shariah-compliant home financing program does not constitute a debtor and creditor loan arrangement. Lending money to profit from any commercial or investment activity including the financing of real estate is not an acceptable method for commerce, according to Islam. Therefore, our home financing program has been modeled on the Islamic financing concept known as Musharakah Mutanaqisa or Diminishing Partnership. The relationship between Guidance Residential and the homebuyer is that of co-owners in a property and not that of a borrower-lender. The initial financing provided by Guidance Residential is applied to acquire an interest in the property and not to provide a loan. Watch our FAQ video: What is the difference between Guidances Shariah-compliant home financing program and a mortgage loan?"
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT MAKES YOUR PROGRAM HALAL?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The unique thing about Guidance Residential is that we brought together seven of the leading scholars in the world on Islamic finance. These scholars are all widely known for their expertise in Islamic legal jurisprudence in financial transactions and they come from across the globe. The Scholars help us offer a product that is both Shariah-complaint and work within each individual states home protection laws. Our Shariah Supervisory Board has authored numerous fatwas on various aspects including one on the overall structure. Check out video library to find more about the program."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW CAN I KNOW IF I AM ELIGIBLE TO APPLY?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "You can get started by doing our online Pre-Qualification. It is a simple process to start your home financing journey. No credit check is required, and it only takes 10 minutes. Visit this link to get Pre-Qualified."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT TYPES OF REFINANCING DO YOU OFFER?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Currently, Guidance Residential provides Shariah-compliant home refinancing for both primary properties (homes which are occupied by the owner) and non-owner occupied properties (also known as investment properties). Visit this page to see details of the financing options we offer"
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW TO START REFINANCE PROCESS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "You can get started by completing our online Pre-Qualification form. It is a simple process that takes less 10 minutes to complete. You will need to provide us with general information that will be used to provide you with the best estimate and Pre-Qualification status."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW LONG DOES IT TAKE TO CLOSE THE REFINANCE PROCESS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The timeline varies case by case. On average, it takes 45 days to close a file from the day the application is taken. Timeline can be shortened to 30 days, dependent upon third party vendors' timeliness, customer's responsiveness and credit profile."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT IS PRE-QUALIFICATION?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Pre-Qualification is the process that provides you estimates on your affordability based on the information you provide. It does not require a credit check and takes less than 10 minutes to complete."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT HAPPENS AFTER I GET PRE-QUALIFIED?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "After you complete the Pre-Qualification form, you will be assigned an expert Account Executive that will guide you through the home financing process. He or she will be able to answer any questions you may have to help get you pre-approved and receive conditional approval for your home purchase or to help you complete a refinance."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW DOES THE APPLICATION PROCESS WORK?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Once you Pre-Qualify with Guidance, your licensed Account Executive will follow up with you periodically to see if your purchase offer has been accepted. You can also contact your Account Executive if your offer is accepted prior to hearing from him or her. In either situation, your Account Executive will begin the finance application process for you once your offer is accepted. This typically takes 10 minutes over the phone. During the finance application process, you will need to provide a number of documents and signed paperwork before you can be approved for financing. This process takes an average of 45 days, but can be reduced (dependent upon third party vendors timeliness, your credit profile and responsiveness). Please view the Application Checklist page to know what documents will be needed to complete the application process."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "WHAT DOCUMENTATIONS ARE REQUIRED TO APPLY FOR FINANCING?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Visit the Application Checklist page to view the documents needed to complete the application process. Please note documentation requirements may vary based on individual situation."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW DOES THE CLOSING PROCESS WORK?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Arrange for Closing Costs: Your Account Executive will make sure that you are provided with a closing cost estimate, which is the amount you will need to bring to the closing in the form of a cashiers check or wire transfer. Avoid making large purchases and taking on new debt - such as furniture, home appliances or vehicle - so that you do not delay or stop the closing process. Sign the Closing Paperwork, Get the Keys and Move In: Its the big day! The closing itself and signing of paperwork typically takes 1-2 hours. Then enjoy your new home and find comfort in knowing that you became a homeowner without compromising your faith. Visit the Refinance Process page to learn more."
    }
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Question",
    "text": "HOW DO I CONNECT WITH AN ACCOUNT EXECUTIVE?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "After you complete Guidances online Pre-Qualification, you will be assigned a licensed Account Executive who will verify the information you provided and assist you with the pre-approval and conditional approval process. Your Account Executive will follow up with you shortly after you Pre-Qualify; however, you will receive contact information if you wish to connect sooner."
    }
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_RefinanceResources'])
@endsection
