@extends('layouts.default')

@section('title', 'E-Books')

@section('content')
  <div class="container ebooks">
    <div class="main-heading">
      <h1 class="title">
        DOWNLOAD E-BOOKS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Browse Guidance Residential's e-book library and download e-books to learn about the home buying and refinance process.
      </p>
    </div>
    <div class="ebook-list">
      <div class="split-content-card">
        <div class="image-container background-image"></div>
        <div class="main-heading ">
          <h3 class="title three">
            First Time Homebuyer's Guide
          </h3>
          <span class="seperate-line sub"></span>
          <div class="desc-text">
            <div class="list-step">
              <span class="app-circle">
                <span class="fa fa-check"></span>
              </span>
              <span class="text">What factors affect home cost?</span>
            </div>
            <div class="list-step">
              <span class="app-circle">
                <span class="fa fa-check"></span>
              </span>
              <span class="text">How to save for your first home.</span>
            </div>
            <div class="list-step">
              <span class="app-circle">
                <span class="fa fa-check"></span>
              </span>
              <span class="text">Choose an alternative home financing option.</span>
            </div>
            <div class="list-step">
              <span class="app-circle">
                <span class="fa fa-check"></span>
              </span>
              <span class="text">Shopping for a home with an agent</span>
            </div>
            <div class="list-step">
              <span class="app-circle">
                <span class="fa fa-check"></span>
              </span>
              <span class="text">Close on your new home.</span>
            </div>
         </div>
         <a class="main-button" href="{{ mix_remote('docs/GR_Ebook_First_Time_Home_Buyers_Jan2018.pdf') }}" target="_blank">
           <span class="fa fa-cloud-download"></span>Download
         </a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "DownloadAction",
    "object": {
      "@type": "Book",
      "url":"{{ Request::url() }}",
      "name": "Guidance Residential's e-book"
    }
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Ebooks'])
@endsection
