@extends('layouts.default')

@section('title', 'Sell With Guidance Realty')

@section('content')
<div class="middle">
  <div class="container sell-with-guidance">
    <div class="main-heading">
      <h1 class="title">
        How to Save With GuidanceRealty.com
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Home sellers get a $500 commission credit when they work with a Platinum-Level GuidanceRealty.com Partner Agent and list their home on the GuidanceRealty.com property engine.
      </p>
    </div>
    <div class="image-container"></div>
  </div>
  <div class="how-it-works">
    <div class="main-heading">
      <h1 class="title six">
        How it works
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        List your home with a Platinum Agent and receive a $500 commission credit at closing from the agent.
      </p>
    </div>
    <div class="realty-steps">
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-id-card-o"></span>
        </div>
        <h4 class="title three">FIND AN AGENT</h4>
        <p class="desc-text">Find a GuidanceRealty.com Platinum-Level Agent who has a strong grasp on the local real estate market in your area and helps you assess the right pricing for your home.</p>
        <div class="button">
          <a class="main-button" href="{{ config('urls.grealty.find-a-platinum-agent') }}">Find An Agent</a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-building-o"></span>
        </div>
        <h4 class="title three">LIST YOUR PROPERTY</h4>
        <p class="desc-text">Our robust property search engine allows you to showcase your property to Pre-Qualified customers who are ready to buy a home.</p>
        <div class="button">
          <a class="main-button" data-toggle="modal" data-target="#listYourHomeModal">List Your Property</a>
        </div>
      </div>
    </div>
    <p class="desc-text italic">
      <span class="bold">New York consumers and agents:</span> ACCESS TO FEATURES OF GUIDANCE REALTY PROGRAM IS RESTRICTED PENDING APPROVAL BY THE NY DEPARTMENT OF FINANCIAL SERVICES. COMMISSION CREDIT PROGRAM FOR SELLERS NOT AVAILABLE IN KS, NJ, OR AND TN.<a href="{{ config('urls.grealty.terms-of-use') }}">CLICK TO VIEW OTHER RESTRICTIONS THAT MAY APPLY.</a>
    </p>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "HOW TO SAVE WITH GUIDANCEREALTY.COM",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Home sellers get a $500 commission credit when they work with a Platinum-Level GuidanceRealty.com Partner Agent and list their home on the GuidanceRealty.com property engine. List your home with a Platinum Agent and receive a $500 commission credit at closing from the agent. FIND AN AGENT: Find a GuidanceRealty.com Platinum-Level Agent who has a strong grasp on the local real estate market in your area and helps you assess the right pricing for your home. LIST YOUR PROPERTY: Our robust property search engine allows you to showcase your property to Pre-Qualified customers who are ready to buy a home."
  }
  </script>
@endsection
