@extends('layouts.default')

@section('title', 'Why Guidance?')

@section('content')
  <div class="container why-guidance">
    <div class="main-heading">
      <h1 class="title">
        The Guidance Difference
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Explore the difference between Guidance Residential's Declining Balance Co-ownership Program and other home financing options.
      </p>
    </div>
    <div class="compare-table wrapper">
      <div class="table-container">
        <table class="table compare-guidance">
          <tbody>
            <tr>
              <td></td>
              <td><h3 class="title three">Conventional Mortgages</h3></td>
              <td><img src="{{ mix_remote('images/GR_LOGO.png') }}" alt="dev"></td>
              <td><h3 class="title three">Bank Owned Entities</h3></td>
            </tr>
            <tr>
              <td>Co-Ownership
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="Guidance Residential and home buyers each own a percentage as co-owners. Home buyers increase their share over a period of time through a monthly payment.">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>No</td>
            </tr>
            <tr>
              <td>Riba-free
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="The Declining Balance Co-ownership Program, does not involve payment of interest. It is 100% riba-free.">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>No (Banks involve in interest bearing loans)</td>
            </tr>
            <tr>
              <td>Non-recourse commitment
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="In many conventional loan programs, other assets which may include your various investments - such as personal savings accounts, pensions and college funds - are all subject to seizure in the event of a default. The Declining Balance Co-ownership Program has a non-recourse clause, meaning that in the event the customer defaults, Guidance Residential does not have recourse against the customer's other assets. The customer is only at risk for his/her equity position in the property.">
                </span>
              </td>
              <td>No (Variable with Conditions)</td>
              <td>Yes</td>
              <td>No</td>
            </tr>
            <tr>
              <td>Capped late payment fees
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="Traditionally, late fees are calculated by conventional providers as a percentage of the late monthly amount due from the customer and are often realized as profit. There are no interest charges on late payments under our program. With our Shariah Supervisory Board's approval, late payments are only subject to a capped fee that covers the expenses involved in administering a late payment. Under Shariah principles, profiting off of such a situation is prohibited.">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>Variable with Conditions</td>
            </tr>
            <tr>
              <td>Risk sharing
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="The risk is shared if the property is lost in the case of a natural disaster, or a public service project initiated by the government forces you out of the property. In this situation the proceeds provided by insurance or government are shared based upon the percentage of ownership at the point of the loss. In a similar situation, conventional loan providers will apply the proceeds to pay off the loan without any allocation.">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>No</td>
            </tr>
            <tr>
              <td>No pre-payment penalty
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="There is no pre-payment penalty required by Guidance Residential when a home buyer wants to pay ahead of the agreed schedule.">
                </span>
              </td>
              <td>No (Variable with Conditions)</td>
              <td>Yes</td>
              <td>Variable with Conditions</td>
            </tr>
            <tr>
              <td>Independent Shariah Board
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="Guidance Residential has an independent Shariah Board comprised of notable scholarly authority headed by Justice Muhammad Taqi Usmani, who is also the Chairman of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI).">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>No</td>
            </tr>
            <tr>
              <td>Affiliation with a Bank
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="Guidance Residential is a 'Non-Bank' Shariah compliant home financing provider. The company is internally funded and is not affiliated with a Bank.">
                </span>
              </td>
              <td>Yes (Bank Owned)</td>
              <td>No (Internally funded)</td>
              <td>Yes (Bank Owned)</td>
            </tr>
            <tr>
              <td>Equity Partnership
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-html="true"
                  data-content="&lt;p&gt;Guidance Residential's &lt;b&gt;Declining Balance Co-ownership Program&lt;/b&gt; is &lt;b&gt;a Musharakah&lt;/b&gt;, or an &lt;b&gt;equity partnership&lt;/b&gt; contract through which homeowners receive the benefits of ownership even as they continue making acquisition payments. &lt;b&gt;Co-ownership conforms to the Islamic ideal of sharing risk&lt;/b&gt; and reward in financial transactions. Other programs for home finance in the United States, based on either Ijarah or Murabahah, are either less suitable or outright prohibited.&lt;/p&gt;&lt;p&gt;It is clear that &lt;b&gt;the lender-borrower relationship on which conventional home loans and mortgages are based is impermissible&lt;/b&gt;, especially when interest is present. But, it should also be clear that home acquisitions based solely on Ijarah, with its tenant-landlord relationship, are less than the Islamic ideal of sharing risk and reward, even if they comply with the letter of Shariah.&lt;/p&gt;&lt;p&gt;While Murabaha is a permitted contract for buying and selling, it is prohibited for use as an instrument for home finance in the United States when the Murabaha contract, once concluded, is sold to other investors. This is because &lt;b&gt;Murabaha creates a debt;&lt;/b&gt; and &lt;b&gt;the sale of debt is prohibited&lt;/b&gt; since Shariah treats debt as a personal responsibility while prohibiting trafficking in the debt of others.&lt;/p&gt;&lt;p&gt;Guidance may introduce other investors, such as Freddie Mac, to take a co-ownership stake in a property it co-owns. In doing so, Guidance continues to service its customers' needs to their satisfaction, while ensuring that investors will always be bound by their rights and obligations as coowners in the property. For purposes of clarification, &lt;b&gt;the sale of a Shariah-compliant Musharakah contract to Freddie Mac (which is an investor and NOT a bank) is perfectly Shariah-compliant because (1) it is a sale of a real asset and (2) the terms of the sale state unequivocally that the contract between the buyer/home, owner/co-owner and the investor (Freddie Mac or any other) shall remain unaltered, i.e., will continue to comply with Shariah in every way.&lt;/b&gt;&lt;/p&gt;">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>No</td>
            </tr>
            <tr>
              <td>Endorsed By AMJA
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-html="true"
                  data-content="The Assembly of Muslim Jurists of America (AMJA) is an independent, nonpartisan, non-profit organization that was established to address the needs of an Islamic Jurisprudence body for Muslims in the United States. The AMJA fatwa committee met to issue a resolution concerning Islamic home financing in the U.S. Their ruling on Guidance Residential was: &lt;b&gt;&#34;Guidance Residential's Declining Balance Co-ownership Program is a 'permissible path' to Muslim Americans in need of home financing.&#34;&lt;/b&gt;">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>No</td>
            </tr>
            <tr>
              <td>Equal Protection with LLC structure
                <span class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="right"
                  data-content="An LLC is formed to create the co-ownership structure that allows Guidance Residential to repudiate trading in debt and offer consumers a truly Shariah compliant home financing contract with benefits such as capped late payment fees, no pre-payment penalties and non-recourse policies where personal assets are safe in case of default. These benefits outweigh the small fee ($18.75) charged for covering the actual transaction costs of administrating the LLC structure.">
                </span>
              </td>
              <td>No</td>
              <td>Yes</td>
              <td>No</td>
            </tr>
            <tr>
              <td></td>
              <td><a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_WhyGuidance_ConventionalMortgages_Feb2018" target="_blank" class="main-button">Switch to guidance</a></td>
              <td><a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_WhyGuidance_GuidanceResidential_Feb2018" target="_blank" class="main-button gold">Pre Qualify Now</a></td>
              <td><a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_WhyGuidance_BankOwnedEntities_Feb2018" target="_blank" class="main-button">Switch to guidance</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="single-video-section">
    <div class="main-heading">
      <h1 class="title">
        Watch a video on the topic
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        How does Islamic financing work?
      </p>
    </div>
    <div class="video-container why-guidance">
      <div class="video" data-video-id="DZeE3FPWza0">
        <img src="{{ mix_remote('images/whyGuidance/WhyGuidance.jpg') }}" alt="HOW DOES ISLAMIC HOME FINANCING WORK" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "How Does Islamic Home Finance Work? (2013)",
    "description": "The current financial system works like a factory that sells money for more money, creating crisis after crisis. Is Islamic home finance a real alternative to traditional mortgage financing? Watch the video and learn about Guidance Residential's faith-based home financing program. This co-ownership program uses a unique non-lending method to help people of all faiths fulfill their dream of owning a home.",
    "uploadDate": "2013-03-21T07:58:49.000Z",
    "interactionCount": "802401",
    "duration": "PT2M34S",
    "embedURL": "https://youtube.googleapis.com/v/DZeE3FPWza0",
    "thumbnailURL": "https://i.ytimg.com/vi/DZeE3FPWza0/hqdefault.jpg"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Whyguidance'])
@endsection
