@extends('layouts.default')

@section('title')
Find An Account Executive{{ (empty($selected_state) && !empty($states[$selected_state]) ? '' : ' in '.$states[$selected_state]) }}
@endsection

@section('content')
  <div class="container find-account-executive">
    <div class="main-heading">
      <h1 class="title">
        Find an Account Executive
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Select your property state to get in touch with one of our licensed Account Executives.
      </p>
    </div>
    <div class="account-executives-wrapper">
      <div class="acc-executive-buttons tab-buttons">
        @foreach ($states as $state => $state_name)
          <a href="{{ route('account-executive.index', ['state' => $state]) }}" class="main-button {{ ($selected_state == $state ? 'active' : '') }}">{{ $state_name }}</a>
        @endforeach
      </div>
      <div class="executive-list tab-content active">
        @if (empty($aes))
          <p>There are no active Account Executives licensed in {{ $states[$selected_state] }}. Please try selecting another state.</p>
        @else
          @foreach ($aes as $ae)
            <div class="acc-executive">
              <div class="app-circle">
                @if ($ae->profile_image)
                  <img src="{{ $ae->profile_image }}" alt="{{ $ae->first_name }} {{ $ae->last_name }}">
                @else
                  <span class="fa fa-user"></span>
                @endif
              </div>
              <h3 class="title three">{{ $ae->first_name }} {{ $ae->last_name }}</h3>
              <h4 class="title four bold">{{ $ae->title }}</h4>
              <span class="seperate-line"></span>
              <a href="{{ route('account-executive.show', ['ae' => $ae->profile_url]) }}" class="main-button">Connect with me</a>
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div>
@endsection
