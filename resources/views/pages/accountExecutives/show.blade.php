@extends('layouts.default')

@section('title')
{{ $ae->title }} - {{ $ae->first_name }} {{ $ae->last_name }}
@endsection

@section('content')
  <div class="container account-executive-profile">
    <div class="main-heading">
      <h1 class="title">
        {{ $ae->title }}
      </h1>
      <span class="seperate-line"></span>
    </div>
  </div>
  <div class="executive-info">
    <div class="executive-card personal-info">
      <div class="app-circle">
        @if ($ae->profile_image)
          <img src="{{ $ae->profile_image }}" alt="{{ $ae->first_name }} {{ $ae->last_name }}">
        @else
          <span class="fa fa-user"></span>
        @endif
      </div>
      <div class="name">
        <h2 class="title three bold">{{ $ae->first_name }} {{ $ae->last_name }}</h2>
        <h3 class="title four">{{ implode(', ', array_filter([$ae->address_city, $ae->address_state])) }}</h3>
      </div>
      <div class="line" title="License Number">
        <div class="fa fa-newspaper-o"></div><div class="desc-text">NMLS License # {{ $ae->nmls_license_number }}</div>
      </div>
      @if (!empty($ae->phone_mobile))
      <a class="line" href="tel:{{ preg_replace('/[^0-9]/', '', $ae->phone_mobile) }}" title="Mobile Phone">
        <div class="fa fa-phone"></div><div class="desc-text">{{ $ae->phone_mobile }}</div>
      </a>
      @endif
      <a class="line" href="mailto:{{ $ae->email1 }}" title="Email Address">
        <div class="fa fa-envelope-o"></div><div class="desc-text">{{ $ae->email1 }}</div>
      </a>
      <div class="line" title="Office Location">
        <div class="fa fa-building"></div>
        <div class="desc-text">
          @foreach(array_filter(explode("\n", $ae->address_street)) as $addr_piece)
            {{ $addr_piece }}<br>
          @endforeach
          {{ implode(', ', array_filter([$ae->address_city, $ae->address_state])) }} {{ $ae->address_postalcode }}
         </div>
      </div>
      @if (!empty($ae->phone_fax))
        <a class="line" href="tel:{{ preg_replace('/[^0-9]/', '', $ae->phone_fax) }}" title="Fax Number">
          <div class="fa fa-print"></div><div class="desc-text">{{ $ae->phone_fax }}</div>
        </a>
      @endif
    </div>
    <div class="executive-card register">
      <div class="title three bold">
        Get pre-qualified online today
      </div>
      <form action="{{ config('urls.customer-portal.prequalify') }}" id="start_pq_ae_profile_id" name="start_pq_ae_profile" method="get" target="_blank">
        <input type="hidden" name="referral" value="ae-profile-page-{{ $ae->user_name }}" />
        <input type="hidden" name="preferred_ae_id_c" value="{{ $ae->id }}" />
          <div class="line">
            <div class="desc-text">
              Enter your e-mail address:
            </div>
            <input type="email" name="email_address" id="accExecutiveRegisterEmail" required>
            <div class="desc-text email-error-message">
              Email address cannot be empty*
            </div>
          </div>
          <div class="line">
            <div class="desc-text">
              Are you looking to:
            </div>
            <div class="button-container">
              <input class="main-button" type="submit" name="contract_type" value="Purchase" />
              <input class="main-button" type="submit" name="contract_type" value="Refinance" />
            </div>
          </div>
      </form>
      <div class="line no-border">
        <h5 class="title five skyblue">
          Are you a realtor interested in participating with {{ $ae->first_name }}?
        </h5>
        <a class="main-button" href="{{ config('urls.grealty.agent-signup') }}" target="_blank">Register Today</a>
      </div>
    </div>
  </div>
  @if (!empty($ae->testimonials))
  <div class="customer-testimonials">
    <div class="main-heading">
      <h3 class="title three bold">Customer Testimonials For {{ $ae->first_name }}</h3>
      <span class="seperate-line sub"></span>
    </div>
    <div class="testimonial-list tab-content active center">
      @foreach ($ae->testimonials as $testimonial)
      <div class="testimonial content rectangle">
        <div class="icon"><span class="fa fa-quote-left"></span></div>
        <p class="desc-text quote italic">{{ $testimonial['body'] }}</p>
        <div class="text-right">
          @if (!empty($testimonial['valediction']))<h5 class="desc-text">{{ $testimonial['valediction'] }}</h5>@endif
          @if (!empty($testimonial['author']))<h5 class="sub-text">{{ $testimonial['author'] }}</h5>@endif
        </div>
      </div>
      @endforeach
    </div>
  </div>
  @endif
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "{{ $ae->first_name }} {{ $ae->last_name }}",
    "jobTitle": "{{ $ae->title }}",
    "url": "{{ Request::url() }}",
    @if ($ae->profile_image)
      "image" : "{{ $ae->profile_image }}",
    @endif
    "description": "NMLS License # {{ $ae->nmls_license_number }}"
  }
  </script>
@endsection
