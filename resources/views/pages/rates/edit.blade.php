@extends('layouts.default')

@section('title', 'Edit Rates')

@section('content')
<div class="middle">
  <div class="container rates-edit">
    <div class="main-heading">
      <div class="title">
        Edit Rates
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Edit the rates that show on the rates page and in the mobile app.
      </p>
    </div>
    <div class="rates-wrapper">
      @if ($save_result === true)
        <p class="bg-success" style="padding: 20px;">Rates saved successfully.</p>
      @elseif ($save_result === false || $errors->any())
          <p class="bg-danger" style="padding: 20px;">
            There was error saving your rates.<br/>
              @if ($errors->any())
                @foreach ($errors->all() as $error)
                  {{ $error }}<br/>
                @endforeach
              @endif
          </p>
      @endif
      <form method="POST" action="{{ route('rates.update') }}">
        {{ csrf_field() }}
        <h2>Purchase</h2>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>&nbsp;</th>
              <th>Guidance</th>
              <th>Quicken</th>
              <th>Citi</th>
              <th>Wells Fargo</th>
              <th>Bank of America</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Profit Rates</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.purchase_profit_rate')) has-error @endif"><input name="rates[guidance-residential][purchase_profit_rate]" value="{{ old('rates.guidance-residential.purchase_profit_rate', sprintf('%0.3f', $rates['guidance-residential']['purchase_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.purchase_profit_rate')) has-error @endif"><input name="rates[quicken-loans][purchase_profit_rate]" value="{{ old('rates.quicken-loans.purchase_profit_rate', sprintf('%0.3f', $rates['quicken-loans']['purchase_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.citi.purchase_profit_rate')) has-error @endif"><input name="rates[citi][purchase_profit_rate]" value="{{ old('rates.citi.purchase_profit_rate', sprintf('%0.3f', $rates['citi']['purchase_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.purchase_profit_rate')) has-error @endif"><input name="rates[wells-fargo][purchase_profit_rate]" value="{{ old('rates.wells-fargo.purchase_profit_rate', sprintf('%0.3f', $rates['wells-fargo']['purchase_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.boa.purchase_profit_rate')) has-error @endif"><input name="rates[boa][purchase_profit_rate]" value="{{ old('rates.boa.purchase_profit_rate', sprintf('%0.3f', $rates['boa']['purchase_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
            </tr>
            <tr>
              <th>APR</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.purchase_apr')) has-error @endif"><input name="rates[guidance-residential][purchase_apr]" value="{{ old('rates.guidance-residential.purchase_apr', sprintf('%0.3f', $rates['guidance-residential']['purchase_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.purchase_apr')) has-error @endif"><input name="rates[quicken-loans][purchase_apr]" value="{{ old('rates.quicken-loans.purchase_apr', sprintf('%0.3f', $rates['quicken-loans']['purchase_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.citi.purchase_apr')) has-error @endif"><input name="rates[citi][purchase_apr]" value="{{ old('rates.citi.purchase_apr', sprintf('%0.3f', $rates['citi']['purchase_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.purchase_apr')) has-error @endif"><input name="rates[wells-fargo][purchase_apr]" value="{{ old('rates.wells-fargo.purchase_apr', sprintf('%0.3f', $rates['wells-fargo']['purchase_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.boa.purchase_apr')) has-error @endif"><input name="rates[boa][purchase_apr]" value="{{ old('rates.boa.purchase_apr', sprintf('%0.3f', $rates['boa']['purchase_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
            </tr>
            <tr>
              <th>Discount Points</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.purchase_discount_points')) has-error @endif"><input name="rates[guidance-residential][purchase_discount_points]" value="{{ old('rates.guidance-residential.purchase_discount_points', round($rates['guidance-residential']['purchase_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.purchase_discount_points')) has-error @endif"><input name="rates[quicken-loans][purchase_discount_points]" value="{{ old('rates.quicken-loans.purchase_discount_points', round($rates['quicken-loans']['purchase_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.citi.purchase_discount_points')) has-error @endif"><input name="rates[citi][purchase_discount_points]" value="{{ old('rates.citi.purchase_discount_points', round($rates['citi']['purchase_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.purchase_discount_points')) has-error @endif"><input name="rates[wells-fargo][purchase_discount_points]" value="{{ old('rates.wells-fargo.purchase_discount_points', round($rates['wells-fargo']['purchase_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.boa.purchase_discount_points')) has-error @endif"><input name="rates[boa][purchase_discount_points]" value="{{ old('rates.boa.purchase_discount_points', round($rates['boa']['purchase_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
            </tr>
            <tr>
              <th>Origination Fee</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.purchase_origin_fee')) has-error @endif"><input name="rates[guidance-residential][purchase_origin_fee]" value="{{ old('rates.guidance-residential.purchase_origin_fee', round($rates['guidance-residential']['purchase_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.purchase_origin_fee')) has-error @endif"><input name="rates[quicken-loans][purchase_origin_fee]" value="{{ old('rates.quicken-loans.purchase_origin_fee', round($rates['quicken-loans']['purchase_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.citi.purchase_origin_fee')) has-error @endif"><input name="rates[citi][purchase_origin_fee]" value="{{ old('rates.citi.purchase_origin_fee', round($rates['citi']['purchase_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.purchase_origin_fee')) has-error @endif"><input name="rates[wells-fargo][purchase_origin_fee]" value="{{ old('rates.wells-fargo.purchase_origin_fee', round($rates['wells-fargo']['purchase_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.boa.purchase_origin_fee')) has-error @endif"><input name="rates[boa][purchase_origin_fee]" value="{{ old('rates.boa.purchase_origin_fee', round($rates['boa']['purchase_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
            </tr>
            <tr>
              <th>Total Fees</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.purchase_total_fee')) has-error @endif"><input name="rates[guidance-residential][purchase_total_fee]" value="{{ old('rates.guidance-residential.purchase_total_fee', round($rates['guidance-residential']['purchase_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.purchase_total_fee')) has-error @endif"><input name="rates[quicken-loans][purchase_total_fee]" value="{{ old('rates.quicken-loans.purchase_total_fee', round($rates['quicken-loans']['purchase_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.citi.purchase_total_fee')) has-error @endif"><input name="rates[citi][purchase_total_fee]" value="{{ old('rates.citi.purchase_total_fee', round($rates['citi']['purchase_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.purchase_total_fee')) has-error @endif"><input name="rates[wells-fargo][purchase_total_fee]" value="{{ old('rates.wells-fargo.purchase_total_fee', round($rates['wells-fargo']['purchase_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.boa.purchase_total_fee')) has-error @endif"><input name="rates[boa][purchase_total_fee]" value="{{ old('rates.boa.purchase_total_fee', round($rates['boa']['purchase_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
            </tr>
          </tbody>
        </table>
        <hr/>

        <h2>Refinance</h2>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>&nbsp;</th>
              <th>Guidance</th>
              <th>Quicken</th>
              <th>Citi</th>
              <th>Wells Fargo</th>
              <th>Bank of America</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Profit Rates</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.refinance_profit_rate')) has-error @endif"><input name="rates[guidance-residential][refinance_profit_rate]" value="{{ old('rates.guidance-residential.refinance_profit_rate', sprintf('%0.3f', $rates['guidance-residential']['refinance_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.refinance_profit_rate')) has-error @endif"><input name="rates[quicken-loans][refinance_profit_rate]" value="{{ old('rates.quicken-loans.refinance_profit_rate', sprintf('%0.3f', $rates['quicken-loans']['refinance_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.citi.refinance_profit_rate')) has-error @endif"><input name="rates[citi][refinance_profit_rate]" value="{{ old('rates.citi.refinance_profit_rate', sprintf('%0.3f', $rates['citi']['refinance_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.refinance_profit_rate')) has-error @endif"><input name="rates[wells-fargo][refinance_profit_rate]" value="{{ old('rates.wells-fargo.refinance_profit_rate', sprintf('%0.3f', $rates['wells-fargo']['refinance_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.boa.refinance_profit_rate')) has-error @endif"><input name="rates[boa][refinance_profit_rate]" value="{{ old('rates.boa.refinance_profit_rate', sprintf('%0.3f', $rates['boa']['refinance_profit_rate'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
            </tr>
            <tr>
              <th>APR</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.refinance_apr')) has-error @endif"><input name="rates[guidance-residential][refinance_apr]" value="{{ old('rates.guidance-residential.refinance_apr', sprintf('%0.3f', $rates['guidance-residential']['refinance_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.refinance_apr')) has-error @endif"><input name="rates[quicken-loans][refinance_apr]" value="{{ old('rates.quicken-loans.refinance_apr', sprintf('%0.3f', $rates['quicken-loans']['refinance_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.citi.refinance_apr')) has-error @endif"><input name="rates[citi][refinance_apr]" value="{{ old('rates.citi.refinance_apr', sprintf('%0.3f', $rates['citi']['refinance_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.refinance_apr')) has-error @endif"><input name="rates[wells-fargo][refinance_apr]" value="{{ old('rates.wells-fargo.refinance_apr', sprintf('%0.3f', $rates['wells-fargo']['refinance_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
              <td class="form-group @if ($errors->has('rates.boa.refinance_apr')) has-error @endif"><input name="rates[boa][refinance_apr]" value="{{ old('rates.boa.refinance_apr', sprintf('%0.3f', $rates['boa']['refinance_apr'] ?? 0)) }}" type="text" class="form-control" placeholder="0.000" pattern="[0-9]+\.[0-9]{3}" required title="Must be a decimal number with exactly three decimal places."></td>
            </tr>
            <tr>
              <th>Discount Points</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.refinance_discount_points')) has-error @endif"><input name="rates[guidance-residential][refinance_discount_points]" value="{{ old('rates.guidance-residential.refinance_discount_points', round($rates['guidance-residential']['refinance_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.refinance_discount_points')) has-error @endif"><input name="rates[quicken-loans][refinance_discount_points]" value="{{ old('rates.quicken-loans.refinance_discount_points', round($rates['quicken-loans']['refinance_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.citi.refinance_discount_points')) has-error @endif"><input name="rates[citi][refinance_discount_points]" value="{{ old('rates.citi.refinance_discount_points', round($rates['citi']['refinance_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.refinance_discount_points')) has-error @endif"><input name="rates[wells-fargo][refinance_discount_points]" value="{{ old('rates.wells-fargo.refinance_discount_points', round($rates['wells-fargo']['refinance_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.boa.refinance_discount_points')) has-error @endif"><input name="rates[boa][refinance_discount_points]" value="{{ old('rates.boa.refinance_discount_points', round($rates['boa']['refinance_discount_points'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
            </tr>
            <tr>
              <th>Origination Fee</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.refinance_origin_fee')) has-error @endif"><input name="rates[guidance-residential][refinance_origin_fee]" value="{{ old('rates.guidance-residential.refinance_origin_fee', round($rates['guidance-residential']['refinance_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.refinance_origin_fee')) has-error @endif"><input name="rates[quicken-loans][refinance_origin_fee]" value="{{ old('rates.quicken-loans.refinance_origin_fee', round($rates['quicken-loans']['refinance_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.citi.refinance_origin_fee')) has-error @endif"><input name="rates[citi][refinance_origin_fee]" value="{{ old('rates.citi.refinance_origin_fee', round($rates['citi']['refinance_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.refinance_origin_fee')) has-error @endif"><input name="rates[wells-fargo][refinance_origin_fee]" value="{{ old('rates.wells-fargo.refinance_origin_fee', round($rates['wells-fargo']['refinance_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.boa.refinance_origin_fee')) has-error @endif"><input name="rates[boa][refinance_origin_fee]" value="{{ old('rates.boa.refinance_origin_fee', round($rates['boa']['refinance_origin_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
            </tr>
            <tr>
              <th>Total Fees</th>
              <td class="form-group @if ($errors->has('rates.guidance-residential.refinance_total_fee')) has-error @endif"><input name="rates[guidance-residential][refinance_total_fee]" value="{{ old('rates.guidance-residential.refinance_total_fee', round($rates['guidance-residential']['refinance_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.quicken-loans.refinance_total_fee')) has-error @endif"><input name="rates[quicken-loans][refinance_total_fee]" value="{{ old('rates.quicken-loans.refinance_total_fee', round($rates['quicken-loans']['refinance_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.citi.refinance_total_fee')) has-error @endif"><input name="rates[citi][refinance_total_fee]" value="{{ old('rates.citi.refinance_total_fee', round($rates['citi']['refinance_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.wells-fargo.refinance_total_fee')) has-error @endif"><input name="rates[wells-fargo][refinance_total_fee]" value="{{ old('rates.wells-fargo.refinance_total_fee', round($rates['wells-fargo']['refinance_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
              <td class="form-group @if ($errors->has('rates.boa.refinance_total_fee')) has-error @endif"><input name="rates[boa][refinance_total_fee]" value="{{ old('rates.boa.refinance_total_fee', round($rates['boa']['refinance_total_fee'] ?? 0)) }}" type="text" class="form-control" placeholder="0000" pattern="[\-]?[0-9]+" required title="Must be a whole number only. No commas, dollar signs, or decimals. Negative numbers are allowed."></td>
            </tr>
          </tbody>
        </table>
        <hr/>

        <input class="main-button" type="submit" name="saveRates" value="Save Rates" />
      </form>
      <br/><br/>
    </div>
  </div>
</div>
@endsection
