@extends('layouts.default')

@section('title', ((empty($rate_default_type) || $rate_default_type == 'purchase') ? 'Purchase' : 'Refinance').' Rates')

@section('content')
<div class="middle">
  <div class="container purchase-rates">
    <div class="main-heading">
      <div class="title">
        Our Competitive Rates
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Affordable, competitive pricing, and dedicated service demonstrate our leadership in the U.S. Islamic Home Financing Market.
      </p>
    </div>
    <div class="rates-wrapper">
      <div class="rates-buttons tab-buttons">
        <a name="purchase" class="main-button {{ ((empty($rate_default_type) || $rate_default_type == 'purchase') ? 'active' : '') }}">Purchase Rates</a>
        <a name="refinance" class="main-button {{ ((!empty($rate_default_type) && $rate_default_type == 'refinance') ? 'active' : '') }}">Refinance Rates</a>
      </div>
      <a href="#ratesConfig" class="assumption-accordion" data-toggle="collapse"  aria-expanded="false" aria-controls="ratesConfig">
        <h4 class="title four bold">Below values are for a $250,000 home as of {{ Carbon\Carbon::parse($rates['guidance-residential']['rate_date'])->format('M j, Y') }}. Click here for additional assumptions.</h4>
      </a>
      <div class="configurations collapse" id="ratesConfig">
        <div class="item">
          <span class="desc-text">Effective Date</span>
          <div class="split-input-button">
            <input type="text" name="effectiveDate" value="{{ Carbon\Carbon::parse($rates['guidance-residential']['rate_date'])->format('M j, Y') }}">
            <span class="button-box">Date</span>
          </div>
        </div>
        <div class="item">
          <span class="desc-text">Financing Program</span>
          <div class="split-input-button">
            <input type="text" name="finYears" value="30YR Fixed">
            <span class="button-box">Years</span>
          </div>
        </div>
        <div class="item">
          <span class="desc-text">Financing Amount</span>
          <div class="split-input-button">
            <input type="text" name="finAmount" value="200,000">
            <span class="button-box">$</span>
          </div>
        </div>
        <div class="item">
          <span class="desc-text">"LTV" - CTV</span>
          <div class="split-input-button">
            <input type="number" name="ltvCtv" value="80">
            <span class="button-box">%</span>
          </div>
        </div>
        <div class="item">
          <span class="desc-text">Purchasing Price</span>
          <div class="split-input-button">
            <input type="text" name="purchasePrice" value="200,000">
            <span class="button-box">$</span>
          </div>
        </div>
        <div class="item">
          <span class="desc-text">No. of Months</span>
          <div class="split-input-button">
            <input type="text" name="numMonths" value="360">
            <span class="button-box">Months</span>
          </div>
        </div>
      </div>
      <div class="rates-content tab-content purchase {{ ((empty($rate_default_type) || $rate_default_type == 'purchase') ? 'active' : '') }}">
        <div class="rates-table-wrapper">
          <table class="rate-table bold">
            <tr>
              <th class="blue">
                <span>Purchase Rates</span><br>
                <span class="golden">30 years fixed</span>
              </th>
            </tr>
            <tr>
              <td>Profit Rates</td>
            </tr>
            <tr>
              <td>APR</td>
            </tr>
            <tr>
              <td>Discount Points</td>
            </tr>
            <tr>
              <td>Origination Fee</td>
            </tr>
            <tr>
              <td>Total Fees</td>
            </tr>
          </table>
          <table class="rate-table sky-blue">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate1.png') }}" alt="guidance">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['guidance-residential']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['guidance-residential']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['guidance-residential']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['guidance-residential']['purchase_origin_fee']) }}*</td>
            </tr>
            <tr>
              <td class="last">${{ number_format($rates['guidance-residential']['purchase_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
              <img src="{{ mix_remote('images/rates/bank_rate2.png') }}" alt="QL">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['purchase_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['purchase_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate3.png') }}" alt="CITI">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['purchase_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['purchase_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate4.png') }}" alt="Wells Fargo">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['purchase_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['purchase_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate5.png') }}" alt="BOA">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['purchase_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['purchase_total_fee']) }}</td>
            </tr>
          </table>
        </div>
        <div class="table-banner">
          <h3 class="title three">PURCHASE RATES<span class="golden"> 30 YEARS FIXED</span></h3>
        </div>
        <div class="rates-table-wrapper mobile">
          <table class="rate-table first">
            <tr>
              <th>
              </th>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate1.png') }}" alt="guidance"> </td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate2.png') }}" alt="QL"></td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate3.png') }}" alt="CITI"></td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate4.png') }}" alt="Wells Fargo"></td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate5.png') }}" alt="BOA"></td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Profit Rates
              </th>
            </tr>
            <tr>
              <td class="sky-blue">{{ number_format($rates['guidance-residential']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['purchase_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['purchase_profit_rate'], 3) }}%</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr >
              <th>
                APR
              </th>
            </tr>
            <tr>
              <td class="sky-blue">{{ number_format($rates['guidance-residential']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['purchase_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['purchase_apr'], 3) }}%</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Discount Points
              </th>
            </tr>
            <tr>
              <td class="sky-blue">${{ number_format($rates['guidance-residential']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['purchase_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['purchase_discount_points']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Origination Fee
              </th>
            </tr>
            <tr>
              <td class="sky-blue">${{ number_format($rates['guidance-residential']['purchase_origin_fee']) }}*</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['purchase_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['purchase_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['purchase_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['purchase_origin_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Total Fees
              </th>
            </tr>
            <tr>
              <td class="sky-blue">${{ number_format($rates['guidance-residential']['purchase_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['purchase_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['purchase_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['purchase_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['purchase_total_fee']) }}</td>
            </tr>
          </table>
        </div>
        <div class="sky-blue-flag">
          <span class="triangle"></span>
          <a href="{{ route('guidanceRealty') }}">Receive an appraisal credit up to $500 at closing when you finance with Guidance Residential and use a GuidanceRealty.com partner agent.</a>
        </div>
        <div class="blue-flag">
          <span class="text">To get more accurate pricing, complete an online Pre-Qualification in less than 10 minutes.</span>
          <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_CompetitiveRates_Feb2018" target="_blank" class="main-button gold">Get Started</a>
        </div>
        <p class="desc-text small">
          *$99 application fee still applies. Additional state regulation costs may apply.
        </p>
        <div class="image-container">
          <a href="{{ route('guidanceRealty') }}"><img src="{{ mix_remote('images/rates/rates_graphic.jpg') }}" alt="Save with Guidance Residential &amp; GuidanceRealty.com"></a>
        </div>
      </div>
      <div class="rates-content tab-content refinance {{ ((!empty($rate_default_type) && $rate_default_type == 'refinance') ? 'active' : '') }}">
        <div class="rates-table-wrapper">
          <table class="rate-table bold">
            <tr>
              <th class="blue">
                <span>Refinance Rates</span><br>
                <span class="golden">30 years fixed</span>
              </th>
            </tr>
            <tr>
              <td>Profit Rates</td>
            </tr>
            <tr>
              <td>APR</td>
            </tr>
            <tr>
              <td>Discount Points</td>
            </tr>
            <tr>
              <td>Origination Fee</td>
            </tr>
            <tr>
              <td>Total Fees</td>
            </tr>
          </table>
          <table class="rate-table sky-blue">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate1.png') }}" alt="guidance">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['guidance-residential']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['guidance-residential']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['guidance-residential']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['guidance-residential']['refinance_origin_fee']) }}*</td>
            </tr>
            <tr>
              <td class="last">${{ number_format($rates['guidance-residential']['refinance_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
              <img src="{{ mix_remote('images/rates/bank_rate2.png') }}" alt="QL">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['refinance_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['refinance_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate3.png') }}" alt="CITI">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['refinance_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['refinance_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate4.png') }}" alt="Wells Fargo">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['refinance_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['refinance_total_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                <img src="{{ mix_remote('images/rates/bank_rate5.png') }}" alt="BOA">
              </th>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['refinance_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['refinance_total_fee']) }}</td>
            </tr>
          </table>
        </div>
        <div class="table-banner">
          <h3 class="title three">REFINANCE RATES<span class="golden"> 30 YEARS FIXED</span></h3>
        </div>
        <div class="rates-table-wrapper mobile">
          <table class="rate-table first">
            <tr>
              <th>
              </th>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate1.png') }}" alt="guidance"> </td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate2.png') }}" alt="QL"></td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate3.png') }}" alt="CITI"></td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate4.png') }}" alt="Wells Fargo"></td>
            </tr>
            <tr>
              <td><img src="{{ mix_remote('images/rates/bank_rate5.png') }}" alt="BOA"></td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Profit Rates
              </th>
            </tr>
            <tr>
              <td class="sky-blue">{{ number_format($rates['guidance-residential']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['refinance_profit_rate'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['refinance_profit_rate'], 3) }}%</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr >
              <th>
                APR
              </th>
            </tr>
            <tr>
              <td class="sky-blue">{{ number_format($rates['guidance-residential']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['quicken-loans']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['citi']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['wells-fargo']['refinance_apr'], 3) }}%</td>
            </tr>
            <tr>
              <td>{{ number_format($rates['boa']['refinance_apr'], 3) }}%</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Discount Points
              </th>
            </tr>
            <tr>
              <td class="sky-blue">${{ number_format($rates['guidance-residential']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['refinance_discount_points']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['refinance_discount_points']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Origination Fee
              </th>
            </tr>
            <tr>
              <td class="sky-blue">${{ number_format($rates['guidance-residential']['refinance_origin_fee']) }}*</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['refinance_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['refinance_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['refinance_origin_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['refinance_origin_fee']) }}</td>
            </tr>
          </table>
          <table class="rate-table">
            <tr>
              <th>
                Total Fees
              </th>
            </tr>
            <tr>
              <td class="sky-blue">${{ number_format($rates['guidance-residential']['refinance_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['quicken-loans']['refinance_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['citi']['refinance_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['wells-fargo']['refinance_total_fee']) }}</td>
            </tr>
            <tr>
              <td>${{ number_format($rates['boa']['refinance_total_fee']) }}</td>
            </tr>
          </table>
        </div>
        <p class="desc-text small">
          *$99 application fee still applies. Additional state regulation costs may apply.
        </p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "home-financing-rates",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Affordable, competitive pricing, and dedicated service demonstrate our leadership in the U.S. Islamic Home Financing Market."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "home-refinancing-rates",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Affordable, competitive pricing, and dedicated service demonstrate our leadership in the U.S. Islamic Home Financing Market."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => (empty($rate_default_type) || $rate_default_type == 'purchase') ? 'BuyingResources' : 'RefinanceResources'])
@endsection
