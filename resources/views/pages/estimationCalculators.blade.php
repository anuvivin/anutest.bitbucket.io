@extends('layouts.default')

@section('title', 'Estimation Calculators')

@section('content')
  <div class="container estimation-calculators" id="estimationCalculator">
    <div class="main-heading">
      <h1 class="title">
        ESTIMATION CALCULATORS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Use the calculator tools to assess your financial position. Calculate what you can afford, the time it will take to payoff, or whether it's best for you to rent or buy.
      </p>
    </div>
    <div class="accordion-split-content">
      <div class="description-content">
        <div class="main-heading">
          <div class="app-circle">
            <span class="fa fa-calculator"></span>
          </div>
          <h3 class="title three bold">GET A MORE ACCURATE ESTIMATE</h3>
          <span class="seperate-line"></span>
          <p class="desc-text">
            While these tools can help you get started with the initial estimation, a more accurate estimation can be generated when you complete the online Pre-Qualification.
            Pre-Qualification is the process that provides you estimates on your affordability based on the information you provide. It does not require a credit check and takes
             less than 10 minutes to complete.
          </p>
          <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_EstimationCalculators_Feb2018" target="_blank" class="main-button gold">Pre-Qualify in 10 Minutes</a>
        </div>
      </div>
      <div class="accordion-content">
        <a href="#affordabilityCalculator" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="affordabilityCalculator">
          <span class="fa expand-icon"></span><h4 class="title four bold">Affordability Calculator</h4>
        </a>
        <div class="text-content left collapse" id="affordabilityCalculator">
          <div class="text">
            <h4 class="title four bold">WHAT IS YOUR COMBINED MONTHLY HOUSEHOLD INCOME?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$1K</span>
                <input id="affMonthlyIncome" data-slider-id='affMonthlyIncome' type="text" data-slider-min="1000" data-slider-max="50000" />
              <span class="slider-label">$50K</span>
            </div>
            <h4 class="title four bold">WHAT ARE YOUR TOTAL MONTHLY EXPENSES?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$0K</span>
                <input id="affMonthlyExpense" data-slider-id='affMonthlyExpense' type="text" data-slider-min="0" data-slider-max="20000" />
              <span class="slider-label">$20K</span>
            </div>
            <h4 class="title four bold">HOW MUCH CAN YOU PAY FOR DOWN PAYMENT?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$5K</span>
                <input id="affDownPayment" data-slider-id='affDownPayment' type="text" data-slider-min="5000" data-slider-max="150000" />
              <span class="slider-label">$150K</span>
            </div>
            <a href="#affordabilityCalculatorConfig" class="assumption-accordion" data-toggle="collapse"  aria-expanded="false" aria-controls="affordabilityCalculatorConfig">
              <h4 class="title four bold">Assumptions for the above calculations</h4>
            </a>
            <div class="assumptions collapse" id="affordabilityCalculatorConfig">
              <div class="configurations">
                <div class="item">
                  <span class="desc-text">Financing Term</span>
                  <div class="split-input-button">
                    <input type="text" name="financingTerm" value="30" disabled>
                    <span class="button-box">Years</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Property Tax</span>
                  <div class="split-input-button">
                    <input type="text" name="propertyTax" value="1.25" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Home Isurance Rate</span>
                  <div class="split-input-button">
                    <input type="text" name="homeInsRate" value="0.3" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Profit Rate</span>
                  <div class="split-input-button">
                    <input type="number" name="profitRate" value="4.125" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="error-message" id="affordabilityError">

          </div>
          <a class="main-button result" id="affordabilityCalculatorButton">View Result</a>
          <div class="result-box affordabilityResultWrapper">
            <div class="box affordabilityResult">
              <div class="details">
                <div class="line" id="affHomeValue">
                  <span class="text">You can afford a house worth:</span><span class="number large"></span>
                </div>
                <div class="line" id="affMontlyPay">
                  <span class="text">Approximate Monthly Payment:</span><span class="number"></span>
                </div>
              </div>
              <p class="desc-text">Please <span class="golden">Pre-qualify</span> to know your accurate montly payment and closing cost.</p>
            </div>
          </div>
        </div>
        <a href="#rentBuyCalculator" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="rentBuyCalculator">
          <span class="fa expand-icon"></span><h4 class="title four bold">Rent / Buy Calculator</h4>
        </a>
        <div class="text-content left collapse" id="rentBuyCalculator">
          <div class="text">
            <h4 class="title four bold">What is your target monthly rent?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$1K</span>
                <input id="targetMonthlyRent" data-slider-id='targetMonthlyRent' type="text" data-slider-min="1000" data-slider-max="10000" />
              <span class="slider-label">$10K</span>
            </div>
            <h4 class="title four bold">What is your target home value?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$75K</span>
                <input id="targetHomeValue" data-slider-id='targetHomeValue' type="text" data-slider-min="75000" data-slider-max="1000000" />
              <span class="slider-label">$1M</span>
            </div>
            <h4 class="title four bold">What is your down payment amount?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$5K</span>
                <input id="targetDownPayment" data-slider-id='targetDownPayment' type="text" data-slider-min="5000" data-slider-max="150000" />
              <span class="slider-label">$150K</span>
            </div>
            <h4 class="title four bold">How long do you plan to live in the property?</h4>
            <div class="slidecontainer">
              <span class="slider-label">1 Year</span>
                <input id="stayExpectation" data-slider-id='stayExpectation' type="text" data-slider-min="1" data-slider-max="30" />
              <span class="slider-label">30 Year</span>
            </div>
            <h4 class="title four bold">What is your income tax rate?</h4>
            <div class="slidecontainer">
              <span class="slider-label">0%</span>
                <input id="targetIncomeTax" data-slider-id='targetIncomeTax' type="text" data-slider-min="0" data-slider-max="50" />
              <span class="slider-label">50%</span>
            </div>
            <a href="#rentBuyCalculatorConfig" class="assumption-accordion" data-toggle="collapse"  aria-expanded="false" aria-controls="rentBuyCalculatorConfig">
              <h4 class="title four bold">Assumptions for the above calculations</h4>
            </a>
            <div class="assumptions collapse" id="rentBuyCalculatorConfig">
              <h4 class="title four bold">Financing</h4>
              <div class="configurations">
                <div class="item">
                  <span class="desc-text">Financing Term</span>
                  <div class="split-input-button">
                    <input type="text" name="financingTerm" value="30" disabled>
                    <span class="button-box">Years</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Profit Rate Tax</span>
                  <div class="split-input-button">
                    <input type="text" name="profitRate" value="4.125" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
              </div>
              <h4 class="title four bold">Ongoing Costs</h4>
              <div class="configurations">
                <div class="item">
                  <span class="desc-text">Annual Property Tax</span>
                  <div class="split-input-button">
                    <input type="text" name="annualPropertyTax" value="1.02" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Rent Insurance</span>
                  <div class="split-input-button">
                    <input type="text" name="rentInsurance" value="1.32" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Homeowner's Insurance</span>
                  <div class="split-input-button">
                    <input type="text" name="homeInsurance" value="0.5" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Closing Costs</span>
                  <div class="split-input-button">
                    <input type="text" name="profitRate" value="4" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
              </div>
              <h4 class="title four bold">Economic</h4>
              <div class="configurations">
                <div class="item">
                  <span class="desc-text">Rent appreciation</span>
                  <div class="split-input-button">
                    <input type="text" name="rentAppreciation" value="2.3" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Home Value Appreciation</span>
                  <div class="split-input-button">
                    <input type="number" name="homeValueAppreciation" value="2" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
                <div class="item">
                  <span class="desc-text">Inflation</span>
                  <div class="split-input-button">
                    <input type="number" name="inflation" value="2" disabled>
                    <span class="button-box">%</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <a class="main-button result" id="rentBuyCalculatorButton">View Result</a>
          <div class="result-box rentBuyResultWrapper">
            <div class="accordion-row">
              <div class="accordion">
                <a href="#totalRentingCost" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="totalRentingCost">
                  <h4 class="title four bold">Total Renting Cost:</h4><span class="number" id="rentTotalRentCost"></span><span class="fa expand-icon"></span>
                </a>
                <div id="totalRentingCost" class="accordion-box collapse">
                  <div class="details">
                    <div class="empty-line"></div>
                    <div class="empty-line"></div>
                    <div class="line" id="rentTotalRentIns">
                      <span class="text">Total Rent &amp; Insurance Payments:</span><span class="number"></span>
                    </div>
                    <div class="line" id="rentTotalRentCostAmount">
                      <span class="text">Total Amount:</span><span class="number"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <a href="#homeownershipCost" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="homeownershipCost">
                  <h4 class="title four bold">COST OF HOMEOWNERSHIP:</h4><span class="number" id="rentcostHomeOwnsership"></span><span class="fa expand-icon"></span>
                </a>
                <div class="accordion-box collapse" id="homeownershipCost">
                  <div class="details">
                    <div class="line" id="rentcombinedMonthlyPay">
                      <span class="text">Combined Monthly Payments:</span><span class="number"></span>
                    </div>
                    <div class="line" id="rentPropertyTaxes">
                      <span class="text">Property Taxes:</span><span class="number"></span>
                    </div>
                    <div class="line" id="rentHomeownerIns">
                      <span class="text">Homeowner's Insurance:</span><span class="number"></span>
                    </div>
                    <div class="line" id="rentTotalHomeownership">
                      <span class="text">Total Amount:</span><span class="number"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-row">
              <div class="accordion">
                <a href="#opportunityCost" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="opportunityCost">
                  <h4 class="title four bold">Opportunity Cost:</h4><span class="number" id="rentOpportunityCost"></span><span class="fa expand-icon"></span>
                </a>
                <div class="accordion-box collapse" id="opportunityCost">
                  <div class="details">
                    <div class="empty-line"></div>
                    <div class="line" id="rentInterestEarned">
                      <span class="text">Interest Earned on Invested Funds:</span><span class="number"></span>
                    </div>
                    <div class="line" id="rentTotalOpportunityCost">
                      <span class="text">Total Amount:</span><span class="number"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <a href="#opportunityCostTwo" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="opportunityCostTwo">
                  <h4 class="title four bold">Opportunity Cost:</h4><span class="number" id="rentOpportunityCostTwo"></span><span class="fa expand-icon"></span>
                </a>
                <div class="accordion-box collapse" id="opportunityCostTwo">
                  <div class="details">
                    <div class="line" id="rentTaxSavings">
                      <span class="text">Tax Savings:</span><span class="number"></span>
                    </div>
                    <div class="line" id="rentHomeAppreciation">
                      <span class="text">Home Appreciation:</span><span class="number"></span>
                    </div>
                    <div class="line" id="rentTotalOpportunityCostTwo">
                      <span class="text">Total Amount:</span><span class="number"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-row">
              <div class="accordion">
                <a class="accordion-item"  aria-expanded="false">
                  <h4 class="title four bold">NET COST OF RENTING:</h4><span class="number" id="rentNetCostRenting"></span>
                </a>
              </div>
              <div class="accordion">
                <a class="accordion-item"  aria-expanded="false">
                  <h4 class="title four bold">NET COST OF BUYING:</h4><span class="number" id="rentNetCostBuying"></span>
                </a>
              </div>
            </div>
            <div class="box rentSaveAmount">
              <div class="title three" id="rentSaveAmount">
              </div>
            </div>
            <div class="bar-box rentBuy">
              <div class="line" id="rentRentingCost">
                <span class="title three">Renting Costs</span>
                <div class="bar">
                  <div id="rentRentingBar" class="sub-bar"></div>
                </div>
                <span class="number title three"></span>
              </div>
              <div class="line" id="rentBuyingCost">
                <span class="title three">Buying Costs</span>
                <div class="bar">
                  <div id="rentBuyingBar" class="sub-bar"></div>
                </div>
                <span class="number title three"></span>
              </div>
            </div>
          </div>
        </div>
        <a href="#refinanceCalculator" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="refinanceCalculator">
          <span class="fa expand-icon"></span><h4 class="title four bold">Refinance Calculator</h4>
        </a>
        <div class="text-content left collapse" id="refinanceCalculator">
          <div class="text">
            <h4 class="title four bold">What is your total outstanding balance?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$75K</span>
                <input id="refinanceAmount" data-slider-id='refinanceAmount' type="text" data-slider-min="75000" data-slider-max="750000" />
              <span class="slider-label">$750K</span>
            </div>
            <h4 class="title four bold">How many monthly payments do you have remaining?</h4>
            <div class="slidecontainer">
              <span class="slider-label">1</span>
                <input id="remainingMonths" data-slider-id='remainingMonths' type="text" data-slider-min="1" data-slider-max="360" />
              <span class="slider-label">360</span>
            </div>
            <h4 class="title four bold">What is your current monthly payment?</h4>
            <div class="slidecontainer">
              <span class="slider-label">$500</span>
                <input id="currentMonPay" data-slider-id='currentMonPay' type="text" data-slider-min="500" step="100" data-slider-max="15000" />
              <span class="slider-label">$15K</span>
            </div>
          </div>
          <a class="main-button result" id="refinanceCalculatorButton">View Result</a>
          <div class="result-box refinanceResultWrapper">
            <div class="box refinanceResult15">
              <h3 class="title three">Reduce my total payments</h3>
              <span class="seperate-line"></span>
              <div class="details">
                <div class="line">
                  <span class="text bold">Refinance to a <span class="golden">15 year fixed</span> rate at <span class="golden">3.875%</span></span>
                </div>
                <div class="line" id="refNewMonthlyPayment">
                  <span class="text">Your New Monthly Payment:</span><span class="number"></span>
                </div>
                <div class="line" id="refPaymentIncrease">
                  <span class="text">Monthly Payment Decreased By:</span><span class="number"></span>
                </div>
                <div class="line" id="refTotalPayment">
                  <span class="text">Total of Payments:</span><span class="number"></span>
                </div>
                <div class="line" id="refIncTotalPayment">
                  <span class="text">Increase in Total of Payments:</span><span class="number golden"></span>
                </div>
              </div>
            </div>
            <div class="box refinanceResult30">
              <h3 class="title three">Reduce my monthly payments</h3>
              <span class="seperate-line"></span>
              <div class="details">
                <div class="line">
                  <span class="text bold">Refinance to a <span class="golden">30 year fixed</span> rate at <span class="golden">4.125%</span></span>
                </div>
                <div class="line" id="refNewMonthlyPayment30">
                  <span class="text">Your New Monthly Payment:</span><span class="number"></span>
                </div>
                <div class="line" id="refPaymentIncrease30">
                  <span class="text">Monthly Payment Decreased By:</span><span class="number golden"></span>
                </div>
                <div class="line" id="refTotalPayment30">
                  <span class="text">Total of Payments:</span><span class="number"></span>
                </div>
                <div class="line" id="refIncTotalPayment30">
                  <span class="text">Increase in Total of Payments:</span><span class="number"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "ESTIMATION CALCULATORS",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Use the calculator tools to assess your financial position. Calculate what you can afford, the time it will take to payoff, or whether its best for you to rent or buy. While these tools can help you get started with the initial estimation, a more accurate estimation can be generated when you complete the online Pre-Qualification. Pre-Qualification is the process that provides you estimates on your affordability based on the information you provide. It does not require a credit check and takes less than 10 minutes to complete."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_BuyingResources'])
@endsection
