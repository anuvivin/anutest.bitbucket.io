@extends('layouts.default')

@section('title', 'Contact Us')

@section('content')
<div class="middle">
  <div class="container contact-us">
    <div class="main-heading">
      <div class="title">
        Contact Us
      </div>
      <span class="seperate-line"></span>
    </div>
    <div class="realty-steps contact-us">
      <a class="step" data-toggle="modal" data-target="#contactUsModal">
        <div class="app-circle">
          <span class="fa fa-phone"></span>
        </div>
        <h4 class="title three">Call US</h4>
      </a>
      <a class="step" href="{{ route('appDownload') }}">
        <div class="app-circle">
          <span class="fa fa-mobile"></span>
        </div>
        <h4 class="title three">Download Our App</h4>
      </a>
      <a class="step" data-toggle="modal" data-target="#scheduleConsultationModal">
        <div class="app-circle">
          <span class="fa fa-user-circle"></span>
        </div>
        <h4 class="title three">Schedule Free Consultation</h4>
      </a>
      <a class="step" href="{{ route('operationalStates') }}" >
        <div class="app-circle">
          <span class="fa fa-building-o"></span>
        </div>
        <h4 class="title three">Visit a Local Office</h4>
      </a>
      <a class="step" href="{{ route('careers') }}">
        <div class="app-circle">
          <span class="fa fa-users"></span>
        </div>
        <h4 class="title three">Join Our Team</h4>
      </a>
      <a class="step" data-toggle="modal" data-target="#contactUsModal">
        <div class="app-circle">
          <span class="fa fa-envelope-o"></span>
        </div>
        <h4 class="title three">Email Us</h4>
      </a>
    </div>
  </div>
</div>
@endsection
