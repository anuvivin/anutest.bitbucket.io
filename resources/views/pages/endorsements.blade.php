@extends('layouts.default')

@section('title', 'Endorsements')

@section('content')
<div class="container endorsements">
  <div class="main-heading">
    <h1 class="title">
      COMMUNITY ENDORSEMENTS
    </h1>
    <span class="seperate-line"></span>
    <p class="desc-text">
      Watch videos of scholars and community leaders as they share about their experience with Guidance Residential.
    </p>
  </div>
  <div class="endorse-video-container">
    <div class="video-container">
      <div class="video" data-video-id="K5G6QCdKQAQ">
        <img src="{{ mix_remote('images/endorsements/Endorsements_1.jpg') }}" alt="What should homebuyers look for in an Islamic Finance company?" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="iPG7vO48fk8">
        <img src="{{ mix_remote('images/endorsements/Endorsements_2.jpg') }}" alt="We need Muslims who own homes" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="c_yDBO0g4IE">
        <img src="{{ mix_remote('images/endorsements/Endorsements_3.jpg') }}" alt="Shaykh Nomaan Baig Thanks Guidance for Islamic Finance Seminar - LEARN MORE" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="Ak32O4Rv068">
        <img src="{{ mix_remote('images/endorsements/Endorsements_4.jpg') }}" alt="Imam Tahir Anwar Praises Guidance Residential's Imam Training Seminar with Shaykh Yusuf DeLorenzo" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="AGHXVNN8y6I">
        <img src="{{ mix_remote('images/endorsements/Endorsements_5.jpg') }}" alt="Finally! You can now own a home without compromising your faith and values" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="yjK2y7QIlvU">
        <img src="{{ mix_remote('images/endorsements/Endorsements_6.jpg') }}" alt="Don't lose out on this opportunity to own a home in a halal way" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "What should homebuyers look for in an Islamic Finance company? - Shaykh Yusuf DeLorenzo",
    "description": "We asked world-renowned Islamic finance scholar Shaykh Yusuf Talal DeLorenzo what potential homebuyers should look for when selecting an Islamic Finance company & how do you determine if a company's products are Sharia compliant. Watch the video to hear his recommendations! To learn more, visit https://www.guidanceresidential.com/us-islamic-home-finance Guidance Residential, LLC #1 National Provider of Islamic Home Financing",
    "uploadDate": "2013-04-30T16:20:35.000Z",
    "interactionCount": "2743",
    "duration": "PT1M",
    "embedURL": "https://youtube.googleapis.com/v/K5G6QCdKQAQ",
    "thumbnailURL": "https://i.ytimg.com/vi/K5G6QCdKQAQ/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "We need Muslims who own homes. - Imam Siraj Wahhaj on the Importance of Islamic Home Financing",
    "description": "Imam Siraj Wahhaj, one of Americas most prominent Muslim leaders, explains the importance of home ownership and encourages each of us to take the time to study Islamic finance. Watch the video and learn about Guidance Residential's faith-based home financing program.  This co-ownership program uses a unique non-lending method to help people of all faiths fulfill their dream of owning a home. To learn more about Islamic Home Financing, visit: https://www.guidanceresidential.com/us-islamic-home-finance",
    "uploadDate": "2013-04-22T20:00:39.000Z",
    "interactionCount": "10146",
    "duration": "PT1M58S",
    "embedURL": "https://youtube.googleapis.com/v/iPG7vO48fk8",
    "thumbnailURL": "https://i.ytimg.com/vi/iPG7vO48fk8/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Shaykh Nomaan Baig Thanks Guidance for Islamic Finance Seminar - LEARN MORE",
    "description": "Shaykh Nomaan Baig, Director for the Institute of Knowledge in Southern California, thanks Guidance Residential for their informative sessions on Islamic Finance with Shaykh Yusuf DeLorenzo. To learn more about Islamic Home Financing, visit: https://www.guidanceresidential.com/us-islamic-home-finance",
    "uploadDate": "2013-06-04T18:39:59.000Z",
    "interactionCount": "4310",
    "duration": "PT33S",
    "embedURL": "https://youtube.googleapis.com/v/c_yDBO0g4IE",
    "thumbnailURL": "https://i.ytimg.com/vi/c_yDBO0g4IE/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Imam Tahir Anwar Praises Guidance Residential's Imam Training Seminar with Shaykh Yusuf DeLorenzo",
    "description": "When Imams across the country are asked questions about Islamic Home Financing by their congregants, they turn to Guidance Residential to provide vision, leadership, and instruction. Guidance organizes monthly Imam Islamic Finance Training Seminars with illustrious Islamic Finance scholar Shaykh Yusuf DeLorenzo where Imams can ask frank and honest questions about the Islamic Finance industry and specifically about Guidance Residential's Diminishing Musharaka (Declining Balance Co-ownership Program). In this video, Imam Tahir Anwar, Imam of the South Bay Islamic Association and teacher of Islamic Law at Zaytuna College, praises a recent Guidance Residential Imam Islamic Finance Training Seminar at Zaytuna College with Shaykh Yusuf DeLorenzo. To learn more about Islamic Home Financing, visit: https://www.guidanceresidential.com/us-islamic-home-finance",
    "uploadDate": "2013-06-11T14:06:23.000Z",
    "interactionCount": "3476",
    "duration": "PT1M36S",
    "embedURL": "https://youtube.googleapis.com/v/Ak32O4Rv068",
    "thumbnailURL": "https://i.ytimg.com/vi/Ak32O4Rv068/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Finally! You can now own a home without compromising your faith and values. (Ustadh Nihal Khan)",
    "description": "Ustadh Nihal Khan, a young up and coming Imam from New Jersey, describes the benefit of owning a home in a Sharia-compliant manner, after he attended a seminar with world-renowned Islamic Finance Scholar, Shaykh Yusuf DeLorenzo hosted by Guidance Residential. Watch the video and learn about Guidance Residential's faith-based home financing program. This co-ownership program uses a unique non-lending method to help people of all faiths fulfill their dream of owning a home. To learn more about Islamic Home Financing, visit: https://www.guidanceresidential.com/us-islamic-home-finance",
    "uploadDate": "2013-05-07T21:10:57.000Z",
    "interactionCount": "476",
    "duration": "PT56S",
    "embedURL": "https://youtube.googleapis.com/v/AGHXVNN8y6I",
    "thumbnailURL": "https://i.ytimg.com/vi/AGHXVNN8y6I/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Dont lose out on this opportunity to own a home in a halal way. (Imam Qatanani)",
    "description": "Imam Qatanani of ICPC - one of the largest mosques in NJ - encourages American Muslims to purchase a home in halal manner by using Sharia-compliant home financing with Guidance Residential. Watch the video and learn about Guidance Residential's faith-based home financing program. This co-ownership program uses a unique non-lending method to help people of all faiths fulfill their dream of owning a home. To learn more about Islamic Home Financing, visit: https://www.guidanceresidential.com/us-islamic-home-finance",
    "uploadDate": "2013-05-13T16:59:21.000Z",
    "interactionCount": "2462",
    "duration": "PT1M24S",
    "embedURL": "https://youtube.googleapis.com/v/yjK2y7QIlvU",
    "thumbnailURL": "https://i.ytimg.com/vi/yjK2y7QIlvU/hqdefault.jpg"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Endorsements'])
@endsection
