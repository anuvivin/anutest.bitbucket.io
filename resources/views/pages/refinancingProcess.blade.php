@extends('layouts.default')

@section('title', 'Refinancing Process')

@section('content')
  <div class="container refinancing-process">
    <div class="main-heading">
      <h1 class="title">
        REFINANCING PROCESS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Discover the four steps in the refinancing process. Guidance Residential will guide you through each step to help you complete your refinancing journey.
      </p>
    </div>
    <div class="split-list-container">
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-lightbulb-o"></span>
              </div>
              <h3 class="title three bold">Step 1: Connect</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Complete the online Pre-Qualification form and connect with an Account Executive. No credit check is required and it only takes 10 minutes.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#completeOnlinePreQualification" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="completeOnlinePreQualification">
              <span class="fa expand-icon"></span><h4 class="title four bold">Complete the online Pre-Qualification form</h4>
            </a>
            <div class="text-content left collapse" id="completeOnlinePreQualification">
              <div class="text">
                <p class="desc-text">
                  Complete the online Pre-Qualification form to get started with your refinancing journey. Pre-Qualification is the process that provides you estimates on your affordability
                   based on the information you provide.  Our Pre-Qualification form takes less than 10 minutes to complete and there is no credit check required. You can use our <a href="{{ route('estimationCalculators') }}">calculator
                  tools</a> to estimate what you can afford and the time it will take to payoff.
                </p>
              </div>
            </div>
            <a href="#connectWithAccExecutive" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="connectWithAccExecutive">
              <span class="fa expand-icon"></span><h4 class="title four bold">Connect with an Account Executive</h4>
            </a>
            <div class="text-content left collapse" id="connectWithAccExecutive">
              <div class="text">
                <p class="desc-text">
                  After you do Guidance's online Pre-Qualification, you will need to get connected with a licensed Account Executive who will verify the information with you and assist you
                  with the pre-approval process. If you purchased your home with financing from Guidance Residential, you can contact the Account Executive that you were assigned, and he or
                  she will also be able to assist you with your refinance.
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a href="{{ route('account-executive.index') }}" class="main-button flex-button">Connect with an Account Executive</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-pencil-square-o"></span>
              </div>
              <h3 class="title three bold">Step 2: Apply</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Connect with a licensed Guidance Account Executive to apply for refinancing and provide necessary documentations.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#applyForFinancing" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="applyForFinancing">
              <span class="fa expand-icon"></span><h4 class="title four bold">Apply for Financing</h4>
            </a>
            <div class="text-content left collapse" id="applyForFinancing">
              <div class="text">
                <p class="desc-text">
                	Once you Pre-Qualify with Guidance, you will be assigned an Account Executive, who will guide you through the process of applying to refinance.
                  This typically takes 10 minutes over the phone.  During the finance application process, you will need to provide a number of documents and signed
                  paperwork before you can be approved for financing. This process takes an average of 30 to 45 days, but can be reduced (dependent upon third party vendors'
                   timeliness, your credit profile and responsiveness).
                </p>
              </div>
            </div>
            <a href="#provideNecDocumentation" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="provideNecDocumentation">
              <span class="fa expand-icon"></span><h4 class="title four bold">Provide Necessary Documentation</h4>
            </a>
            <div class="text-content left collapse" id="provideNecDocumentation">
              <div class="text">
                <p class="desc-text">
                  Once you apply for financing with Guidance Residential, you will receive a letter about what to expect. The application process can be completed in 5 steps in which we
                  contact you, confirm your information, confirm your home's value, request your signature on important documents, and finally approve your financing amount. Avoid delays by
                  having the following files ready to provide upon request:
                  <ul>
                    <li><span class="list-text">I-9 Documents (or documents that contain a photo ID)</span></li>
                    <li><span class="list-text">Income Documents</span></li>
                    <li><span class="list-text">Asset/Bank Statements</span></li>
                    <li><span class="list-text">Disclosure Documents - these will be provided to you by Guidance Residential for you to sign and return</span></li>
                    <li><span class="list-text">Other documents, including Home Owner's Insurance Policy on subject property and taxes, insurance, and HOA dues for other real estate owned</span></li>
                  </ul>
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a href="{{ route('account-executive.index') }}" class="main-button flex-button">Connect with an Account Executive</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-user-circle-o"></span>
              </div>
              <h3 class="title three bold">Step 3: Track</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Track the progress of your application.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#reviewEstimatedValue" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="reviewEstimatedValue">
              <span class="fa expand-icon"></span><h4 class="title four bold">Review Estimated Value</h4>
            </a>
            <div class="text-content left collapse" id="reviewEstimatedValue">
              <div class="text">
                <p class="desc-text">
                  Once you submit the necessary documentation, Guidance Residential will determine the value of your home so that we can provide you with the best rate.
                  You can research your home's current value by checking your neighborhood for sales of recent homes like yours.
                </p>
              </div>
            </div>
            <a href="#trackAppStatus" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="trackAppStatus">
              <span class="fa expand-icon"></span><h4 class="title four bold">Track Application Status</h4>
            </a>
            <div class="text-content left collapse" id="trackAppStatus">
              <div class="text">
                <p class="desc-text">
                  Monitor your application process closely to stay updated on your status. Should more information or additional documents be required, you will want to handle it quickly to avoid
                  potential delays. The Guidance Residential customer portal will allow you to receive instant notifications about your status in the application process. You will also be able to
                  authorize your real estate agent to receive notifications about your status so that all interested parties can stay current and updated at all times.
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a href="{{ route('appDownload') }}" class="main-button flex-button">Download Mobile App</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-shopping-cart"></span>
              </div>
              <h3 class="title three bold">Step 4: Close</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Sign paperwork and complete the process to refinance your home.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#connectWithRealEstate" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="connectWithRealEstate">
              <span class="fa expand-icon"></span><h4 class="title four bold">Closing Process</h4>
            </a>
            <div class="text-content left collapse" id="connectWithRealEstate">
              <div class="text">
                <p class="desc-text">
                  <ul>
                    <li><span class="list-text">Prepare for closing</span></li>
                    <li><span class="list-text">Arrange for closing costs</span></li>
                    <li><span class="list-text">Sign the contracts, enjoy savings!</span></li>
                  </ul>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "REFINANCING PROCESS",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Discover the four steps in the refinancing process. Guidance Residential will guide you through each step to help you complete your refinancing journey. STEP 1: CONNECT, Complete the online Pre-Qualification form and connect with an Account Executive. No credit check is required and it only takes 10 minutes. STEP 2: APPLY, Connect with a licensed Guidance Account Executive to apply for refinancing and provide necessary documentations. STEP 3: TRACK, Track the progress of your application. STEP 4: CLOSE, Sign paperwork and complete the process to refinance your home."
  }
  </script>
@endsection
