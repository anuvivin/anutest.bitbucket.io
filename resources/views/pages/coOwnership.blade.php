@extends('layouts.default')

@section('title', 'Partnership Model')

@section('content')
<div class="middle">
  <div class="container co-owner-model">
    <div class="main-heading">
      <h1 class="title">
        CO-OWNERSHIP MODEL
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Learn about Guidance Residential's Declining Balance Co-ownership Program and how it differs from home loans provided by banking institutions.
      </p>
    </div>
    <div class="accordion-split-content full">
      <div class="accordion-content">
        <a href="#decliningBalance" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="decliningBalance">
          <span class="fa expand-icon"></span><h4 class="title four bold">Declining Balance Co-Ownership Program</h4>
        </a>
        <div class="steps-container" id="decliningBalance">
          <div class="main-heading">
            <h1 class="title light six">
              Declining Balance Co-ownership Program
            </h1>
            <span class="seperate-line sub"></span>
            <p class="desc-text">
              Guidance Residential's Shariah-compliant, riba-free home financing
            </p>
          </div>
          <div class="step-row">
            <div class="step-item">
              <span class="title">STEP 1</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/buyer_co_owner.png') }}" alt="step-1">
              </div>
              <div class="content">
                <p class="desc">The homebuyer and Guidance Residential agree to be co-owners in the property.</p>
              </div>
            </div>
            <span class="connector-line"></span>
            <div class="step-item">
              <span class="title">STEP 2</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/ownership.png') }}" alt="step-2">
              </div>
              <div class="content">
                <p class="desc">The two parties buy the home and the ownership of the property is determined by each party's investment.</p>
              </div>
            </div>
            <span class="connector-line"></span>
            <div class="step-item">
              <span class="title">STEP 3</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/45_55.png') }}" alt="step-3">
              </div>
              <div class="content">
                <p class="desc">
                  The homebuyer makes monthly payments to Guidance Residential.*
                </p>
              </div>
            </div>
            <span class="connector-line"></span>
            <div class="step-item">
              <span class="title">STEP 4</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/100_per_home.png') }}" alt="step-4">
              </div>
              <div class="content">
                <p class="desc">Over the course of the arrangement, the homebuyer purchases all of Guidance Residential's
                  ownership stake and becomes the sole owner of the property.
                </p>
              </div>
            </div>
          </div>
          <p class="desc-text italic">
            *The monthly payment includes a usage fee similar to rental fee for full usage of the home, and the other part of the payment is to increase the equity
            ownership of the property.
          </p>
        </div>
        <a href="#conventionalMortgages" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="conventionalMortgages">
          <span class="fa expand-icon"></span><h4 class="title four bold">Conventional Mortgages</h4>
        </a>
        <div class="steps-container" id="conventionalMortgages">
          <div class="main-heading">
            <h1 class="title light six">
              Conventional Mortgages
            </h1>
            <span class="seperate-line sub"></span>
            <p class="desc-text">
              Home loans offered by banking institutions
            </p>
          </div>
          <div class="step-row">
            <div class="step-item">
              <span class="title">STEP 1</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/buyer_co_owner.png') }}" alt="step-1">
              </div>
              <div class="content">
                <p class="desc">The homebuyer arranges a loan from a bank or mortgage company according to a fixed or floating interest rate.</p>
              </div>
            </div>
            <span class="connector-line"></span>
            <div class="step-item">
              <span class="title">STEP 2</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/100_per_loan.png') }}" alt="step-2">
              </div>
              <div class="content">
                <span class="desc">The homebuyer purchases the home.</span>
              </div>
            </div>
            <span class="connector-line"></span>
            <div class="step-item">
              <span class="title">STEP 3</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/100_per_home.png') }}" alt="step-3">
              </div>
              <div class="content">
                <p class="desc">The homebuyer makes monthly payments to repay principal and interest on the loan.</p>
              </div>
            </div>
            <span class="connector-line"></span>
            <div class="step-item">
              <span class="title">STEP 4</span>
              <div class="step-img">
                <img src="{{ mix_remote('images/coOwnership/100_per_home.png') }}" alt="step-4">
              </div>
              <div class="content">
                <p class="desc">Over the course of the loan, the homebuyer repays the debt in full.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="single-video-section">
      <div class="main-heading">
        <h1 class="title six">
          Watch a video on the topic
        </h1>
        <span class="seperate-line"></span>
        <p class="desc-text">
          What is the difference between Guidance's Shariah-compliant home financing and a conventional mortgage?
        </p>
      </div>
      <div class="video-container">
        <div class="video" data-video-id="5n4I9Or2dII">
          <img src="{{ mix_remote('images/coOwnership/maxres.jpg') }}" alt="coOwnership video" class="video-thumbnail">
          <div class="video-play"></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "FAQ 1: What is the difference between Guidance's Islamic home financing program and a home loan?",
    "description": "What is the difference between Guidance's sharia compliant home financing program and a mortgage loan? Guidances program is not a loan. Money lending is a charitable act in which charging interest is prohibited. Lending money to profit from the financing of real estate is not a legitimate form of commerce. Islam promotes equity based agreements and generates profit from the sale of real goods and services. Guidances program is based on musharakah (diminishing partnership). The relationship is that of co-owners, not borrower/lender. Guidance acquired a share in the property, it does not provide a loan.",
    "uploadDate": "2013-05-16T22:51:51.000Z",
    "interactionCount": "10714",
    "duration": "PT1M8S",
    "embedURL": "https://youtube.googleapis.com/v/5n4I9Or2dII",
    "thumbnailURL": "https://i.ytimg.com/vi/5n4I9Or2dII/hqdefault.jpg"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Co-ownershipmodel'])
@endsection
