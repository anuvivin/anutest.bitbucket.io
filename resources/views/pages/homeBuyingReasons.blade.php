@extends('layouts.default')

@section('title', 'Home Buying Reasons')

@section('content')
<div class="middle">
  <div class="container homeBuyingReasons">
    <div class="main-heading">
      <div class="title">
        Home Buying Reasons
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Whether you are buying your first home, purchasing a residential property for investment purposes or buying a vacation home to enjoy with family during the holiday season,
        Guidance Residential can walk you through the process to obtain Shariah-compliant financing and fulfill your dream.
      </p>
    </div>
    <div class="split-list-container">
      <div class="item" id="firstTimeHomeBuyer">
        <div class="image-container background-image"></div>
        <div class="description">
          <h3 class="title three">First Time Homebuyer</h3>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            Becoming a homeowner is a rewarding milestone, but it can also be a challenging process to navigate. At Guidance, we strive to simplify the process for first-time homebuyers.
             Our expert Account Executives are ready to answer your most pressing questions about eligibility, down payments and the home financing process.
          </p>
          <div class="button-container">
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingReasons_FirstTimeHomebuyer_Feb2018" target="_blank" class="main-button gold solid" > <span class="fa fa-check-square-o"></span>Get Pre-Qualified</a>
            <a href="{{ mix_remote('docs/GR_Ebook_First_Time_Home_Buyers_Jan2018.pdf') }}" class="main-button" target="_blank"> <span class="fa fa-download"></span>Download first time homebuyers' guidebook</a>
          </div>
        </div>
      </div>
      <div class="item" id="buyToRentOut">
        <div class="image-container background-image"></div>
        <div class="description">
          <h3 class="title three">INVESTMENT PROPERTY | (BUYING TO RENT OUT) </h3>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            Guidance Residential can help you with your plans to generate rental income. We offer Shariah-compliant financing for single family homes, townhomes, condominiums and multi-units (up to 4 units).
            If you are interested in buying residential properties for investment purposes, connect with a licensed Account Executive or complete our online Pre-Qualification form to get started.
          </p>
          <div class="button-container">
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingReasons_InvestmentProperty_Feb2018" target="_blank" class="main-button gold solid" > <span class="fa fa-check-square-o"></span>Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="main-button " > <span class="fa fa-user-circle-o"></span>Find an account Executive </a>
          </div>
        </div>
      </div>
      <div class="item" id="vacationHomes">
        <div class="image-container background-image"></div>
        <div class="description">
          <h3 class="title three">Vacation Homes</h3>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            With Guidance Residential, your wish to invest in a vacation home is granted. We offer Shariah-compliant financing for non-owner occupied residential properties - including vacation
            homes for personal use or vacation homes to use as a rental investment.
            To get started, complete the online Pre-Qualification form or connect with a licensed Account Executive.
          </p>
          <div class="button-container">
            <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingReasons_VacationHomes_Feb2018" target="_blank" class="main-button gold solid" > <span class="fa fa-check-square-o"></span>Get Pre-Qualified</a>
            <a href="{{ route('account-executive.index') }}" class="main-button " > <span class="fa fa-user-circle-o"></span>Find an account Executive </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "Home Buying Reasons",
    "url": "{{ Request::url() }}#firstTimeHomeBuyer",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Whether you are buying your first home, purchasing a residential property for investment purposes or buying a vacation home to enjoy with family during the holiday season, Guidance Residential can walk you through the process to obtain Shariah-compliant financing and fulfill your dream."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "FIRST TIME HOMEBUYER",
    "url": "{{ Request::url() }}#firstTimeHomeBuyer",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Becoming a homeowner is a rewarding milestone, but it can also be a challenging process to navigate. At Guidance, we strive to simplify the process for first-time homebuyers. Our expert Account Executives are ready to answer your most pressing questions about eligibility, down payments and the home financing process."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "INVESTMENT PROPERTY | (BUYING TO RENT OUT)",
    "url": "{{ Request::url() }}#buyToRentOut",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Guidance Residential can help you with your plans to generate rental income. We offer Shariah-compliant financing for single family homes, townhomes, condominiums and multi-units (up to 4 units). If you are interested in buying residential properties for investment purposes, connect with a licensed Account Executive or complete our online Pre-Qualification form to get started."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "VACATION HOMES",
    "url": "{{ Request::url() }}#vacationHomes",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "With Guidance Residential, your wish to invest in a vacation home is granted. We offer Shariah-compliant financing for non-owner occupied residential properties - including vacation homes for personal use or vacation homes to use as a rental investment. To get started, complete the online Pre-Qualification form or connect with a licensed Account Executive."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_BuyingReasons'])
@endsection
