@extends('layouts.default')

@section('title', 'Our History')

@section('content')
<div class="middle">
  <div class="container ourHistory">
    <div class="main-heading">
      <h1 class="title">
        Our History
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Since the company's inception in 1999, Guidance Residential has demonstrated its commitment to providing competitive home financing solutions to faith-conscious
        Americans. Our journey to become the leading Islamic home financing provider in the U.S. began with a strong foundation with the motto "Modern Finance, Timeless Values".
      </p>
    </div>
  </div>
  <div class="history-timeline">
    <div class="years-timeline">
      <ul class="timeline-list">
        <li class="active"><span class="text">2018</span></li>
        <span class="divider"></span>
        <li><span class="text">2017</span></li>
        <span class="divider"></span>
        <li><span class="text">2016</span></li>
        <span class="divider"></span>
        <li><span class="text">2015</span></li>
        <span class="divider"></span>
        <li><span class="text">2013</span></li>
        <span class="divider"></span>
        <li><span class="text">2012</span></li>
        <span class="divider"></span>
        <li><span class="text">2009</span></li>
        <span class="divider"></span>
        <li><span class="text">2007</span></li>
        <span class="divider"></span>
        <li><span class="text">2002</span></li>
        <span class="divider"></span>
        <li><span class="text">1999</span></li>
      </ul>
    </div>
    <div class="year 2018 active">
      <div class="title three bold">
        2018
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-dollar"></span>
          </div>
          <div class="title three bold">
            Surpasses $5 billion in funding &amp; helps over 20,000 Families
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance helps over 20,000 families achieve the dream of homeownership without compromising their faith and provides over $5 billion in home financing.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-trophy"></span>
          </div>
          <div class="title three bold">
            Awarded "Best Islamic Home Financier, USA"
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance awarded the "Best Islamic Home Financier, USA" by <a target="_blank" href="https://www.worldfinance.com/islamic-finance-awards">World Finance Islamic Finance Awards</a>.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-desktop"></span>
          </div>
          <div class="title three bold">
            Revamped website
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance revamps website to simplify home financing process for customers and to increase resources for customers at all stages of the home financing process.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line second last"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-mobile"></span>
          </div>
          <div class="title three bold">
            New mobile app
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance Residential's new mobile app provides a faster, more transparent and convenient home financing experience to consumers.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="history-card last">
          <div class="app-circle center">
            <span class="fa fa-map-marker"></span>
          </div>
          <div class="title three bold">
            NEW STATES &amp; EXPANSION
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Operations expand to Colorado, West Virginia, and Mississippi.</p>
        </div>
      </div>
    </div>
    <div class="year 2017">
      <div class="title three bold">
        2017
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-birthday-cake"></span>
          </div>
          <div class="title three bold">
            15 year anniversary
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Operational in 29 States, 174 employees, 19,000 families served.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-dollar"></span>
          </div>
          <div class="title three bold">
            EXCEEDS $4.9 BILLION IN FUNDING
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance reaches more than $4.9 billion in total Shariah-compliant home financing.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-building-o"></span>
          </div>
          <div class="title three bold">
            CORPORATE GROWTH
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">New office openings in Plano, Texas and Sterling, Virginia.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line second last"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-map-marker"></span>
          </div>
          <div class="title three bold">
            NEW STATES &amp; EXPANSION
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Operations expand to Rhode Island, Alabama, Arkansas, Washington, D.C. and Colorado.</p>
        </div>
      </div>
    </div>
    <div class="year 2016">
      <div class="title three bold">
        2016
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-map-marker"></span>
          </div>
          <div class="title three bold">
            NEW STATES
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Operations expand to Kentucky and Delaware.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line single"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-usd"></span>
          </div>
          <div class="title three bold">
            INCREASED FUNDING
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance reaches more than $4 billion in total Shariah-compliant home financing.</p>
        </div>
      </div>
    </div>
    <div class="year 2015">
      <div class="title three bold">
        2015
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-gavel"></span>
          </div>
          <div class="title three bold">
            AMJA ENDORSEMENT
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">The Assembly of Muslim Jurists of America (AMJA) endorses Guidance Residential and its Declining Balance Co-ownership Program as a "permissible" path to homeownership for Muslim Americans.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line single"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-dollar"></span>
          </div>
          <div class="title three bold">
            GROWTH &amp; INCREASED FUNDING
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance Residential operates across 23 states, reaches nearly $3.5 billion in total home financing.</p>
        </div>
      </div>
    </div>
    <div class="year 2013">
      <div class="title three bold">
        2013
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card last">
          <div class="app-circle center">
            <span class="fa fa-trophy"></span>
          </div>
          <div class="title three bold">
            GUIDANCE BECOMES THE NATION'S LEADER
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance Residential becomes the nation's leading provider of Islamic home financing.  It is now the only company with an independent Shariah Supervisory Board made up of world-renowned scholars who also serve on the board for the Accounting &amp; Auditing Organization for Islamic Financial Institutions (AAOIFI).</p>
        </div>
      </div>
    </div>
    <div class="year 2012">
      <div class="title three bold">
        2012
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-birthday-cake"></span>
          </div>
          <div class="title three bold">
            10 year anniversary
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Operational in 21  States, 89 employees, 1,683 contracts closed.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line single"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-users"></span>
          </div>
          <div class="title three bold">
            GUIDANCE IN THE COMMUNITY
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance becomes the largest corporate sponsor of Muslim American community initiatives, funding more than $1.5 million.</p>
        </div>
      </div>
    </div>
    <div class="year 2009">
      <div class="title three bold">
        2009
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card">
          <div class="app-circle left">
            <span class="fa fa-trophy"></span>
          </div>
          <div class="title three bold">
            HONORS &amp; NOMINATIONS
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance Residential is nominated for the Islamic Business and Finance magazine's "Best Islamic Home Finance Provider" award.</p>
        </div>
        <div class="mobile-line"></div>
        <div class="divider-line single"><div class="divider-circle"></div></div>
        <div class="history-card right">
          <div class="app-circle right">
            <span class="fa fa-newspaper-o"></span>
          </div>
          <div class="title three bold">
            MAINSTREAM NEWS RECOGNITION
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Receives media recognition from major outlets like The Washington Post, CNN, The New York Times, PBS, Aljazeera, The Wall Street Journal, USA Today, Houston Chronicle, Associated Press and Detroit Free Press.</p>
        </div>
      </div>
    </div>
    <div class="year 2007">
      <div class="title three bold">
        2007
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card last">
          <div class="app-circle center">
            <span class="fa fa-usd"></span>
          </div>
          <div class="title three bold">
            PROOF THERE IS A NEED FOR ISLAMIC HOME FINANCING
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance Residential provides more than $1 billion in Islamic home financing.</p>
        </div>
      </div>
    </div>
    <div class="year 2002">
      <div class="title three bold">
        2002
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card last">
          <div class="app-circle center">
            <span class="fa fa-map-marker"></span>
          </div>
          <div class="title three bold">
            GUIDANCE BEGINS OPERATIONS
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance Residential begins operations in 3 states: Virginia, Maryland and Washington, D.C.</p>
        </div>
      </div>
    </div>
    <div class="year 1999">
      <div class="title three bold">
        1999
      </div>
      <span class="seperate-line"></span>
      <div class="history-details">
        <div class="history-card last">
          <div class="app-circle center">
            <span class="fa fa-leaf"></span>
          </div>
          <div class="title three bold">
            GUIDANCE RESIDENTIAL IS CREATED
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">Guidance Residential embarks on a 3-year, multi-million dollar research &amp; development project involving eighteen law firms and six of the world's leading Islamic finance scholars to provide an authentic home financing model compliant with both U.S. and Islamic legal systems.</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "OUR HISTORY",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Since the company's inception in 1999, Guidance Residential has demonstrated its commitment to providing competitive home financing solutions to faith-conscious Americans. Our journey to become the leading Islamic home financing provider in the U.S. began with a strong foundation with the motto Modern Finance, Timeless Values."
  }
  </script>
@endsection
