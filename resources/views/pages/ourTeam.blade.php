@extends('layouts.default')

@section('title', 'Our Team')

@section('content')
  <div class="container ourteam">
    <div class="main-heading">
      <h1 class="title">
        Our Team
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
      Guidance Residential's expert leadership and management team brings over 100 years of collective experience in the industry we serve.
      </p>
    </div>
    <div class="team-list">
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#kalModal">
          <img src="{{ mix_remote('images/ourTeam/Kal_photo.jpg') }}" alt="Kal Elsayed" class="member-image">
        </div>
        <!-- Modal -->
        <div class="modal fade" id="kalModal" tabindex="-1" role="dialog" aria-labelledby="Kal Elsayed" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Kal Elsayed
                    </h1>
                    <h4 class="sub-heading"><span></span><span></span>President &amp; CEO</h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Kal Elsayed, based in Reston, Va., is president and CEO of Guidance Residential where he has overall responsibility for the management
                      and performance of the company. He is a leading U.S. home finance industry executive with broad national experience in all aspects of the
                      business, including sales and operations. He is an accomplished leader in start-up and high-growth business environments and has extensive
                      experience in building motivated teams to achieve and exceed target objectives. He was previously president and CEO of HomeView
                      Lending, where he helped start the company and build its sales and operating structure. Prior to HomeView, he held various executive positions
                      with New Century Mortgage Corp, culminating in the position of President of the Wholesale Division, where he assumed full P&amp;L responsibility
                      and grew annual production from US $4 billion to US $46 billion over a three-year period.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#kalModal">
          <h3 class="title three">Kal Elsayed</h3>
          <h4 class="sub-heading"><span></span><span></span>President &amp; CEO</h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#nickModal">
          <img src="{{ mix_remote('images/ourTeam/nick_photo.jpg') }}" alt="Nick Minardi" class="member-image">
        </div>
        <div class="modal fade" id="nickModal" tabindex="-1" role="dialog" aria-labelledby="Nick Minardi" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Nick Minardi
                    </h1>
                    <h4 class="sub-heading"><span>Senior Vice President</span><span>Chief Operating Officer</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Nick Minardi rejoined Guidance Residential as the SVP chief operating officer in 2015. He formerly worked as the SVP chief financial officer
                      director of secondary marketing activity from 2011 to 2014.  With over 30 years of banking and home financing experience, his expertise is
                      being able to assist, oversee and continually improve our secondary, capital markets and finance activity. In between his two stints of
                      employment with Guidance, he served as the SVP chief credit officer for LOANZ, Inc.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#nickModal">
          <h3 class="title three">Nick Minardi</h3>
          <h4 class="sub-heading"><span>Senior Vice President</span><span>Chief Operating Officer</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#suhaModal">
          <img src="{{ mix_remote('images/ourTeam/suha_z_photo.jpg') }}" alt="Suha Zehl" class="member-image">
        </div>
        <div class="modal fade" id="suhaModal" tabindex="-1" role="dialog" aria-labelledby="Suha Zehl" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Suha Zehl
                    </h1>
                    <h4 class="sub-heading"><span>Executive Vice President</span><span>Chief Information Officer</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Suha Zehl, based in Reston, Va., is chief information officer for Guidance Residential where she is responsible for overseeing all technology
                      services for the organization and its offices throughout the 29 states and the District of Columbia including infrastructure management,
                      application and web development, security, business intelligence, telecom, and helpdesk support. With over 30 years of experience, she
                      is an accomplished technology executive and thought-leader with demonstrated achievement in rapid growth environments. She is a thoughtful and
                      creative technology advocate and value innovator driven to turn unrealized potential into results. Prior to joining Guidance Residential, she
                      was the director of technology services at Long &amp; Foster. Prior to Long &amp; Foster, she was Founder and President of her own technology
                      consulting firm, Z Technology Solutions. She is a certified project management professional; she received her Master of Business Administration
                      from the University of Phoenix and her Bachelor of Science in Computer Science from North Carolina State University. She is member of NAWBO, NAPW
                      and PMI.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#suhaModal">
          <h3 class="title three">Suha Zehl</h3>
          <h4 class="sub-heading"><span>Executive Vice President</span><span>Chief Information Officer</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#chrisModal">
          <img src="{{ mix_remote('images/ourTeam/chris_photo.jpg') }}" alt="Chris Gallant" class="member-image">
        </div>
        <div class="modal fade" id="chrisModal" tabindex="-1" role="dialog" aria-labelledby="Chris Gallant" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Chris Gallant
                    </h1>
                    <h4 class="sub-heading"><span>Senior Vice President</span><span>Finance and Accounting</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Chris Gallant, senior vice president of Accounting and Finance, manages the company's finances and plays a key role in the strategic direction of
                      the company. Prior to Guidance Residential, he served as North American controller for Octagon, a leading sport marketing and management company.
                      His experience also spans into the technology and real estate industries, with experience in realty and in property management companies.  He is
                      an active certified public accountant and certified global management accountant, and holds an accounting degree and Bachelor of Science in Finance
                      from Virginia Tech University. Outside of work, he stays busy spending time with family and friends and remains active physically through training
                      and participating in long distance triathlons and other endurance events.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#chrisModal">
          <h3 class="title three">Chris Gallant</h3>
          <h4 class="sub-heading"><span>Senior Vice President</span><span>Finance and Accounting</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#heidiModal">
          <img src="{{ mix_remote('images/ourTeam/heidi_photo.jpg') }}" alt="Heidi Partida" class="member-image">
        </div>
        <div class="modal fade" id="heidiModal" tabindex="-1" role="dialog" aria-labelledby="Heidi Partida" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Heidi Partida
                    </h1>
                    <h4 class="sub-heading"><span>Senior Vice President</span><span>Human Resources and Administration</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Heidi Partida is the senior vice president of Human Resources at Guidance Residential, where she has served for over eight years. She has over 15 years of
                      demonstrated achievements contributing to superior corporate performance. She attended Azusa Pacific University, where she received a Bachelor of
                      Science in Organizational Leadership, and Ashford University, where she received her Master of Business Administration in Human Resources Management.
                      Prior to joining Guidance Residential, she was the vice president of administration at HomeView Lending.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#heidiModal">
          <h3 class="title three">Heidi Partida</h3>
          <h4 class="sub-heading"><span>Senior Vice President</span><span>Human Resources and Administration</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#thomasModal">
          <img src="{{ mix_remote('images/ourTeam/tom_g_photo.jpg') }}" alt="Thomas gainor" class="member-image">
        </div>
        <div class="modal fade" id="thomasModal" tabindex="-1" role="dialog" aria-labelledby="Thomas gainor" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Tom Gainor
                    </h1>
                    <h4 class="sub-heading"><span>Senior Vice President</span><span>General Counsel</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Tom Gainor has over 30 years of diversified professional experience as a lawyer, accountant and financial product engineer.  He has been with Guidance
                      Residential since its inception in 2002, providing a broad range of legal services with an emphasis on product design and delivery. Prior to joining
                      the organization, he served as managing partner in the London office of The International Investor ("TII"), a then prominent Islamic investment bank.
                      He is a licensed attorney, a certified public accountant and a certified management accountant.  He also holds Series 7, 24 and 66 securities licenses.
                      He earned his Bachelor of Arts in Accounting from the University of Rhode Island and both his J.D. and LL.M. in taxation from the University of Miami
                      School of Law.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#thomasModal">
          <h3 class="title three">Tom Gainor</h3>
          <h4 class="sub-heading"><span>Senior Vice President</span><span>General Counsel</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#susanModal">
          <img src="{{ mix_remote('images/ourTeam/susan_photo.jpg') }}" alt="Susan Palmer" class="member-image">
        </div>
        <div class="modal fade" id="susanModal" tabindex="-1" role="dialog" aria-labelledby="Susan Palmer" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Susan Palmer
                    </h1>
                    <h4 class="sub-heading"><span>Vice President</span><span>Compliance</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Susan Palmer is a mortgage compliance professional with over 15 years of experience.  Prior to joining Guidance Residential in 2007, she worked as
                      a compliance analyst with First Nationwide Mortgage Corporation and Citimortgage, Inc. She was also engaged as an editorial/research assistant for
                      Andrea Lee Negroni for the Residential Mortgage Lending: Brokers and Pratt's State Regulation of Second Mortgage and Home Equity Loans publications
                      from 2008 through 2012. She received a juris doctor degree from the University of Baltimore School of Law in 2003.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#susanModal">
          <h3 class="title three">Susan Palmer</h3>
          <h4 class="sub-heading"><span>Vice President</span><span>Compliance</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#riffatModal">
          <img src="{{ mix_remote('images/ourTeam/riffat_photo.jpg') }}" alt="Riffat Lakhani" class="member-image">
        </div>
        <div class="modal fade" id="riffatModal" tabindex="-1" role="dialog" aria-labelledby="Riffat Lakhani" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Riffat Lakhani
                    </h1>
                    <h4 class="sub-heading"><span>Vice President</span><span>Marketing</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Riffat Lakhani is the vice president of Marketing at Guidance Residential. She oversees all marketing services and activities for the organization,
                      including brand awareness, lead generation and nurturing, social media, events, content creation, graphic design and general marketing support.
                      Her background is a fusion of graphic design, real estate sales, business and marketing. Her dedication for excellence and passion to make a difference
                      keeps her focused. She graduated summa cum laude from George Mason University, with a Bachelor of Arts in Graphic Design and minor in business
                      and multimedia. She received her Master of Business Administration from the University of Maryland - Robert H. Smith School of Business.  She is a
                      member of AMA.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#riffatModal">
          <h3 class="title three">Riffat Lakhani</h3>
          <h4 class="sub-heading"><span>Vice President</span><span>Marketing</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#hussamModal">
          <img src="{{ mix_remote('images/ourTeam/hussam_photo.jpg') }}" alt="Hussam Qutub" class="member-image">
        </div>
        <div class="modal fade" id="hussamModal" tabindex="-1" role="dialog" aria-labelledby="Hussam Qutub" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Hussam Qutub
                    </h1>
                    <h4 class="sub-heading"><span>President</span><span>Guidance Realty Homes</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Hussam Qutub is the president of Guidance Realty Homes, LLC, Guidance Residential's real estate brokerage sister company, where he has served for
                      nearly four years. Prior, he was the vice president of Marketing and Communications with Guidance Residential. In this role, he played a significant
                      role in the company's first 10 years of expansion and its path to dominance in the marketplace. He attended George Mason University where he
                      studied international relations. He specializes in leadership and team building, business development, communications management, branding and
                      positioning, public relations, digital marketing and lead generation.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#hussamModal">
          <h3 class="title three">Hussam Qutub</h3>
          <h4 class="sub-heading"><span>President</span><span>Guidance Realty Homes</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#suhaElModal">
          <span class="fa fa-user"></span>
        </div>
        <div class="modal fade" id="suhaElModal" tabindex="-1" role="dialog" aria-labelledby="Suha Elsayed" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Suha Elsayed
                    </h1>
                    <h4 class="sub-heading"><span>Senior Vice President</span><span>Chief Credit Officer</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Suha Elsayed is the senior vice president, chief credit officer at Guidance Residential, a role in which she has served since 2015. She has over 15
                      years of experience in home finance operations and management and has been instrumental in training staff on how to increase productivity and
                      efficiency using technology. She has been with Guidance Residential for over 15 years. She served with the company from 2002-2006, and rejoined
                      the organization in 2007. Prior to rejoining the organization, she worked as a senior default underwriter for Freddie Mac. She received a
                      bachelor's degree in information technology and operations management from George Mason University.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#suhaElModal">
          <h3 class="title three">Suha Elsayed</h3>
          <h4 class="sub-heading"><span>Senior Vice President</span><span>Chief Credit Officer</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#denModal">
          <img src="{{ mix_remote('images/ourTeam/den_photo.jpg') }}" alt="Den Moyo" class="member-image">
        </div>
        <div class="modal fade" id="denModal" tabindex="-1" role="dialog" aria-labelledby="Den Moyo" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Den Moyo
                    </h1>
                    <h4 class="sub-heading"><span>Controller</span><span>Accounting</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Den Moyo is the corporate controller for Guidance Financial Group, where he has served since 2013. He is responsible for all financial activities
                      in the company, and oversees the preparation, consolidation and reporting of financial statements for all the company's subsidiaries. Prior to joining
                      Guidance Financial Group, he served as the director of accounting for BerkleyNet.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#denModal">
          <h3 class="title three">Den Moyo</h3>
          <h4 class="sub-heading"><span>Controller</span><span>Accounting</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#danielModal">
          <img src="{{ mix_remote('images/ourTeam/daniel_photo.jpg') }}" alt="Daniel ReibSamen" class="member-image">
        </div>
        <div class="modal fade" id="danielModal" tabindex="-1" role="dialog" aria-labelledby="Daniel ReibSamen" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Daniel Reibsamen
                    </h1>
                    <h4 class="sub-heading"><span>Director</span><span>Software Engineering</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Daniel Reibsamen is the director of Software Engineering where he has been for over two years. He plays an integral role in digitalizing the home
                      finance application process and building technology that allows the sales force to instantly communicate and collaborate with each other and
                      customers, regardless of location. He received an associates in science from Richard Bland College of The College of William and Mary. He has a
                      Bachelor of Science in Mathematics and minor in computer science from the University of South Carolina, and an AAS in computer programming, ECPI
                      Technical College.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#danielModal">
          <h3 class="title three">Daniel Reibsamen</h3>
          <h4 class="sub-heading"><span>Director</span><span>Software Engineering</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#abdessamadModal">
          <img src="{{ mix_remote('images/ourTeam/abdessamad_photo.jpg') }}" alt="Abdessamad Meloune" class="member-image">
        </div>
        <div class="modal fade" id="abdessamadModal" tabindex="-1" role="dialog" aria-labelledby="Abdessamad Meloune" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Abdessamad Melouane
                    </h1>
                    <h4 class="sub-heading"><span>Sales</span><span>Northeast Region</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Abdessamad Melouane is the Northeast regional manager at Guidance Residential. A few of his specialties include finance, Islamic finance,
                      Shariah-compliant home financing, public relations and business development. He received his Bachelor of Arts in Economics from the Universite
                      Cadi Ayyad Marrakech, and has been with Guidance Residential for over 11 years.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#abdessamadModal">
          <h3 class="title three">Abdessamad Melouane</h3>
          <h4 class="sub-heading"><span>Sales</span><span>Northeast Region</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#salmanAliModal">
          <img src="{{ mix_remote('images/ourTeam/salman_photo.jpg') }}" alt="Salman Ali" class="member-image">
        </div>
        <div class="modal fade" id="salmanAliModal" tabindex="-1" role="dialog" aria-labelledby="Salman Ali" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Salman Ali
                    </h1>
                    <h4 class="sub-heading"><span>Sales</span><span>Mid-Atlantic Region</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Salman Ali is the Mid-Atlantic regional manager. He has been with the company for 12 years, and now recruits and trains account executives in
                      Washington, D.C., Maryland, Virginia and Kentucky. He holds a bachelor's degree in business administration, and has over 20 years of experience
                      in the banking and financial services industry.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#salmanAliModal">
          <h3 class="title three">Salman Ali</h3>
          <h4 class="sub-heading"><span>Sales</span><span>Mid-Atlantic Region</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#fayeSafi">
          <img src="{{ mix_remote('images/ourTeam/faye_photo.jpg') }}" alt="Faye Safi" class="member-image">
        </div>
        <div class="modal fade" id="fayeSafi" tabindex="-1" role="dialog" aria-labelledby="Faye Safi" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Faye Safi
                    </h1>
                    <h4 class="sub-heading"><span>Sales</span><span>Midwest region</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Faye Safi is the Midwest regional manager at Guidance Residential. She has been with the company since 2011, and now recruits and trains
                      account executives to provide expert service to customers. She has over 22 years of retail residential home financing and 14 years in
                      management. Prior to joining Guidance Residential, she worked as a sales manager for Wells Fargo. She is licensed and certified to assist
                      first time home buyers, provide home renovation financing and reverse mortgages.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#fayeSafi">
          <h3 class="title three">Faye Safi</h3>
          <h4 class="sub-heading"><span>Sales</span><span>Midwest region</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#junaidIqbal">
          <img src="{{ mix_remote('images/ourTeam/junaid_iqbal.jpg') }}" alt="Junaid Iqbal" class="member-image">
        </div>
        <div class="modal fade" id="junaidIqbal" tabindex="-1" role="dialog" aria-labelledby="Junaid Iqbal" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Junaid Iqbal
                    </h1>
                    <h4 class="sub-heading"><span>Sales</span><span>Southwest Region</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Junaid Iqbal is a seasoned Islamic financing industry professional with over 13 years of experience in Islamic home finance. He joined Guidance
                      Residential in 2005 and currently serves as the Southwest regional manager. He holds a bachelor's degree in economics from Pakistan, is a
                      certified Islamic finance executive from Ethica in Dubai, and an associate of certified chartered accountants from the UK.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#junaidIqbal">
          <h3 class="title three">Junaid Iqbal</h3>
          <h4 class="sub-heading"><span>Sales</span><span>Southwest Region</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#aliKhurram">
          <img src="{{ mix_remote('images/ourTeam/ali_photo.jpg') }}" alt="Ali Khurrum" class="member-image">
        </div>
        <div class="modal fade" id="aliKhurram" tabindex="-1" role="dialog" aria-labelledby="Ali Khurrum" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Ali Khurrum
                    </h1>
                    <h4 class="sub-heading"><span>Sales</span><span>West Coast Region</span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Ali Khurrum is the West Coast regional manager. He began working in the finance and real estate industries as an investor flipping properties
                      over 20 years ago. After finishing graduate school, he worked on commercial property investments and direct hard money lending. Prior to joining
                      Guidance in 2011, he served as the vice president in a boutique real estate management and investment firm and worked on projects throughout the
                      nation.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#aliKhurram">
          <h3 class="title three">Ali Khurrum</h3>
          <h4 class="sub-heading"><span>Sales</span><span>West Coast Region</span></h4>
        </div>
      </div>
      <div class="member">
        <div class="icon" data-toggle="modal" data-target="#jenniferSimbulan">
          <img src="{{ mix_remote('images/ourTeam/jen_photo.jpg') }}" alt="Jennifer Simbulan" class="member-image">
        </div>
        <div class="modal fade" id="jenniferSimbulan" tabindex="-1" role="dialog" aria-labelledby="Jennifer Simbulan" aria-hidden="true">
          <div class="modal-dialog our-team blue-modal" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="content">
                  <div class="main-heading left">
                    <h1 class="title">
                      Jennifer Simbulan
                    </h1>
                    <h4 class="sub-heading"><span>Servicing</span><span></span></h4>
                    <span class="seperate-line sub"></span>
                    <p class="desc-text">
                      Jennifer Simbulan has worked for Guidance Residential for over 14 years. In her current position in the Servicing Department, she streamlines
                      processes to better assist our internal and external clients. She has a bachelor's degree in computer science.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="content" data-toggle="modal" data-target="#jenniferSimbulan">
          <h3 class="title three">Jennifer Simbulan</h3>
          <h4 class="sub-heading"><span>Servicing</span><span></span></h4>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "KAL ELSAYED",
    "jobTitle": "President & CEO",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/Kal_photo.jpg') }}",
    "description": "Kal Elsayed, based in Reston, Va., is president and CEO of Guidance Residential where he has overall responsibility for the management and performance of the company. He is a leading U.S. home finance industry executive with broad national experience in all aspects of the business, including sales and operations. He is an accomplished leader in start-up and high-growth business environments and has extensive experience in building motivated teams to achieve and exceed target objectives. He was previously president and CEO of HomeView Lending, where he helped start the company and build its sales and operating structure. Prior to HomeView, he held various executive positions with New Century Mortgage Corp, culminating in the position of President of the Wholesale Division, where he assumed full P&L responsibility and grew annual production from US $4 billion to US $46 billion over a three-year period."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "NICK MINARDI",
    "jobTitle": "Senior Vice President, Chief Operating Officer",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/nick_photo.jpg') }}",
    "description": "Nick Minardi rejoined Guidance Residential as the SVP chief operating officer in 2015. He formerly worked as the SVP chief financial officer director of secondary marketing activity from 2011 to 2014. With over 30 years of banking and home financing experience, his expertise is being able to assist, oversee and continually improve our secondary, capital markets and finance activity. In between his two stints of employment with Guidance, he served as the SVP chief credit officer for LOANZ, Inc."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "SUHA ZEHL",
    "jobTitle": "Executive Vice President, Chief Information Officer",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/suha_z_photo.jpg') }}",
    "description": "Suha Zehl, based in Reston, Va., is chief information officer for Guidance Residential where she is responsible for overseeing all technology services for the organization and its offices throughout the 29 states and the District of Columbia including infrastructure management, application and web development, security, business intelligence, telecom, and helpdesk support. With over 30 years of experience, she is an accomplished technology executive and thought-leader with demonstrated achievement in rapid growth environments. She is a thoughtful and creative technology advocate and value innovator driven to turn unrealized potential into results. Prior to joining Guidance Residential, she was the director of technology services at Long & Foster. Prior to Long & Foster, she was Founder and President of her own technology consulting firm, Z Technology Solutions. She is a certified project management professional; she received her Master of Business Administration from the University of Phoenix and her Bachelor of Science in Computer Science from North Carolina State University. She is member of NAWBO, NAPW and PMI."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "CHRIS GALLANT",
    "jobTitle": "Senior Vice President, Finance and Accounting",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/chris_photo.jpg') }}",
    "description": "Chris Gallant, senior vice president of Accounting and Finance, manages the company's finances and plays a key role in the strategic direction of the company. Prior to Guidance Residential, he served as North American controller for Octagon, a leading sport marketing and management company. His experience also spans into the technology and real estate industries, with experience in realty and in property management companies. He is an active certified public accountant and certified global management accountant, and holds an accounting degree and Bachelor of Science in Finance from Virginia Tech University. Outside of work, he stays busy spending time with family and friends and remains active physically through training and participating in long distance triathlons and other endurance events."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "HEIDI PARTIDA",
    "jobTitle": "Senior Vice President, Human Resources and Administration",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/heidi_photo.jpg') }}",
    "description": "Heidi Partida is the senior vice president of Human Resources at Guidance Residential, where she has served for over eight years. She has over 15 years of demonstrated achievements contributing to superior corporate performance. She attended Azusa Pacific University, where she received a Bachelor of Science in Organizational Leadership, and Ashford University, where she received her Master of Business Administration in Human Resources Management. Prior to joining Guidance Residential, she was the vice president of administration at HomeView Lending."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "TOM GAINOR",
    "jobTitle": "Senior Vice President, General Counsel",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/tom_g_photo.jpg') }}",
    "description": "Tom Gainor has over 30 years of diversified professional experience as a lawyer, accountant and financial product engineer. He has been with Guidance Residential since its inception in 2002, providing a broad range of legal services with an emphasis on product design and delivery. Prior to joining the organization, he served as managing partner in the London office of The International Investor ("TII"), a then prominent Islamic investment bank. He is a licensed attorney, a certified public accountant and a certified management accountant. He also holds Series 7, 24 and 66 securities licenses. He earned his Bachelor of Arts in Accounting from the University of Rhode Island and both his J.D. and LL.M. in taxation from the University of Miami School of Law."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "SUSAN PALMER",
    "jobTitle": "Vice President, Compliance",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/susan_photo.jpg') }}",
    "description": "Susan Palmer is a mortgage compliance professional with over 15 years of experience. Prior to joining Guidance Residential in 2007, she worked as a compliance analyst with First Nationwide Mortgage Corporation and Citimortgage, Inc. She was also engaged as an editorial/research assistant for Andrea Lee Negroni for the Residential Mortgage Lending: Brokers and Pratt's State Regulation of Second Mortgage and Home Equity Loans publications from 2008 through 2012. She received a juris doctor degree from the University of Baltimore School of Law in 2003."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "RIFFAT LAKHANI",
    "jobTitle": "Vice President, Marketing",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/riffat_photo.jpg') }}",
    "description": "Riffat Lakhani is the vice president of Marketing at Guidance Residential. She oversees all marketing services and activities for the organization, including brand awareness, lead generation and nurturing, social media, events, content creation, graphic design and general marketing support. Her background is a fusion of graphic design, real estate sales, business and marketing. Her dedication for excellence and passion to make a difference keeps her focused. She graduated summa cum laude from George Mason University, with a Bachelor of Arts in Graphic Design and minor in business and multimedia. She received her Master of Business Administration from the University of Maryland - Robert H. Smith School of Business. She is a member of AMA."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "HUSSAM QUTUB",
    "jobTitle": "President, Guidance Realty Homes",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/hussam_photo.jpg') }}",
    "description": "Hussam Qutub is the president of Guidance Realty Homes, LLC, Guidance Residential's real estate brokerage sister company, where he has served for nearly four years. Prior, he was the vice president of Marketing and Communications with Guidance Residential. In this role, he played a significant role in the company's first 10 years of expansion and its path to dominance in the marketplace. He attended George Mason University where he studied international relations. He specializes in leadership and team building, business development, communications management, branding and positioning, public relations, digital marketing and lead generation."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "SUHA ELSAYED",
    "jobTitle": "Senior Vice President, Chief Credit Officer",
    "url": "{{ Request::url() }}",
    "description": "Suha Elsayed is the senior vice president, chief credit officer at Guidance Residential, a role in which she has served since 2015. She has over 15 years of experience in home finance operations and management and has been instrumental in training staff on how to increase productivity and efficiency using technology. She has been with Guidance Residential for over 15 years. She served with the company from 2002-2006, and rejoined the organization in 2007. Prior to rejoining the organization, she worked as a senior default underwriter for Freddie Mac. She received a bachelor's degree in information technology and operations management from George Mason University."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "DEN MOYO",
    "jobTitle": "Controller, Accounting",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/den_photo.jpg') }}",
    "description": "Den Moyo is the corporate controller for Guidance Financial Group, where he has served since 2013. He is responsible for all financial activities in the company, and oversees the preparation, consolidation and reporting of financial statements for all the company's subsidiaries. Prior to joining Guidance Financial Group, he served as the director of accounting for BerkleyNet."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "DANIEL REIBSAMEN",
    "jobTitle": "Director, Software Engineering",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/daniel_photo.jpg') }}",
    "description": "Daniel Reibsamen is the director of Software Engineering where he has been for over two years. He plays an integral role in digitalizing the home finance application process and building technology that allows the sales force to instantly communicate and collaborate with each other and customers, regardless of location. He received an associates in science from Richard Bland College of The College of William and Mary. He has a Bachelor of Science in Mathematics and minor in computer science from the University of South Carolina, and an AAS in computer programming, ECPI Technical College."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "ABDESSAMAD MELOUANE",
    "jobTitle": "Sales, Northeast Region",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/abdessamad_photo.jpg') }}",
    "description": "Abdessamad Melouane is the Northeast regional manager at Guidance Residential. A few of his specialties include finance, Islamic finance, Shariah-compliant home financing, public relations and business development. He received his Bachelor of Arts in Economics from the Universite Cadi Ayyad Marrakech, and has been with Guidance Residential for over 11 years."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "SALMAN ALI",
    "jobTitle": "Sales, Mid-Atlantic Region",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/salman_photo.jpg') }}",
    "description": "Salman Ali is the Mid-Atlantic regional manager. He has been with the company for 12 years, and now recruits and trains account executives in Washington, D.C., Maryland, Virginia and Kentucky. He holds a bachelor's degree in business administration, and has over 20 years of experience in the banking and financial services industry."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "FAYE SAFI",
    "jobTitle": "Sales, Midwest region",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/faye_photo.jpg') }}",
    "description": "Faye Safi is the Midwest regional manager at Guidance Residential. She has been with the company since 2011, and now recruits and trains account executives to provide expert service to customers. She has over 22 years of retail residential home financing and 14 years in management. Prior to joining Guidance Residential, she worked as a sales manager for Wells Fargo. She is licensed and certified to assist first time home buyers, provide home renovation financing and reverse mortgages."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "JUNAID IQBAL",
    "jobTitle": "Sales, Southwest Region",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/junaid_iqbal.jpg') }}",
    "description": "Junaid Iqbal is a seasoned Islamic financing industry professional with over 13 years of experience in Islamic home finance. He joined Guidance Residential in 2005 and currently serves as the Southwest regional manager. He holds a bachelor's degree in economics from Pakistan, is a certified Islamic finance executive from Ethica in Dubai, and an associate of certified chartered accountants from the UK."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "ALI KHURRUM",
    "jobTitle": "Sales, West Coast Region",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/ali_photo.jpg') }}",
    "description": "Ali Khurrum is the West Coast regional manager. He began working in the finance and real estate industries as an investor flipping properties over 20 years ago. After finishing graduate school, he worked on commercial property investments and direct hard money lending. Prior to joining Guidance in 2011, he served as the vice president in a boutique real estate management and investment firm and worked on projects throughout the nation."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Person",
    "name" : "JENNIFER SIMBULAN",
    "jobTitle": "Servicing",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/ourTeam/jen_photo.jpg') }}",
    "description": "Jennifer Simbulan has worked for Guidance Residential for over 14 years. In her current position in the Servicing Department, she streamlines processes to better assist our internal and external clients. She has a bachelor's degree in computer science."
  }
  </script>
@endsection
