@extends('layouts.default')

@section('title', 'Glossary Of Terms')

@section('content')
  <div class="container glossary-of-terms">
    <div class="main-heading">
      <div class="title">
        Glossary Of Terms
      </div>
      <span class="seperate-line"></span>
    </div>
    <h3 class="title three">Account Executive</h3>
    <p class="desc-text">
      A representative of the home financing company, who will coordinate with and guide the applicant from application through closing.
    </p>
    <h3 class="title three">Acquisition payment</h3>
    <p class="desc-text">
      The portion of the consumer's monthly payments that is applied to increase his or her ownership interest in the property, which keeps varying by month in accordance with the schedule.
      If consumer makes an acquisition payment earlier than due, such payment shall be applied in accordance with the schedule agreed upon at the time of closing. A periodical statement
      will be provided to the consumer, which will reflect the consumer's ownership amount and the remaining acquisition balance.
    </p>
    <h3 class="title three">Adjustable Rate program</h3>
    <p class="desc-text">
      A type of mortgage (home financing program) in which rates automatically adjust based on certain market indices. It begins with an initial rate, which is usually marked lower as against
      a comparable fixed rate program. This helps in making bigger payments in the beginning, which could reduce the duration of the program. However, the rates are subject to fluctuations
      as mentioned earlier.
    </p>
    <h3 class="title three">Appreciation</h3>
    <p class="desc-text">
      The value of the property in excess of the purchase price or of the initial property value as the case may be as of any measurement data.
    </p>
    <h3 class="title three">Buyout</h3>
    <p class="desc-text">
      When a customer acquires the remaining equity of the co-owner (Guidance Residential), in order to own all of the equity in the home.
    </p>
    <h3 class="title three">Cash Out Refinance</h3>
    <p class="desc-text">
      A Cash Out Refinance allows homeowners to refinance to cash out in the amount of 80- to 90% of their home equity.
    </p>
    <h3 class="title three">Closing</h3>
    <p class="desc-text">
      The settlement transaction whereby (i) Co-owner and consumer obtain title to the property from the seller or as the case may be consummate the mortgage replacement program (ii)
       the consumer and co-owner enter into and execute the co-ownership agreement and the security instrument and (iii) the consumer enters into the obligation to pay and agrees to the
        execution of the assignment agreement and amendment of security instrument in favor of the financier (and subsequent co-owner's assignees) and executes any and all other documents
        necessary to legally bind the consumer and co-owner under the transaction.
    </p>
    <p class="desc-text">
      The formal documented sale of a home, which includes the signing of documents associated with the purchase and the payment of the required closing fees.
    </p>
    <h3 class="title three">Closing Costs</h3>
    <p class="desc-text">
      Fees payable by both buyer and seller during a closing, towards the transaction. It could include the following, but not limited to i) appraisal fees; (ii) credit report fees; (iii) title search and abstract fees;
      (iv) title insurance premiums; (v) flood review and certification fees; (vi) real property tax review search and certification fees; (vii) notary fees; (viii) document preparation fees; (i) realtor fees and commissions;
       and, (x) recording costs and fees and intangible taxes and document stamp taxes.
    </p>
    <h3 class="title three">Co-ownership</h3>
    <p class="desc-text">
      It means the period during which the property is commonly owned by a customer and co-owner (Guidance Residential).
    </p>
    <h3 class="title three">Conforming limit </h3>
    <p class="desc-text">
      A home finance limit that conforms to Government-sponsored enterprise (GSE) guidelines.
    </p>
    <h3 class="title three">Contract to value ratio</h3>
    <p class="desc-text">
      It is the ratio of the home financing amount to the actual sale price of the home being purchased.
    </p>
    <h3 class="title three">Community association dues, fees and assessments</h3>
    <p class="desc-text">
      It means all dues, fees, assessments and other charges that are imposed on the customer or the home by a condominium association, homeowners' association or similar organizations.
    </p>
    <h3 class="title three">Costs of sale </h3>
    <p class="desc-text">
      It means the costs and expenses associated with the re-sale of the property, including real estate transfer taxes, real estate brokerage commissions and the costs normally
       incurred in closing a residential real estate transaction.
    </p>
    <h3 class="title three">Credit information (Credit score)</h3>
    <p class="desc-text">
      A credit score is a numerical expression of a person's creditworthiness, used by banks and financiers to assess a person's ability to repay his/her debts.
    </p>
    <h3 class="title three">Default</h3>
    <p class="desc-text">
      It means the failure of the customer to perform any promises, obligations or covenants under the co-ownership agreement, the obligation to pay or the security instrument.
    </p>
    <h3 class="title three">Down Payment</h3>
    <p class="desc-text">
      The initial amount which the buyer can pay towards a home purchase, from his/her own personal savings.
    </p>
    <h3 class="title three">Escrow</h3>
    <p class="desc-text">
      It is a term referring to the part of payments, given to the financing company that is used to pay for property taxes and hazard insurance. Usually it is required by the company
      when the customer is making a down payment less than 20% of the home value.
    </p>
    <h3 class="title three">Fatwa</h3>
    <p class="desc-text">
      It is a religious decree given by a Shariah Scholar or group of Shariah Scholars in accordance to Shariah rulings. In Islamic finance, it is given by the members of the Shariah board to
      certify the permissibility of financial products that a particular company is offering.
    </p>
    <h3 class="title three">Fixed Rate program</h3>
    <p class="desc-text">
      It is a home financing program that has a profit rate, which remains fixed over the duration of repayment.
    </p>
    <h3 class="title three">Foreclosure</h3>
    <p class="desc-text">
      When a homeowner is not in a position to repay the borrowed money, the home financing company tries to recover the remaining portion of the amount, by proceeding to sell the asset used as the collateral for the loan.
    </p>
    <h3 class="title three">Halal</h3>
    <p class="desc-text">
      In simple words, "permissible". Things that are permissible as per Shariah principles are termed Halal.
    </p>
    <h3 class="title three">Haram</h3>
    <p class="desc-text">
      Things or acts that are forbidden by Shariah principles are termed haram.
    </p>
    <h3 class="title three">Home Affordable Refinance Program (HARP)  </h3>
    <p class="desc-text">
      HARP makes it possible for homeowners with little equity in their home, or who owe as much or more than their home is worth, and have remained current on their payments, to refinance their home.
    </p>
    <h3 class="title three">Home value</h3>
    <p class="desc-text">
      The total amount of money for which a customer wants to purchase a home.
    </p>
    <h3 class="title three">Late payment fees</h3>
    <p class="desc-text">
      It means the amounts assessed by the co-owner to consumer for processing a monthly payment due from consumer that is not received by the last day of the grace period following the
      applicable due date. The amount of this charge to be retained by the co-owner is not designed to be an advantage to the co-owner but to merely defray the additional administrative
      costs incurred by co-owner for monitoring, tracking, communication and collecting any monthly payments due from consumer that are not timely paid. If the financier is the co-ownership
      agreement servicer the late payment fees may accrue but will not be assessed to the consumer by the financier while acting as the co-ownership agreement servicer.
    </p>
    <h3 class="title three">Monthly payment</h3>
    <p class="desc-text">
      It means the total of the profit payment, acquisition payment and other payments paid by the consumer to the co-owner each month for his/her enjoyment and use of the whole property
      and to acquire co-owner's interest in the property.
    </p>
    <h3 class="title three">Musharaka Mutanaqisa</h3>
    <p class="desc-text">
      A contract of partnership that allows one (or more) partner (s) to give a right to gradually own his share of the asset to the remaining partners based on agreed terms set forth in the partnership contract.
    </p>
    <h3 class="title three">Pre-approval letter</h3>
    <p class="desc-text">
      It means an initial indication by a financing company that the consumer has the financial capacity to consummate a transaction that results in the issuance of a pre-approval letter by financier if the consumer is qualified.
    </p>
    <p class="desc-text">
      It is a written document, which is an initial indication by a financing company that a customer has the financial capacity to qualify for a home financing program.
    </p>
    <h3 class="title three">Profit payment</h3>
    <p class="desc-text">
      It means that portion of the monthly payment that customer pays to the co-owner or the co-owner's assignee(s) for the customer's enjoyment and use of the whole property.
    </p>
    <h3 class="title three">Profit rate</h3>
    <p class="desc-text">
      It is a rate which determine the total amount that you will be paying in return for using Guidance Residential's ownership share.
    </p>
    <h3 class="title three">Property</h3>
    <p class="desc-text">
      It means the home which the customer will purchase with co-owner, or which co-owner will acquire an equity in, under the mortgage replacement transaction.
    </p>
    <h3 class="title three">Property insurance</h3>
    <p class="desc-text">
      It means the insurance for the protection of the property for loss or damage from fire and other perils that provides for guaranteed replacement cost valuation with coverage for
      additional cost for updated code requirements but does not include other coverages provided under consumer's insurance.
    </p>
    <h3 class="title three">Purchase price</h3>
    <p class="desc-text">
      It means the price stated in the residential contract of sale as such may be amended through the date of closing.
    </p>
    <h3 class="title three">Rebate points in percentage (%)</h3>
    <p class="desc-text">
      Rebate points are a form of upfront rent payment. Paying it upfront at closing gives you a lower monthly payment throughout the home financing period.
    </p>
    <h3 class="title three">Rebate amount in dollars ($)</h3>
    <p class="desc-text">
      This amount is calculated depending on the number of rebate points purchased. One rebate point is equal to 1% of the total financing amount.
    </p>
    <h3 class="title three">Residential contract of sale</h3>
    <p class="desc-text">
      It means the purchase and sale agreement under which the customer agrees to purchase the property from the seller.
    </p>
    <h3 class="title three">Riba</h3>
    <p class="desc-text">
      Any amount that is charged in excess which is not in exchange for a due consideration. Conventionally it is referred to as interest and is prohibited in Islam.
    </p>
    <h3 class="title three">Riba-free</h3>
    <p class="desc-text">
      Anything that is free of Riba.
    </p>
    <h3 class="title three">Seller</h3>
    <p class="desc-text">
      It means the owner of the home, who sells it to the customer and the co-owner.
    </p>
    <h3 class="title three">Shariah</h3>
    <p class="desc-text">
      Islamic Law. Divine law derived from Quran, Sunnah and Consensus of Scholars.
    </p>
    <h3 class="title three">Shariah Board</h3>
    <p class="desc-text">
      A panel of Islamic scholars who are experts in Islamic legal jurisprudence with respect to economics and finance.
      They supervise the financial contracts in Islamic financial institutions, to ensure the Shariah-compliance of the financial products.
    </p>
    <h3 class="title three">Short sale</h3>
    <p class="desc-text">
      Also known as a pre-foreclosure sale, a short sale happens when a homeowner sells the home for a price lower than the balance remaining on the home financing contract.
    </p>
    <h3 class="title three">Shirkat-ul-Aqd</h3>
    <p class="desc-text">
      A contract agreed upon by two or more people for a commercial venture to make a profit.
    </p>
    <h3 class="title three">Shirkat-ul-Milk</h3>
    <p class="desc-text">
      Primarily a 'partnership of joint ownership' which may come about deliberately or involuntarily.
    </p>
    <p class="desc-text">
      This kind of "shirkah" may come into existence in two different ways: Sometimes it comes into operation at the option of the parties. For example, if two or more persons purchase an
      equipment, it will be owned jointly by both of them and the relationship between them with regard to that property is called "shirkat-ul-milk." Here this relationship has come into
      existence at their own option, as they themselves elected to purchase the equipment jointly.
    </p>
    <p class="desc-text">
      But there are cases where this kind of "shirkah" comes to operate automatically without any action taken by the parties. For example, after the death of a person, all his heirs inherit his property which
      comes into their joint ownership as an automatic consequence of the death of that person.
    </p>
  </div>
@endsection
