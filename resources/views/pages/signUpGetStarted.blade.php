@extends('layouts.default')

@section('title', 'Sign Up and Get Started')

@section('content')
  <div class="container sign-up-get-started">
    <div class="main-heading">
      <h1 class="title">
        BECOME A GUIDANCEREALTY.COM PARTNER AGENT
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Select a level to get started.
      </p>
    </div>
    <div class="compare-table wrapper">
      <table class="table compare-guidance">
        <tbody>
          <tr>
            <td></td>
            <td><h3 class="title three bold">Gold Agent</h3><br><span class="sub-text">FREE</span></td>
            <td><h3 class="title three bold white">Platinum Agent</h3><br><span class="sub-text white">FREE</span></td>
          </tr>
          <tr>
            <td>Showcase Agent Profile in Select Markets</td>
            <td>Yes</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td>Get Featured Profile On Select Markets</td>
            <td>Yes</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td>Receive Pre-Qualified Referrals</td>
            <td>Yes</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td>Get Free Digital Marketing Tips</td>
            <td>Yes</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td>Access Free Online Training Session</td>
            <td>Yes</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td>Offer an appraisal credit up to $500 for your customers who finance with Guidance Residential</td>
            <td>Yes</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td>Provide sellers with a $500 commission credit at closing</td>
            <td>No</td>
            <td>Yes</td>
          </tr>
          <tr>
            <td></td>
            <td><a href="{{ config('urls.grealty.agent-signup') }}" class="main-button">Sign up now</a></td>
            <td><a href="{{ config('urls.grealty.agent-signup') }}" class="main-button">Sign up now</a></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
@endsection
