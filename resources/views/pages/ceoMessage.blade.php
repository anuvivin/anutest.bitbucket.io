@extends('layouts.default')

@section('title', 'Message from the CEO')

@section('content')
<div class="middle">
  <div class="container ceo-message">
    <div class="main-heading">
      <div class="title">
        MESSAGE FROM THE CEO AND LEADERSHIP TEAM
      </div>
      <span class="seperate-line"></span>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="MWRVpIyJNkQ">
        <img src="{{ mix_remote('images/ceoMessage/ceoMessage.jpg') }}" alt="Message from the ceo and leadership team video" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Guidance Residential - The #1 US  Islamic Home Finance Provider",
    "description": "Guidance Residential is the largest U.S. provider of Shariah-compliant home financing having provided over $4.9 billion in home financing to American Muslim homeowners over the last 15 years and capturing nearly 80% of the market served.",
    "uploadDate": "2018-02-20T19:16:59.000Z",
    "interactionCount": "89",
    "duration": "PT1M31S",
    "embedURL": "https://youtube.googleapis.com/v/MWRVpIyJNkQ",
    "thumbnailURL": "https://i.ytimg.com/vi/MWRVpIyJNkQ/hqdefault.jpg"
  }
  </script>
@endsection
