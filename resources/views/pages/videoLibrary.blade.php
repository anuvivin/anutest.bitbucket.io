@extends('layouts.default')

@section('title', 'Video Library')

@section('content')
  <div class="container video-library">
    <div class="main-heading">
      <h1 class="title">
        VIEW OUR VIDEO LIBRARY
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Watch our videos and learn more about how Guidance's Shariah-compliant home financing works.
      </p>
    </div>
    <div class="split-list-container">

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="DZeE3FPWza0">
            <img src="{{ mix_remote('images/videoLibrary/islamic_financing_works.jpg') }}" alt="HOW DOES ISLAMIC HOME FINANCING WORK?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            HOW DOES ISLAMIC HOME FINANCING WORK?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">How does the current financial system work?</span></li>
              <li><span class="text">Shortcomings of the current financial system.</span></li>
              <li><span class="text">The alternative way to finance a home with Guidance's Declining Balance Co-ownership Program.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="LjpToMspe1g">
            <img src="{{ mix_remote('images/videoLibrary/why_guidance_llc.jpg') }}" alt="WHY DOES GUIDANCE RESIDENTIAL SET UP AN LLC TO FACILITATE YOUR HOME FINANCING?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            WHY DOES GUIDANCE RESIDENTIAL SET UP AN LLC TO FACILITATE YOUR HOME FINANCING?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">Guidance is a wholly owned subsidiary of its Muslim-owned parent company and not a bank.</span></li>
              <li><span class="text">Guidance utilizes the Musharakah method of home financing for strict Shariah adherence.</span></li>
              <li><span class="text">LLC is created to avoid trading in debt which is prohibited by Shariah.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="5n4I9Or2dII">
            <img src="{{ mix_remote('images/videoLibrary/why_guidance_different.jpg') }}" alt="WHAT IS THE DIFFERENCE BETWEEN GUIDANCE'S SHARIAH-COMPLIANT HOME FINANCE PROGRAM AND A MORTGAGE LOAN?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            WHAT IS THE DIFFERENCE BETWEEN GUIDANCE'S SHARIAH-COMPLIANT HOME FINANCE PROGRAM AND A MORTGAGE LOAN?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">Guidance's Shariah-compliant home financing program is not a loan.</span></li>
              <li><span class="text">Interest is charged on a loan. Shariah prohibits charging interest.</span></li>
              <li><span class="text">Guidance's program is an equity based co-ownership model which is permissible by Shariah.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="G92ZcCLw9Fk">
            <img src="{{ mix_remote('images/videoLibrary/why_guidance_freddie_mac.jpg') }}" alt="WHY DOES GUIDANCE SELL ITS CONTRACTS TO FREDDIE MAC?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            WHY DOES GUIDANCE SELL ITS CONTRACTS TO FREDDIE MAC?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">Freddie Mac is not a lender nor is it a bank.</span></li>
              <li><span class="text">Freddie Mac was created by Congress in 1970 to provide liquidity in the mortgage industry.</span></li>
              <li><span class="text">Freddie Mac buys contracts issued by banks and sells it to investors in the secondary market, but the terms of the co-ownership agreement remains the same.</span></li>
              <li><span class="text">Guidance uses its own fund to facilitate initial financing to purchase the property alongside the homebuyers.</span></li>
              <li><span class="text">Shariah permits to sell what is owned.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="u8W12ZpfVkk">
            <img src="{{ mix_remote('images/videoLibrary/how_does_GR_determine_the_price.jpg') }}" alt="HOW DOES GUIDANCE RESIDENTIAL DETERMINE THE PRICE FOR FACILITATING HOME OWNERSHIP THROUGH THEIR PROGRAM?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            HOW DOES GUIDANCE RESIDENTIAL DETERMINE THE PRICE FOR FACILITATING HOME OWNERSHIP THROUGH THEIR PROGRAM?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">How Guidance Residential competes with conventional home finance markets.</span></li>
              <li><span class="text">How the usage fee price is determined.</span></li>
              <li><span class="text">Why the usage fee is permissible in Shariah.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="6byRHj3HU5Q">
            <img src="{{ mix_remote('images/videoLibrary/what_type_of_partnership.jpg') }}" alt="WHAT TYPE OF PARTNERSHIP IS THIS HOME FINANCING PROGRAM BASED ON?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            WHAT TYPE OF PARTNERSHIP IS THIS HOME FINANCING PROGRAM BASED ON?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">Different types of partnership concepts in Islamic jurisprudence.</span></li>
              <li><span class="text">Learn about The Declining Balance Co-ownership Program.</span></li>
              <li><span class="text">Customers retain all appreciating value in their home through the program.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="OH_KJu7mHrc">
            <img src="{{ mix_remote('images/videoLibrary/what_you_should_and_shouldnt_do.jpg') }}" alt="WHAT YOU SHOULD AND SHOULDN'T DO WHEN FINANCING YOUR HOME." class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            WHAT YOU SHOULD AND SHOULDN'T DO WHEN FINANCING YOUR HOME.
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">Best and worst practices for financing your home.</span></li>
              <li><span class="text">How to maintain good credit.</span></li>
              <li><span class="text">Bad financial decisions to avoid before closing.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="SaZH8Jr8vQc">
            <img src="{{ mix_remote('images/videoLibrary/how_do_you_determine_shariah-compliant.jpg') }}" alt="HOW DO YOU DETERMINE IF A COMPANY'S PRODUCTS ARE SHARIAH-COMPLIANT?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            HOW DO YOU DETERMINE IF A COMPANY'S PRODUCTS ARE SHARIAH-COMPLIANT?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">See what measures the company has taken to be Shariah-compliant.</span></li>
              <li><span class="text">Does the company have a Shariah board.</span></li>
              <li><span class="text">Does the company undergo a Shariah-compliant audit.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="bLcIpeevxIE">
            <img src="{{ mix_remote('images/videoLibrary/why_we_do_what_we_do.jpg') }}" alt="WHY WE DO WHAT WE DO." class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            WHY WE DO WHAT WE DO.
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">Provide modern financial solutions that adhere to the Muslim faith.</span></li>
              <li><span class="text">Help Muslim communities obtain a clear and trusted path to homeownership.</span></li>
            </ul>
         </div>
        </div>
      </div>

      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="K5G6QCdKQAQ">
            <img src="{{ mix_remote('images/videoLibrary/what_questions_should_I_be_asking.jpg') }}" alt="WHAT QUESTIONS SHOULD I BE ASKING A COMPANY PROVIDING FINANCIAL SERVICES TO MUSLIMS?" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            WHAT QUESTIONS SHOULD I BE ASKING A COMPANY PROVIDING FINANCIAL SERVICES TO MUSLIMS?
          </div>
          <span class="seperate-line"></span>
          <div class="desc-text">
            <span class="sub-text">In this video:</span>
            <ul>
              <li><span class="text">Ask questions about authenticity.</span></li>
              <li><span class="text">Ask about competitiveness with conventional home finance providers.</span></li>
              <li><span class="text">Apply the same criteria for conventional providers to Islamic providers.</span></li>
            </ul>
         </div>
        </div>
      </div>

    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "How Does Islamic Home Finance Work? (2013)",
    "description": "The current financial system works like a factory that sells money for more money, creating crisis after crisis. Is Islamic home finance a real alternative to traditional mortgage financing? Watch the video and learn about Guidance Residentials faith-based home financing program. This co-ownership program uses a unique non-lending method to help people of all faiths fulfill their dream of owning a home. Guidance Residential, LLC #1 National Provider of Islamic Home Financing www.GuidanceResidential.com",
    "uploadDate": "2013-03-21T07:58:49.000Z",
    "interactionCount": "802403",
    "duration": "PT2M34S",
    "embedURL": "https://youtube.googleapis.com/v/DZeE3FPWza0",
    "thumbnailURL": "https://i.ytimg.com/vi/DZeE3FPWza0/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "FAQ 5: Why does Guidance Residential set up an LLC to facilitate your home financing?",
    "description": "FAQ 5: Why does Guidance Residential set up an LLC to facilitate your home financing?",
    "uploadDate": "2016-06-22T22:20:01.000Z",
    "interactionCount": "1974",
    "duration": "PT1M55S",
    "embedURL": "https://youtube.googleapis.com/v/LjpToMspe1g",
    "thumbnailURL": "https://i.ytimg.com/vi/LjpToMspe1g/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "FAQ 1: What is the difference between Guidance's Islamic home financing program and a home loan?",
    "description": "What is the difference between Guidance's sharia compliant home financing program and a mortgage loan? Guidance's program is not a loan. Money lending is a charitable act in which charging interest is prohibited. Lending money to profit from the financing of real estate is not a legitimate form of commerce. Islam promotes equity based agreements and generates profit from the sale of real goods and services. Guidances program is based on musharakah (diminishing partnership). The relationship is that of co-owners, not borrower/lender. Guidance acquired a share in the property, it does not provide a loan.",
    "uploadDate": "2013-05-16T22:51:51.000Z",
    "interactionCount": "10714",
    "duration": "PT1M8S",
    "embedURL": "https://youtube.googleapis.com/v/5n4I9Or2dII",
    "thumbnailURL": "https://i.ytimg.com/vi/5n4I9Or2dII/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "FAQ 3: Why does Guidance sell its contracts to Freddie Mac?",
    "description": "This is one of the most common questions that we receive about Guidances lending practices. Who is Freddie Mac and what is our relationship with them? Freddie Mac is not a lender or a bank. This means they are barred from being able to lend money to anyone. They were created in 1970 as a private entity to provide additional capital to lenders (wiki). This helped to better facilitate the dream of home ownership for Americans. Their primary mode of business is as an investor that buys the loans and re-sells them to other investors such as pension funds and insurance companies. Buying and selling debt, however, is not permissible according to the Islamic Shariah. Guidances shariah board, along with 18 law firms, developed a unique contract with Freddie Mac based upon the co-ownership model. This ensures that when Guidance transacts with Freddie Mac, a sale of the debt does not occur. It is also important to note that the contract signed by the homeowner remains unchanged throughout the process. Guidance simply brings on Freddie Mac as an additional investor on the back-end (but again, with a shariah compliant contract). Guidance is the only institution that has set up such an agreement with Freddie Mac. This process is also laid out in one of our White Papers explaining the co-ownership model: The Declining Balance Co-ownership Program has been designed to comply with Sharia principles from A to Z -- from the point when a customer enters a home financing transaction to the point where the ultimate funding for that transaction is provided. Guidance obtains external funding for the Program through an agreement with Freddie Mac, a corporation chartered by Congress to support the home financing market. In this agreement, Freddie Mac makes investments to take a co-ownership stake in properties financed under the Program. At a second stage, Freddie Mac creates Sharia-compliant securities invested in the co-ownership assets. These securities will be offered by Guidance to Islamic banks and other Islamic capital market participants around the world. These securities constitute a significant innovation that contributes to the development of the international Islamic finance market. See more at: http://www.guidanceresidential.com/",
    "uploadDate": "2013-05-16T22:56:27.000Z",
    "interactionCount": "7566",
    "duration": "PT2M",
    "embedURL": "https://youtube.googleapis.com/v/G92ZcCLw9Fk",
    "thumbnailURL": "https://i.ytimg.com/vi/G92ZcCLw9Fk/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "FAQ 4: How does Guidance determine the price for facilitating home ownership through their program?",
    "description": "Guidance's program is distinctly different from conventional models where a lender charges for money lent. Guidance charges a usage fee (profit payment) for occupation of the home. Homebuyers naturally insist that shariah alternatives be competitive. This led to the usage fee be indexed against prices from the conventional home finance market. Shariah scholars deemed this permissible as the pricing doesnt change the ruling. However, if an Islamic index existed, it would be preferred to use that. See more at: http://www.guidanceresidential.com",
    "uploadDate": "2013-05-16T22:57:54.000Z",
    "interactionCount": "4414",
    "duration": "PT1M1S",
    "embedURL": "https://youtube.googleapis.com/v/u8W12ZpfVkk",
    "thumbnailURL": "https://i.ytimg.com/vi/u8W12ZpfVkk/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "FAQ 2: What type of Partnership is this?",
    "description": "Learn more about the Guidance Residential Diminishing Musharakah Partnership model. There are various types of musharakah (partnerships) in Islamic jurisprudence. Shirkat-ul-Aqd -- A traditional partnership normally used for joint commercial ventures. Shirkat-ul-Milk -- Declining balance co-ownership. The program is not intended to be a commercial venture in which gains/losses are shared. The program is intended for the home buyer to buy out Guidances shares over time while retaining all of the appreciating value in the property. See more at http://www.guidanceresidential.com",
    "uploadDate": "2013-05-16T22:52:38.000Z",
    "interactionCount": "5198",
    "duration": "PT51S",
    "embedURL": "https://youtube.googleapis.com/v/6byRHj3HU5Q",
    "thumbnailURL": "https://i.ytimg.com/vi/6byRHj3HU5Q/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Guidance Residential - Do's and Don'ts when financing your Home",
    "description": "While you are in the process of getting a new financing contract, keep your financial status as stable as possible until the transaction is funded and recorded. Here are some things that you should and shouldnt do! Contact us with any questions you might have throughout the process so that you dont do anything that may cause you to be denied for your home financing.",
    "uploadDate": "2015-12-23T18:33:23.000Z",
    "interactionCount": "5992",
    "duration": "PT2M20S",
    "embedURL": "https://youtube.googleapis.com/v/OH_KJu7mHrc",
    "thumbnailURL": "https://i.ytimg.com/vi/OH_KJu7mHrc/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "How do you determine if a companys products are Sharia compliant?  Shaykh Yusuf DeLorenzo",
    "description": "Ever wonder whether your Islamic Finance company is truly Sharia-compliant? In this brief video, world-renowned Islamic Finance scholar Shaykh Yusuf DeLorenzo says that there is a simple rule of thumb when evaluating whether an Islamic Finance company is truly Sharia-compliant. Watch the video to listen to his recommendations! To learn more, visit https://www.guidanceresidential.com/us-islamic-home-finance Guidance Residential, LLC #1 National Provider of Islamic Home Financing.",
    "uploadDate": "2013-05-02T14:30:02.000Z",
    "interactionCount": "1865",
    "duration": "PT48S",
    "embedURL": "https://youtube.googleapis.com/v/SaZH8Jr8vQc",
    "thumbnailURL": "https://i.ytimg.com/vi/SaZH8Jr8vQc/hqdefault.jpg"
  }
  </script><script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Guidance Residential – Why we do What we do",
    "description": "Guidance Residential, LLC strives to provide access for every Muslim-American family to own a home in accordance with the values and principles of Islam. By offering Shariah-compliant home financing programs for home purchase and refinance, we help Muslim-Americans own a home without compromising their faith.",
    "uploadDate": "2015-06-05T17:08:29.000Z",
    "interactionCount": "14043",
    "duration": "PT1M52S",
    "embedURL": "https://youtube.googleapis.com/v/bLcIpeevxIE",
    "thumbnailURL": "https://i.ytimg.com/vi/bLcIpeevxIE/hqdefault.jpg"
  }
  </script><script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "What should homebuyers look for in an Islamic Finance company? - Shaykh Yusuf DeLorenzo",
    "description": "We asked world-renowned Islamic finance scholar Shaykh Yusuf Talal DeLorenzo what potential homebuyers should look for when selecting an Islamic Finance company & how do you determine if a company's products are Sharia compliant. Watch the video to hear his recommendations!. To learn more, visit https://www.guidanceresidential.com/us-islamic-home-finance.",
    "uploadDate": "2013-04-30T16:20:35.000Z",
    "interactionCount": "2743",
    "duration": "PT1M",
    "embedURL": "https://youtube.googleapis.com/v/K5G6QCdKQAQ",
    "thumbnailURL": "https://i.ytimg.com/vi/K5G6QCdKQAQ/hqdefault.jpg"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Videos'])
@endsection
