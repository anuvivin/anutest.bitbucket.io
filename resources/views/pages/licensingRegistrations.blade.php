@extends('layouts.default')

@section('title', 'Licensing And Registrations')

@section('content')
<div class="middle">
  <div class="container licensing-registrations">
    <div class="main-heading">
      <div class="title">
        LICENSING AND REGISTRATIONS
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance Residential, LLC, (Nationwide Mortgage Licensing System No.2908), 11107 Sunset Hills Rd, Suite 200, Reston, VA 20190 is licensed by the Department of Business Oversight under the California Residential Mortgage Lending Act (413-0427); Alabama Consumer Credit (22410); Arizona Mortgage Banker (0923408); Arkansas Mortgage Banker-Broker-Servicer (112069); Colorado Mortgage Company Registration ; Connecticut Mortgage Lender (18263); Delaware Lender (022090); District of Columbia Mortgage Dual Authority (MLB2908); Florida Mortgage Lender (MLD656); Georgia Mortgage Lender (17286); Illinois Residential Mortgage Licensee (MB.0006455 11107 Sunset Hills Road, Suite 200, , Reston, VA 20190; MB.0006455-001 100 East Roosevelt Rd, Units 44 &amp; 45, Villa Park, IL 60181) licensed by the ILDFPR, James R Thompson Center (JRTC), 100 West Randolph Street, 9th Floor, Chicago, IL 60601, 888-473-4858 (General), 844-768-1713 (Banking Div); Kansas Licensed Mortgage Company (MC.0025178); Kentucky Mortgage Company (MC361158 &amp;MC361566); Maryland Mortgage Lender (12927); Massachusetts Mortgage Lender (ML2908); Michigan 1st Mortgage Lender, Broker &amp; Servicer Registrant (FR-0941); Minnesota Mortgage Originator (MN-MO 20320419); New Jersey Licensed Lender (9933648); New York - Licensed Mortgage Banker - New York Department of Financial Services (B500726) (NY location: 171-21 Jamaica Ave, 1st Floor, Jamaica, NY 11432); North Carolina Mortgage Lender (L-112542); Ohio Mortgage Broker Act Mortgage Banker Exemption (MBMB 850079.000); Oregon Mortgage Lender (ML-4145); Pennsylvania - licensed by the Pennsylvania Department of Banking, Mortgage Lender (21050); Rhode Island Lender License (20163318LL); South Carolina BFI Mortgage Lender/Servicer (MLS-2908); Tennessee Mortgage License (109274); Texas SML Mortgage Banker Registrant ;  Virginia Lender Licensee (MC-2138) licensed by the Virginia State Corporation Commission; Washington Consumer Loan Company (CL-2908); Wisconsin Mortgage Banker (46355BA).
      </p>
      <p class="desc-text">
        Guidance Residential, LLC is not licensed and does not originate in: Alaska, Hawaii, Idaho, Indiana, Iowa, Louisiana, Maine, Mississippi, Missouri, Montana, Nebraska, Nevada, New Hampshire, New Mexico, North Dakota, Oklahoma, South Dakota, Utah, Vermont, West Virginia, Wyoming, Puerto Rico, and the US Virgin Islands.
      </p>
      <p class="desc-text">
        Under Minnesota law, this advertisement is not an offer to enter into a rate lock-in agreement.
      </p>
      <p class="desc-text">
        TEXAS DEPARTMENT OF SAVINGS AND MORTGAGE LENDING – COMPLAINT AND RECOVERY FUND NOTICE
      </p>
      <p class="desc-text">
        CONSUMERS WISHING TO FILE A COMPLAINT AGAINST A COMPANY OR A RESIDENTIAL MORTGAGE LOAN ORIGINATOR SHOULD COMPLETE AND SEND A COMPLAINT FORM TO THE TEXAS DEPARTMENT OF
        SAVINGS AND MORTGAGE LENDING, 2601 NORTH LAMAR, SUITE 201, AUSTIN, TEXAS 78705. COMPLAINT FORMS AND INSTRUCTIONS MAY BE OBTAINED FROM THE DEPARTMENT'S WEBSITE AT
        WWW.SML.TEXAS.GOV. A TOLL-FREE CONSUMER HOTLINE IS AVAILABLE AT 1-877-276-5550. THE DEPARTMENT MAINTAINS A RECOVERY FUND TO MAKE PAYMENTS OF CERTAIN ACTUAL OUT OF
        POCKET DAMAGES SUSTAINED BY BORROWERS CAUSED BY ACTS OF LICENSED RESIDENTIAL MORTGAGE LOAN ORIGINATORS. A WRITTEN APPLICATION FOR REIMBURSEMENT FROM THE RECOVERY FUND
        MUST BE FILED WITH AND INVESTIGATED BY THE DEPARTMENT PRIOR TO THE PAYMENT OF A CLAIM. FOR MORE INFORMATION ABOUT THE RECOVERY FUND, PLEASE CONSULT THE DEPARTMENT'S
        WEBSITE AT WWW.SML.TEXAS.GOV.
      </p>
      <p class="desc-text">
        This was last updated on April 19, 2018.
      </p>
    </div>
  </div>
</div>
@endsection
