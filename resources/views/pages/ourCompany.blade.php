@extends('layouts.default')

@section('title', 'Our Company')

@section('content')
  <div class="container ourCompany">
    <div class="main-heading">
      <h1 class="title">
        What We Do
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance Residential, a wholly owned subsidiary of Guidance Financial Group, is the #1 U.S. Islamic Home Financing Provider&reg;. Learn more about our company and unique home financing solution.
      </p>
    </div>
    <div class="tab-step-container">
      <div class="steps">
        <ul class="tab-steps">
          <li class="tab-step-item selected">
            <div class="app-circle">
              <span class="icon fa fa-th-list"></span>
            </div>
            <h4 class="sub-text">
              <span class="step-num" id="overview">Overview</span>
            </h4>
          </li>
          <li class="tab-step-item">
            <div class="app-circle">
              <span class="icon fa fa-bookmark-o"></span>
            </div>
            <h4 class="sub-text">
              <span class="step-num" id="guidPrinciples">Guiding Principles</span>
            </h4>
          </li>
          <li class="tab-step-item">
            <div class="app-circle">
              <span class="icon fa fa-group"></span>
            </div>
            <h4 class="sub-text">
              <span class="step-num" id="culture">Culture</span>
            </h4>
          </li>
          <li class="tab-step-item">
            <div class="app-circle">
              <span class="icon fa fa-pencil-square-o"></span>
            </div>
            <h4 class="sub-text">
              <span class="step-num" id="whatWeDo">What We Do</span>
            </h4>
          </li>
        </ul>
      </div>
      <div class="content overview active">
        <div class="main-heading">
          <h1 class="title six">
            Specialization
          </h1>
          <hr class="seperate-line"></hr>
          <p class="desc-text">
            Guidance Residential specializes in Shariah-compliant home financing in the U.S. Our unique Declining Balance Co-ownership Program was created under the guidance of seven globally renowned Shariah scholars
            and eighteen U.S. law firms to ensure it meets strict adherence to legal jurisprudence of the Islamic faith and conforms to U.S. legal and regulatory requirements.
          </p>
        </div>
        <div class="split-content-section difference">
          <div class="image-container"></div>
          <div class="main-heading">
            <h1 class="title six">
              Differentiation
            </h1>
            <span class="seperate-line"></span>
            <p class="desc-text">
              Guidance Residential's Declining Balance Co-ownership Program is based on a partnership model rather than a lender-borrower relationship. Unlike traditional home financing, homebuyers enter an equitable,
               Shariah-compliant partnership contract and enjoys benefits of home ownership without compromising faith-based principles.
            </p>
          </div>
        </div>
        <div class="split-content-section blue">
          <div class="main-heading">
            <h1 class="title six">
              Real Estate Agent Services
            </h1>
            <span class="seperate-line"></span>
            <p class="desc-text">
              GuidanceRealty.com, Guidance Residential's real estate agent partner program, facilitates the homebuying and selling process through an integrated website that uses agent data and customer queries to connect consumers with real estate agents.
              Customers who use a <a href="{{ route('guidanceRealty') }}">GuidanceRealty.com</a> affiliated agent and finance with Guidance Residential receive an appraisal credit up to $500* at closing.
            </p>
            <div class="button-wrapper">
              <a href="{{ route('getPreQualifiedRefferals') }}" class="main-button gold">Learn More</a>
            </div>
            <p class="desc-text italic disclaimer">
              *Restrictions Apply: Buyer must use Guidance Residential, LLC for home financing and a GuidanceRealty.com participating agent to receive up to a $500 appraisal credit at closing. Closing credit is subject to financing provider approval and is not available where prohibited by law. Closing credit cannot be combined with any other current promotions.
            </p>
          </div>
          <div class="app-circle">
            <span class="fa fa-home"></span>
          </div>
        </div>
      </div>
      <div class="content guidPrinciples">
        <div class="main-heading">
          <h1 class="title six">
            Guiding Principles
          </h1>
          <hr class="seperate-line"></hr>
          <p class="desc-text">Guidance is dedicated to serving consumers financial products that are both compliant with faith-based principles and also meet industry
            standards of excellence. We always strive to offer an ethical and principled alternative financial solution for U.S. homebuyers and refinancers.
          </p>
        </div>
        <div class="details">
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-check"></span>
            </div>
            <div class="description">
              <p class="desc-text">Treating others with Integrity and Fairness.</p>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-check"></span>
            </div>
            <div class="description">
              <p class="desc-text">Responsibility and Transparency in Business</p>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-check"></span>
            </div>
            <div class="description">
              <p class="desc-text">Professionalism in Dealing with Customers and Stakeholders</p>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-check"></span>
            </div>
            <div class="description">
              <p class="desc-text">Innovation in Solutions and Services</p>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-check"></span>
            </div>
            <div class="description">
              <p class="desc-text">Commitment in Being a Significant Contributor in Societal Growth</p>
            </div>
          </div>
        </div>
      </div>
      <div class="content culture">
        <div class="main-heading">
          <h1 class="title six">
            Culture
          </h1>
          <hr class="seperate-line"></hr>
        </div>
        <div class="details">
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-line-chart"></span>
            </div>
            <div class="description">
              <h3 class="title three">Leadership</h3>
              <p class="desc-text">We always strive to be at the forefront of the Shariah-compliant home financing industry. We actively engage in the learning and development of our employees and organization to provide the most contemporary, relevant and appropriate Shariah-compliant solutions and services at all time.</p>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-handshake-o"></span>
            </div>
            <div class="description">
              <h3 class="title three">Trustworthy</h3>
              <p class="desc-text">We always act ethically and morally - as individuals and as an organization. We conduct transparent business practices,
                ensure we are all held accountable for our actions and strive to provide our customers with professional, knowledgeable and reliable service at all times.</p>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-comments-o"></span>
            </div>
            <div class="description">
              <h3 class="title three">Responsive</h3>
              <p class="desc-text">We always listen to understand our customers' needs and desires with respect courtesy and care. We employ an empathetic response and approachable attitude at all times.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="content whatWeDo">
        <div class="main-heading">
          <h1 class="title six">
            What We Do
          </h1>
          <hr class="seperate-line"></hr>
        </div>
        <div class="details">
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-building"></span>
            </div>
            <div class="description">
              <h3 class="title three">Residential Home Financing</h3>
              <span class="seperate-line sub"></span>
              <p class="desc-text">Guidance Residential is the leading and largest provider of Shariah-compliant home financing in the U.S. market. We offer financing to first-time homebuyers and residential investment property buyers.</p>
              <div class="button-wrapper">
                <a href="{{ route('financingOptions') }}" class="main-button flex-button">Learn More</a>
              </div>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-sign-out"></span>
            </div>
            <div class="description">
              <h3 class="title three">Refinancing</h3>
              <span class="seperate-line sub"></span>
              <p class="desc-text">We offer refinancing from traditional and other alternative home financing contracts to our Declining Balance Co-ownership Program as well as refinancing from existing Guidance Residential contracts.</p>
              <div class="button-wrapper">
                <a href="{{ route('refinancingOptions') }}" class="main-button flex-button">Learn More</a>
              </div>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-handshake-o"></span>
            </div>
            <div class="description">
              <h3 class="title three">Real Estate Agent Partner Program</h3>
              <span class="seperate-line sub"></span>
              <p class="desc-text">GuidanceRealty.com is a one stop platform for homebuyers and sellers to connect with experienced, local real estate agents. The GuidanceRealty.com partner program helps to simplify the homebuying and selling experience with an additional savings opportunity.</p>
              <div class="button-wrapper">
                <a href="{{ route('guidanceRealty') }}" class="main-button flex-button">Learn More</a>
              </div>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-search"></span>
            </div>
            <div class="description">
              <h3 class="title three">Home Search engine</h3>
              <span class="seperate-line sub"></span>
              <p class="desc-text">
                Our home search engine helps homebuyers find their dream home in their preferred community and helps them connect with experienced real estate agents.
              </p>
              <div class="button-wrapper">
                <a href="{{ config('urls.grealty.search') }}" class="main-button flex-button">Learn More</a>
              </div>
            </div>
          </div>
          <div class="step">
            <div class="app-circle">
              <span class="fa fa-mobile-phone"></span>
            </div>
            <div class="description">
              <h3 class="title three">Mobile Apps</h3>
              <span class="seperate-line sub"></span>
              <p class="desc-text">
                Our proprietary mobile app offers a seamless closing experience to homebuyers and real estate agents. The app allows customers and their real estate agents to access important information
                regarding the homebuying process as well as stay updated on the status of finance applications.
              </p>
              <div class="button-wrapper">
                <a href="{{ route('appDownload') }}" class="main-button flex-button">Learn More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-step-mobile">
      <a href="#overviewTab" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="overviewTab">
        <h4 class="sub-text">
          Overview
        </h4>
      </a>
      <div class="content collapse" id="overviewTab">
        <div class="icon-wrapper">
          <div class="icon app-circle">
            <span class="fa fa-th-list"></span>
          </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title six">
              Specialization
            </h1>
            <hr class="seperate-line"></hr>
            <p class="desc-text">
              Guidance Residential specializes in Shariah-compliant home financing in the U.S. Our unique Declining Balance Co-ownership Program was created under the guidance of six globally renowned Shariah scholars
              and eighteen U.S. law firms to ensure it meets strict adherence to legal jurisprudence of the Islamic faith and conforms to U.S. legal and regulatory requirements.
            </p>
          </div>
          <div class="split-content-section difference">
            <div class="image-container"></div>
            <div class="main-heading">
              <h1 class="title six">
                Differentiation
              </h1>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Guidance Residential's Declining Balance Co-ownership Program is based on a partnership model rather than a lender-borrower relationship. Unlike traditional home financing, homebuyers enter an equitable,
                 Shariah-compliant partnership contract and enjoys benefits of home ownership without compromising faith-based principles.
              </p>
            </div>
          </div>
          <div class="split-content-section blue">
            <div class="main-heading">
              <h1 class="title six">
                Real Estate Agent Services
              </h1>
              <span class="seperate-line"></span>
              <p class="desc-text">
                GuidanceRealty.com, Guidance Residential's real estate agent partner program, facilitates the homebuying and selling process through an integrated website that uses agent data and customer queries to connect consumers with real estate agents.
                Customers who use a GuidanceRealty.com affiliated agent and finance with Guidance Residential receive an appraisal credit up to $500* at closing.
              </p>
              <div class="button-wrapper">
                <a href="{{ route('getPreQualifiedRefferals') }}" class="main-button gold">Learn More</a>
              </div>
              <p class="desc-text italic disclaimer">
                *Restrictions Apply: Buyer must use Guidance Residential, LLC for home financing and a GuidanceRealty.com participating agent to receive up to a $500 appraisal credit at closing. Closing credit is subject to financing provider approval and is not available where prohibited by law. Closing credit cannot be combined with any other current promotions.
              </p>
            </div>
            <div class="app-circle">
              <span class="fa fa-home"></span>
            </div>
          </div>
        </div>
      </div>
      <a href="#guidPrincipleTab" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="guidPrincipleTab">
        <h4 class="sub-text">
          Guiding Principles
        </h4>
      </a>
      <div class="content collapse" id="guidPrincipleTab">
        <div class="icon-wrapper">
            <div class="icon app-circle">
              <span class="fa fa-bookmark-o"></span>
            </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title six">
              Guiding Principles
            </h1>
            <hr class="seperate-line"></hr>
            <p class="desc-text">Guidance is dedicated to serving consumers financial products that are both compliant with faith-based principles and also meet industry
              standards of excellence. We always strive to offer an ethical and principled alternative financial solution for U.S. homebuyers and refinancers.
            </p>
          </div>
          <div class="details">
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-check"></span>
              </div>
              <div class="description">
                <p class="desc-text">Treating others with Integrity and Fairness.</p>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-check"></span>
              </div>
              <div class="description">
                <p class="desc-text">Responsibility and Transparency in Business</p>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-check"></span>
              </div>
              <div class="description">
                <p class="desc-text">Professionalism in Dealing with Customers and Stakeholders</p>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-check"></span>
              </div>
              <div class="description">
                <p class="desc-text">Innovation in Solutions and Services</p>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-check"></span>
              </div>
              <div class="description">
                <p class="desc-text">Commitment in Being a Significant Contributor in Societal Growth</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <a href="#cultureTab" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="cultureTab">
        <h4 class="sub-text">
          Culture
        </h4>
      </a>
      <div class="content collapse" id="cultureTab">
        <div class="icon-wrapper">
            <div class="icon app-circle">
              <span class="fa fa-user-circle-o"></span>
            </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title six">
              Culture
            </h1>
            <hr class="seperate-line"></hr>
          </div>
          <div class="details">
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-line-chart"></span>
              </div>
              <div class="description">
                <h3 class="title three">Leadership</h3>
                <p class="desc-text">We always strive to be at the forefront of the Shariah-compliant home financing industry. We actively engage in the learning and development of our employees and organization to provide the most contemporary, relevant and appropriate Shariah-compliant solutions and services at all time.</p>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-handshake-o"></span>
              </div>
              <div class="description">
                <h3 class="title three">Trustworthy</h3>
                <p class="desc-text">We always act ethically and morally - as individuals and as an organization. We conduct transparent business practices,
                  ensure we are all held accountable for our actions and strive to provide our customers with professional, knowledgeable and reliable service at all times.</p>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-comments-o"></span>
              </div>
              <div class="description">
                <h3 class="title three">Responsive</h3>
                <p class="desc-text">We always listen to understand our customers' needs and desires with respect courtesy and care. We employ an empathetic response and approachable attitude at all times.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <a href="#whatWeDoTab" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="whatWeDoTab">
        <h4 class="sub-text">
          What We Do
        </h4>
      </a>
      <div class="content collapse" id="whatWeDoTab">
        <div class="icon-wrapper">
          <div class="icon app-circle">
            <span class="fa fa-pencil-square-o"></span>
          </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title six">
              What We Do
            </h1>
            <hr class="seperate-line"></hr>
          </div>
          <div class="details">
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-building"></span>
              </div>
              <div class="description">
                <h3 class="title three">Residential Home Financing</h3>
                <span class="seperate-line sub"></span>
                <p class="desc-text">Guidance Residential is the leading and largest provider of Shariah-compliant home financing in the U.S. market. We offer financing to first-time homebuyers and residential investment property buyers.</p>
                <div class="button-wrapper">
                  <a href="{{ route('financingOptions') }}" class="main-button">Learn More</a>
                </div>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-sign-out"></span>
              </div>
              <div class="description">
                <h3 class="title three">Refinancing</h3>
                <span class="seperate-line sub"></span>
                <p class="desc-text">We offer refinancing from traditional and other alternative home financing contracts to our Declining Balance Co-ownership Program as well as refinancing from existing Guidance Residential contracts.</p>
                <div class="button-wrapper">
                  <a href="{{ route('refinancingOptions') }}" class="main-button">Learn More</a>
                </div>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-handshake-o"></span>
              </div>
              <div class="description">
                <h3 class="title three">Real Estate Agent Partner Program</h3>
                <span class="seperate-line sub"></span>
                <p class="desc-text">GuidanceRealty.com is a one stop platform for homebuyers and sellers to connect with experienced, local real estate agents. The GuidanceRealty.com partner program helps to simplify the homebuying and selling experience with an additional savings opportunity.</p>
                <div class="button-wrapper">
                  <a href="{{ route('guidanceRealty') }}" class="main-button">Learn More</a>
                </div>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-search"></span>
              </div>
              <div class="description">
                <h3 class="title three">Home Search engine</h3>
                <span class="seperate-line sub"></span>
                <p class="desc-text">
                  Our home search engine helps homebuyers find their dream home in their preferred community and helps them connect with experienced real estate agents.
                </p>
                <div class="button-wrapper">
                  <a href="{{ config('urls.grealty.search') }}" class="main-button">Learn More</a>
                </div>
              </div>
            </div>
            <div class="step">
              <div class="app-circle">
                <span class="fa fa-mobile-phone"></span>
              </div>
              <div class="description">
                <h3 class="title three">Mobile Apps</h3>
                <span class="seperate-line sub"></span>
                <p class="desc-text">
                  Our proprietary mobile app offers a seamless closing experience to homebuyers and real estate agents. The app allows customers and their real estate agents to access important information
                  regarding the homebuying process as well as stay updated on the status of finance applications.
                </p>
                <div class="button-wrapper">
                  <a href="{{ route('appDownload') }}" class="main-button">Learn More</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "RESIDENTIAL HOME FINANCING",
    "url": "{{ Request::url() }}",
    "description": "Guidance Residential is the leading and largest provider of Shariah-compliant home financing in the U.S. market. We offer financing to first-time homebuyers and residential investment property buyers."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "REFINANCING",
    "url": "{{ Request::url() }}",
    "description": "We offer refinancing from traditional and other alternative home financing contracts to our Declining Balance Co-ownership Program as well as refinancing from existing Guidance Residential contracts."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "REAL ESTATE AGENT PARTNER PROGRAM",
    "url": "{{ Request::url() }}",
    "description": "GuidanceRealty.com is a one stop platform for homebuyers and sellers to connect with experienced, local real estate agents. The GuidanceRealty.com partner program helps to simplify the homebuying and selling experience with an additional savings opportunity."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "HOME SEARCH ENGINE",
    "url": "{{ Request::url() }}",
    "description": "Our home search engine helps homebuyers find their dream home in their preferred community and helps them connect with experienced real estate agents."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "MOBILE APPS",
    "url": "{{ Request::url() }}",
    "description": "Our proprietary mobile app offers a seamless closing experience to homebuyers and real estate agents. The app allows customers and their real estate agents to access important information regarding the homebuying process as well as stay updated on the status of finance applications."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Whatwedo'])
@endsection
