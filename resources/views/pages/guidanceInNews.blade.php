@extends('layouts.default')

@section('title', 'Guidance In The News')

@section('content')
<div class="container guidanceInNews">
  <div class="main-heading">
    <h1 class="title">
      DISCOVER GUIDANCE RESIDENTIAL NEWS AND MEDIA COVERAGE
    </h1>
    <span class="seperate-line"></span>
    <p class="desc-text italic">As Seen In:</p>
  </div>
  <div class="owl-carousel channels">
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/thewashingtonpost_colored_transparent.png') }}" alt="The Washington Post">
    </div>
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/cnn_bloombergbusiness_colored_transparent.png') }}" alt="CNN & Bloomberg Business">
    </div>
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/thenewyorktimes_colored_transparent.png') }}" alt="New York Times">
    </div>
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/usatoday_colored_transparent.png') }}" alt="USA Today">
    </div>
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/thewallstreetjournal_colored_transparent.png') }}" alt="The Wall Street Journal">
    </div>
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/npr_houstonchronicle_colored_transparent.png') }}" alt="NPR &amp; Houston Chronicle">
    </div>
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/aljazeera_pbs_colored_transparent.png') }}" alt="PBS &amp; Aljazeera">
    </div>
    <div class="item">
      <img src="{{ mix_remote('images/guidanceInNews/huffingtonpost_colored_transparent.png') }}" alt="The Huffington Post">
    </div>
  </div>
  <div class="news-container">
    <div class="news">
      <div class="app-circle">
        <span class="fa fa-globe"></span>
      </div>
      <div class="description">
        <h3 class="title three bold">Fintech: The new barrier to entry in Islamic Finance<br><span class="light">(ISLAMIC FINANCE NEWS)</span></h3>
        <span class="seperate-line"></span>
        <p class="desc-text italic">In the US, financial institutions have become very familiar with the word fintech, and indeed have come to fear it. . .</p>
        <div class="button-wrapper">
          <a href="{{ mix_remote('docs/The_New_Barrier_to_Entry_in_Islamic_Finance_by_Kal_Elsayed_(IFN_Fintech_Newsletter).pdf') }}" class="main-button flex-button pdf-download" target="_blank"><span class="fa fa-download"></span>download the pdf</a>
        </div>
      </div>
    </div>
    <div class="news">
      <div class="app-circle">
        <span class="fa fa-globe"></span>
      </div>
      <div class="description">
        <h3 class="title three bold">WHEN HEDGE FUNDS MEET ISLAMIC FINANCE<br><span class="light">(THE WALL STREET JOURNAL)</span></h3>
        <span class="seperate-line"></span>
        <p class="desc-text italic">Sheik Yusuf Talal DeLorenzo is on a mission to meld centuries-old Islamic law with modern finance in the U.S. . .</p>
        <div class="button-wrapper">
          <a href="https://www.guidanceresidential.com/blog/wp-content/uploads/2015/05/Wall-Street-Article.pdf" class="main-button flex-button pdf-download" target="_blank"><span class="fa fa-download"></span>download the pdf</a>
        </div>
      </div>
    </div>
    <div class="news">
      <div class="app-circle">
        <span class="fa fa-globe"></span>
      </div>
      <div class="description">
        <h3 class="title three bold">A HIGHER LAW FOR LENDING <br><span class="light">(THE WASHINGTON POST)</span></h3>
        <span class="seperate-line"></span>
        <p class="desc-text italic">The mortgage industry may be in meltdown, but at least one class of lender appears to be flourishing: Islamic finance companies. . .</p>
        <div class="button-wrapper">
          <a href="http://www.washingtonpost.com/wp-dyn/content/article/2008/05/12/AR2008051202740.html" class="main-button flex-button" target="_blank"><span class="fa fa-arrow-circle-right"></span>Read more</a>
        </div>
      </div>
    </div>
    <div class="news">
      <div class="app-circle">
        <span class="fa fa-globe"></span>
      </div>
      <div class="description">
        <h3 class="title three bold">FIRMS OFFER MUSLIMS ALTERNATIVES TO MORTGAGES<br><span class="light">(NPR)</span></h3>
        <span class="seperate-line"></span>
        <p class="desc-text italic">Islamic finance firms help Muslim customers buy homes without compromising their religious principles. This conservative approach to finance keeps the firms clear of the subprime drama . . .</p>
        <div class="button-wrapper">
          <a href="https://www.npr.org/templates/story/story.php?storyId=92787430" class="main-button flex-button" target="_blank"><span class="fa fa-arrow-circle-right"></span>Read more</a>
        </div>
      </div>
    </div>
    <div class="news">
      <div class="app-circle">
        <span class="fa fa-globe"></span>
      </div>
      <div class="description">
        <h3 class="title three bold">ISLAMIC MORTGAGES IN U.S. SEEM TO BE BOOMING IN RECESSION<br><span class="light">(THE HOUSTON CHRONICLE)</span></h3>
        <span class="seperate-line"></span>
        <p class="desc-text italic">At a time when most of the mortgage industry is still reeling from the real estate crisis, alternative home financing arrangements designed to adhere to Islamic principles are thriving. . .</p>
        <div class="button-wrapper">
          <a href="http://www.chron.com/life/houston-belief/article/Islamic-mortgages-in-U-S-seem-to-be-booming-in-1738159.php" class="main-button flex-button" target="_blank"><span class="fa fa-arrow-circle-right"></span>Read more</a>
        </div>
      </div>
    </div>
    <div class="news">
      <div class="app-circle">
        <span class="fa fa-globe"></span>
      </div>
      <div class="description">
        <h3 class="title three bold">CLOSING ON A DREAM<br><span class="light">(THE NEW YORK TIMES)</span></h3>
        <span class="seperate-line"></span>
        <p class="desc-text italic">Muslims are increasingly using faith-based financing options to buy houses. . .</p>
        <div class="button-wrapper">
          <a href="http://www.nytimes.com/2009/05/03/nyregion/thecity/03rmort.html?_r=2" class="main-button flex-button" target="_blank"><span class="fa fa-arrow-circle-right"></span>Read more</a>
        </div>
      </div>
    </div>
  </div>
  <div class="single-video-section">
    <div class="main-heading">
      <h1 class="title six">
        Deen TV Interview with Guidance Residential CEO
      </h1>
      <span class="seperate-line"></span>
    </div>
    <div class="video-container">
      <div class="video" data-video-id="NXLE6ea8kxw">
        <img src="{{ mix_remote('images/guidanceInNews/deen_tv_interview.jpg') }}" alt="Deen TV Interview with guidance residential CEO" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
  </div>
  <div class="media-inquiry-flag">
    <div class="app-circle">
      <span class="fa fa-newspaper-o"></span>
    </div>
    <div class="main-heading left">
      <h3 class="title three golden">Media inquiry</h3>
      <span class="seperate-line sub"></span>
      <div class="contact-info">
        <div class="item">
          <a href="mailto:Communications@GuidanceResidential.com">Communications@GuidanceResidential.com</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Deen TV Interview with Guidance Residential CEO",
    "description": "Joshua Salaam from Deen TV tours the beautiful Guidance Residential office in Reston, Virginia and chats with CEO Khaled Elsayed about Islamic Finance.",
    "uploadDate": "2016-07-26T20:37:43.000Z",
    "interactionCount": "4495",
    "duration": "PT18M12S",
    "embedURL": "https://youtube.googleapis.com/v/NXLE6ea8kxw",
    "thumbnailURL": "https://i.ytimg.com/vi/NXLE6ea8kxw/hqdefault.jpg"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_News'])
@endsection
