@extends('layouts.default')

@section('title', 'Download GiOS Mobile App')

@section('content')
<div class="middle">
  <div class="container app-download">
    <div class="main-heading">
      <h1 class="title">
        <span class="lowercase">gi</span>OS App for Homeowners
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Download the GiOS Apps on the App Store and Google Play
      </p>
    </div>
    <div class="download-image-back">
      <div class="image-container app-download-img"></div>
    </div>
    <div class="download-image-container">
      <a href="{{ config('urls.apps.apple') }}" target="_blank"><img src="{{ mix_remote('images/appDownload/appleLogo.svg') }}" alt="Apple store Image" class='app-store-img'></a>
      <a href="{{ config('urls.apps.android') }}" target="_blank"><img src="{{ mix_remote('images/appDownload/google_play.png') }}" alt="Google Play Image" class='app-store-img android'></a>
    </div>
  </div>
  <div class="how-it-works app-download">
    <div class="main-heading">
      <h1 class="title six">
        Features
      </h1>
      <span class="seperate-line"></span>
    </div>
    <div class="realty-steps">
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-film"></span>
        </div>
        <h4 class="title three">View Multimedia</h4>
        <p class="desc-text">Access our Knowledge Center for videos, glossaries, FAQs, blogs and more.</p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-book"></span>
        </div>
        <h4 class="title three">Understand Islamic Finance</h4>
        <p class="desc-text">Learn about the Declining Balance Co-ownership Program and riba-free home financing.</p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-pencil-square-o"></span>
        </div>
        <h4 class="title three">Start the Process</h4>
        <p class="desc-text">Pre-Qualify for home financing.</p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-home"></span>
        </div>
        <h4 class="title three">Achieve Homeownership</h4>
        <p class="desc-text">Track the progress of your home financing and purchase.</p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-users"></span>
        </div>
        <h4 class="title three">Connect with Experts</h4>
        <p class="desc-text">Connect with an Account Executive and your Real Estate Agent in one streamlined location.</p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-exclamation-circle"></span>
        </div>
        <h4 class="title three">Never Miss a Beat</h4>
        <p class="desc-text">Receive automatic notifications that provide clarity about each step in the home financing process.</p>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-calculator"></span>
        </div>
        <h4 class="title three">Calculate Finances</h4>
        <p class="desc-text">Use our calculator tools to determine affordability, make rent/buy decisions and refinance savings.</p>
      </div>
    </div>
  </div>
</div>
@endsection
