@extends('layouts.default')

@section('title', 'Assistance And Repayment Options')

@section('content')
<div class="middle">
  <div class="container assistance-repayment">
    <div class="main-heading">
      <div class="title">
        Assistance And Repayment Options
      </div>
      <hr class="seperate-line"></hr>
    </div>
    <div class="accordion-split-content full">
      <div class="accordion-content">
        <a href="#contactUs" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="contactUs">
          <span class="fa expand-icon"></span><h4 class="title four bold">CONTACT US</h4>
        </a>
        <div class="text-content left collapse" id="contactUs">
          <div class="text">
            <p class="desc-text">
              Call at 1-877-402-0537 Monday — Friday, 7 a.m. to 7 p.m. Central Time. When you call, be prepared to provide:
              <ul>
                <li><span class="list-text">A brief explanation of your situation.</span></li>
                <li><span class="list-text">A detailed list of your household expenses.</span></li>
                <li><span class="list-text">Proof of household income (recent pay stubs, tax returns or profit-and-loss statements).</span></li>
                <li><span class="list-text">Completed consumer response packages may be faxed to 877-903-6972.</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#homeAffordableProgram" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="homeAffordableProgram">
          <span class="fa expand-icon"></span><h4 class="title four bold">THE MAKING HOME AFFORDABLE PROGRAM</h4>
        </a>
        <div class="text-content left collapse" id="homeAffordableProgram">
          <div class="text">
            <p class="desc-text">
              This program has two different options for homeowners - one for loan modification (HAMP) and another for refinancing (HARP). If you do not qualify for these options, you may qualify for other foreclosure prevention alternatives (HAFA).
            </p>
          </div>
        </div>
        <a href="#hampModification" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="hampModification">
          <span class="fa expand-icon"></span><h4 class="title four bold">HAMP MODIFICATION</h4>
        </a>
        <div class="text-content left collapse" id="hampModification">
          <div class="text">
            <p class="desc-text">
              This modification program adds delinquent Profit Payment, escrow items and foreclosure fees and costs (if applicable) to your Remaining Acquisition Balance, which is re-amortized over a new term. This will:
              <ul>
                <li><span class="list-text">Bring your account up to date immediately.</span></li>
                <li><span class="list-text">Change the terms of the Co-ownership Agreement and Obligation to Pay for a fresh start in managing your home financing.</span></li>
              </ul>
            </p>
            <p class="desc-text">
              Find out if you're eligible for a Home Affordable Modification.
            </p>
          </div>
        </div>
        <a href="#harpRefinance" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="harpRefinance">
          <span class="fa expand-icon"></span><h4 class="title four bold">HARP REFINANCE</h4>
        </a>
        <div class="text-content left collapse" id="harpRefinance">
          <div class="text">
            <p class="desc-text">
              This program, which is only available on Fannie Mae or Freddie Mac contracts, allows you to take advantage of lower Profit Rates by refinancing your existing contract,
              even if the Remaining Acquisition Balance is greater than the value of your home. This will:
              <ul>
                <li><span class="list-text">Lower your Profit Rate and monthly payment.</span></li>
                <li><span class="list-text">Save money and your home by lowering your Profit Rate.</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#complaintProcess" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="complaintProcess">
          <span class="fa expand-icon"></span><h4 class="title four bold">OUR COMPLAINT PROCESS</h4>
        </a>
        <div class="text-content left collapse" id="complaintProcess">
          <div class="text">
            <p class="desc-text">
              If you have a complaint about your contract regarding, or related to, our loss mitigation foreclosure alternatives process or our foreclosure process, please direct your complaint,
              along with the name of each consumer and the contract number, to:
            </p>
            <p class="desc-text">
              Guidance Residential, LLC,<br>
              Attention: Escalation Center<br>
              P.O. Box #21977<br>
              Eagan, MN 55121
            </p>
          </div>
        </div>
        <a href="#repaymentOptions" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="repaymentOptions">
          <span class="fa expand-icon"></span><h4 class="title four bold">CALL TO DETERMINE YOUR REPAYMENT OPTIONS</h4>
        </a>
        <div class="text-content left collapse" id="repaymentOptions">
          <div class="text">
            <p class="desc-text">
              To determine your eligibility for the Home Affordable Modification Program (HAMP) or other contract modification programs:
              <ul>
                <li><span class="list-text">Call Default Assessment at 855-698-7627.</span></li>
              </ul>
            </p>
            <p class="desc-text">
              To talk to a counselor about delinquent payments:
              <ul>
                <li><span class="list-text">Call Default Counseling at 800-365-7900.</span></li>
              </ul>
            </p>
            <p class="desc-text">
              For free or low-cost general advice about buying a home, renting, default or avoiding foreclosure:
              <ul>
                <li><span class="list-text">Contact a housing counselor at the U.S. Department of Housing and Urban Development (HUD) at 800-569-4287.</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#retainingYourHome" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="retainingYourHome">
          <span class="fa expand-icon"></span><h4 class="title four bold">OPTIONS FOR RETAINING YOUR HOME</h4>
        </a>
        <div class="text-content left collapse" id="retainingYourHome">
          <div class="text">
            <p class="desc-text">
              We offer several options to help you retain your home. To determine which might best suit your needs, please review the following:
            </p>
            <h3 class="title three bold">Repayment Plan</h3>
            <hr class="seperate-line"></hr>
            <p class="desc-text">
              A repayment plan allows you to pay your regular monthly payment plus additional funds applied to past-due amounts. Payments are distributed over an agreed-upon period of time.
            </p>
            <p class="desc-text">
              This option may work for you if:
              <ul>
                <li><span class="list-text">You can afford your regular monthly payments and other expenses.</span></li>
                <li><span class="list-text">You have surplus funds at the end of the month.</span></li>
              </ul>
            </p>
            <h3 class="title three bold">Hardship Contract Modification</h3>
            <hr class="seperate-line"></hr>
            <p class="desc-text">
              This option allows you to roll Profit Payment and escrow shortage from delinquent payments into the existing contract. You may qualify for a Profit Rate reduction to have the term of the contract extended.
            </p>
            <p class="desc-text">
              This option may work for you if:
              <ul>
                <li><span class="list-text">You can afford your regular monthly payment or a slight increase in your payment, plus other monthly expenses.</span></li>
                <li><span class="list-text">You don't have substantial funds left at the end of the month.</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#sellingYourHome" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="sellingYourHome">
          <span class="fa expand-icon"></span><h4 class="title four bold">OPTIONS REGARDING SELLING YOUR HOME</h4>
        </a>
        <div class="text-content left collapse" id="sellingYourHome">
          <div class="text">
            <p class="desc-text">
              If you face the possibility of selling your home, ask yourself the following before starting the process:
              <ul>
                <li><span class="list-text">Are you prepared to sell your home?</span></li>
                <li><span class="list-text">Are you unable to recover from a situation that caused you to fall behind on your contract payments?</span></li>
                <li><span class="list-text">Are you unable to afford your regular monthly payment and have no means to catch up on delinquent payments?</span></li>
              </ul>
            </p>
            <p class="desc-text">
              If you decide to sell your home, consider the following options.
            </p>
            <h3 class="title three bold">Short Sale</h3>
            <hr class="seperate-line"></hr>
            <p class="desc-text">
              In a short sale, the financier agrees to discount the contract balance due to hardship. The home is sold but proceeds fall short of the amount owed.
            </p>
            <p class="desc-text">
              This option may work for you if:
              <ul>
                <li><span class="list-text">You can't afford your regular monthly payment and expenses.</span></li>
                <li><span class="list-text">You are interested in selling your home, which is worth less than you owe.</span></li>
              </ul>
            </p>
            <h3 class="title three bold">Deed in Lieu of Foreclosure</h3>
            <hr class="seperate-line"></hr>
            <p class="desc-text">
              This option allows you to deed your home back to your financier or investor instead of facing foreclosure.
            </p>
            <p class="desc-text">
              This option may work for you if:
              <ul>
                <li><span class="list-text">You can't afford your regular monthly payment or a slight increase in your payment, plus other monthly expenses.</span></li>
                <li><span class="list-text">You don't have substantial funds left at the end of the month.</span></li>
              </ul>
            </p>
            <h3 class="title three bold">Home Affordable Foreclosure Alternative (HAFA)</h3>
            <hr class="seperate-line"></hr>
            <p class="desc-text">
              HAFA provides additional options to avoid foreclosures and offers incentives to consumers who utilize a short sale or deed-in-lieu (DIL) to avoid foreclosure.
            </p>
            <p class="desc-text">
              HAFA alternatives are available to all HAMP-eligible consumers who:
              <ul>
                <li><span class="list-text">Do not qualify for a Trial Period Plan;</span></li>
                <li><span class="list-text">Do not successfully complete a Trial Period Plan;</span></li>
                <li><span class="list-text">Miss at least two consecutive payments during a HAMP modification; or,</span></li>
                <li><span class="list-text">Request a short sale or DIL.</span></li>
              </ul>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
