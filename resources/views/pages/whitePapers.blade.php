@extends('layouts.default')

@section('title', 'White Papers')

@section('content')
  <div class="container white-papers">
    <div class="main-heading">
      <h1 class="title">
        READ OUR WHITE PAPERS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Download our white papers to learn more about Shariah-compliant home financing and our Declining Balance Co-ownership Program.
      </p>
    </div>
    <div class="paper-list">
      <div class="split-content-card">
        <div class="image-container">
          <img src="{{ mix_remote('images/whitePapers/white_papers_declining_balance.jpg') }}" alt="THE DECLINING BALANCE CO-OWNERSHIP PROGRAM">
        </div>
        <div class="main-heading left">
          <div class="title three">
            THE DECLINING BALANCE CO-OWNERSHIP PROGRAM
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            Guidance has created the Declining Balance Co-ownership Program, a unique, soundly designed home financing
             program in the United States that is competitive and, most importantly, Shariah-compliant.
         </p>
         <a class="main-button" href="{{ mix_remote('docs/Guidance-White-Paper.pdf') }}" target="_blank">
           <span class="fa fa-cloud-download"></span>Download
         </a>
        </div>
      </div>
      <div class="split-content-card">
        <div class="image-container">
          <img src="{{ mix_remote('images/whitePapers/white_papers_shariah_supervision.jpg') }}" alt="SHARIAH SUPERVISION IN MODERN ISLAMIC FINANCE">
        </div>
        <div class="main-heading left">
          <div class="title three">
            SHARIAH SUPERVISION IN MODERN ISLAMIC FINANCE
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            While a business may attempt to represent itself as "Islamic," unless it has qualified Shariah supervision,
            it has no way of certifying that its services, products, and operations are actually Shariah-compliant.
         </p>
         <a class="main-button" href="{{ mix_remote('docs/shariah_supervision_in_modern_islamic_finance.pdf') }}" target="_blank">
           <span class="fa fa-cloud-download"></span>Download
         </a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "THE DECLINING BALANCE CO-OWNERSHIP PROGRAM",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/whitePapers/white_papers_declining_balance.jpg') }}",
    "description": "Guidance has created the Declining Balance Co-ownership Program, a unique, soundly designed home financing program in the United States that is competitive and, most importantly, Shariah-compliant."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "SHARIAH SUPERVISION IN MODERN ISLAMIC FINANCE",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/whitePapers/white_papers_shariah_supervision.jpg') }}",
    "description": "While a business may attempt to represent itself as Islamic, unless it has qualified Shariah supervision, it has no way of certifying that its services, products, and operations are actually Shariah-compliant."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Whitepapers'])
@endsection
