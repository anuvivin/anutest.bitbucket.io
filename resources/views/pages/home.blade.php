@extends('layouts.default')

@section('title', '#1 U.S. Islamic Home Financing Provider&reg;')

@section('content')
  <div class="container home">
    <div class="home-video">
      <div class="owl-container">
        <h3 class="title three italic bold">As seen in:</h3>
        <div class="owl-carousel channels-homepage">
          <div class="item">
            <img src="{{ mix_remote('images/home/thewashingtonpost_colored_transparent.png') }}" alt="The Washington Post">
          </div>
          <div class="item">
            <img src="{{ mix_remote('images/home/cnn_bloombergbusiness_colored_transparent.png') }}" alt="CNN &amp; Bloomberg Business">
          </div>
          <div class="item">
            <img src="{{ mix_remote('images/home/thenewyorktimes_colored_transparent.png') }}" alt="New York Times">
          </div>
          <div class="item">
            <img src="{{ mix_remote('images/home/usatoday_colored_transparent.png') }}" alt="USA Today">
          </div>
          <div class="item">
            <img src="{{ mix_remote('images/home/thewallstreetjournal_colored_transparent.png') }}" alt="The Wall Street Journal">
          </div>
          <div class="item">
            <img src="{{ mix_remote('images/home/npr_houstonchronicle_colored_transparent.png') }}" alt="NPR &amp; Houston Chronicle">
          </div>
          <div class="item">
            <img src="{{ mix_remote('images/home/aljazeera_pbs_colored_transparent.png') }}" alt="PBS &amp; Aljazeera">
          </div>
          <div class="item">
            <img src="{{ mix_remote('images/home/huffingtonpost_colored_transparent.png') }}" alt="The Huffington Post">
          </div>
        </div>
        <span class="download-image-container">
          <a href="{{ config('urls.apps.apple') }}"><img src="{{ mix_remote('images/appDownload/appleLogo.svg') }}" alt="Apple store Image" class='app-store-img'></a>
          <a href="{{ config('urls.apps.android') }}"><img src="{{ mix_remote('images/appDownload/google_play.png') }}" alt="Google Play Image" class='app-store-img android'></a>
        </span>
      </div>
      <div class="text">
        <h1 class="mainTitle golden">We finance dreams</h1>
        <h2 class="mainTitle two">Consistent with your values</h2>
        <span class="seperate-line"></span>
        <h3 class="mainDesc" id="mainTypedText">The #1 U.S. Islamic Home Financing Provider<sup>&reg;</sup></h3>
        <div class="button-container">
          <a href="#sixStepContainer" class="main-button blue">Buy a Home</a>
          <a href="{{ route('refinancingProcess') }}" class="main-button gold">Refinance</a>
        </div>
      </div>
      <img src="{{ mix_remote('images/home/home-video.jpg') }}" id="homeVideoImage">
      <video id="homeVideo" autoplay loop>
        <source src="{{ mix_remote('media/homeMain_cut.mp4') }}" type="video/mp4">
          Your browser does not support the video tag.
      </video>
    </div>
    <div class="tab-step-heading" id="sixStepContainer">
      <div class="main-heading">
        <h1 class="title">
          6 STEPS TOWARDS BUYING YOUR NEW HOME
        </h1>
        <span class="seperate-line"></span>
        <p class="desc-text">
          We are here to guide you through the homebuying process
        </p>
      </div>
    </div>
    <div class="six-step-container">
      <div class="step">
        <div class="app-circle">
          <span class="step-number-icon">1</span>
        </div>
        <h3 class="title three">PLAN</h3>
        <span class="seperate-line sub"></span>
        <p class="desc-text">
          Estimate what you can afford and plan ahead
        </p>
        <div class="button-wrapper">
          <a href="{{ route('estimationCalculators') }}#affordabilityCalculator" class="main-button"><span>Use our</span> <span>calculators</span></a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="step-number-icon">2</span>
        </div>
        <h3 class="title three">ACT</h3>
        <span class="seperate-line sub"></span>
        <p class="desc-text">
          Pre-Qualify online in 10 minutes - No credit check required
        </p>
        <div class="button-wrapper">
          <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Mobile_Home_6Steps_Act_Feb2018" class="main-button" target="_blank"><span>Get</span> <span>Pre-Qualified</span></a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="step-number-icon">3</span>
        </div>
        <h3 class="title three">Connect</h3>
        <span class="seperate-line sub"></span>
        <p class="desc-text">
          Connect with a licensed Guidance Account Executive
        </p>
        <div class="button-wrapper">
          <a href="{{ route('account-executive.index') }}" class="main-button"><span>Get</span> <span>Connected</span></a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="step-number-icon">4</span>
        </div>
        <h3 class="title three">SHOP</h3>
        <span class="seperate-line sub"></span>
        <p class="desc-text">
          Use our property search engine and find your dream home
        </p>
        <div class="button-wrapper">
          <a href="{{ config('urls.grealty.search') }}" class="main-button" target="_blank"><span>Find</span> <span>Your Home</span></a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="step-number-icon">5</span>
        </div>
        <h3 class="title three">APPLY</h3>
        <span class="seperate-line sub"></span>
        <p class="desc-text">
          View application checklist and apply for financing
        </p>
        <div class="button-wrapper">
          <a href="{{ route('buyingApplicationChecklist') }}" class="main-button"><span>Apply For</span> <span>Financing</span></a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="step-number-icon">6</span>
        </div>
        <h3 class="title three">CLOSE</h3>
        <span class="seperate-line sub"></span>
        <p class="desc-text">
          Give a final walk through and prepare for the closing day!
        </p>
        <div class="button-wrapper">
          <a href="{{ mix_remote('docs/GR_Closing Summary_118.pdf') }}" class="main-button" target="_blank"><span>Prepare for</span> <span>closing</span></a>
        </div>
      </div>
    </div>
    <div class="tab-step-mobile">
      <a href="#stepOne" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="stepOne">
        <h4 class="sub-text">
          <span>Step 1: </span><span class="thin-subtext">PLAN</span>
        </h4>
      </a>
      <div class="content collapse" id="stepOne">
        <div class="icon-wrapper">
          <div class="icon app-circle">
            <span class="fa fa-lightbulb-o"></span>
          </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title">
              Estimate what you can afford and plan ahead
            </h1>
            <hr class="seperate-line"></hr>
            <div class="button-wrapper">
              <a href="{{ route('estimationCalculators') }}#affordabilityCalculator" class="main-button"><span>Use our</span> <span>calculators</span></a>
            </div>
          </div>
        </div>
      </div>
      <a href="#stepTwo" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="stepTwo">
        <h4 class="sub-text">
          <span>Step 2: </span><span class="thin-subtext">ACT</span>
        </h4>
      </a>
      <div class="content collapse" id="stepTwo">
        <div class="icon-wrapper">
          <div class="icon app-circle">
            <span class="fa fa-pencil-square-o"></span>
          </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title">
              Pre-Qualify online in 10 minutes - No credit check required
            </h1>
            <hr class="seperate-line"></hr>
            <div class="button-wrapper">
              <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_Mobile_Home_6Steps_Act_Feb2018" class="main-button" target="_blank"><span>Get</span> <span>Pre-Qualified</span></a>
            </div>
          </div>
        </div>
      </div>
      <a href="#stepThree" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="stepThree">
        <h4 class="sub-text">
          <span>Step 3: </span><span class="thin-subtext">Connect</span>
        </h4>
      </a>
      <div class="content collapse" id="stepThree">
        <div class="icon-wrapper">
            <div class="icon app-circle">
              <span class="fa fa-user-circle-o"></span>
            </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title">
              Connect with a licensed Guidance Account Executive
            </h1>
            <hr class="seperate-line"></hr>
            <div class="button-wrapper">
              <a href="{{ route('account-executive.index') }}" class="main-button"><span>Get</span> <span>Connected</span></a>
            </div>
          </div>
        </div>
      </div>
      <a href="#stepFour" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="stepFour">
        <h4 class="sub-text">
          <span>Step 4: </span><span class="thin-subtext">Shop</span>
        </h4>
      </a>
      <div class="content collapse" id="stepFour">
        <div class="icon-wrapper">
          <div class="icon app-circle">
            <span class="fa fa-shopping-cart"></span>
          </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title">
              Use our property search engine and find your dream home
            </h1>
            <hr class="seperate-line"></hr>
            <div class="button-wrapper">
              <a href="{{ config('urls.grealty.search') }}" class="main-button" target="_blank"><span>Find</span> <span>Your Home</span></a>
            </div>
          </div>
        </div>
      </div>
      <a href="#stepFive" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="stepFive">
        <h4 class="sub-text">
          <span>Step 5: </span><span class="thin-subtext">Apply</span>
        </h4>
      </a>
      <div class="content collapse" id="stepFive">
        <div class="icon-wrapper">
          <div class="icon app-circle">
            <span class="fa fa-check-circle-o"></span>
          </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title">
              View application checklist and apply for financing
            </h1>
            <hr class="seperate-line"></hr>
            <div class="button-wrapper">
              <a href="{{ route('buyingApplicationChecklist') }}" class="main-button"><span>Apply For</span> <span>Financing</span></a>
            </div>
          </div>
        </div>
      </div>
      <a href="#stepSix" class="mobile-step" data-toggle="collapse"  aria-expanded="false" aria-controls="stepSix">
        <h4 class="sub-text">
          <span>Step 6: </span><span class="thin-subtext">Close</span>
        </h4>
      </a>
      <div class="content collapse" id="stepSix">
        <div class="icon-wrapper">
          <div class="icon app-circle">
            <span class="fa fa-home"></span>
          </div>
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title">
              Give a final walk through and prepare for the closing day!
            </h1>
            <hr class="seperate-line"></hr>
            <div class="button-wrapper">
              <a href="{{ mix_remote('docs/GR_Closing Summary_118.pdf') }}" class="main-button" target="_blank"><span>Prepare for</span> <span>closing</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="guidance-different-section">
      <div class="main-heading">
        <h1 class="title">
          WHAT MAKES GUIDANCE RESIDENTIAL DIFFERENT?
        </h1>
        <span class="seperate-line"></span>
        <p class="desc-text">
          Guidance Residential offers unique benefits that distinguish it from other home financing providers. These benefits provide a transparent, consumer friendly home financing option which is consistent with the Muslim faith's strict prohibition on riba (usury).
        </p>
      </div>
      <div class="steps">
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon1.png') }}" alt="Co-ownership icon">
          </div>
          <h4 class="title three">Co-ownership</h4>
          <p class="desc-text">Guidance Residential and home buyers each own a percentage as co-owners. Home buyers increase their share over a period of time through a monthly payment.</p>
        </div>
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon2.png') }}" alt="Risk sharing icon">
          </div>
          <h4 class="title three">Risk Sharing</h4>
          <p class="desc-text">Guidance Residential shares the risk with the homebuyer in case of natural disasters, eminent domain or foreclosure.
            <span class="fa fa-info-circle" data-container="body" data-toggle="popover" data-placement="auto bottom"
              data-content="The risk is shared if the property is lost in the case of a natural disaster, or a public service project initiated by the government forces you out of the property. In this situation the proceeds provided by insurance or government are shared based upon the percentage of ownership at the point of the loss. In a similar situation, conventional loan providers will apply the proceeds to pay off the loan without any allocation.">
            </span>
          </p>
        </div>
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon3.png') }}" alt="Risk sharing icon">
          </div>
          <h4 class="title three">Riba-free</h4>
          <p class="desc-text">The Declining Balance Co-ownership program, does not involve payment of riba. It is 100% riba-free.</p>
        </div>
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon4.png') }}" alt="Risk sharing icon">
          </div>
          <h4 class="title three">No Pre-payment Penalty</h4>
          <p class="desc-text">There is no pre-payment penalty required by Guidance Residential when a homebuyer wants to pay ahead of the agreed schedule.</p>
        </div>
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon5.png') }}" alt="Risk sharing icon">
          </div>
          <h4 class="title three">Capped Late Payment Fees</h4>
          <p class="desc-text">Guidance Residential will only charge a capped fee of $50 or less for late payments. The fee covers the expenses involved in administering a late payment rather than the standard 5% penalty, and will not be profited by Guidance Residential.
            <span class="fa fa-info-circle" data-container="body" data-toggle="popover" data-placement="auto right"
              data-content="Traditionally, late fees are calculated by conventional providers as a percentage of the late monthly amount due from the customer and are often realized as profit. There are no interest charges on late payments under our program. With our Shariah Supervisory Board's approval, late payments are only subject to a capped fee that covers the expenses involved in administering a late payment. Under Shariah principles, profiting off of such a situation is prohibited.">
            </span>
          </p>
        </div>
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon6.png') }}" alt="Risk sharing icon">
          </div>
          <h4 class="title three">Non-recourse Commitment</h4>
          <p class="desc-text">In the event of payment default, Guidance Residential does not pursue any of the homebuyer's other assets.
            <span class="fa fa-info-circle" data-container="body" data-toggle="popover" data-placement="auto right"
              data-content="In many conventional loan programs, other assets which may include your various investments - such as personal savings accounts, pensions and college fund - are all subject to seizure in the event of a default. The Declining Balance Co-ownership Program has a non-recourse clause, meaning that in the event the customer defaults, Guidance Residential does not have recourse against the customer's other assets. The customer is only at risk for his/her equity position in the property.">
            </span>
          </p>
        </div>
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon7.png') }}" alt="Risk sharing icon">
          </div>
          <h4 class="title three">Independent Shariah Board</h4>
          <p class="desc-text">Guidance Residential has an independent Shariah Board comprised of notable scholarly authority headed by Justice Muhammad Taqi Usmani, who is also the chairman of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI).</p>
        </div>
        <div class="step">
          <div class="image">
            <img src="{{ mix_remote('images/home/icon8.png') }}" alt="Risk sharing icon">
          </div>
          <h4 class="title three">Endorsed By AMJA</h4>
          <p class="desc-text">The Declining Balance Co-Ownership Program of Guidance Residential has been endorsed by the Assembly of Muslim Jurists of America (AMJA) - an advisory group to the Muslim American public with experts in the Islamic jurisprudence.</p>
        </div>
      </div>
    </div>
    <div class="split-content-section">
      <div class="image-container background-image">
      </div>
      <div class="main-heading left">
        <h1 class="title">
          The world is on the go
        </h1>
        <span class="seperate-line"></span>
        <p class="desc-text">
          So are we. Download our mobile app and get access to your application status anytime, anywhere. Our app is available on the App Store and Google Play.
        </p>
        <h3 class="title three no-transform"><span class="fa fa-check-circle"></span>Pre-Qualify anytime, anywhere</h3>
        <h3 class="title three no-transform"><span class="fa fa-check-circle"></span>Get real time updates</h3>
        <h3 class="title three no-transform"><span class="fa fa-check-circle"></span>Connect with your Account Executive</h3>
        <h3 class="title three no-transform"><span class="fa fa-check-circle"></span>Track application status on the go</h3>
        <a href="{{ route('appDownload') }}" class="app-page-link"><img src="{{ mix_remote('images/home/appstore.png') }}" alt="App store Image" class='app-store-img'></a>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "Co-ownership",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon1.png') }}",
    "description": "Guidance Residential and home buyers each own a percentage as co-owners. Home buyers increase their share over a period of time through a monthly payment."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "Risk Sharing",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon2.png') }}",
    "description": "Guidance Residential shares the risk with the homebuyer in case of natural disasters, eminent domain or foreclosure. The risk is shared if the property is lost in the case of a natural disaster, or a public service project initiated by the government forces you out of the property. In this situation the proceeds provided by insurance or government are shared based upon the percentage of ownership at the point of the loss. In a similar situation, conventional loan providers will apply the proceeds to pay off the loan without any allocation."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "Riba-free",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon3.png') }}",
    "description": "The Declining Balance Co-ownership program, does not involve payment of riba. It is 100% riba-free."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "No Pre-payment Penalty",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon4.png') }}",
    "description": "There is no pre-payment penalty required by Guidance Residential when a homebuyer wants to pay ahead of the agreed schedule."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "Capped Late Payment Fees",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon5.png') }}",
    "description": "Guidance Residential will only charge a capped fee of $50 or less for late payments. The fee covers the expenses involved in administering a late payment rather than the standard 5% penalty, and will not be profited by Guidance Residential. Traditionally, late fees are calculated by conventional providers as a percentage of the late monthly amount due from the customer and are often realized as profit. There are no interest charges on late payments under our program. With our Shariah Supervisory Boards approval, late payments are only subject to a capped fee that covers the expenses involved in administering a late payment. Under Shariah principles, profiting off of such a situation is prohibited."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "Non-recourse Commitment",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon6.png') }}",
    "description": "In the event of payment default, Guidance Residential does not pursue any of the homebuyer's other assets. In many conventional loan programs, other assets which may include your various investments - such as personal savings accounts, pensions and college fund - are all subject to seizure in the event of a default. The Declining Balance Co-ownership Program has a non-recourse clause, meaning that in the event the customer defaults, Guidance Residential does not have recourse against the customer's other assets. The customer is only at risk for his/her equity position in the property."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "Independent Shariah Board",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon7.png') }}",
    "description": "Guidance Residential has an independent Shariah Board comprised of notable scholarly authority headed by Justice Muhammad Taqi Usmani, who is also the chairman of the Shariah Board for the Accounting and Auditing Organization for Islamic Financial Institutions (AAOIFI)."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "Endorsed By AMJA",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/home/icon8.png') }}",
    "description": "The Declining Balance Co-Ownership Program of Guidance Residential has been endorsed by the Assembly of Muslim Jurists of America (AMJA) - an advisory group to the Muslim American public with experts in the Islamic jurisprudence."
  }
  </script>
@endsection

@if (App::environment('production'))
  @section('end')
    @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Homepage'])
    <script type="text/javascript">
      window._mfq = window._mfq || [];
      (function() {
          var mf = document.createElement("script");
          mf.type = "text/javascript"; mf.async = true;
          mf.src = "//cdn.mouseflow.com/projects/9daa4189-a315-40ee-ae4c-423e51c8dfe1.js";
          document.getElementsByTagName("head")[0].appendChild(mf);
      })();
    </script>
  @endsection
@endif
