@extends('layouts.default')

@section('title', 'Refinancing Options')

@section('content')
  <div class="container financing-options">
    <div class="main-heading">
      <div class="title">
        REFINANCING OPTIONS
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance Residential offers a variety of Shariah-compliant refinancing options to suit your needs. Whatever your needs, we're happy to help. Call us to learn more: 1.866.484.3262
      </p>
    </div>
    <div class="options-container">
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Product Type</h3>
          <div class="fa fa-calendar"></div>
          <div class="desc-text">
            Connect with a licensed Account Executive to learn more about our financial products.
          </div>
        </div>
        <div class="content">
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Fixed rates for 30, 20, and 15-year contracts</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Adjustable rates for 30, 20, and 15-year contracts</span></p>
        </div>
      </div>
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Financing Type</h3>
          <div class="fa fa-gears"></div>
          <div class="desc-text">
            Connect with a licensed Account Executive to learn more about our financing options.
          </div>
        </div>
        <div class="content full">
          <div class="accordion-split-content full">
            <div class="accordion-content">
              <a href="#conforming" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="conforming">
                <span class="fa expand-icon"></span><h4 class="title four bold">conforming </h4>
              </a>
              <div class="text-content left collapse" id="conforming">
                <div class="text">
                  <p class="desc-text italic">
                    A loan that conforms to Government-sponsored enterprise (GSE) guidelines.
                  </p>
                  <div class="table ">
                    <table>
                      <tr>
                        <td>Property Type</td>
                        <td>Maximum Base Conforming Loan Limits</td>
                      </tr>
                      <tr>
                        <td>1-Unit</td>
                        <td>$453,100</td>
                      </tr>
                      <tr>
                        <td>2-Unit</td>
                        <td>$580,150</td>
                      </tr>
                      <tr>
                        <td>3-Unit</td>
                        <td>$701,250</td>
                      </tr>
                      <tr>
                        <td>4-Unit</td>
                        <td>$871,450</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
              <a href="#conformingJumbo" class="accordion-item second" data-toggle="collapse"  aria-expanded="false" aria-controls="conformingJumbo">
                <span class="fa expand-icon"></span><h4 class="title four bold">Conforming Jumbo</h4>
              </a>
              <div class="text-content left collapse" id="conformingJumbo">
                <div class="text">
                  <div class="table ">
                    <table>
                      <tr>
                        <td>Property Type</td>
                        <td>Minimum and Maximum* Base Conforming Loan Limits</td>
                      </tr>
                      <tr>
                        <td>1-Unit</td>
                        <td>$453,100 or $679,650</td>
                      </tr>
                      <tr>
                        <td>2-Unit</td>
                        <td>$580,150 or $870,225</td>
                      </tr>
                      <tr>
                        <td>3-Unit</td>
                        <td>$701,250 or $1,051,875</td>
                      </tr>
                      <tr>
                        <td>4-Unit</td>
                        <td>$871,450 or $1,307,175</td>
                      </tr>
                    </table>
                  </div>
                  <p class="desc-text italic">
                    * These are the maximum potential loan limits for designated high cost areas. Actual loan limits are established for each country (or equivalent) and the loan limits for specific high-cost areas may be lower. The original principal balance of a mortgage must not exceed the maximum loan limit for the specific area in which the mortgaged premises is located.
                  </p>
                </div>
              </div>
              <a href="#harp" class="accordion-item second" data-toggle="collapse"  aria-expanded="false" aria-controls="harp">
                <span class="fa expand-icon"></span><h4 class="title four bold">Home Affordable Refinance Program (HARP)</h4>
              </a>
              <div class="text-content left collapse" id="harp">
                <div class="text">
                  <p class="desc-text">
                    Home Affordable Refinance Program (HARP) - HARP makes it possible for homeowners with little equity in their home, or who owe as much or more than their home is worth, and have remained current on their payments, to refinance their home.
                  </p>
                </div>
              </div>
              <a href="#cashRefinance" class="accordion-item second" data-toggle="collapse"  aria-expanded="false" aria-controls="cashRefinance">
                <span class="fa expand-icon"></span><h4 class="title four bold">Cash Out Refinance</h4>
              </a>
              <div class="text-content left collapse" id="cashRefinance">
                <div class="text">
                  <p class="desc-text">
                    Cash Out Refinance - a Cash Out Refinance allows homeowners to refinance to cash out in the amount of 80- to 90% of their home equity
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Property Type</h3>
          <div class="fa fa-home"></div>
          <div class="desc-text">
            Connect with a licensed Account Executive to learn more about property types.
          </div>
        </div>
        <div class="content">
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Single Family Homes</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Condominiums </span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Townhomes</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Planned Unit Developments</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Multi-unit (2 to 4 units)</span></p>
        </div>
      </div>
      <div class="option">
        <div class="type">
          <h3 class="title three bold">Occupancy Type</h3>
          <div class="fa fa-building-o"></div>
          <div class="desc-text">
            Connect with a licensed Account Executive to learn more about occupancy types.
          </div>
        </div>
        <div class="content">
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Owner Occupied</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Second Home</span></p>
          <p class="desc-text"><span class="fa fa-check-circle"></span><span class="text">Non-owner Occupied (Investment Property)</span></p>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "REFINANCING OPTIONS",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Guidance Residential offers a variety of Shariah-compliant refinancing options to suit your needs. Whatever your needs, we're happy to help. Call us to learn more: 1.866.484.3262"
  }
  </script>
@endsection
