@extends('layouts.default')

@section('title', 'Save at Closing')

@section('content')
<div class="middle">
  <div class="container guidanceRealty">
    <div class="main-heading">
      <h1 class="title">
        How to Save at closing
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance Residential homebuyers save at closing when they work with a GuidanceRealty.com partner agent. Scroll down to learn more.
      </p>
    </div>
    <div class="image-container"></div>
  </div>
  <div class="how-it-works">
    <div class="main-heading">
      <h1 class="title six">
        How it works
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Save up to $500 on your appraisal at closing when you work with a GuidanceRealty.com agent and finance your home with Guidance Residential.
      </p>
    </div>
    <div class="realty-steps">
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-check-square-o"></span>
        </div>
        <h4 class="title three">Get Pre-Qualified</h4>
        <p class="desc-text">Find out what you can afford when you Pre-Qualify with Guidance Residential. There is no credit check for online Pre-Qualification and it takes less than 10 minutes.</p>
        <div class="button">
          <a class="main-button" href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_RealtyProgram_SaveAtClosing_Feb2018" target="_blank">Pre-qualify now</a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-building-o"></span>
        </div>
        <h4 class="title three">Search for Properties</h4>
        <p class="desc-text">Our robust property search engine allows you to spot that perfect home in the community that's right for you. It also allows you to search homes with advanced criteria.</p>
        <div class="button">
          <a class="main-button" href="{{ config('urls.grealty.search') }}">Find your home</a>
        </div>
      </div>
      <div class="step">
        <div class="app-circle">
          <span class="fa fa-dollar"></span>
        </div>
        <h4 class="title three">Save at Closing</h4>
        <p class="desc-text">Receive an appraisal credit up to $500* at closing, simply by using a GuidanceRealty.com partner agent and financing with Guidance Residential. Get started by finding an agent.</p>
        <div class="button">
          <a class="main-button" href="{{ config('urls.grealty.find-an-agent') }}">find an agent</a>
        </div>
      </div>
    </div>
    <p class="desc-text italic">
      <span class="bold">New York consumers and agents:</span> Access to features of this website is restricted pending approval by the NY Department of Financial Services.
    </p>
    <p class="desc-text italic">
      *Commission Credit Program for Sellers not Available in KS, NJ, OR and TN. Other restrictions may apply.
    </p>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "HOW TO SAVE AT CLOSING",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Save up to $500 on your appraisal at closing when you work with a GuidanceRealty.com agent and finance your home with Guidance Residential. GET PRE-QUALIFIED: Find out what you can afford when you Pre-Qualify with Guidance Residential. There is no credit check for online Pre-Qualification and it takes less than 10 minutes. SEARCH FOR PROPERTIES: Our robust property search engine allows you to spot that perfect home in the community that's right for you. It also allows you to search homes with advanced criteria. SAVE AT CLOSING: Receive an appraisal credit up to $500* at closing, simply by using a GuidanceRealty.com partner agent and financing with Guidance Residential. Get started by finding an agent."
  }
  </script>
@endsection
