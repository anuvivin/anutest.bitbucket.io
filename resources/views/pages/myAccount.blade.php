@extends('layouts.default')

@section('title', 'My Account')

@section('content')
<div class="middle">
  <div class="container my-account">
    <div class="main-heading">
      <div class="title">
        My Account
      </div>
      <span class="seperate-line"></span>
    </div>
    <div class="main-heading sub">
      <h3 class="title two">APPLYING FOR FINANCING</h3>
      <span class="seperate-line sub"></span>
    </div>
    <div class="realty-steps my-account">
      <a class="step" href="{{ config('urls.customer-portal.login') }}">
        <div class="app-circle">
          <span class="fa fa-tasks"></span>
        </div>
        <h4 class="title three">View Application Status</h4>
      </a>
      <a class="step" href="{{ config('urls.customer-portal.login') }}">
        <div class="app-circle">
          <span class="fa fa-file"></span>
        </div>
        <h4 class="title three">Upload Documents</h4>
      </a>
      <a class="step" href="{{ config('urls.customer-portal.login') }}">
        <div class="app-circle">
          <span class="fa fa-edit"></span>
        </div>
        <h4 class="title three">Manage Application Status</h4>
      </a>
    </div>
    <div class="main-heading sub">
      <h3 class="title two">EXISTING CUSTOMERS</h3>
      <span class="seperate-line sub"></span>
    </div>
    <div class="realty-steps my-account">
      <a class="step" href="{{ config('urls.customer-care-net.login') }}">
        <div class="app-circle">
          <span class="fa fa-credit-card"></span>
        </div>
        <h4 class="title three">Make Online Payment</h4>
      </a>
      <a class="step" href="{{ config('urls.customer-care-net.login') }}">
        <div class="app-circle">
          <span class="fa fa-users"></span>
        </div>
        <h4 class="title three">View Account Statements</h4>
      </a>
      <a class="step" href="{{ config('urls.customer-care-net.login') }}">
        <div class="app-circle">
          <span class="fa fa-envelope-o"></span>
        </div>
        <h4 class="title three">Update Billing Information</h4>
      </a>
    </div>
  </div>
</div>
@endsection
