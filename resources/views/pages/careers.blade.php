@extends('layouts.default')

@section('title', 'Careers')

@section('content')
  <div class="container careers">
    <div class="main-heading">
      <div class="title">
        Careers
      </div>
      <span class="seperate-line"></span>
    </div>
    <div id="iframeHeightDiv" name="HRM Direct Career Site iFrame Container" align="center" style="height: 1225px;">
      <iframe id="inlineframe" name="HRM Direct Career Site iFrame" src="//guidanceresidential.hrmdirect.com/employment/job-openings.php?search=true&amp;nohd=&amp;dept=-1&amp;city=-1&amp;state=-1" frameborder="0" allowtransparency="true">
      </iframe>
    </div>
  </div>
@endsection
