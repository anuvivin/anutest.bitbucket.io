@extends('layouts.default')

@section('title', 'Webinars')

@section('content')
  <div class="container webinars">
    <div class="main-heading">
      <h1 class="title">
        WATCH OUR WEBINARS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Browse our webinar library and access on-demand replay to learn about home financing, Islamic finance and Guidance Residential
      </p>
    </div>
    <div class="split-list-container">
      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="O3d4Lpurr-Q">
            <img src="{{ mix_remote('images/webinars/islam_purchasing_webinar.jpg') }}" alt="ISLAM AND PURCHASING A HOME IN AMERICA" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            ISLAM AND PURCHASING A HOME IN AMERICA
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            What is the Islamic perspective of home ownership? How can Muslim Americans purchase their homes without riba? What role can educational
             institutions play to demystify the concepts of Shariah-compliant home ownership contracts?
         </p>
         <a class="main-button" href="https://www.youtube.com/watch?v=O3d4Lpurr-Q" target="_blank">
           <span class="fa fa-unlock-alt"></span><span>Watch webinar replay on YouTube</span>
         </a>
        </div>
      </div>
      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="g_o_usd1jEA">
            <img src="{{ mix_remote('images/webinars/changing_lives_one.jpg') }}" alt="CHANGING LIVES, ONE HOME AT A TIME" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            CHANGING LIVES, ONE HOME AT A TIME
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            We are in the business of changing lives, one home at a time. Join Guidance Residential's career webinar to learn more about our company
             and how you can be part of an engaging team that is trying to make a difference while embarking on a financially rewarding journey.
         </p>
         <a class="main-button" href="https://www.youtube.com/watch?v=g_o_usd1jEA" target="_blank">
           <span class="fa fa-unlock-alt"></span><span>Watch webinar replay on YouTube</span>
         </a>
        </div>
      </div>
      <div class="item">
        <div class="video-container">
          <div class="video" data-video-id="U166NjwhPkE">
            <img src="{{ mix_remote('images/webinars/understanding_islamic_finance.jpg') }}" alt="UNDERSTANDING ISLAMIC HOME FINANCE" class="video-thumbnail">
            <div class="video-play"></div>
          </div>
        </div>
        <div class="description">
          <div class="title three">
            UNDERSTANDING ISLAMIC HOME FINANCE
          </div>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            What is Islamic home financing? What are the differences between conventional mortgage and Guidance Residential's Shariah-compliant
             home financing program? How can Guidance help you become a homeowner? Join us in this webinar to learn these and more.
         </p>
         <a class="main-button" href="https://www.youtube.com/watch?v=U166NjwhPkE" target="_blank">
           <span class="fa fa-unlock-alt"></span><span>Watch webinar replay on YouTube</span>
         </a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Islam & Purchasing a Home in America | Guidance Residential, Islamic Home Financing USA",
    "description": "What is the Islamic perspective of home ownership? How can Muslim Americans purchase their homes without riba? What role can educational institutions play to demystify the concepts of Shariah-compliant home ownership contracts? Live digital panel discussion hosted by Guidance Residential, LLC, in collaboration with Guidance College (formerly known as Al-Huda University). Speakers: Shaykh Yusuf Talal DeLorenzo, Shariah & Islamic Finance Scholar. Shaykh DeLorenzo is considered a leading authority on Islamic finance in the United States. He has translated over twenty books from Arabic, Persian, and Urdu for publication, including a three-volume Compendium of Legal Rulings on the Operations of Islamic Banks. Shaykh DeLorenzo has also been a pioneer in internet education with a course entitled Principles of Islamic Investing. He is a member of Shariah Boards of several Islamic financial institutions in the United States and abroad, including Dow Jones Islamic Markets and Guidance Residential Financial Group. Shaykh DeLorenzo has served as secretary of the Fiqh Council of North America and was an advisor to the government of Pakistan, the Central Bank of Malaysia, the Hamdan bin Muhammad University in Dubai, INCEIF, and the Stanford University Libraries. Shaykh DeLorenzo studied the classical Shariah Sciences with scholars in Pakistan and Egypt. Dr. Main Alqudah, Associate Professor, Guidance College. Dr. Main Alqudah holds Ph.D. in Islamic studies from American Open University and a Bachelor of Arts in Economics from Al-Azhar. In addition, he is an Associate Professor, and a member of the Resident Fatwa Committee of AMJA. Dr. Main is an experienced lecturer in Islamic Studies. In his teaching career, he has provided instruction and training to learners at schools as well as college level. He is a speaker at regional and national conferences. Moderated by: Salman Ali, Regional Manager, Mid-Atlantic Region. GuidanceResidential.com. Guidance Residential NMLS #2908. Licensing & Registrations: https://www.guidanceresidential.com/licensing-and-registrations",
    "uploadDate": "2018-01-30T17:48:54.000Z",
    "interactionCount": "259",
    "duration": "PT1H32M40S",
    "embedURL": "https://youtube.googleapis.com/v/O3d4Lpurr-Q",
    "thumbnailURL": "https://i.ytimg.com/vi/O3d4Lpurr-Q/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Guidance Residential Career Webinar - Dec 2015",
    "description": "At Guidance Residential, the nation's leading provider of Islamic home financing, our sales division is our most valued company resource. Our Field Account Executives (AEs) are considered to be the front lines for our company's brand.",
    "uploadDate": "2015-12-21T18:51:55.000Z",
    "interactionCount": "589",
    "duration": "PT1H13M12S",
    "embedURL": "https://youtube.googleapis.com/v/g_o_usd1jEA",
    "thumbnailURL": "https://i.ytimg.com/vi/g_o_usd1jEA/hqdefault.jpg"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Islamic Home Finance Webinar - 1/12/2017",
    "description": "Islamic Home Finance Webinar - 1/12/2017",
    "uploadDate": "2017-01-17T21:00:17.000Z",
    "interactionCount": "611",
    "duration": "PT1H19M53S",
    "embedURL": "https://youtube.googleapis.com/v/U166NjwhPkE",
    "thumbnailURL": "https://i.ytimg.com/vi/U166NjwhPkE/hqdefault.jpg"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Webinars'])
@endsection
