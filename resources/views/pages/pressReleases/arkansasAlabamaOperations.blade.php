@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Residential Announces Expansion of Islamic Home Finance Operations in Southeast')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'October 2, 2017')
@section('prevPressReleaseUrl', route('pressReleases.awardedByWorldFinance'))
@section('nextPressReleaseUrl', route('pressReleases.islamicBankMorocco'))

@section('pressReleaseBody')
<p class="desc-text italic">
  Expansion a part of strategic plan to better serve the growing Muslim American market in Alabama and Arkansas
</p>
<p class="desc-text">
  <span class="bold">RESTON, Va., Oct. 2, 2017</span> - Guidance Residential, the leading U.S. Islamic home finance provider, announced the launch of operations in Arkansas and
   Alabama. The launch expands Guidance's operations in the U.S., helping to better serve the growing Muslim American market.
</p>
<p class="desc-text">
  Residents in Arkansas and Alabama will now be able to obtain Islamic home financing for purchasing and refinancing a property through the Declining Balance Co-ownership
  Program. The Program is the only one of its kind to have been reviewed and approved by an independent board of distinguished Shariah scholars, many of whom serve or have
  served as members of the Accounting and Auditing Organization for Islamic Financial Institutions - a world renowned regulatory body for global standards in Islamic finance.
</p>
<p class="desc-text">
  "Our expansion into Arkansas and Alabama further demonstrates our dedication to making the dream of homeownership possible for more and more Muslim Americans," said Guidance
   Residential CEO Khaled Elsayed. "We set off to meet the demand for Islamic home financing over 15 years ago. Since our launch in 2002, we have now helped finance over 19,000
    homes, and we are excited to begin offering our proprietary program and exceptional services to the residents of Arkansas and Alabama."
</p>
<p class="desc-text">
  According to the latest Census information, the Muslim population in Alabama has been growing steadily since 2000 with some indicators suggesting double digit growth rates
   over the last decade. Becoming operational in Alabama and Arkansas is important to the growth of Guidance Residential as it continues to increase its market share in serving
   the Muslim American community.
</p>
<p class="desc-text">
  In 2017, Guidance Residential has expanded into 5 states and continues to dominate the Muslim American market. The company has provided over $4.5 billion in financing and more
   than $2 million in corporate sponsorships to Muslim American community initiatives.
</p>
<p class="desc-text bold">
  About Guidance Residential
</p>
<p class="desc-text bold">
  Guidance Residential is the leading provider of Islamic home financing in the United States. Since its inception in 2002, Guidance Residential has funded over $4.9 billion in
  Shariah-compliant home financing contracts. Guidance Residential's proprietary Declining Balance Co-ownership program is trusted as a strictly Shariah-compliant home financing
   program in the U.S. Islamic finance market. The Declining Balance Co-ownership Program received the highest rating by The Assembly of Muslim Jurists of America (AMJA).
</p>
<p class="desc-text bold">
  Guidance Residential offers Shariah-compliant home financing in the following U.S. states:<br>
  Connecticut, Massachusetts, New Jersey, New York, Pennsylvania, Rhode Island, Illinois, Michigan, Minnesota, Ohio, Wisconsin, Arizona, Kansas, Texas, Delaware, Kentucky,
  Maryland, Virginia, Washington D.C., California, Oregon, Washington, Alabama, Arkansas, Florida, Georgia, North Carolina, South Carolina, Tennessee
</p>
<p class="desc-text small">
  Guidance Residential, LLC, (Nationwide Mortgage Licensing System No.2908), 11107 Sunset Hills Rd, Suite 200, Reston, VA 20190 is licensed by the Department of Business
  Oversight under the California Residential Mortgage Lending Act (413-0427); Alabama Consumer Credit (22410); Arizona Mortgage Banker (0923408); Arkansas Mortgage
  Banker-Broker-Servicer (112069); Colorado Mortgage Company Registration ; Connecticut Mortgage Lender (18263); Delaware Lender (022090); District of Columbia Mortgage Dual
  Authority (MLB2908); Florida Mortgage Lender (MLD656); Georgia Mortgage Lender (17286); Illinois Residential Mortgage Licensee (MB.0006455 11107 Sunset Hills Road, Suite 200,
   , Reston, VA 20190; MB.0006455-001 100 East Roosevelt Rd, Units 44 &amp; 45, Villa Park, IL 60181) licensed by the ILDFPR, James R Thompson Center (JRTC), 100 West Randolph
   Street, 9th Floor, Chicago, IL 60601, 888-473-4858 (General), 844-768-1713 (Banking Div); Kansas Licensed Mortgage Company (MC.0025178); Kentucky Mortgage Company
   (MC361158 &amp;MC361566); Maryland Mortgage Lender (12927); Massachusetts Mortgage Lender (ML2908); Michigan 1st Mortgage Lender, Broker &amp; Servicer Registrant (FR-0941);
   Minnesota Mortgage Originator (MN-MO 20320419); New Jersey Licensed Lender (9933648); New York – Licensed Mortgage Banker – New York Department of Financial Services
   (B500726) (NY location: 171-21 Jamaica Ave, 1st Floor, Jamaica, NY 11432); North Carolina Mortgage Lender (L-112542); Ohio Mortgage Broker Act Mortgage Banker Exemption
   (MBMB 850079.000); Oregon Mortgage Lender (ML-4145); Pennsylvania – licensed by the Pennsylvania Department of Banking, Mortgage Lender (21050); Rhode Island Lender License
    (20163318LL); South Carolina BFI Mortgage Lender/Servicer (MLS-2908); Tennessee Mortgage License (109274); Texas SML Mortgage Banker Registrant ;  Virginia Lender Licensee
    (MC-2138) licensed by the Virginia State Corporation Commission; Washington Consumer Loan Company (CL-2908); Wisconsin Mortgage Banker (46355BA).
</p>
@endsection
