@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'University Bank of Ann Arbor Liable for "willful and Malicious" Trade Secret Misappropriation')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'June 15, 2016')
@section('prevPressReleaseUrl', route('pressReleases.southeastOperations'))
@section('nextPressReleaseUrl', route('pressReleases.newYorkMetroOffice'))

@section('pressReleaseBody')
<p class="desc-text italic">
  Judge Awards Guidance Residential Damages in Excess of $1 Million
</p>
<p class="desc-text">
  <span class="bold">RESTON, VIRGINIA, June 21, 2016</span> - Guidance Residential, LLC of Reston, Virginia was awarded a judgment exceeding $1 Million in the King
  County, Washington Superior Court on June 15, 2016 against former employees and their employer, University Islamic Financial ("UIF") Corporation, as well as parent
  company University Bank of Ann Arbor, MI.
</p>
<p class="desc-text">
  The judgment follows a March 2015 jury trial that found former Guidance Residential employees Mohamad Aijaz Hussain, Anwer Mangrio, and others used proprietary information
  in soliciting applicants for mortgages shortly after joining their new employer, UIF.  The jury found that the information used was a trade secret of Guidance Residential
  and that the actions of all defendants were "willful and malicious" in the misappropriation.  All defendants were jointly and severally liable.
</p>
<p class="desc-text">
  "While this is welcomed news, it is surprising that even after these facts were proven in a month-long jury trial, it appears that not a single employee was terminated or
  censured," says Guidance Residential President and CEO, Khaled Elsayed.
</p>
<p class="desc-text">
  The theft of trade secrets is an important issue for corporations in the U.S. and abroad.  A recent report from The Center for Responsible Enterprise and Trade and
  PricewaterhouseCoopers LLP estimates the economic impact of trade secret theft to be anywhere from 1 to 3 percent of U.S. Gross Domestic Product.
</p>
<p class="desc-text">
  ###
</p>
<p class="desc-text italic">
  Guidance Residential offers home financing products and services for Muslim Americans through their Shariah-compliant, Declining Balance Co-ownership Program. Guidance
  has financed over $3.9 Billion and is the #1 U.S. Islamic Home Financing Provider. www.GuidanceResidential.com
</p>
@endsection
