@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Press Release Title')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'Month D, YYYY')
@section('prevPressReleaseUrl', route('routeName'))
@section('nextPressReleaseUrl', route('routeName'))

@section('pressReleaseBody')
<p class="desc-text">
  Coming Soon
</p>
@endsection