@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Surpasses $2 Billion in Sharia-Compliant Residential Financing in US')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'October 4, 2010')
@section('prevPressReleaseUrl', route('pressReleases.corporateHeadquartersMovesToNewHome'))
@section('nextPressReleaseUrl', route('pressReleases.nominatedForBestIslamicHomeFinanceProviderAward'))

@section('pressReleaseBody')
<p class="desc-text italic bold">
  Major Milestone Crossed Despite Turbulent Economic Climate
</p>
<p class="desc-text">
  Reston, VA - Guidance Residential ("Guidance"), the leading US provider of Sharia-compliant home financing, announced that it has now provided over $2 billion in
  Sharia-compliant home financing since its inception in 2002.  Despite today's challenging financial climate, Guidance has been able to sustain its growth over the
  last several years and has now set the bar higher for the Islamic home finance industry.
</p>
<p class="desc-text">
  Over the last eight and a half years, Guidance has made significant strides for Islamic finance.  In June 2007 it became the first US-based Islamic home finance
  company to provide over $1 billion dollars in Sharia-compliant home financing.  Amid the ongoing volatility in the market, Guidance thrived in 2009 by surpassing
  its own annual production record.  Later that year, the company was nominated for Best Islamic Home Finance Provider by Dubai's Islamic Business &amp; Finance Awards.
</p>
<p class="desc-text">
  Based on a concept known as diminishing musharaka or partnership, the company's Declining Balance Co-ownership program has helped thousands of Muslim-American
  households  achieve home ownership without compromising their beliefs.  Guidance has a growing workforce of over 70 employees nationwide.  The company plans to
  continue expanding its products and services to meet the needs of the Sharia-sensitive market.
</p>
<p class="desc-text">
  ###
</p>
<p class="desc-text italic">
  Guidance Residential is a subsidiary of Guidance Financial Group, an international company dedicated to serving the market for Sharia-compliant financial products
  and services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide, and provides financial
  services to its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products
  has allowed it to develop unique financial solutions that address unmet needs of the Sharia-compliant market.
</p>
@endsection
