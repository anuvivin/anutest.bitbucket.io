@extends('layouts.default')

@section('title', 'Press Releases')

@section('content')
<div class="container pressReleases">
  <div class="main-heading">
    <h1 class="title">Press Releases</h1>
    <span class="seperate-line"></span>
  </div>
  <div class="press-list">
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.launchesNewWebsite') }}" class="press-link">Guidance Residential Launches New Website to Simplify Islamic Home Financing for U.S. Consumers</a>
        <span class="seperate-line"></span>
        <p class="desc-date">March 29, 2018</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.launchesNewMobileApp') }}" class="press-link">The Leading U.S. Islamic Home Financing Provider Launches New Mobile App</a>
        <span class="seperate-line"></span>
        <p class="desc-date">February 28, 2018</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.grSimplifiesHomeFinancing') }}" class="press-link">Guidance Residential Simplifies Islamic Home Financing for Consumers</a>
        <span class="seperate-line"></span>
        <p class="desc-date">February 28, 2018</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.awardedByWorldFinance') }}" class="press-link">Guidance Residential Awarded Best Islamic Home Financier, USA by World Finance</a>
        <span class="seperate-line"></span>
        <p class="desc-date">February 12, 2018</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.arkansasAlabamaOperations') }}" class="press-link">Guidance Residential Announces Expansion of Islamic Home Finance Operations in Southeast</a>
        <span class="seperate-line"></span>
        <p class="desc-date">October 2, 2017</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.islamicBankMorocco') }}" class="press-link">Guidance Financial Group's Effort to Establish Islamic Bank in Morocco Wins Approval</a>
        <span class="seperate-line"></span>
        <p class="desc-date">January 9, 2017</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.delawareOperations') }}" class="press-link">Guidance Residential, LLC Expands Home Finance Operations in Delaware</a>
        <span class="seperate-line"></span>
        <p class="desc-date">December 21, 2016</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.southeastOperations') }}" class="press-link">#1 U.S. Islamic Home Financing Provider Expands Operations in Kentucky</a>
        <span class="seperate-line"></span>
        <p class="desc-date">July 12, 2016</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.tradeSecretMisappropriation') }}" class="press-link">University Bank of Ann Arbor Liable for "willful and Malicious" Trade Secret Misappropriation</a>
        <span class="seperate-line"></span>
        <p class="desc-date">June 15, 2016</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.newYorkMetroOffice') }}" class="press-link">Guidance to Celebrate Opening of New York Metro Office</a>
        <span class="seperate-line"></span>
        <p class="desc-date">March 11, 2015</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.amjaResolution') }}" class="press-link">Guidance Encouraged by AMJA Resolution</a>
        <span class="seperate-line"></span>
        <p class="desc-date">October 17, 2014</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.newJerseyOffice') }}" class="press-link">Guidance Celebrates Northern New Jersey Office Opening</a>
        <span class="seperate-line"></span>
        <p class="desc-date">January 18, 2012</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.chicagoOffice') }}" class="press-link">Guidance to Celebrate Opening of Chicago Office</a>
        <span class="seperate-line"></span>
        <p class="desc-date">July 12, 2011</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.southernCaliOffice') }}" class="press-link">Guidance Celebrate Southern California Office Opening</a>
        <span class="seperate-line"></span>
        <p class="desc-date">July 12, 2011</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.historicIslamicFinance') }}" class="press-link">Historic Islamic Finance Training Draws Imams from Across the Nation</a>
        <p class="desc-date">June 14, 2011</p>
        <span class="seperate-line"></span>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.landmarkIslamicTraining') }}" class="press-link">Guidance Introduces Landmark Islamic Finance Training for American Imams</a>
        <span class="seperate-line"></span>
        <p class="desc-date">April 25, 2011</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.pioneeringEvent') }}" class="press-link">Pioneering Event is a Collaborative Effort between Major American-Muslim Groups</a>
        <span class="seperate-line"></span>
        <p class="desc-date">April 4, 2011</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.corporateHeadquartersMovesToNewHome') }}" class="press-link">Guidance Corporate Headquarters Moves to New Home</a>
        <span class="seperate-line"></span>
        <p class="desc-date">March 29, 2011</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.twoBillionInShariaCompliantFinancing') }}" class="press-link">Guidance Surpasses $2 Billion in Sharia-Compliant Residential Financing in US</a>
        <span class="seperate-line"></span>
        <p class="desc-date">October 4, 2010</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.nominatedForBestIslamicHomeFinanceProviderAward') }}" class="press-link">Guidance Residential Nominated For "Best Islamic Home Finance Provider" Award</a>
        <span class="seperate-line"></span>
        <p class="desc-date">October 30, 2009</p>
      </div>
    </div>
    <div class="press">
      <span class="fa fa-newspaper-o"></span>
      <div class="description">
        <a href="{{ route('pressReleases.corporateHeadquartersMovesToRestonVa') }}" class="press-link">Guidance Corporate Headquarters Moves to Reston, VA</a>
        <span class="seperate-line"></span>
        <p class="desc-date">December 20, 2004</p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('mainFooter')
  <div class="media-inquiry-flag">
    <div class="app-circle">
      <span class="fa fa-newspaper-o"></span>
    </div>
    <div class="main-heading left">
      <h3 class="title three golden">Media inquiry</h3>
      <span class="seperate-line sub"></span>
      <div class="contact-info">
        <div class="item">
          <a href="mailto:Communications@GuidanceResidential.com">Communications@GuidanceResidential.com</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Pressreleases'])
@endsection
