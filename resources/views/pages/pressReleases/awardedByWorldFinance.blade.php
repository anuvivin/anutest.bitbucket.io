@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Residential Awarded Best Islamic Home Financier, USA by World Finance')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'February 12, 2018')
@section('prevPressReleaseUrl', route('pressReleases.grSimplifiesHomeFinancing'))
@section('nextPressReleaseUrl', route('pressReleases.arkansasAlabamaOperations'))

@section('pressReleaseBody')
<p class="desc-text italic">
  Award to be officially announced by World Finance in April
</p>
<p class="desc-text">
  <span class="bold">RESTON, Va., Feb. 12, 2018</span> - Guidance Residential, the leading U.S. Islamic home financing provider, announced today that it will be awarded the
  'Best Islamic Home Financier, USA' by World Finance.
</p>
<p class="desc-text">
  The World Finance Islamic Finance Awards celebrate the most forward-thinking players in this rapidly expanding - and growing market. To be recognized as the best is an amazing
   honor and truly a testament of hard work and commitment to driving innovations in the Islamic home finance market. Guidance Residential will soon release an app that will
   allow customers to complete and monitor the home financing process from a mobile device with internet capabilities.
</p>
<p class="desc-text">
  The news of the award was welcomed with excitement from the organization, which recently celebrated over 15 years of being in business as the leading Islamic home financing
  provider in the U.S.
</p>
<p class="desc-text">
  President and CEO Khaled Elsayed said, "It is absolutely paramount that we recognize that success came from motivation, hunger and hard work." While Guidance Residential has
  long maintained its dominance in the U.S. Islamic home financing market, the award reaffirms the company's commitment to helping customers achieve homeownership.
</p>
<p class="desc-text">
  Islamic finance is growing in popularity as consumers begin to find that its principles are more attractive than conventional banking. As more and more consumers outside the
  market of practicing Muslims switch to Islamic finance providers, awards like the World Finance Islamic Finance Awards will be invaluable to the Guidance Residential brand.
</p>
<p class="desc-text bold">
  World Finance awards the Islamic Finance Awards every year and will make an official announcement for the 2018 award recipients in April.
</p>
<p class="desc-text bold">
  About Guidance Residential
</p>
<p class="desc-text bold">
  Guidance Residential is the leading provider of Islamic home financing in the United States. Since its inception in 2002, Guidance Residential has funded over $4.9 billion in
  Shariah-compliant home financing contracts. Guidance Residential's proprietary Declining Balance Co-ownership program is trusted as a strictly Shariah-compliant home financing
   program in the U.S. Islamic finance market. The Declining Balance Co-ownership Program received the highest rating by The Assembly of Muslim Jurists of America (AMJA).
</p>
<p class="desc-text bold">
  Guidance Residential offers Shariah-compliant home financing in the following U.S. states:<br>
  Connecticut, Massachusetts, New Jersey, New York, Pennsylvania, Rhode Island, Illinois, Michigan, Minnesota, Ohio, Wisconsin, Arizona, Kansas, Texas, Delaware, Kentucky,
  Maryland, Virginia, Washington D.C., California, Oregon, Washington, Alabama, Arkansas, Florida, Georgia, North Carolina, South Carolina, Tennessee
</p>
<p class="desc-text small">
  Guidance Residential, LLC, (Nationwide Mortgage Licensing System No.2908), 11107 Sunset Hills Rd, Suite 200, Reston, VA 20190 is licensed by the Department of Business
  Oversight under the California Residential Mortgage Lending Act (413-0427); Alabama Consumer Credit (22410); Arizona Mortgage Banker (0923408); Arkansas Mortgage
  Banker-Broker-Servicer (112069); Colorado Mortgage Company Registration ; Connecticut Mortgage Lender (18263); Delaware Lender (022090); District of Columbia Mortgage Dual
  Authority (MLB2908); Florida Mortgage Lender (MLD656); Georgia Mortgage Lender (17286); Illinois Residential Mortgage Licensee (MB.0006455 11107 Sunset Hills Road, Suite 200,
   , Reston, VA 20190; MB.0006455-001 100 East Roosevelt Rd, Units 44 &amp; 45, Villa Park, IL 60181) licensed by the ILDFPR, James R Thompson Center (JRTC), 100 West Randolph
   Street, 9th Floor, Chicago, IL 60601, 888-473-4858 (General), 844-768-1713 (Banking Div); Kansas Licensed Mortgage Company (MC.0025178); Kentucky Mortgage Company
   (MC361158 &amp;MC361566); Maryland Mortgage Lender (12927); Massachusetts Mortgage Lender (ML2908); Michigan 1st Mortgage Lender, Broker &amp; Servicer Registrant (FR-0941);
   Minnesota Mortgage Originator (MN-MO 20320419); New Jersey Licensed Lender (9933648); New York – Licensed Mortgage Banker – New York Department of Financial Services
   (B500726) (NY location: 171-21 Jamaica Ave, 1st Floor, Jamaica, NY 11432); North Carolina Mortgage Lender (L-112542); Ohio Mortgage Broker Act Mortgage Banker Exemption
   (MBMB 850079.000); Oregon Mortgage Lender (ML-4145); Pennsylvania – licensed by the Pennsylvania Department of Banking, Mortgage Lender (21050); Rhode Island Lender License
    (20163318LL); South Carolina BFI Mortgage Lender/Servicer (MLS-2908); Tennessee Mortgage License (109274); Texas SML Mortgage Banker Registrant ;  Virginia Lender Licensee
    (MC-2138) licensed by the Virginia State Corporation Commission; Washington Consumer Loan Company (CL-2908); Wisconsin Mortgage Banker (46355BA).
</p>
@endsection
