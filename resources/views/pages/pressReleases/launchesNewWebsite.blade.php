@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Residential Launches New Website to Simplify Islamic Home Financing for U.S. Consumers')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'March 29, 2018')
@section('nextPressReleaseUrl', route('pressReleases.launchesNewMobileApp'))

@section('pressReleaseBody')
<p class="desc-text italic">
  The new and completely redesigned website serves as a one-stop hub to facilitate home financing process from planning to closing
</p>
<p class="desc-text">
  <span class="bold">RESTON, Va., March 29, 2018</span> - Guidance Residential, the leading U.S. Islamic home financing provider launched a revamped website and
  inviting visitors to explore the website at <a href="https://www.guidanceresidential.com/">www.GuidanceResidential.com</a>. The new website has been designed to
   streamline the home financing process with easy navigation, quick access to essential information, integrated residential property search engine, and enriched
   contents that offer a comprehensive understanding of the company's proprietary Islamic home financing program. The company launched its redesigned website along
   its new mobile app "gios for Homeowners," both aim to offer an improved customer experience to first time home buyers, and residential investment property
   buyers.
</p>
<p class="desc-text">
  "The home financing process can be very complex. With the revamped website and the new mobile app, we are making the home financing experience more informative,
  transparent, and convenient. These digital initiatives showcase our commitment in customer experience innovation in the Shariah-compliant home financing market,"
  said President and CEO Khaled Elsayed.
</p>
<p class="desc-text">
  Simplifying user experience, providing clarity about the complex home financing process, and fast access to information were three major focus areas in
  redesigning the website. New features of the website include:
  <ul>
    <li>
      Structured, streamlined, and convenient information access: "Six steps towards buying your home" guides a first-time homebuyer from planning to closing
      phase by providing easy access to estimation calculators, online pre-qualification tool, Account Executive finder, property search engine, application
      checklist and closing related information.
    </li>
    <li>
      Knowledge center: The new website provides rich contents explaining Guidance Residential's proprietary Declining Balance Co-ownership Program through
      eBooks, articles, white papers, videos, podcasts, and webinars.
    </li>
    <li>
      Event listing and registration: The website's "Upcoming Events" section offers an easy way to find and register for company hosted upcoming Islamic finance
      seminars and realtor networking events.
    </li>
    <li>
      Convenient navigation: Consumers can access relevant information within few clicks about home buying and refinancing process, latest competitive rates,
      frequently asked questions and much more with a user-friendly navigation menu. Real estate agents can become realty program partners by signing up at the
      website within few clicks.
    </li>
    <li>
      Secure customer portal: Financing applicants can follow their application status in real time through secure customer portal, add their real estate agent
      and family members and get updates on the status of application at the same time.
    </li>
  </ul>
</p>
<p class="desc-text">
  For more information about Guidance Residential and to explore the new website,<br>
  visit: <a href="https://www.guidanceresidential.com/">https://www.guidanceresidential.com/</a><br><br>
  For media inquiry, contact: <a href="mailto:Communications@GuidanceResidential.com">Communications@GuidanceResidential.com</a><br>
</p>
<p class="desc-text bold">
  About Guidance Residential
</p>
<p class="desc-text bold">
  Guidance Residential is the leading provider of Islamic home financing in the United States. Since its inception in 2002, Guidance Residential has funded over $4.9 billion in
  Shariah-compliant home financing contracts. Guidance Residential's proprietary Declining Balance Co-ownership program is trusted as a strictly Shariah-compliant home financing
   program in the U.S. Islamic finance market. The Declining Balance Co-ownership Program received the highest rating by The Assembly of Muslim Jurists of America (AMJA).
</p>
<p class="desc-text bold">
  Guidance Residential offers Shariah-compliant home financing in the following U.S. states:<br>
  Connecticut, Massachusetts, New Jersey, New York, Pennsylvania, Rhode Island, Illinois, Michigan, Minnesota, Ohio, Wisconsin, Arizona, Kansas, Texas, Delaware, Kentucky,
  Maryland, Virginia, Washington D.C., California, Oregon, Washington, Alabama, Arkansas, Florida, Georgia, North Carolina, South Carolina, Tennessee
</p>
<p class="desc-text small">
  Guidance Residential, LLC, (Nationwide Mortgage Licensing System No.2908), 11107 Sunset Hills Rd, Suite 200, Reston, VA 20190 is licensed by the Department of Business
  Oversight under the California Residential Mortgage Lending Act (413-0427); Alabama Consumer Credit (22410); Arizona Mortgage Banker (0923408); Arkansas Mortgage
  Banker-Broker-Servicer (112069); Colorado Mortgage Company Registration ; Connecticut Mortgage Lender (18263); Delaware Lender (022090); District of Columbia Mortgage Dual
  Authority (MLB2908); Florida Mortgage Lender (MLD656); Georgia Mortgage Lender (17286); Illinois Residential Mortgage Licensee (MB.0006455 11107 Sunset Hills Road, Suite 200,
   , Reston, VA 20190; MB.0006455-001 100 East Roosevelt Rd, Units 44 &amp; 45, Villa Park, IL 60181) licensed by the ILDFPR, James R Thompson Center (JRTC), 100 West Randolph
   Street, 9th Floor, Chicago, IL 60601, 888-473-4858 (General), 844-768-1713 (Banking Div); Kansas Licensed Mortgage Company (MC.0025178); Kentucky Mortgage Company
   (MC361158 &amp;MC361566); Maryland Mortgage Lender (12927); Massachusetts Mortgage Lender (ML2908); Michigan 1st Mortgage Lender, Broker &amp; Servicer Registrant (FR-0941);
   Minnesota Mortgage Originator (MN-MO 20320419); New Jersey Licensed Lender (9933648); New York – Licensed Mortgage Banker – New York Department of Financial Services
   (B500726) (NY location: 171-21 Jamaica Ave, 1st Floor, Jamaica, NY 11432); North Carolina Mortgage Lender (L-112542); Ohio Mortgage Broker Act Mortgage Banker Exemption
   (MBMB 850079.000); Oregon Mortgage Lender (ML-4145); Pennsylvania – licensed by the Pennsylvania Department of Banking, Mortgage Lender (21050); Rhode Island Lender License
    (20163318LL); South Carolina BFI Mortgage Lender/Servicer (MLS-2908); Tennessee Mortgage License (109274); Texas SML Mortgage Banker Registrant ;  Virginia Lender Licensee
    (MC-2138) licensed by the Virginia State Corporation Commission; Washington Consumer Loan Company (CL-2908); Wisconsin Mortgage Banker (46355BA).
</p>
@endsection
