@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'The Leading U.S. Islamic Home Financing Provider Launches New Mobile App')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'February 28, 2018')
@section('prevPressReleaseUrl', route('pressReleases.launchesNewWebsite'))
@section('nextPressReleaseUrl', route('pressReleases.grSimplifiesHomeFinancing'))

@section('pressReleaseBody')
<p class="desc-text italic">
  Guidance Residential's new mobile app, 'giOS for Homeowners' brings riba-free home financing to the fingertips of homebuyers and refinancers
</p>
<p class="desc-text">
  <span class="bold">RESTON, Va., Feb. 28, 2018</span> - Guidance Residential, the leading U.S. Islamic home financing provider, announced today the launch of its new mobile
  app for homeowners that provides a faster, more transparent and convenient home financing experience to consumers. The new app, named 'giOS for Homeowners,' allows homebuyers
  to track their financing application status in real time from any place with an internet connected mobile phone.
</p>
<p class="desc-text">
  The addition of this technology platform demonstrates Guidance Residential's commitment to continue its leadership as an innovative, consumer-focused and cutting-edge digital
  home financing provider in the Islamic fintech market. "GiOS for Homeowners" uses innovative technology to simplify and automate the back-end operations of the home financing
  process, while streamlining the communication between homebuyers, financing associates and real estate agents. The platform also provides planning tools, white papers and
  explainer videos to help homebuyers learn about Guidance Residential's proprietary Declining Balance Co-ownership Program.
</p>
<p class="desc-text">
  "We see Guidance Residential as a technology company that happens to operate in the home financing industry, and a company that leads the way in the U.S. Islamic finance
  market through digital innovation," said Khaled Elsayed, president and chief executive officer of Guidance Residential. "This new technology platform will allow us to connect
  with the millennial homebuyers, facilitate their home financing process from start to finish in a more transparent and simpler way, and bridge the communication gaps that
  traditionally exist in the home financing industry between consumers, real estate agents and the financing provider."
</p>
<p class="desc-text">
  The new mobile app will allow consumers to:
  <ul>
    <li>Pre-Qualify for riba-free home financing in less than 10 minutes.</li>
    <li>Use estimation calculators to determine affordability and evaluate home financing options.</li>
    <li>Find latest rates and compare rates with other providers.</li>
    <li>Access educational content to learn more about how Guidance Residential's Islamic home financing program works.</li>
    <li>Stay informed via notifications throughout the home financing process from applying to closing.</li>
    <li>Connect with a Guidance Residential Account Executive with a single click.</li>
    <li>Add real estate agents or family members to follow the progress of the home buying process.</li>
  </ul>
</p>
<p class="desc-text">
  The "giOS for Homeowners" app is now available on the App Store and Google Play.<br>
  To download the app, visit the following links:<br>
  App Store: <a href="https://apple.co/2BSt0oZ">https://apple.co/2BSt0oZ</a><br>
  Google Play: <a href="http://bit.ly/2CeXzRs">http://bit.ly/2CeXzRs</a><br>
  For media inquiry, contact: <a href="mailto:Communications@GuidanceResidential.com">Communications@GuidanceResidential.com</a><br>
  For more information about Guidance Residential, visit: <a href="https://www.GuidanceResidential.com">https://www.GuidanceResidential.com</a>
</p>
<p class="desc-text bold">
  About Guidance Residential
</p>
<p class="desc-text bold">
  Guidance Residential is the leading provider of Islamic home financing in the United States. Since its inception in 2002, Guidance Residential has funded over $4.9 billion in
  Shariah-compliant home financing contracts. Guidance Residential's proprietary Declining Balance Co-ownership program is trusted as a strictly Shariah-compliant home financing
   program in the U.S. Islamic finance market. The Declining Balance Co-ownership Program received the highest rating by The Assembly of Muslim Jurists of America (AMJA).
</p>
<p class="desc-text bold">
  Guidance Residential offers Shariah-compliant home financing in the following U.S. states:<br>
  Connecticut, Massachusetts, New Jersey, New York, Pennsylvania, Rhode Island, Illinois, Michigan, Minnesota, Ohio, Wisconsin, Arizona, Kansas, Texas, Delaware, Kentucky,
  Maryland, Virginia, Washington D.C., California, Oregon, Washington, Alabama, Arkansas, Florida, Georgia, North Carolina, South Carolina, Tennessee
</p>
<p class="desc-text small">
  Guidance Residential, LLC, (Nationwide Mortgage Licensing System No.2908), 11107 Sunset Hills Rd, Suite 200, Reston, VA 20190 is licensed by the Department of Business
  Oversight under the California Residential Mortgage Lending Act (413-0427); Alabama Consumer Credit (22410); Arizona Mortgage Banker (0923408); Arkansas Mortgage
  Banker-Broker-Servicer (112069); Colorado Mortgage Company Registration ; Connecticut Mortgage Lender (18263); Delaware Lender (022090); District of Columbia Mortgage Dual
  Authority (MLB2908); Florida Mortgage Lender (MLD656); Georgia Mortgage Lender (17286); Illinois Residential Mortgage Licensee (MB.0006455 11107 Sunset Hills Road, Suite 200,
   , Reston, VA 20190; MB.0006455-001 100 East Roosevelt Rd, Units 44 &amp; 45, Villa Park, IL 60181) licensed by the ILDFPR, James R Thompson Center (JRTC), 100 West Randolph
   Street, 9th Floor, Chicago, IL 60601, 888-473-4858 (General), 844-768-1713 (Banking Div); Kansas Licensed Mortgage Company (MC.0025178); Kentucky Mortgage Company
   (MC361158 &amp;MC361566); Maryland Mortgage Lender (12927); Massachusetts Mortgage Lender (ML2908); Michigan 1st Mortgage Lender, Broker &amp; Servicer Registrant (FR-0941);
   Minnesota Mortgage Originator (MN-MO 20320419); New Jersey Licensed Lender (9933648); New York – Licensed Mortgage Banker – New York Department of Financial Services
   (B500726) (NY location: 171-21 Jamaica Ave, 1st Floor, Jamaica, NY 11432); North Carolina Mortgage Lender (L-112542); Ohio Mortgage Broker Act Mortgage Banker Exemption
   (MBMB 850079.000); Oregon Mortgage Lender (ML-4145); Pennsylvania – licensed by the Pennsylvania Department of Banking, Mortgage Lender (21050); Rhode Island Lender License
    (20163318LL); South Carolina BFI Mortgage Lender/Servicer (MLS-2908); Tennessee Mortgage License (109274); Texas SML Mortgage Banker Registrant ;  Virginia Lender Licensee
    (MC-2138) licensed by the Virginia State Corporation Commission; Washington Consumer Loan Company (CL-2908); Wisconsin Mortgage Banker (46355BA).
</p>
@endsection
