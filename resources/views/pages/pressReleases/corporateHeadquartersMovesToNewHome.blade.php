@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Corporate Headquarters Moves to New Home')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'March 29, 2011')
@section('prevPressReleaseUrl', route('pressReleases.pioneeringEvent'))
@section('nextPressReleaseUrl', route('pressReleases.twoBillionInShariaCompliantFinancing'))

@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Introduces Landmark Islamic Finance Training for American Imams')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'April 25, 2011')
@section('prevPressReleaseUrl', route('pressReleases.historicIslamicFinance'))
@section('nextPressReleaseUrl', route('pressReleases.pioneeringEvent'))

@section('pressReleaseBody')
<p class="desc-text italic bold">
  Company Relocates to New Office Space
</p>
<p class="desc-text">
  Reston, VA - Guidance Residential ("Guidance") announced today the relocation of its corporate headquarters. Although the Company will remain in Reston, Virginia,
  the new address will be 11107 Sunset Hills Road, Suite 200, Reston, Virginia.  The move occurs as Guidance Residential LLC, gears itself for added expansion of its
  Sharia-compliant home financing product to several new states in the New Year.
</p>
<p class="desc-text">
  The company's previous headquarters located in Falls Church, Virginia, housed just over 50 employees and reached its capacity in just over two years since the company
  was launched in April of 2002.  The increase in staff was mainly to handle Guidance's Sharia-compliant home financing operation which has been growing at a remarkable pace.
  Among the many benefits the new space provides is a larger working environment allowing the company greater freedom in scaling-up its staffing to meet the growing demand for
  its products and services.
</p>
<p class="desc-text">
  Over the last two and half years Guidance Residential has grown to employ over 65 employees across the country with operations in eleven states and Washington, DC.
  In this short period of time the company has established itself as the prime provider of high quality Sharia-compliant home financing in the United States.  To date,
  Guidance has originated over $375 million of home financing contracts in the US through its Declining Balance Co-ownership program.
</p>
<p class="desc-text">
  ###
</p>
<p class="desc-text italic">
  Guidance Financial Group is an international company dedicated to serving the market for Sharia-compliant financial products and services. Guidance offers unique investment
  products to institutional investors and financial intermediaries worldwide, and provides financial services to its retail customer base in the United States. Guidance's
  commitment to intensive research and development and solid operational support for its products has allowed it to develop unique financial solutions that address unmet
  needs of the Sharia-compliant market.
</p>
@endsection
