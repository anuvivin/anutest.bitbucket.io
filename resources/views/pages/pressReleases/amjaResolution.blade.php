@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Encouraged by AMJA Resolution')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'October 17, 2014')
@section('prevPressReleaseUrl', route('pressReleases.newYorkMetroOffice'))
@section('nextPressReleaseUrl', route('pressReleases.newJerseyOffice'))

@section('pressReleaseBody')
<p class="desc-text">
  Reston, VA, (October 17, 2014): Guidance Residential ("Guidance"), the leading US provider of Sharia-compliant home financing, is encouraged by the recent statement of the Assembly
  of Muslim Jurists of America ("AMJA") declaring Guidance's home financing program as being permissible from a Sharia-compliance perspective.
</p>
<p class="desc-text">
  The AMJA resolution, which was released after a review of all US home financing programs that claim to be compliant with Islamic finance principles, named Guidance Residential and its
  Declining Balance Co-Ownership Program as a permissible path available to Muslim Americans in need of home financing.
</p>
<p class="desc-text">
  "It was profound to learn of AMJA Resident Fatwa Committee's decision that outside of Guidance's Program and a small cooperative, all other programs are only permissible in dire need or
  not permissible at all", said Khaled Elsayed, President and CEO of Guidance Residential. "This is a great testament to the level of commitment our leadership and Sharia Board demonstrated in
  structuring our Program over 13 years ago."
</p>
<p class="desc-text">
  To ensure strict standards of Sharia compliance, the Declining Balance Co-ownership program was developed under the close supervision of Guidance's Sharia Supervisory Board. An independent body,
  the Sharia Board comprises some of the world's leading experts in the Islamic law of financial transactions and includes;
</p>
<table>
  <tbody>
    <tr>
      <td>Justice Muhammad Taqi Usmani</td>
      <td>Board Chairman - Pakistan</td>
    </tr>
    <tr>
      <td>Shaykh Abdul-Sattar Abu-Ghuddah</td>
      <td>Board Member - Syria</td>
    </tr>
    <tr>
      <td>Shaykh Nizam Yaquby</td>
      <td>Board Member - Bahrain</td>
    </tr>
    <tr>
      <td>Shaykh Mohammad Elgari</td>
      <td>Board Member - Saudi Arabia</td>
    </tr>
    <tr>
      <td>Shaykh Mohamad Daud Bakar</td>
      <td>Board Member - Malaysia</td>
    </tr>
    <tr>
      <td>Shaykh Yusuf Talal DeLorenzo</td>
      <td>Board Member - US</td>
    </tr>
    <tr>
      <td>Dr. Muhammad Imran Usmani</td>
      <td>Board Member - Pakistan</td>
    </tr>
  </tbody>
</table>
<p class="desc-text">
  ###
</p>
<p class="desc-text">
  With over $3.3 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international company dedicated to
  serving the market for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide,
  and provides financial services to its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products has
  allowed it to develop unique financial solutions that address unmet needs of the Sharia-compliant market.
</p>
@endsection