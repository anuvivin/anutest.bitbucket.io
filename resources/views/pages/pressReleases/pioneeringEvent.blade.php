@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Pioneering Event is a Collaborative Effort between Major American-Muslim Groups')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'April 4, 2011')
@section('prevPressReleaseUrl', route('pressReleases.landmarkIslamicTraining'))
@section('nextPressReleaseUrl', route('pressReleases.corporateHeadquartersMovesToNewHome'))

@section('pressReleaseBody')
<p class="desc-text">
  Reston, VA <br>
  (April 4, 2011) Guidance Residential ("Guidance"), announced today the formation of the American Islamic Finance Project ("AIF") which is a collaborative effort between the Islamic Society of North America
  ("ISNA"), Ethica Institute of Islamic Finance ("Ethica") and Guidance to provide the first-ever Islamic finance training to over 100 American imams.
</p>
<p class="desc-text">
  The role of AIF is to raise the level of knowledge and understanding of Islamic financial transaction law under the belief that financial markets can benefit significantly from the ethical and
  moral guidelines that Islam's financial concepts provide.
</p>
<p class="desc-text">
  The landmark training, which is to be held from May 2 to May 5 in Berkeley, California, is designed exclusively for imams serving communities in the United States. Muslim communities across America are increasingly
  considering Islamic finance as a legitimate option for their personal investment strategies and in doing so they are turning to their imams for direction, making this training more important than ever.
</p>
<p class="desc-text">
  The opening ceremony will feature a broad discussion by keynote presenters such as Shaykh Nizam Yaqub, a renowned Islamic finance scholar, Shaykh Hamza Yusuf, founder of Zaytuna College, and Imam Mohammed Maged,
  President of ISNA. The bulk of the training will be led by Atif Khan, Managing Director at Ethica, who will conduct two full days of intensive training about the principles of Islamic financial transactions.
</p>
<p class="desc-text">
  The sole underwriter of this event was Guidance Financial Group, who made the training possible so that all participating imams can increase their knowledge of Islamic finance without becoming concerned about expenses.
</p>
<p class="desc-text">
  ###
</p>
<p class="desc-text">
  Islamic Society of North America
</p>
<p class="desc-text">
  ISNA is an association of Muslim organizations and individuals that provides a common platform for presenting Islam, supporting Muslim communities, developing educational, social and outreach programs and
  fostering good relations with other religious communities, and civic and service organizations.
</p>
<p class="desc-text">
  Ethica
</p>
<p class="desc-text">
  With over 20,000 paid users in more than 40 countries, Ethica is the world's leading accredited Islamic finance training and certification institute. Ethica remains the only institute in the world to deliver
  standardized certification based entirely on the Accounting and Auditing Organization of Islamic Financial Institutions (AAOIFI), the leading standard-setting body in the Islamic finance industry.
</p>
<p class="desc-text">
  Guidance Residential
</p>
<p class="desc-text italic">
  With over $2 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international company dedicated to serving the market
  for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide, and provides financial services to
  its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products has allowed it to develop unique financial solutions that
  address unmet needs of the Sharia-compliant market.
</p>
@endsection