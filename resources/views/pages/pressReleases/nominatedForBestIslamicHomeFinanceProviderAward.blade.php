@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Residential Nominated For "Best Islamic Home Finance Provider" Award')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'October 30, 2009')
@section('prevPressReleaseUrl', route('pressReleases.twoBillionInShariaCompliantFinancing'))
@section('nextPressReleaseUrl', route('pressReleases.corporateHeadquartersMovesToRestonVa'))

@section('pressReleaseBody')
<p class="desc-text italic bold">
  Global Industry Magazine Recognizes Significant Strides Made in the US
</p>
<p class="desc-text">
  Reston, VA - Guidance Residential ("Guidance"), the leading US provider of Sharia-compliant home financing, has been nominated for the "Best Islamic Home Finance Provider"
  Award in the 2009 Islamic Business &amp; Finance Awards.  The Awards have been instituted by Islamic Business &amp; Finance Magazine and have been designed to encourage,
  inspire and reward excellence within the global Islamic Business and Finance community.
</p>
<p class="desc-text">
  Since its inception in 2003, Guidance has made significant strides for Islamic finance, becoming the first Islamic home finance company to provide over $1.5 billion dollars
  in Sharia-compliant home financing to the US market.  Amid the ongoing volatility in the market, Guidance thrived, and by the end of this year's third quarter it had set
  monthly production records that were broken six times over. During that same time frame, Guidance embarked on numerous initiatives designed to take the company to the next
  level in areas of organizational performance, structure, production and overall results. The company's nomination for this prestigious award is a direct result of the hard
  work and dedication exemplified by Guidance's employees in the pursuit of excellence.
</p>
<p class="desc-text">
  The winners of the 2009 Islamic Business &amp; Finance Awards will be announced at an awards ceremony to be held on December 14th at The Emirates Towers Hotel in Dubai.
  The event has become the yardstick by which Islamic finance institutions and practitioners across the globe measure themselves. These Awards have assumed such significance
  due to Islamic Business &amp; Finance Magazine's widely recognized position as a highly respected voice of the industry.
</p>
<p class="desc-text italic">
  ###
</p>
<p class="desc-text italic">
  Guidance Residential is a subsidiary of Guidance Financial Group, an international company dedicated to serving the market for Sharia-compliant financial products and
  services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide, and provides financial services
  to its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products has allowed
  it to develop unique financial solutions that address unmet needs of the Sharia-compliant market.
</p>
@endsection
