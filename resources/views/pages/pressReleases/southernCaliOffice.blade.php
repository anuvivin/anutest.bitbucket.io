@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Celebrate Southern California Office Opening')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'July 12, 2011')
@section('prevPressReleaseUrl', route('pressReleases.chicagoOffice'))
@section('nextPressReleaseUrl', route('pressReleases.historicIslamicFinance'))

@section('pressReleaseBody')
<p class="desc-text">
  Century City, CA  <br>
  (July 12, 2011) As part of its commitment to provide superior customer service and a local presence in the communities it serves, Guidance Residential ("Guidance"), the leading US provider of Sharia-compliant home financing,
  announced the opening of its Southern California office located at 1999 Avenue of the Stars, 11th Floor, Century City, California 90067.
</p>
<p class="desc-text">
  To commemorate the opening, Guidance hosted a grand opening event for community members on October 12th at The Century Plaza Hyatt Regency 2025 Avenue of the Stars, Los Angeles, California 90067.
  Local business owners, realtors&reg;, community members and leaders were welcomed at the celebration and had a chance to network with one another over light refreshments and snacks.
</p>
<p class="desc-text">
  "Guidance is proud to be part of this community," said Aziz Molai, Guidance's new Regional Manager.
  "We're ready as ever to meet the needs of all our customers by providing the nation's best Sharia-compliant home financing program."
</p>
<p class="desc-text">
  Guidance's Southern California office is open Monday through Friday from 9:00 am to 6:00 pm. At this office, experienced Account Executives will be able to assist customers in English and Urdu.
  From the home office in Reston, Virginia, Account Executives are able to assist customers in English, Arabic, Urdu and Bangali.
</p>
<p class="desc-text">
  Guidance Residential
</p>
<p class="desc-text italic">
  With over $2 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international company dedicated to serving the market
  for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide, and provides financial services to
  its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products has allowed it to develop unique financial solutions that
  address unmet needs of the Sharia-compliant market.
</p>
@endsection