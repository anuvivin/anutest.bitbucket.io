@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Celebrates Northern New Jersey Office Opening')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'January 18, 2012')
@section('prevPressReleaseUrl', route('pressReleases.amjaResolution'))
@section('nextPressReleaseUrl', route('pressReleases.chicagoOffice'))

@section('pressReleaseBody')
<p class="desc-text">
  Hasbrouck Heights, NJ <br>
  (January 18, 2012) As part of its commitment to provide superior customer service and a strong local presence in the communities it serves, Guidance Residential ("Guidance"), the leading US provider of
  Sharia-compliant home financing, announced the opening of its northern New Jersey office located at 21 Main Street, Suite 351, Hackensack, New Jersey 07601.
</p>
<p class="desc-text">
  To commemorate the opening, Guidance hosted a grand opening reception for community members on January 18th at the Hilton hotel in Hasbrouck Heights, New Jersey. In attendance were local business owners, realtors&reg;,
  community members and leaders, all of whom had the opportunity to network with one another over light refreshments and snacks.
</p>
<p class="desc-text">
  "Guidance is proud to be part of this community," said Abdessamad Melouane, Guidance's Regional Manager. "Our commitment to providing the nation's best Sharia-compliant home financing program has never been stronger."
</p>
<p class="desc-text">
  Guidance's northern New Jersey office is open Monday through Friday from 9:00 am to 6:00 pm. At this office, experienced Account Executives will be able to assist customers in English, Arabic, and Bengali. From the home
  office in Reston, Virginia, Account Executives are able to assist customers in English, Arabic, Urdu and Bengali.
</p>
<p class="desc-text">
  Guidance Residential
</p>
<p class="desc-text italic">
  With over $2 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international company dedicated to serving the market
  for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide, and provides financial services to
  its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products has allowed it to develop unique financial solutions that
  address unmet needs of the Sharia-compliant market.
</p>
@endsection