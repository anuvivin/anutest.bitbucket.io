@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Effort to Establish Islamic Bank in Morocco Wins Approval')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'January 9, 2017')
@section('prevPressReleaseUrl', route('pressReleases.arkansasAlabamaOperations'))
@section('nextPressReleaseUrl', route('pressReleases.delawareOperations'))

@section('pressReleaseBody')
<p class="desc-text">
  RESTON, Va., Jan. 9, 2017 - Guidance Financial Group ("Guidance"), a financial services company dedicated to innovation in the field of Shariah-compliant finance and investment, is pleased to announce that
  Morocco's Central Bank has approved its proposal for the establishment of an Islamic bank in the Kingdom of Morocco.  The proposal was submitted in partnership with Banque Centrale Populaire (BCP), one of
  Morocco's largest banks.
</p>
<p class="desc-text">
  This announcement comes on the heels of new legislation in Morocco authorizing the establishment of independent Islamic financial institutions characterized as "participative banks".
  The collaboration between Guidance and BCP is aimed at fulfilling the growing market demand in Morocco for Islamic financial products while maintaining their authenticity.
</p>
<p class="desc-text">
  "We are delighted by the news that we will be able to positively impact the Moroccan consumer finance market," said Khaled Elsayed, President and CEO of Guidance Financial Group, USA.
  He added, "It is also nice to know that this achievement is yet another positive result of the remarkable success our distinctive US Islamic home finance program has had since it was introduced over 15
  years ago to the US Muslim consumer market."
</p>
<p class="desc-text">
  Established in 2000, Guidance Financial Group's businesses include: Guidance Residential, the dominant Shariah-compliant housing finance provider in the United States; Dar Al Tamleek, a leading housing
  finance provider in Saudi Arabia; Guidance Investments, a specialty investment company that has created a series of Shariah-compliant institutional funds focused on the private equity, real estate and
  equipment leasing sectors across the globe; Guidance Capital Markets; a DIFC capital markets advisory firm: and Shaza Hotels, a hotel management company established in partnership with Kempinski Hotels.
</p>
@endsection
