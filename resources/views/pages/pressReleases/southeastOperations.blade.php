@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = '#1 U.S. Islamic Home Financing Provider Expands Operations in Kentucky')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'July 12, 2016')
@section('prevPressReleaseUrl', route('pressReleases.delawareOperations'))
@section('nextPressReleaseUrl', route('pressReleases.tradeSecretMisappropriation'))

@section('pressReleaseBody')
<p class="desc-text italic">
  GUIDANCE RESIDENTIAL, LLC announces opening in Kentucky to serve growing population of Muslim Americans
</p>
<p class="desc-text">
  <span class="bold">RESTON, VIRGINIA</span> - A premier provider of Shariah-compliant home financing in the United States is expanding its national footprint with the growth of its
  operations in Kentucky. Guidance Residential, LLC currently has locations in 23 states and was operational in Kentucky as of July 8, 2016. The decision to expand its products and
  services will allow Guidance Residential to meet the demand for Islamic home financing for the growing population of Muslim Americans throughout the state.
</p>
<p class="desc-text">
  According to Islamic law, or Shariah, Muslims are prohibited from participating in financial transactions that charge interest for loans. Therefore, many Muslim Americans were unable
  to participate in the American dream of home ownership. Founded in 2002, Guidance Residential developed a unique financial solution, the Declining Balance Co-Ownership Program, to
  address the unmet need for financial products and services that adhere to Islamic principles.
</p>
<p class="desc-text">
  Other than being the nation's leading provider of Islamic home financing, funding over $3.9 billion dollars, Guidance Residential is the largest corporate sponsor of Muslim American
  community initiatives.
</p>
<p class="desc-text italic">
  "We are looking forward to expanding our operations in Kentucky. It's an exciting time for our company and we are proud to bring Shariah-compliant products and services to the state.
  With an exceptional workforce and dedicated professionals, the Bluegrass State has proven to be the perfect place to grow our business."<br>
  <span class="bold">- Guidance Residential President &amp; CEO, Khaled Elsayed</span>
</p>
<p class="desc-text">
  For more information on Guidance Residential, please visit <a href="{{ route('home') }}">{{ route('home') }}</a>
  For career opportunities, please visit: <a href="{{ route('careers') }}">{{ route('careers') }}</a>
  <ul>
    <li>Founded in 2002</li>
    <li>Licensed in 24 states</li>
    <li>Authentic home financing model compliant with both US and Islamic legal systems</li>
    <li>Independent Shariah Supervisory Board comprised of world-renowned Shariah scholars</li>
    <li>Endorsed by Assembly of Muslim Jurists of America (AMJA)</li>
    <li>Industry leader featured in national and international media including CNN, The Wall Street Journal, The New York Times, The Kiplinger Letter, Bloomberg Business, USA Today, and Al Jazeera.</li>
  </ul>
</p>
@endsection
