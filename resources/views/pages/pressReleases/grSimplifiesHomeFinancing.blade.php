@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Residential Simplifies Islamic Home Financing for Consumers')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'February 28, 2018')
@section('prevPressReleaseUrl', route('pressReleases.launchesNewMobileApp'))
@section('nextPressReleaseUrl', route('pressReleases.awardedByWorldFinance'))

@section('pressReleaseBody')
<p class="desc-text italic">
  New website to serve as a one-stop hub for information and resource for home financing needs
</p>
<p class="desc-text">
  <span class="bold">RESTON, Va., Feb. 28, 2018</span> - Guidance Residential, the leading U.S. Islamic home financing provider is making it easier for consumers to understand
  alternative options for financing a home. The company launched a new website that has resources for customers at all stages in the home financing process - whether they're in
  the planning stage, ready to apply for financing or are existing customers.
</p>
<p class="desc-text">
  "The home financing process can be very complex," said President and CEO Khaled Elsayed. "And, when you add the need for it to comply with Shariah principles, it can get even
   more complex." The new website provides information that not only simplifies the process, but also clarifies what characteristics are needed for a home financing contract to
    truly be Shariah-compliant.
</p>
<p class="desc-text">
  Because Muslim Americans make up an underserved market of faith-conscious consumers, there is not much information about Shariah-compliant products and services available.
  One goal of the new website is to address that need for more information. Another is to inform other consumers, who may not identify as Muslim American, about the option for
  a more consumer-friendly product.
</p>
<p class="desc-text">
  While website visitors will find standard home financing website features, such as estimation calculators and home finance application checklists, new features will give them
  more insight into the Guidance Residential brand and the company's expertise in the industry. The Knowledge Center, which consists of content in multiple forms - such as
  e-books, podcasts and white papers, to name a few - is a new feature saturated with educational content about Islamic home financing. Customers can look into News and Events
  to gain a better understanding of the company and its community footprint.
</p>
<p class="desc-text bold">
  Visit <a href="https://www.guidanceresidential.com/">www.guidanceresidential.com</a> to view the new website and learn more about Guidance Residential.
</p>
<p class="desc-text bold">
  About Guidance Residential
</p>
<p class="desc-text bold">
  Guidance Residential is the leading provider of Islamic home financing in the United States. Since its inception in 2002, Guidance Residential has funded over $4.9 billion in
  Shariah-compliant home financing contracts. Guidance Residential's proprietary Declining Balance Co-ownership program is trusted as a strictly Shariah-compliant home financing
   program in the U.S. Islamic finance market. The Declining Balance Co-ownership Program received the highest rating by The Assembly of Muslim Jurists of America (AMJA).
</p>
<p class="desc-text bold">
  Guidance Residential offers Shariah-compliant home financing in the following U.S. states:<br>
  Connecticut, Massachusetts, New Jersey, New York, Pennsylvania, Rhode Island, Illinois, Michigan, Minnesota, Ohio, Wisconsin, Arizona, Kansas, Texas, Delaware, Kentucky,
  Maryland, Virginia, Washington D.C., California, Oregon, Washington, Alabama, Arkansas, Florida, Georgia, North Carolina, South Carolina, Tennessee
</p>
<p class="desc-text small">
  Guidance Residential, LLC, (Nationwide Mortgage Licensing System No.2908), 11107 Sunset Hills Rd, Suite 200, Reston, VA 20190 is licensed by the Department of Business
  Oversight under the California Residential Mortgage Lending Act (413-0427); Alabama Consumer Credit (22410); Arizona Mortgage Banker (0923408); Arkansas Mortgage
  Banker-Broker-Servicer (112069); Colorado Mortgage Company Registration ; Connecticut Mortgage Lender (18263); Delaware Lender (022090); District of Columbia Mortgage Dual
  Authority (MLB2908); Florida Mortgage Lender (MLD656); Georgia Mortgage Lender (17286); Illinois Residential Mortgage Licensee (MB.0006455 11107 Sunset Hills Road, Suite 200,
   , Reston, VA 20190; MB.0006455-001 100 East Roosevelt Rd, Units 44 &amp; 45, Villa Park, IL 60181) licensed by the ILDFPR, James R Thompson Center (JRTC), 100 West Randolph
   Street, 9th Floor, Chicago, IL 60601, 888-473-4858 (General), 844-768-1713 (Banking Div); Kansas Licensed Mortgage Company (MC.0025178); Kentucky Mortgage Company
   (MC361158 &amp;MC361566); Maryland Mortgage Lender (12927); Massachusetts Mortgage Lender (ML2908); Michigan 1st Mortgage Lender, Broker &amp; Servicer Registrant (FR-0941);
   Minnesota Mortgage Originator (MN-MO 20320419); New Jersey Licensed Lender (9933648); New York – Licensed Mortgage Banker – New York Department of Financial Services
   (B500726) (NY location: 171-21 Jamaica Ave, 1st Floor, Jamaica, NY 11432); North Carolina Mortgage Lender (L-112542); Ohio Mortgage Broker Act Mortgage Banker Exemption
   (MBMB 850079.000); Oregon Mortgage Lender (ML-4145); Pennsylvania – licensed by the Pennsylvania Department of Banking, Mortgage Lender (21050); Rhode Island Lender License
    (20163318LL); South Carolina BFI Mortgage Lender/Servicer (MLS-2908); Tennessee Mortgage License (109274); Texas SML Mortgage Banker Registrant ;  Virginia Lender Licensee
    (MC-2138) licensed by the Virginia State Corporation Commission; Washington Consumer Loan Company (CL-2908); Wisconsin Mortgage Banker (46355BA).
</p>
@endsection
