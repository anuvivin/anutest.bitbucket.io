@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance to Celebrate Opening of Chicago Office')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'July 12, 2011')
@section('prevPressReleaseUrl', route('pressReleases.newJerseyOffice'))
@section('nextPressReleaseUrl', route('pressReleases.southernCaliOffice'))

@section('pressReleaseBody')
<p class="desc-text">
  Lombard, IL <br>
  (July 12, 2011) As part of its commitment to provide superior customer service and a local presence in the communities it serves, Guidance Residential ("Guidance"), the leading US provider of Sharia-compliant home financing,
  announced the opening of its Chicago office located at 77 East Butterfield Road, Suite 213, in Lombard, Illinois.
</p>
<p class="desc-text">
  To commemorate the opening, Guidance will be hosting a grand opening event for all to attend on July 20th at the Lombard office from 6:00 pm to 9:00 pm. Local business owners, realtors&reg;, community members and leaders are all welcomed
  to join in the celebration and to network with one another over light refreshments and snacks (See below for RSVP info).
</p>
<p class="desc-text">
  "Guidance is proud to be part of this community," said Faye Safi, Guidance's new Regional Manager who brings over 13 years of experience in the financial services industry. "We're ready as ever to meet the needs of all our customers
  by providing the nation's best Sharia-compliant home financing program."
</p>
<p class="desc-text">
  Guidance's Lombard office is open Monday through Friday from 9:00 am to 6:00 pm. At this office, experienced Account Executives will be able to assist customers in English, Arabic and soon Urdu. From the home office in Reston,
  Virginia, Account Executives are able to assist customers in English, Arabic, Urdu and Bangali.
</p>
<p class="desc-text">
  Guidance Residential
</p>
<p class="desc-text italic">
  With over $2 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international company dedicated to serving the market
  for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide, and provides financial services to
  its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products has allowed it to develop unique financial solutions that
  address unmet needs of the Sharia-compliant market.
</p>
@endsection