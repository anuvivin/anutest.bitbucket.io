@extends('layouts.default')

@section('title')
@yield('pressReleaseTitle') Press Release
@endsection

@section('content')
<div class="container pressReleases">
  <div class="main-heading">
    <h1 class="title">Press Release</h1>
    <span class="seperate-line"></span>
  </div>
  <div class="press-release">
    <div class="share-icons">
      <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}" target="_blank"><span class="fa fa-facebook-official"></span></a>
      <a href="https://twitter.com/home?status={{ urlencode(Request::url()) }}" target="_blank"><span class="fa fa-twitter-square"></span></a>
      <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(Request::url()) }}&title=@yield('sharedUrlTitle')&summary=&source=" target="_blank"><span class="fa fa-linkedin-square"></span></a>
    </div>
    <div class="press">
      <a class="press-link">@yield('pressReleaseTitle')</a>
      <p class="desc-date">@yield('pressReleaseDate')</p>
      <span class="seperate-line"></span>
    </div>
    @yield('pressReleaseBody')
    <p class="desc-text">
      Media Contact:
    </p>
    <p class="desc-text">
      <a href="mailto:communications@guidanceresidential.com" class="com-link">communications@guidanceresidential.com</a>
    </p>
    <div class="button-container">
      @hasSection('prevPressReleaseUrl')
        <div class="prev-button">
          <a href="@yield('prevPressReleaseUrl')" class="main-button">Previous</a>
        </div>
      @else
      <div class="empty-button">
      </div>
      @endif
      @hasSection('nextPressReleaseUrl')
        <div class="next-button">
          <a href="@yield('nextPressReleaseUrl')" class="main-button">Next</a>
        </div>
      @endif
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "NewsArticle",
    "headline": "@yield('pressReleaseTitle')",
    "url":"{{ Request::url() }}",
    "author": "Guidance Residential",
    "datePublished": "{{ Carbon\Carbon::parse($__env->yieldContent('pressReleaseDate'))->format('Y-m-d') }}",
    "image": {
      "@type": "imageObject",
      "url": "{{ mix_remote('images/GR_LOGO.png') }}",
      "height": "65",
      "width": "123"
    },
    "publisher": {
      "@type": "Organization",
      "name": "Guidance Residential",
      "logo": {
        "@type": "imageObject",
        "url": "{{ mix_remote('images/GR_LOGO.png') }}"
      }
    }
  }
  </script>
@endsection

@section('mainFooter')
<div class="media-inquiry-flag">
  <div class="app-circle">
    <span class="fa fa-newspaper-o"></span>
  </div>
  <div class="main-heading left">
    <h3 class="title three golden">Media inquiry</h3>
    <span class="seperate-line sub"></span>
    <div class="contact-info">
      <div class="item">
        <a href="mailto:Communications@GuidanceResidential.com">Communications@GuidanceResidential.com</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Pressreleases'])
@endsection
