@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance to Celebrate Opening of New York Metro Office')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'March 11, 2015')
@section('prevPressReleaseUrl', route('pressReleases.tradeSecretMisappropriation'))
@section('nextPressReleaseUrl', route('pressReleases.amjaResolution'))

@section('pressReleaseBody')
<p class="desc-text column-flex">
  <span class="bold">WHO:</span><span class="text">Guidance Residential, LLC &amp; Guidance Realty Homes, LLC</span>
</p>
<p class="desc-text column-flex">
  <span class="bold">WHAT:</span><span class="text">Grand Opening of New Office in Jamaica, New York</span>
</p>
<p class="desc-text column-flex">
  <span class="bold">WHEN:</span><span class="text">Saturday, March the 28th from 11:00 am to 2:00 pm</span>
</p>
<p class="desc-text column-flex">
  <span class="bold">WHERE:</span><span class="text">171-21 Jamaica Ave., Jamaica, New York 11432.</span>
</p>
<div class="desc-text column-flex">
  <div class="bold">WHY:</div>
  <div class="text">
    <div class="right-text">
      As part of its commitment to provide superior customer service and a local presence in the communities it serves, Guidance Residential, the nation's leading provider of
      Sharia-compliant home financing, announces the opening of its new retail office in Jamaica, New York.
    </div>
    <div class="right-text">
      The Jamaica location is also the new home for Guidance Residential's sister company, Guidance Realty Homes which is the real estate brokerage arm of the parent company,
      Guidance Financial Group.
    </div>
    <div class="right-text">
      To commemorate the opening, Guidance will be hosting a grand opening event for all to attend on Saturday, March 28th at the new office from 11:00 am to 2:00 pm. Local
      business owners, community members and leaders are all welcomed to join in the celebration and to network with one another over refreshments and snacks.
    </div>
  </div>
</div>
<p class="desc-text">
  ###
</p>
<p class="desc-text italic">
  With over $3.3 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international
  company dedicated to serving the market for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional
  investors and financial intermediaries worldwide, and provides financial services to its retail customer base in the United States. The company's commitment to intensive
  research and development and solid operational support for its products has allowed it to develop unique financial solutions that address unmet needs of the Sharia-compliant
  market.
</p>
@endsection
