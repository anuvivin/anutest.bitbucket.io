@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Historic Islamic Finance Training Draws Imams from Across the Nation')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'June 14, 2011')
@section('prevPressReleaseUrl', route('pressReleases.southernCaliOffice'))
@section('nextPressReleaseUrl', route('pressReleases.landmarkIslamicTraining'))

@section('pressReleaseBody')
<p class="desc-text">
  Reston, VA  <br>
  (June 14, 2011) The American Islamic Finance Project (AIF), a collaborative effort between Guidance Financial Group and the Islamic Society of North America, held its first successful imam training on the basics
  of Islamic finance in Berkeley, California. Imams from across the country attended the training, which is the first-ever training of its kind in the US.
</p>
<p class="desc-text">
  Designed exclusively for imams serving communities in the United States, the historic training was held from May 2nd to May 4th. The purpose of the training was to raise the level of knowledge and understanding
  of Islamic financial transactions under the belief that financial markets can benefit significantly from the ethical and moral guidelines that Islam's financial concepts provide.
</p>
<p class="desc-text">
  The opening ceremony featured presentations by keynote speakers Shaykh Hamza Yusuf, founder of Zaytuna College, Safaa Zarzour, Secretary General of ISNA and Dr. Mohamad Hammour, Founder and Chairman of Guidance
  Financial Group. The audience also had a chance to hear a special audio address by Shaykh Taqi Usmani, Sharia Board Chairman of the Accounting and Auditing Organization of Islamic Financial Institutions (AAOIFI)
  and perhaps today's leading scholar in Islamic finance.
</p>
<p class="desc-text">
  The following day, the intensive, two-day training was conducted by Atif Khan, Managing Director at Ethica Institute of Islamic Finance, the world's leading provider of Islamic finance training.
  Upon successful completion of the training, all of the attendees were honored with a certificate of training, two months of free access to Ethica's e-learning portal and two complimentary nights at the
  five-star Shaza Hotel in Al Madina, Saudi Arabia courtesy of Guidance Financial Group.
</p>
<p class="desc-text">
  The sole underwriter of this event was Guidance Financial Group, who made the training possible so that all participating imams can increase their knowledge of Islamic finance without becoming concerned about expenses.
</p>
<p class="desc-text">
  ###
</p>
<p class="desc-text">
  Islamic Society of North America
</p>
<p class="desc-text">
  ISNA is an association of Muslim organizations and individuals that provides a common platform for presenting Islam, supporting Muslim communities, developing educational, social and outreach programs and
  fostering good relations with other religious communities, and civic and service organizations.
</p>
<p class="desc-text">
  Ethica
</p>
<p class="desc-text">
  With over 20,000 paid users in more than 40 countries, Ethica is the world's leading accredited Islamic finance training and certification institute. Ethica remains the only institute in the world to deliver
  standardized certification based entirely on the Accounting and Auditing Organization of Islamic Financial Institutions (AAOIFI), the leading standard-setting body in the Islamic finance industry.
</p>
<p class="desc-text">
  Guidance Residential
</p>
<p class="desc-text italic">
  With over $2 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international company dedicated to serving the market
  for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional investors and financial intermediaries worldwide, and provides financial services to
  its retail customer base in the United States. The company's commitment to intensive research and development and solid operational support for its products has allowed it to develop unique financial solutions that
  address unmet needs of the Sharia-compliant market.
</p>
@endsection