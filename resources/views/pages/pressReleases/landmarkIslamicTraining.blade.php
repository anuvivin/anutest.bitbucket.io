@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Introduces Landmark Islamic Finance Training for American Imams')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'April 25, 2011')
@section('prevPressReleaseUrl', route('pressReleases.historicIslamicFinance'))
@section('nextPressReleaseUrl', route('pressReleases.pioneeringEvent'))

@section('pressReleaseBody')
<p class="desc-text italic bold">
  Pioneering Event is a Collaborative Effort between Major American-Muslim Groups
</p>
<p class="desc-text">
  Reston, VA - Guidance Residential ("Guidance") announced today the formation of the American Islamic Finance Project ("AIF") which is a collaborative effort between the
  Islamic Society of North America ("ISNA"), Ethica Institute of Islamic Finance ("Ethica") and Guidance to provide the first-ever Islamic finance training to over 100
  American imams.
</p>
<p class="desc-text">
  The role of AIF is to raise the level of knowledge and understanding of Islamic financial transaction law under the belief that financial markets can benefit significantly
  from the ethical and moral guidelines that Islam's financial concepts provide.
</p>
<p class="desc-text">
  The landmark training, which is to be held from May 2 to May 5 in Berkeley, California, is designed exclusively for imams serving communities in the United States. Muslim
  communities across America are increasingly considering Islamic finance as a legitimate option for their personal investment strategies and in doing so they are turning to
  their imams for direction, making this training more important than ever.
</p>
<p class="desc-text">
  The opening ceremony, being held at 5pm on May 2nd, will feature a broad discussion by invited keynote presenters such as Shaykh Nizam Yaqub, a renowned Islamic finance
  scholar, Shaykh Hamza Yusuf, founder of Zaytuna College, and Imam Mohammed Maged, President of ISNA. The bulk of the training will be led by Atif Khan, Managing Director
  at Ethica, who will conduct two full days of intensive training about the principles of Islamic financial transactions.
</p>
<div class="desc-text">
  The sole underwriter of this event is Guidance Financial Group, who made this training possible for all participating imams to increase their knowledge of Islamic finance
  without incurring any expenses. More information can be found at <a href="http://www.americanislamicfinance.com/" target="_blank">www.AmericanIslamicFinance.com</a>.
</div>
<p class="desc-text italic">
  About Guidance Residential
</p>
<p class="desc-text italic">
  With over $2 billion in transactions, Guidance is the leading US provider of Sharia-compliant home financing. It is a subsidiary of Guidance Financial Group, an international
  company dedicated to serving the market for Sharia-compliant financial products and services. Guidance Financial Group offers unique investment products to institutional
  investors and financial intermediaries worldwide, and provides financial services to its retail customer base in the United States. The company's commitment to intensive
  research and development and solid operational support for its products has allowed it to develop unique financial solutions that address unmet needs of the Sharia-compliant
  market.
</p>
@endsection
