@extends('pages.pressReleases.layout')

@section('pressReleaseTitle', $pressReleaseTitle = 'Guidance Residential Expands Home Finance Operations in Delaware')
@section('sharedUrlTitle', urlencode($pressReleaseTitle))
@section('pressReleaseDate', 'December 21, 2016')
@section('prevPressReleaseUrl', route('pressReleases.islamicBankMorocco'))
@section('nextPressReleaseUrl', route('pressReleases.southeastOperations'))

@section('pressReleaseBody')
<p class="desc-text italic">
  The #1 U.S. Islamic home finance provider opening doors in Delaware to serve growing population of Muslim Americans
</p>
<p class="desc-text">
  <span class="bold">RESTON, VIRGINIA</span> - A premier provider of Sharia-compliant home financing in the United States is expanding its national footprint with the growth of its operations in Delaware.
  Guidance Residential, LLC currently has locations in 25 states and was operational in Delaware as of November 21, 2016. The decision to expand its products and services will allow
  Guidance Residential to meet the demand for Islamic home financing for the growing population of Muslim Americans throughout the state.
</p>
<p class="desc-text">
  According to Islamic law, or Sharia, Muslims are prohibited from participating in financial transactions that charge interest for loans. Therefore, many Muslim Americans were unable to
  participate in the American dream of home ownership. Founded in 2002, Guidance Residential developed a unique financial solution, the Declining Balance Co-Ownership Program, to address
  the unmet need for financial products and services that adhere to Islamic principles.
</p>
<p class="desc-text">
  Other than being the nation's leading provider of Islamic home financing, funding over $4 billion dollars, Guidance Residential is the largest corporate sponsor of Muslim American community initiatives.
</p>
<ul>
  <li>Founded in 2002</li>
  <li>Licensed in 25 states</li>
  <li>Authentic home financing model compliant with both US and Islamic legal systems</li>
  <li>Independent Shariah Supervisory Board comprised of world-renowned Shariah scholars</li>
  <li>Endorsed by Assembly of Muslim Jurists of America (AMJA)</li>
  <li>Industry leader featured in national and international media including CNN, The Wall Street Journal, The New York Times, The Kiplinger Letter, Bloomberg Business, USA Today, and Al Jazeera.</li>
</ul>
@endsection