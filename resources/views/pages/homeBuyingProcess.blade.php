@extends('layouts.default')

@section('title', 'Home Buying Process')

@section('content')
  <div class="container home-buying-process">
    <div class="main-heading">
      <h1 class="title">
        Home Buying Process
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Discover the six steps in the home buying process. Guidance Residential will guide you through each step to help you complete your homebuyer's journey.
      </p>
    </div>
    <div class="split-list-container">
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-lightbulb-o"></span>
              </div>
              <h3 class="title three bold">Step 1: Plan</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Research and map out a budget in stage one of the homebuyer's journey.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#whereToLive" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="whereToLive">
              <span class="fa expand-icon"></span><h4 class="title four bold">Research and Plan Where You Want To Live</h4>
            </a>
            <div class="text-content left collapse" id="whereToLive">
              <div class="text">
                <p class="desc-text">
                  You may have some plans about where you want to live. You may want to search and narrow down specific areas and learn about house pricing around those neighborhoods.
                  Use our <a href="{{ config('urls.grealty.search') }}">home search engine</a> to learn more about your preferred community.
                </p>
              </div>
            </div>
            <a href="#planYourBudget" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="planYourBudget">
              <span class="fa expand-icon"></span><h4 class="title four bold">Plan Your Budget</h4>
            </a>
            <div class="text-content left collapse" id="planYourBudget">
              <div class="text">
                <p class="desc-text">
                  <ul>
                    <li>
                      <span class="list-text">
                        Once you have some idea about the range of home pricing, the next step is to find out how much you can afford based on your income and expense situation.
                        You can estimate your price range and approximate monthly payment by using our <a href="{{ route('estimationCalculators') }}#affordabilityCalculator">affordability calculator</a>.
                      </span>
                    </li>
                    <li>
                      <span class="list-text">
                        You can get a more accurate estimate of your affordability if you do our online <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingProcess_Step1_PlanYourBudget_Feb2018" target="_blank">Pre-Qualification</a> or
                        get connected to a licensed <a href="{{ route('account-executive.index') }}">Account Executive</a> who can assist you with finding out the best estimates.
                      </span>
                    </li>
                  </ul>
                </p>
              </div>
            </div>
            <a href="#evaluatePropertyTypes" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="evaluatePropertyTypes">
              <span class="fa expand-icon"></span><h4 class="title four bold">Evaluate Property Types and Financing Options</h4>
            </a>
            <div class="text-content left collapse" id="evaluatePropertyTypes">
              <div class="text">
                <p class="desc-text">
                  Evaluate different home types based on your home buying and family needs. At Guidance Residential, we offer financing for Single Family Homes, Townhomes, Condominiums,
                  Planned Unit Developments (PUDs), and Multi-units (up to 4 units). <a href="{{ route('financingOptions') }}">Click here to see the variety of financing options we offer.</a>
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a data-toggle="modal" data-target="#scheduleConsultationModal" class="main-button flex-button">Schedule a Free Consultation</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-pencil-square-o"></span>
              </div>
              <h3 class="title three bold">Step 2: Act </h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Complete online Pre-Qualification in less than 10 minutes with no credit check required.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#knowCreditScore" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="knowCreditScore">
              <span class="fa expand-icon"></span><h4 class="title four bold">Know Your Credit Score</h4>
            </a>
            <div class="text-content left collapse" id="knowCreditScore">
              <div class="text">
                <p class="desc-text">
                	When the time comes to apply for financing, we'll complete a credit check to obtain your score. Some things that you can do to improve your credit score are:
                  <ul>
                    <li><span class="list-text">Keep credit card balances low</span></li>
                    <li><span class="list-text">Pay bills on time</span></li>
                    <li><span class="list-text">Don't cancel unused credit cards</span></li>
                    <li><span class="list-text">Don't take on any new debt</span></li>
                  </ul>
                </p>
              </div>
            </div>
            <a href="#completePreQualification" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="completePreQualification">
              <span class="fa expand-icon"></span><h4 class="title four bold">Complete Online Pre-Qualification</h4>
            </a>
            <div class="text-content left collapse" id="completePreQualification">
              <div class="text">
                <p class="desc-text">
                  Complete our online <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingProcess_Step2_CompleteOnlinePQ_Feb2018" target="_blank">Pre-Qualification</a> form or connect with a licensed <a href="{{ route('account-executive.index') }}">Account Executive</a> who can assist you in finding the best home financing estimates.
                </p>
              </div>
            </div>
            <a href="#estimateMonthlyPayments" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="estimateMonthlyPayments">
              <span class="fa expand-icon"></span><h4 class="title four bold">Estimate Monthly Payments</h4>
            </a>
            <div class="text-content left collapse" id="estimateMonthlyPayments">
              <div class="text">
                <p class="desc-text">
                  When you complete the online <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingProcess_Step2_GetPQNow_Feb2018" target="_blank">Pre-Qualification</a>, you will get an estimate of your monthly payment based on the information you provide. Start the online <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingProcess_Step2_GetPQNow_Feb2018" target="_blank">Pre-Qualification</a> now.
                  It takes less than 10 minutes to complete and there is no credit check for the process.
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_HomeBuyingProcess_Step2_GetPQNow_Feb2018" target="_blank" class="main-button flex-button">Get Pre-Qualified Now</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-user-circle-o"></span>
              </div>
              <h3 class="title three bold">Step 3: Connect</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Connect with a licensed Account Executive to guide you through the home financing process.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#connectAccExecutive" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="connectAccExecutive">
              <span class="fa expand-icon"></span><h4 class="title four bold">Connect with an Account Executive</h4>
            </a>
            <div class="text-content left collapse" id="connectAccExecutive">
              <div class="text">
                <p class="desc-text">
                  After you complete Guidance's online Pre-Qualification form, you will be assigned a licensed Account Executive who will verify the information you provided and assist you with the pre-approval and conditional approval process.
                </p>
              </div>
            </div>
            <a href="#getPreApproved" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="getPreApproved">
              <span class="fa expand-icon"></span><h4 class="title four bold">Get Pre-Approved</h4>
            </a>
            <div class="text-content left collapse" id="getPreApproved">
              <div class="text">
                <p class="desc-text">
                  Our expert Account Executives will help you prepare for your home financing journey by assisting you with the pre-approval and conditional approval process. A licensed Account Executive will conduct a credit check
                  and verify all information you provide to approve you for a specific financing amount. Once you receive a conditional pre-approval letter, you will have an advantage over other home shoppers in the home buying market.
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a href="{{ route('account-executive.index') }}" class="main-button flex-button">Connect with an Account Executive</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-shopping-cart"></span>
              </div>
              <h3 class="title three bold">Step 4: Shop</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Search and find your dream home with the help of a reliable real estate agent.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#connectWithRealEstate" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="connectWithRealEstate">
              <span class="fa expand-icon"></span><h4 class="title four bold">Connect with a Real Estate Agent</h4>
            </a>
            <div class="text-content left collapse" id="connectWithRealEstate">
              <div class="text">
                <p class="desc-text">
                 Real estate agents are professionals that have expert information about the home buying process and real estate in their licensed areas. GuidanceRealty.com, our real estate partner program,
                 was created to offer customers a simple, cost-effective and consumer-friendly solution to purchase a home. It connects real estate agents with customers who Pre-Qualify online through Guidance
                 Residential and also serves as a search engine for prospective buyers who access the site to search for agents. The program saves customers money when they decide to finance with Guidance Residential
                 and work with an partner agent.
                </p>
              </div>
            </div>
            <a href="#findYourHome" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="findYourHome">
              <span class="fa expand-icon"></span><h4 class="title four bold">Find your home</h4>
            </a>
            <div class="text-content left collapse" id="findYourHome">
              <div class="text">
                <p class="desc-text">
                   Search for your home using the search function on <a href="{{ config('urls.grealty.search') }}">GuidanceRealty.com</a>. The platform makes it easy for you to search for different property types based on your criteria for the community and neighborhood
                   - such as school rankings, proximity to mosques, places of worship and dining options. You can also schedule showings with a partner agent and received tailored monthly home estimates. Narrow down your list by
                   keeping a list of saved searches on GuidanceRealty.com.
                </p>
              </div>
            </div>
            <a href="#makeAnOffer" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="makeAnOffer">
              <span class="fa expand-icon"></span><h4 class="title four bold">Make an offer</h4>
            </a>
            <div class="text-content left collapse" id="makeAnOffer">
              <div class="text">
                <p class="desc-text">
                   A GuidanceRealty.com agent will serve as a local expert with knowledge of comparable homes in your desired home area to help you make an informed offer. Agents will be prepared to guide you through
                   negotiations and get you the best deal possible. Your agent will also be familiar with the home financing process which will begin once your offer is accepted.
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a href="{{ config('urls.grealty.find-an-agent') }}" class="main-button flex-button">Find a Real Estate Agent</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-check-circle-o"></span>
              </div>
              <h3 class="title three bold">Step 5: Apply</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Connect with a licensed Guidance Account Executive to apply for financing and provide necessary documentations.
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#applyForFinancing" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="applyForFinancing">
              <span class="fa expand-icon"></span><h4 class="title four bold">Apply for Financing</h4>
            </a>
            <div class="text-content left collapse" id="applyForFinancing">
              <div class="text">
                <p class="desc-text">
                 Once you Pre-Qualify with Guidance, your licensed Account Executive will follow up with you periodically to see if your purchase offer has been accepted.  You can also contact your Account Executive
                 if your offer is accepted prior to hearing from your Account Executive. In either situation, your Account Executive will begin the finance application process for you once your offer is accepted.
                 This typically takes 10 minutes over the phone.  During the finance application process, you will need to provide a number of documents and signed paperwork before you can be approved for financing.
                 This process takes an average of 30 to 45 days, but can be reduced (dependent upon third party vendors' timeliness, your credit profile and responsiveness).
                </p>
              </div>
            </div>
            <a href="#provideDocumentation" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="provideDocumentation">
              <span class="fa expand-icon"></span><h4 class="title four bold">Provide Necessary Documentation</h4>
            </a>
            <div class="text-content left collapse" id="provideDocumentation">
              <div class="text">
                 <p class="desc-text">
                   Once you apply for financing with Guidance Residential, you will receive a letter about what to expect. The application process can be completed in 5 steps in which we contact you, confirm your information,
                   confirm your home's value, request your signature on important documents, and finally approve your financing amount. Avoid delays by having the following files ready to provide upon request:
                   <ul>
                     <li><span class="list-text">I-9 Documents (or documents that contain a photo ID)</span></li>
                     <li><span class="list-text">Income Documents</span></li>
                     <li><span class="list-text">Asset/Bank Statements</span></li>
                     <li><span class="list-text">Disclosure Documents - these will be provided to you by Guidance Residential for you to sign and return</span></li>
                     <li><span class="list-text">Other Documents - such as a purchase agreement, an active home insurance policy, and taxes, insurance and HOA dues for other real estate owned</span></li>
                   </ul>
                 </p>
              </div>
            </div>
            <a href="#trackApplicationStatus" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="trackApplicationStatus">
              <span class="fa expand-icon"></span><h4 class="title four bold">Track Application Status</h4>
            </a>
            <div class="text-content left collapse" id="trackApplicationStatus">
              <div class="text">
                <p class="desc-text">
                  Track your application process closely to stay updated on your status. Should more information or additional documents be required, you will want to
                  handle it quickly to avoid potential delays. The <a href="{{ route('appDownload') }}">giOS App for Homeowners</a> will allow you to track updates on your status in the application process.
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a href="{{ route('appDownload') }}" class="main-button flex-button">Download giOS for Homeowners App</a>
            </div>
          </div>
        </div>
      </div>
      <div class="item" id="close">
        <div class="accordion-split-content">
          <div class="description-content">
            <div class="main-heading">
              <div class="app-circle">
                <span class="fa fa-home"></span>
              </div>
              <h3 class="title three bold">Step 6: Close</h3>
              <span class="seperate-line"></span>
              <p class="desc-text">
                Get inspections done, sign contracts, get the key and celebrate the closing on your new home!
              </p>
            </div>
          </div>
          <div class="accordion-content">
            <a href="#getInspectionsDone" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="getInspectionsDone">
              <span class="fa expand-icon"></span><h4 class="title four bold">Get Inspections Done</h4>
            </a>
            <div class="text-content left collapse" id="getInspectionsDone">
              <div class="text">
                <p class="desc-text">
                 Hire a licensed home inspector to complete an inspection of your home. Depending on your home, you may opt to have other inspections completed - such as a pest inspection, chimney inspection,
                 electrical inspection, and a heating and air conditioning inspection. Consult with your expert GuidanceRealty.com participating agent to determine what's best for you and to determine a timeline
                 for what needs to be completed prior to closing.
                </p>
              </div>
            </div>
            <a href="#arrangeForClosingCost" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="arrangeForClosingCost">
              <span class="fa expand-icon"></span><h4 class="title four bold">Arrange for Closing Costs</h4>
            </a>
            <div class="text-content left collapse" id="arrangeForClosingCost">
              <div class="text">
                <p class="desc-text">
                  Your Account Executive will make sure that you are provided with a closing cost estimate, which is the amount you will need to bring to the closing in the form of a cashier's check or wire transfer.
                  Avoid making large purchases and taking on new debt - such as furniture, home appliances or vehicle - so that you do not delay or stop the closing process.
                </p>
              </div>
            </div>
            <a href="#signAndMoveIn" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="signAndMoveIn">
              <span class="fa expand-icon"></span><h4 class="title four bold">Sign the Closing Paperwork, Get the Keys and Move In!</h4>
            </a>
            <div class="text-content left collapse" id="signAndMoveIn">
              <div class="text">
                <p class="desc-text">
                  It's the big day! The closing itself and signing of paperwork typically takes 1-2 hours. Then, enjoy your new home and find comfort in knowing that you became a homeowner without compromising your faith.
                </p>
              </div>
            </div>
            <div class="button-wrapper">
              <a class="main-button" href="{{ mix_remote('docs/GR_Closing Summary_118.pdf') }}" target="_blank">
                <span class="fa fa-cloud-download"></span>Download Closing Guide
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection

@section('ldjson-seo')
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebPage",
  "name" : "HOME BUYING PROCESS",
  "url": "{{ Request::url() }}",
  "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
  "description": "Discover the six steps in the home buying process. Guidance Residential will guide you through each step to help you complete your homebuyer's journey. STEP 1: PLAN Research and map out a budget in stage one of the homebuyer's journey. STEP 2: ACT Complete online Pre-Qualification in less than 10 minutes with no credit check required. STEP 3: CONNECT Connect with a licensed Account Executive to guide you through the home financing process. STEP 4: SHOP Search and find your dream home with the help of a reliable real estate agent. STEP 5: APPLY Connect with a licensed Guidance Account Executive to apply for financing and provide necessary documentations. STEP 6: CLOSE Get inspections done, sign contracts, get the key and celebrate the closing on your new home!"
}
</script>
@endsection
