@extends('layouts.default')

@section('title', 'Media Gallery')

@section('content')
<div class="container media-gallery">
  <div class="main-heading">
    <h1 class="title">
      VIEW OUR PHOTO GALLERY
    </h1>
    <span class="seperate-line"></span>
    <p class="desc-text">
      Guidance Residential has been an integral part of community building among Muslim Americans. Here is a glimpse of recent events where we participated and supported.
    </p>
  </div>
  <div class="split-list-container">
    <div class="item">
      <div class="content">
        <h3 class="title three bold">ICNA 2017</h3>
        <p class="desc-text">Guidance was a major sponsor of MAS-ICNA Convention 2017. Islamic Council of North America (ICNA) is a leading organization in the Muslim American community that holds a national conference every year. Guidance is proud to be a supporter of this year's annual conference.</p>
      </div>
      <div class="gallery">
        <div class="owl-carousel media-gallery owl-theme icna">
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_001.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_002.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_003.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_004.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_005.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_006.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_007.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_008.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0010.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0011.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0012.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0013.jpg') }}">
          </div>
          <div class="item icna">
            <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0014.jpg') }}">
          </div>
        </div>
        <div class="modal fade icna" tabindex="-1" role="dialog" aria-labelledby="ICNA Modal" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="carousel-content">
                  <div class="owl-carousel media-gallery-expand owl-theme icna">
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_001.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_002.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_003.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_004.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_005.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_006.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_007.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_008.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0010.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0011.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0012.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0013.jpg') }}">
                    </div>
                    <div class="item icna">
                      <img src="{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0014.jpg') }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="content">
        <h3 class="title three bold">54th Annual ISNA Convention</h3>
        <p class="desc-text">Guidance Residential sponsored the 54th Annual ISNA Convention. The Islamic Society of North America (ISNA) works to foster the development of the Muslim community, interfaith relations, civic engagement, and better understanding of Islam. The convention was held June 30, 2017 - July 3, 2017.</p>
      </div>
      <div class="gallery">
        <div class="owl-carousel media-gallery owl-theme isna">
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_001.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_002.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_005.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_006.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_007.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_008.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_009.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0010.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0011.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0012.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0013.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0014.jpg') }}">
          </div>
          <div class="item isna">
            <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0015.jpg') }}">
          </div>
        </div>
        <div class="modal fade isna" tabindex="-1" role="dialog" aria-labelledby="ISNA Modal" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="carousel-content">
                  <div class="owl-carousel media-gallery-expand owl-theme isna">
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_001.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_002.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_005.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_006.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_007.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_008.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_009.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0010.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0011.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0012.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0013.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0014.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0015.jpg') }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="content">
        <h3 class="title three bold">American Muslim Consumer Consortium 2016</h3>
        <p class="desc-text">Guidance Residential was a sponsor of American Muslim Consumer Consortium's 2016 event. Guidance CEO Kal Elsayed was among the panel speakers at the event.</p>
      </div>
      <div class="gallery">
        <div class="owl-carousel media-gallery owl-theme amcc">
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_001.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_002.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_003.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_005.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_006.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_008.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0014.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0015.jpg') }}">
          </div>
          <div class="item amcc">
            <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0016.jpg') }}">
          </div>
        </div>
        <div class="modal fade amcc" tabindex="-1" role="dialog" aria-labelledby="AMCC Modal" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="carousel-content">
                  <div class="owl-carousel media-gallery-expand owl-theme amcc">
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_001.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_002.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_003.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_005.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_006.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_008.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0014.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0015.jpg') }}">
                    </div>
                    <div class="item">
                      <img src="{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0016.jpg') }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('ldjson-seo')
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ImageGallery",
  "name": "ICNA 2017",
  "description": "Guidance was a major sponsor of MAS-ICNA Convention 2017. Islamic Council of North America (ICNA) is a leading organization in the Muslim American community that holds a national conference every year. Guidance is proud to be a supporter of this year's annual conference.",
  "image": ["{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_001.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_002.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_003.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_004.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_005.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_006.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_007.jpg') }}", "{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_008.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0010.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0011.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0012.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0013.jpg') }}","{{ mix_remote('images/mediaGallery/ICNA2017/icna2017_0014.jpg') }}"]
}
</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ImageGallery",
  "name": "54TH ANNUAL ISNA CONVENTION",
  "description": "Guidance Residential sponsored the 54th Annual ISNA Convention. The Islamic Society of North America (ISNA) works to foster the development of the Muslim community, interfaith relations, civic engagement, and better understanding of Islam. The convention was held June 30, 2017 - July 3, 2017.",
  "image": ["{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_001.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_002.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_005.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_006.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_007.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_008.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_009.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0010.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0011.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0012.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0013.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0014.jpg') }}","{{ mix_remote('images/mediaGallery/54thAnnual/54thisna_0015.jpg') }}"]
}
</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ImageGallery",
  "name": "AMERICAN MUSLIM CONSUMER CONSORTIUM 2016",
  "description": "Guidance Residential was a sponsor of American Muslim Consumer Consortium's 2016 event. Guidance CEO Kal Elsayed was among the panel speakers at the event.",
  "image":["{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_001.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_002.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_003.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_005.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_006.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_008.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0014.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0015.jpg') }}","{{ mix_remote('images/mediaGallery/AMCC2016/amcconsortium2016_0016.jpg') }}"]
}
</script>
@endsection
