@extends('layouts.default')

@section('title', 'Privacy and Security')

@section('content')
<div class="middle">
  <div class="container privacy-security">
    <div class="main-heading">
      <div class="title">
        Privacy and Security
      </div>

    </div>
    <div class="accordion-split-content full">
      <div class="accordion-content">
        <a href="#termsConditions" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="termsConditions">
          <span class="fa expand-icon"></span><h4 class="title four bold">TERMS, CONDITIONS AND NOTICES</h4>
        </a>
        <div class="text-content left collapse" id="termsConditions">
          <div class="text">
            <h3 class="title three bold">Social Media Disclaimer</h3>
            <p class="desc-text">
              Consumers should not submit personal information through posts or messaging at social media websites such as Facebook, Twitter, LinkedIn or similar sites. Guidance Residential cannot guarantee
              that personal information submitted through these sites will be secure and protected from access by hackers or other unauthorized persons. Please contact us securely through our website or
              contact a Guidance Residential Account Executive for more information about submitting personal information.
            </p>
            <h3 class="title three bold">Privacy Notice</h3>

            <p class="desc-text">
              Your privacy is important to Guidance Residential, LLC and its affiliates. Our policy is to recognize and respect our customers' expectation that their personal and financial information will be
              kept confidential. The following information is to help you understand how we protect the privacy of your nonpublic personal information. "Nonpublic personal information" is information about you
              that we obtain in connection with providing you with our financial product(s).
            </p>
            <p class="desc-text">
              Information that We Collect, retain and use information about individual customers only when and to the extent we believe the information would be useful (and allowed by law) to administer our business
               and provide you with our financial product. The following are examples of the types of nonpublic personal information we maintain or collect:
               <ul>
                 <li><span class="list-text">Information that you provided in connection with your application, on other forms, or verbally, such as your name, address, social security number, income, citizenship status or assets and liabilities.</span></li>
                 <li><span class="list-text">Information about your transactions with us or other parties, such as your payment history, account balance and other account information.</span></li>
                 <li><span class="list-text">Information from a consumer reporting agency (such as your credit worthiness and credit history).</span></li>
                 <li><span class="list-text">Information from business partners, vendors and service companies (such as a property appraisal, purchase contract or membership number).</span></li>
               </ul>
            </p>
            <p class="desc-text">
              There are some cases in which we gather information to comply with laws and regulations governing our industry. For example, we are required by federal regulations to obtain a tax identification number
              (generally a social security number) for our contracts, so that we can report Profit Payments you have paid.
            </p>
            <p class="desc-text">
              We are periodically required to disclose some of this data to third parties, such as credit reporting agencies and investors, in order to do business with you. These parties contractually agree to keep your
               information confidential and not use it for any other purpose.
            </p>
            <h3 class="title three bold">Maintenance of Accurate Information</h3>
            <p class="desc-text">
              We will attempt to keep customer files complete, up-to-date, and accurate in accordance with reasonable commercial standards. We will tell our customers how and where to conveniently access their
               account information (except when prohibited by law) and how to notify us about errors. We will quickly respond to any request that we correct inaccurate information. We will take prompt action to
                make the appropriate corrections and to notify anyone with whom we may have shared inaccurate information.
            </p>
            <h3 class="title three bold">Confidentiality and Security of Information</h3>
            <p class="desc-text">
              We limit employee access to personally identifiable information to those employees with a business reason for knowing the information. We regularly conduct training sessions and otherwise educate
              our employees so they understand the importance of confidentially and customer privacy. We maintain physical, electronic and procedural safeguards that comply with federal regulations to guard your
               nonpublic personal information. We will take appropriate disciplinary measures to enforce our employees' privacy responsibilities.
            </p>
            <h3 class="title three bold">Information Sharing</h3>
            <p class="desc-text">
              Sharing with non-affiliates - Non-affiliate means a company that is not an affiliate of another Guidance Residential company, and it also means a person who is not an employee of a Guidance Residential company.
            </p>
            <p class="desc-text">
              Guidance Residential respects your right to privacy, and therefore, we do not sell, share or otherwise disclose nonpublic personal information we collect or maintain about you to nonaffiliated third parties for
               marketing purposes. Guidance Residential also does not sell your information to third parties.
            </p>
            <p class="desc-text">
              Sharing with affiliates - Affiliates are companies that are related to one another by ownership, common ownership, or control. For example, if one company owns or controls another company, they are affiliates of each other.
            </p>
            <p class="desc-text">
              In order to continue offering and recommending valuable financial products and services that are consistent with Shariah law, we may share your nonpublic personal information within our Guidance Residential
              family of companies. All of these Guidance Residential companies follow the same policies and procedures described in this policy as it relates to your information.
            </p>
            <p class="desc-text">
              You have a choice of whether or not you wish your nonpublic personal information to be shared within our Guidance Residential family of companies. This right is called "Opt Out". Opt Out means that you
              can make the decision that your information will not be shared with affiliates of Guidance Residential. This choice may limit your opportunity to benefit from the many valuable products and services
               offered by Guidance Residential.
            </p>
            <h3 class="title three bold">Questions or Comments</h3>
            <p class="desc-text">
              If you have any questions about our Privacy Policy, please do not hesitate to contact us at 1-866-225-5520 or at compliance@guidanceresidential.com.
            </p>
            <h1 class="title">INTERNET PRIVACY STATEMENT</h1>
            <hr class="seperate-line"></hr>
            <h3 class="title three bold">Agreement Between User and Guidance Residential</h3>
            <p class="desc-text">
              This website ("Site") is offered to you, the User, conditioned upon your acceptance without modification of the terms, conditions and notices contained herein. Your use of the Site constitutes your agreement to all such terms, conditions and notices.
            </p>
            <h3 class="title three bold">Copyright/Trademark Notice</h3>
            <p class="desc-text">
              The contents of all material available on the Site are copyrighted by Guidance Residential unless otherwise indicated. All rights are reserved by Guidance Residential, and except for purposes of
              researching or applying for a product or service offered by Guidance Residential or its affiliates, the content may not be reproduced, downloaded, disseminated, published, or transferred in any
              form or by any means, except with the prior written permission of Guidance Residential.
            </p>
            <p class="desc-text">
              The trademarks, logos and service marks ("Marks") displayed on the Site are the property of Guidance Residential and other parties. Users are prohibited from using any Marks for any purpose including,
               but not limited to, use as metatags on other pages or sites on the World Wide Web without the written permission of Guidance Residential or such third party that may own the Marks.
            </p>
            <h3 class="title three bold">Legal Notice and Disclaimer</h3>
            <p class="desc-text">
              Information on this site is provided "as is" without warranty of any kind, either express or implied, including without limitation the implied warranties of merchantability, fitness for a particular purpose or noninfringement.
            </p>
            <p class="desc-text">
              Guidance Residential (including its employees and agents, its affiliates, and its affiliates' employees and agents) assumes no responsibility for consequences from the use of the information herein, or in any respect
               for the content of such information, including, but not limited to, delays, errors or omissions, the accuracy or reasonableness of information, the defamatory nature of statements, ownership of copyright
               or other intellectual property rights, and the violation of property, privacy or personal rights of others. Guidance Residential is not responsible for, and expressly disclaims all liability for, damages of
             any kind arising out of use, reference or reliance on such information.
            </p>
            <h3 class="title three bold">No Unlawful or Prohibited Use</h3>
            <p class="desc-text">
              As a condition of your use of the Site, you warrant to Guidance Residential that you will not use this Site or any information contained on the Site for any purpose that is unlawful or prohibited by these Terms, Conditions and Notices.
            </p>
            <h3 class="title three bold">Hyper Linking</h3>
            <p class="desc-text">
              At certain places on this Site, we may offer live "links" to other Internet addresses ("Linked Sites"). Such Linked Sites contain information created, published, maintained, or otherwise posted by
              institutions or organizations independent of Guidance Residential. Guidance Residential does not endorse, approve, certify or control these Linked Sites and does not guarantee the accuracy,
              completeness, efficacy, timeliness or correct sequencing of information that they contain. Use of Linked Sites is voluntary, and should only be undertaken after an independent review of the accuracy,
               completeness, efficacy and timeliness of information contained therein. In addition, it is the user's responsibility to take precautions to ensure that material selected from such Linked Sites is
               free of such items as viruses, worms, Trojan horses and other items of a destructive nature. Guidance Residential is not responsible for, and expressly disclaims all liability for, damages of any
               kind arising out of the use of such Linked Sites, or reference to or reliance on information contained therein.
            </p>
            <p class="desc-text">
              Guidance Residential prohibits caching, unauthorized hypertext links to the Site and the framing of any content available through the Site. Guidance Residential reserves the right to disable any
               unauthorized links or frames and specifically disclaims any responsibility for the content available on any other Internet sites linked to the Site.
            </p>
            <h3 class="title three bold">Modification of these Terms, Conditions and Notices</h3>
            <p class="desc-text">
              Guidance Residential reserves the right to change the Terms, Conditions and Notices under which this Site is offered.
            </p>
            <h3 class="title three bold">Confidentiality of User Communication</h3>
            <p class="desc-text">
              Except as required or permitted by law and in accordance with the Guidance Residential Internet Privacy Statement, Guidance Residential will maintain the confidentiality of all communications that contain personal information.
            </p>
            <h3 class="title three bold">General Terms and Conditions</h3>
            <p class="desc-text">
              Guidance Residential reserves the right to terminate this agreement at any time for any reason, including, but not limited to, violating any of the terms or conditions of this agreement.
            </p>
            <p class="desc-text">
              By accessing and using this Site, you agree that disputes arising out of or relating to the contents or use of this Site are to be governed by the laws of the Commonwealth of Virginia and, where applicable, federal law.
            </p>
            <p class="desc-text">
              If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law, including, without limitation, the warranty disclaimers and liability limitations set forth above, then the invalid
              or unenforceable provisions will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in full force and effect.
            </p>
            <p class="desc-text">
              This agreement constitutes the entire agreement between you and Guidance Residential with respect to the use of this Site and it supersedes all prior or contemporaneous communications and proposals,
               whether electronic, oral or written, between you and Guidance Residential with respect to this Site. A printed version of this agreement and of any notice given in electronic form shall be admissible
               in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated
                and maintained in printed form.
            </p>
            <p class="desc-text">
              Any rights not expressly granted herein are reserved.
            </p>
            <p class="desc-text">
              This publication is designed to provide general information about the Declining Balance Co-ownership Program and does not constitute a commitment.
            </p>
            <p class="desc-text">
              Participation in this program is limited to qualified individuals. This program is not available in all states. Please call 1-866-484-3262 for program availability.
            </p>
          </div>
        </div>
        <a href="#securityOverview" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="securityOverview">
          <span class="fa expand-icon"></span><h4 class="title four bold">SECURITY OVERVIEW</h4>
        </a>
        <div class="text-content left collapse" id="securityOverview">
          <div class="text">
            <p class="desc-text">
              To ensure the confidentiality of private information that our company sends you via email and comply with financial regulations, we are implementing a new email encryption service through Zix
              Corporation, the leader in email encryption services.
            </p>
            <p class="desc-text">
              ZixCorp's easy-to-use email protection makes it seamless for you to receive, read and reply to all encrypted email communication we send you. If you are a ZixCorp customer, you do not need to
              do anything. Email is securely sent between our organizations and delivered directly to your Inbox. If you are not currently a ZixCorp customer, you receive confidential email through the Secure Message Center.
            </p>
            <p class="desc-text">
              The protection of confidential communication is important to us and we want to ensure your information stays private. This site helps you understand our encrypted email initiatives.
            </p>
          </div>
        </div>
        <a href="#receivingEmails" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="receivingEmails">
          <span class="fa expand-icon"></span><h4 class="title four bold">RECEIVING ENCRYPTED EMAILS</h4>
        </a>
        <div class="text-content left collapse" id="receivingEmails">
          <div class="text">
            <p class="desc-text">
              When we send you an encrypted message, you receive a notification email with instructions on how to open the message. The notification message arrives in your email Inbox.
               You select Open Message in the notification to go to the Secure Message Center and view your email.
            </p>
            <img src="{{ mix_remote('images/privacySecurity/zix-email1.jpg') }}" alt="Open Message">
            <p class="desc-text">You must go through a one-time registration process.</p>
            <img src="{{ mix_remote('images/privacySecurity/zix-email2.jpg') }}" alt="Register">
            <p class="desc-text">After you register and sign in, your message opens and you can view the message details and reply.</p>
            <img src="{{ mix_remote('images/privacySecurity/zix-email3.jpg') }}" alt="View Message">
          </div>
        </div>
        <a href="#sendingEmails" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="sendingEmails">
          <span class="fa expand-icon"></span><h4 class="title four bold">SENDING ENCRYPTED EMAILS</h4>
        </a>
        <div class="text-content left collapse" id="sendingEmails">
          <div class="text">
            <p class="desc-text">
              You can send an encrypted email to recipients at our organization from the Compose tab within the Secure Message Center. When you select the Compose tab you are taken
              to a page where you can compose your message.
            </p>
            <img src="{{ mix_remote('images/privacySecurity/zix-email4.jpg') }}" alt="Encrypted Email">
          </div>
        </div>
        <a href="#faqs" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="faqs">
          <span class="fa expand-icon"></span><h4 class="title four bold">FREQUENTLY ASKED QUESTIONS</h4>
        </a>
        <div class="text-content left collapse" id="faqs">
          <div class="text">
            <h5 class="title five bold">Q. What Internet browsers are recommended?</h5>
            <p class="desc-text">
              Most Internet browsers will work, including Google Chrome, Microsoft&reg; Internet Explorer&reg;, Mozilla&reg; Firefox&reg; and Apple&reg; Safari&trade;.
            </p>
            <h5 class="title five bold">Q. What mobile devices can be used?</h5>
            <p class="desc-text">
              Most major mobile devices can be used, including Apple&reg; iPhone&reg; and iPad&reg;, BlackBerry&reg; devices and Android&reg; devices.
            </p>
            <h5 class="title five bold">Q. How long do I have to read my message?</h5>
            <p class="desc-text">
              Messages you receive expire 30 days from when you are sent the message.
            </p>
            <h5 class="title five bold">Q. Where can I get more information?</h5>
            <p class="desc-text">
              For more information on any of the products described, visit the ZixCorp website at www.zixcorp.com/products.
            </p>
            <p class="desc-text">
              For additional help reading your message, access the online help within the Secure Message Center.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
