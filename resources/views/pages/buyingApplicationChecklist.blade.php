@extends('layouts.default')

@section('title', 'Home Financing Application Checklist')

@section('content')
  <div class="container home-buying-app-checklist">
    <div class="main-heading">
      <h1 class="title">
        Home Financing Application Checklist
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        The home financing application process is dependent upon receipt of all necessary information and documentation for verification and underwriting purposes.
      </p>
    </div>
    <div class="accordion-split-content">
      <div class="description-content">
        <div class="main-heading">
          <div class="app-circle">
            <span class="fa fa-list-alt"></span>
          </div>
          <h3 class="title three bold">INFORMATION AND DOCUMENTATION CHECKLIST</h3>
          <span class="seperate-line"></span>
          <p class="desc-text">
            Our checklist can help you gather important information that may be required for the home finance application
          </p>
          <p class="desc-text">
            * More information may be required based on the specific individual situation
          </p>
          <a href="{{ route('glossaryOfTerms') }}" class="main-button">Glossary of terms</a>
          <a href="{{ mix_remote('docs/GR_Home_Finance_Application_Checklist_2018.pdf') }}" class="main-button" target="_blank">Download Application Checklist</a>
        </div>
      </div>
      <div class="accordion-content">
        <a href="#financingInfo" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="financingInfo">
          <span class="fa expand-icon"></span><h4 class="title four bold">Financing Information</h4>
        </a>
        <div class="text-content left collapse" id="financingInfo">
          <div class="text">
            <p class="desc-text">
              <ul>
                <li><span class="list-text">Type of financing (Fixed or Adjustable, Conforming or Conforming Jumbo)</span></li>
                <li><span class="list-text">Term of financing (30, 20, or 15-year term)</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#propertyInfo" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="propertyInfo">
          <span class="fa expand-icon"></span><h4 class="title four bold">Property Information</h4>
        </a>
        <div class="text-content left collapse" id="propertyInfo">
          <div class="text">
            <p class="desc-text">
              <ul>
                <li><span class="list-text">Property address</span></li>
                <li><span class="list-text">Financing purpose (Owner occupied, Second home, Investment property)</span></li>
                <li><span class="list-text">Year property was built</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#personalInfo" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="personalInfo">
          <span class="fa expand-icon"></span><h4 class="title four bold">Personal Information</h4>
        </a>
        <div class="text-content left collapse" id="personalInfo">
          <div class="text">
            <p class="desc-text">
              <ul>
                <li><span class="list-text">Photo ID</span></li>
                <li><span class="list-text">Social Security Card</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#employmentInfo" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="employmentInfo">
          <span class="fa expand-icon"></span><h4 class="title four bold">Employment Information</h4>
        </a>
        <div class="text-content left collapse" id="employmentInfo">
          <div class="text">
            <p class="desc-text">
              <ul>
                <li><span class="list-text">Employment history for the past two years</span></li>
                <li><span class="list-text">Most recent pay stubs, reflecting 30 days of YTD earnings</span></li>
                <li><span class="list-text">W-2 forms for the past two (2) years</span></li>
                <li><span class="list-text">Tax returns for the past two (2) years</span></li>
                <li><span class="list-text">If self-employed (personal as well as business returns): complete with all schedules for most recent two (2) years</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="#assetLiabilityInfo" class="accordion-item" data-toggle="collapse"  aria-expanded="false" aria-controls="assetLiabilityInfo">
          <span class="fa expand-icon"></span><h4 class="title four bold">Asset and Liabilities Information</h4>
        </a>
        <div class="text-content left collapse" id="assetLiabilityInfo">
          <div class="text">
            <p class="desc-text">
              <ul>
                <li><span class="list-text">
                  Financial/Bank Statements, reflecting 60 days (all accounts & all pages)
                  <ul>
                    <li><span class="list-text">Include most recent statements for investment accounts and stock</span></li>
                    <li><span class="list-text">Include most recent 401(K) statements (terms and conditions)</span></li>
                  </ul>
                </span></li>
                <li><span class="list-text">Auto loan statements</span></li>
                <li><span class="list-text">Home finance statements for the last two years</span></li>
              </ul>
            </p>
          </div>
        </div>
        <a href="{{ route('account-executive.index') }}" class="main-button gold btn-block">Apply with an Account Executive</a>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "HOME FINANCING APPLICATION CHECKLIST",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "The home financing application process is dependent upon receipt of all necessary information and documentation for verification and underwriting purposes. Our checklist can help you gather important information that may be required for the home finance application. * More information may be required based on the specific individual situation"
  }
  </script>
@endsection
