@extends('layouts.default')

@section('title', 'Podcasts')

@section('content')
  <div class="container podcasts">
    <div class="main-heading">
      <h1 class="title">
        LISTEN TO PODCASTS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Turn your daily commute into a fascinating journey of learning Islamic finance.
      </p>
    </div>
    <div class="podcast-list">
      <div class="podcast split-content-section tile">
        <div class="app-circle">
          <span class="fa fa-comments-o"></span>
        </div>
        <div class="main-heading left">
          <h1 class="title three">
            Interview of Guidance Residential CEO Khaled Elsayed by Deen TV
          </h1>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            In a discussion with the Deen TV team, Guidance Residential CEO Khaled Elsayed talks about our purpose and the unique characteristics of our product and its adherence to Shariah.
          </p>
          <div class="track">
            <iframe
              width="100%" height="166" scrolling="no" frameborder="no"
              src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/344042551&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true">
            </iframe>
          </div>
        </div>
      </div>
      <div class="podcast split-content-section tile">
        <div class="app-circle">
          <span class="fa fa-comments-o"></span>
        </div>
        <div class="main-heading left">
          <h1 class="title three">
            History of Islamic Finance with Shaykh Yusuf Talal DeLorenzo
          </h1>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            Shaykh DeLorenzo takes us through Islamic history and the early history of Islamic finance.
          </p>
          <div class="track">
            <iframe
              width="100%" height="166" scrolling="no" frameborder="no"
              src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/343974277&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true">
             </iframe>
          </div>
          <div class="track">
            <iframe
              width="100%" height="166" scrolling="no" frameborder="no"
              src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/343974365&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true">
            </iframe>
          </div>
          <div class="track">
            <iframe
              width="100%" height="166" scrolling="no" frameborder="no"
              src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/343974558&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true">
            </iframe>
          </div>
        </div>
      </div>
      <div class="podcast split-content-section tile">
        <div class="app-circle">
          <span class="fa fa-comments-o"></span>
        </div>
        <div class="main-heading left">
          <h1 class="title three">
            The Three Islamic Finance Models - Shaykh Yusuf Talal DeLorenzo
          </h1>
          <span class="seperate-line sub"></span>
          <p class="desc-text">
            Shaykh DeLorenzo explains the three commonly used Islamic financial contracts.
          </p>
          <div class="track">
            <iframe
              width="100%" height="166" scrolling="no" frameborder="no"
              src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/343973977&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true">
            </iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "AudioObject",
    "contentUrl": "https://soundcloud.com/gr-marketing/deen-tv-interview-with-guidance-residential-ceo",
    "duration": "T18M11S",
    "encodingFormat": "mp3",
    "name": "Guidance Residential Deen TV Interview With Guidance Residential CEO"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "AudioObject",
    "contentUrl": "https://soundcloud.com/gr-marketing/part-1-the-history-of-islamic-finance-1",
    "duration": "T4M52S",
    "encodingFormat": "mp3",
    "name": "Guidance Residential Part 1. The History Of Islamic Finance 1"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "AudioObject",
    "contentUrl": "https://soundcloud.com/gr-marketing/part-2-the-history-of-islamic-finance",
    "duration": "T04M50S",
    "encodingFormat": "mp3",
    "name": "Guidance Residential Part 2. The History Of Islamic Finance"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "AudioObject",
    "contentUrl": "https://soundcloud.com/gr-marketing/part-3-the-history-of-islamic-finance",
    "duration": "T09M22S",
    "encodingFormat": "mp3",
    "name": "Guidance Residential Part 3. The History Of Islamic Finance"
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "AudioObject",
    "contentUrl": "https://soundcloud.com/gr-marketing/the-three-islamic-home-finance-models-musharaka-murabaha-ijara",
    "duration": "T11M08S",
    "encodingFormat": "mp3",
    "name": "Guidance Residential The Three Islamic Home Finance Models - Musharaka, Murabaha, Ijara"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Podcasts'])
@endsection
