@extends('layouts.default')

@section('title', 'Awards')

@section('content')
  <div class="container awards">
    <div class="main-heading">
      <h1 class="title">
        AWARDS and RECOGNITIONS
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance Residential's role in the Shariah-compliant home financing industry has been recognized by prominent local and global organizations.
      </p>
    </div>
    <div class="awards-list-container">
      <div class="awards">
        <div class="app-circle">
          <img src="{{ mix_remote('images/awards/amja-approved.png') }}" alt="Amja Approved">
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title six">
              Endorsed By amja
            </h1>
            <span class="seperate-line"></span>
            <p class="desc-text">
              The Assembly of Muslim Jurists of America's (AMJA) Fatwa committee ruled that Guidance Residential's
              Declining Balance Co-ownership Program is a "permissible path" for Muslim Americans in need of home financing and that the contract is "sound in general."
            </p>
          </div>
        </div>
      </div>
      <div class="awards">
        <div class="app-circle">
          <img src="{{ mix_remote('images/awards/best-islamic.png') }}" alt="Ajma Approved">
        </div>
        <div class="text">
          <div class="main-heading">
            <h1 class="title six">
              BEST ISLAMIC HOME FINANCE PROVIDER
            </h1>
            <span class="seperate-line"></span>
            <p class="desc-text">
              Guidance Residential was nominated for Best Islamic Home Finance Provider, an award from Islamic Business and Finance Magazine, a publication of CPI Financial. The category compared all Islamic home finance providers worldwide.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org/",
    "@type": "Organization",
    "name": "Guidance Residential",
    "url": "{{ Request::url() }}",
      "awards": ["ENDORSED BY AMJA", "BEST ISLAMIC HOME FINANCE PROVIDER"],
    "image": ["{{ mix_remote('images/awards/amja-approved.png') }}",
     "{{ mix_remote('images/awards/best-islamic.png') }}"]
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Awards'])
@endsection
