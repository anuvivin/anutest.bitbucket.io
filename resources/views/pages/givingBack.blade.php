@extends('layouts.default')

@section('title', 'Giving Back to the Community')

@section('content')
  <div class="container givingBack">
    <div class="main-heading">
      <h1 class="title">
        Corporate Philanthropy
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Guidance is proud to support community causes that align with company's core values and guiding principles. Below are causes Guidance cares about and the organizations and initiatives the company has supported through sponsorships.
      </p>
    </div>
    <div class="giving-back-wrapper">
      <div class="causes-wrapper">
        <div class="cause">
          <div class="icon app-circle">
            <span class="fa fa-graduation-cap"></span>
          </div>
          <div class="main-heading">
            <h3 class="title three bold">
              EDUCATIONAL EMPOWERMENT
            </h3>
            <span class="seperate-line sub"></span>
            <p class="desc-text body-text">
              Guidance believes that education empowers communities. Through education, communities maximize their potential. Guidance supports educational causes that align with its core values.
            </p>
          </div>
        </div>
        <div class="cause">
          <div class="icon app-circle">
            <span class="fa fa-leaf"></span>
          </div>
          <div class="main-heading">
            <h3 class="title three bold">
              ENVIRONMENTAL SUSTAINABILITY
            </h3>
            <span class="seperate-line sub"></span>
            <p class="desc-text body-text">
              Environmental protection is key to sustainable development. Guidance supports United Nation's Key Sustainable Development Goals and strives to support causes that take care of the environment.
            </p>
          </div>
        </div>
        <div class="cause">
          <div class="icon app-circle">
            <span class="fa fa-university"></span>
          </div>
          <div class="main-heading">
            <h3 class="title three bold">
              ECONOMIC EMPOWERMENT
            </h3>
            <span class="seperate-line sub"></span>
            <p class="desc-text body-text">
              Economic empowerment is a driving force for societal growth. Guidance's Shariah compliant home financing program and corporate philanthropy contribute to economic empowerment of communities in the U.S.
            </p>
          </div>
        </div>
        <div class="cause">
          <div class="icon app-circle">
            <span class="fa fa-globe"></span>
          </div>
          <div class="main-heading">
            <h3 class="title three bold">
              SOCIAL JUSTICE and EQUITY
            </h3>
            <span class="seperate-line sub"></span>
            <p class="desc-text body-text">
              Guidance's products and services reflect our commitment to socio-economic justice. We facilitate participation in an ethical model of financing that protects the economic rights of society and support causes that align with this core principle.
            </p>
          </div>
        </div>
      </div>
      <div class="give-back-image">
        <img src="{{ mix_remote('images/givingBack/organizations_supported_through_sponsorships.jpg') }}" alt="Sponsorship House">
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name" : "CORPORATE PHILANTHROPY",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Guidance is proud to support community causes that align with company's core values and guiding principles. Below are causes Guidance cares about and the organizations and initiatives the company has supported through sponsorships."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "EDUCATIONAL EMPOWERMENT",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Guidance believes that education empowers communities. Through education, communities maximize their potential. Guidance supports educational causes that align with its core values."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "ENVIRONMENTAL SUSTAINABILITY",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Environmental protection is key to sustainable development. Guidance supports United Nations Key Sustainable Development Goals and strives to support causes that take care of the environment."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "ECONOMIC EMPOWERMENT",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Economic empowerment is a driving force for societal growth. Guidances Shariah compliant home financing program and corporate philanthropy contribute to economic empowerment of communities in the U.S."
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name" : "SOCIAL JUSTICE AND EQUITY",
    "url": "{{ Request::url() }}",
    "image" : "{{ mix_remote('images/GR_LOGO.png') }}",
    "description": "Guidances products and services reflect our commitment to socio-economic justice. We facilitate participation in an ethical model of financing that protects the economic rights of society and support causes that align with this core principle."
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Philathropy'])
@endsection
