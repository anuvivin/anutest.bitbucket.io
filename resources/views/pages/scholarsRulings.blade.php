@extends('layouts.default')

@section('title', 'Scholar\'s Ruling (Fatwa)')

@section('content')
  <div class="container scholars-rulings">
    <div class="main-heading">
      <div class="title">
        Scholar's Rulings (Fatwa)
      </div>
      <span class="seperate-line"></span>
      <p class="desc-text">
        Learn more about the legal opinion on our program provided by the independent Shariah Board.
      </p>
    </div>
    <div class="owl-carousel scholars-rulings owl-theme">
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Disclosures.pdf') }}" target="_blank">
          <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Disclosures_thumb.jpg') }}" alt="Fatwa on Disclosures">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Title-Registration.pdf') }}" target="_blank">
          <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Title_Registration_thumb.jpg') }}" alt="Fatwa on Title Registration">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Disclosures-Urdu.pdf') }}" target="_blank">
          <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Disclosures_Urdu_thumb.jpg') }}" alt="Fatwa on Disclosures Urdu">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Adjustable-Program.pdf') }}" target="_blank">
          <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Adjustable_Program_thumb.jpg') }}" alt="Fatwa on Adjustable Program">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Main-Program-Urdu.pdf') }}" target="_blank">
        <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Main_Program_Urdu_thumb.jpg') }}" alt="Fatwa on Main Program Urdu">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Title-Registration-Urdu.pdf') }}" target="_blank">
        <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Title_Registration_Urdu_thumb.jpg') }}" alt="Fatwa on Title Registration Urdu">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-TX-Program.pdf') }}" target="_blank">
        <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_TX_Program_thumb.jpg') }}" alt="Fatwa on TX Program">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Main-Program-Arabic.pdf') }}" target="_blank">
        <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Main_Program_Arabic_thumb.jpg') }}" alt="Fatwa on Main Program Arabic">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Sharia-Board-Letter.pdf') }}" target="_blank">
        <img src="{{ mix_remote('images/scholarsRulings/Sharia_Board_Letter_thumb.jpg') }}" alt="Sharia Board Letter">
        </a>
      </div>
      <div class="item">
        <a href="{{ mix_remote('docs/Fatwa-on-Main-Program.pdf') }}" target="_blank">
        <img src="{{ mix_remote('images/scholarsRulings/Fatwa_on_Main_Program_thumb.jpg') }}" alt="Fatwa on Main Program">
        </a>
      </div>
    </div>
  </div>
  <div class="single-video-section">
    <div class="main-heading">
      <h1 class="title six">
        WATCH A VIDEO ON THE TOPIC
      </h1>
      <span class="seperate-line"></span>
      <p class="desc-text">
        What is a Shariah compliance audit?
      </p>
    </div>
    <div class="video-container">
      <div class="video scholars" data-video-id="ZS7cd_D4ujI">
        <img src="{{ mix_remote('images/scholarsRulings/ScholarsRulings.jpg') }}" alt="Leading shariah and islamic finance scholar" class="video-thumbnail">
        <div class="video-play"></div>
      </div>
    </div>
  </div>
@endsection

@section('ldjson-seo')
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    "name": "Shariah Compliance Audit with Shaykh Yusuf Talal DeLorenzo",
    "description": "Shariah Compliance Audit with Shaykh Yusuf Talal DeLorenzo",
    "uploadDate": "2017-01-10T22:38:15.000Z",
    "interactionCount": "471",
    "duration": "PT4M41S",
    "embedURL": "https://youtube.googleapis.com/v/ZS7cd_D4ujI",
    "thumbnailURL": "https://i.ytimg.com/vi/ZS7cd_D4ujI/hqdefault.jpg"
  }
  </script>
@endsection

@section('end')
  @include('components.modals.webinar-banner', ['refLink' => 'GR_Website_Fatwas'])
@endsection
