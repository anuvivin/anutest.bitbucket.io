@extends('layouts.default')

@section('title', 'Page Not Found')

@section('content')
<div class="container error-page">
  <div class="main-heading">
    <h1 class="title">OOPS!</h1>
    <span class="seperate-line sub"></span>
    <p class="desc-text no-margin">We can't seem to find the page you're looking for.</p>
    <h4 class="title four bold">Error Code: 404</h4>
    <p class="desc-text link-header">Here are some helpful links instead:</p>
    <a href="{{ route('home') }}" class="sitemap-link">Home</a>
    <a href="{{ route('ourCompany') }}" class="sitemap-link">What We Do</a>
    <a href="{{ route('operationalStates') }}" class="sitemap-link">Operational States</a>
    <a href="{{ config('urls.customer-portal.prequalify') }}?referral=GR_Website_404Page_Feb2018" class="sitemap-link">Pre-Qualify Online</a>
    <a href="{{ route('homeBuyingProcess') }}" class="sitemap-link">Home Buying Process</a>
    <a href="{{ route('refinancingProcess') }}" class="sitemap-link">Refinancing Process</a>
    <a href="{{ route('contactUs') }}" class="sitemap-link">Contact Us</a>
    <a href="{{ route('myAccount') }}" class="sitemap-link">My Account</a>
  </div>
</div>
@endsection
