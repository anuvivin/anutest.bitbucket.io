<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Guidance Residential, LLC - e-Sign Act Disclosure and Consent</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .footer {
            padding-right: 15px;
            padding-left: 15px;
            padding-top: 19px;
            color: #777;
            border-top: 1px solid #e5e5e5;
        }
        @media screen and (min-width: 768px) {
            .footer {
                padding-right: 0;
                padding-left: 0;
            }
        }
    </style>

  </head>
  <body>

    <div class="container">

        @yield('content')

        <footer class="footer">
            <p>&copy; Guidance Residential, LLC</p>
        </footer>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </body>
</html>
