@extends('econsent.layout')
@section('content')

<div class="row">
    <div class="text-center">
        <h2>GUIDANCE RESIDENTIAL, LLC</h2>
        <h4>E-SIGN ACT DISCLOSURE AND CONSENT</h4>
    </div>
</div>
<div class="row">
    <div class="text-center alert alert-danger">
        @if (empty($confirmationCode))
        <strong>Confirmation code missing</strong>
        @else
        <strong>Invalid confirmation code</strong>
        @endif         
    </div>
</div>
@stop
