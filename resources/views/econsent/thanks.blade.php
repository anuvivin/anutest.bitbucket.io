@extends('econsent.layout')
@section('content')

<div class="row">
    <div class="text-center">
        <h2>GUIDANCE RESIDENTIAL, LLC</h2>
        <h4>E-SIGN ACT DISCLOSURE AND CONSENT</h4>
    </div>
</div>
<div class="row">
    <div class="col-xs-10 col-xs-offset-1 panel panel-default">
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt>Name: </dt>
                <dd>{{ $data->name }}</dd>
                <dt>Contract #: </dt>
                <dd>{{ $data->contract_number }}</dd>
                <dt>Email Address: </dt>
                <dd>{{ $data->email }}</dd>
                <dt>Consent Date: </dt>
                <dd>{{ $data->date_given }}</dd>
            </dl>
            <p>Thank you! Your consent has been successfully received and a PDF copy was emailed directly to you. Please feel free to contact your dedicated Account Executive if you have any questions. You may close this window now.</p>
        </div>
    </div>
</div>
@stop