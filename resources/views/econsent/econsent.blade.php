@extends('econsent.layout')
@section('content')

<div class="row">
    <div class="text-center">
        <h2>GUIDANCE RESIDENTIAL, LLC</h2>
        <h4>E-SIGN ACT DISCLOSURE AND CONSENT</h4>
    </div>
</div>
<div class="row">
    <div class="col-xs-10 col-xs-offset-1 panel panel-default">
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt>Name: </dt>
                <dd>{{ $data->name }}</dd>
                <dt>Contract #: </dt>
                <dd>{{ $data->contract_number }}</dd>
                <dt>Email Address: </dt>
                <dd>{{ $data->email }}</dd>
            </dl>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-10 col-xs-offset-1 well well-lg">
        <p>This disclosure and consent provides important information required to be given to you by the Electronic Signatures in Global and National Commerce Act (E-SIGN Act).  In order to engage in the most prompt service possible regarding your home financing transaction, Guidance Residential, LLC provides required documents and disclosures for our home financing transactions electronically.  We must have your consent in order to send the information to you electronically.</p>
        <p>By checking the "I Consent" box below, you affirmatively consent to conduct business electronically with Guidance Residential, LLC in connection with establishing the your home financing application and to receive electronically all related documents, disclosures, or agreements including this E-SIGN Act Disclosure and Consent.</p>
        <p>This E-SIGN Act Disclosure and Consent and any documents, disclosures, or agreements can be printed and/or downloaded by you and are available to you in paper hardcopy upon request.</p>
        <p>Please read the following agreements and disclosures prior to checking "I Consent" below.</p>
        <p><strong>Applicability of Consent</strong> - Your electronic consent to conduct business electronically applies to receiving pre-approval documents and/or establishing your application for a home financing transaction with Guidance Residential, LLC.</p>
        <p><strong>Email Address and Keeping Your Information Current</strong> - In order to communicate with you regarding your application, you must provide us with a valid email address.  It is your responsibility to promptly notify us of any changes to your email address.  You can update your email address with us by calling or emailing the Account Executive working with you on your application, email us at <a href="mailto:compliance@guidanceresidential.com">compliance@guidanceresidential.com</a> or submitting your change in writing via mail to Guidance Residential, LLC, ATTN:  Compliance Department, 11107 Sunset Hills Rd, Suite 100 &amp; 200, Reston, VA 20190.</p>
        <p><strong>Print and Download Agreements and Disclosures</strong> - Most information provided to you will be in PDF format.  You must have Adobe Acrobat Reader 6.0 or later versions on your computer.  A free copy of Adobe Acrobat Reader may be obtained from the Adobe website at <a href="http://www.adobe.com">http://www.adobe.com</a>.</p>
        <p>To print or download agreements, disclosures, and other documents, you must have a printer connected to your computer or have sufficient hard-drive space on your computer to download, store and view the information.  To print, click on the document PDF link, select print, select your printer and click OK.  To download and save the information, click on the document PDF link, select Save or Save As.</p>
        <p><strong>Hardware and Software Requirements</strong> - You must have access to a computer with browser software such as Microsoft Internet Explorer, Adobe Acrobat Reader , Internet access and an active/operating email address (at your cost).  Your computer needs to support the following requirements:  Microsoft Internet Explorer 8.0 or higher, Google Chrome 10.0 or higher or equivalent software and hardware capable of running this software.</p>
        <p>By affirmatively consenting, you confirm that you have access to the necessary hardware and software.</p>
        <p><strong>All Agreements and Disclosures are "In Writing"</strong> - This and all agreements and disclosures delivered electronically are considered "in writing" and are available to you in a form that you may keep by either printing or downloading.</p>
        <p><strong>Obtaining Paper Copies of Agreements and Disclosures</strong> - You can obtain a paper copy of the E-SIGN Disclosure and Consent and any of the agreements and disclosures by printing them yourself.  You can also contact your Account Executive to request a paper copy.  The paper copy will be sent to you free of charge when sent by us through regular US Mail.</p>
        <p><strong>Withdrawing Consent</strong> - You have the right to withdraw your electronic consent to conduct business electronically with us by contacting your Account Executive, emailing us at <a href="mailto:compliance@guidanceresidential.com">compliance@guidanceresidential.com</a> or submitting your change in writing via mail to Guidance Residential, LLC, ATTN:  Compliance Department, 11107 Sunset Hills Rd, Suite 100 &amp; 200, Reston, VA 20190.</p>
        <p><u>Failing to consent to receive documents electronically or withdrawing your consent will significantly delay the receipt of pre-approval documents and/or the processing of your application due to the time required for you to receive documents from us and return documents to us by regular mail.  In addition to a delay in processing, failing to consent or withdrawal could also delay the closing of your transaction for an additional 7 - 10 business days.</u></p>
        <p><strong>Governing Law</strong> - This E-SIGN Act Disclosure and Consent is made in Virginia and shall be governed by the laws of the Commonwealth of Virginia, to the extent that Virginia law is not inconsistent with controlling Federal law.</p>
    </div>
</div>
<form method="POST">
<div class="row">
    <div class="col-xs-10 col-xs-offset-1 panel panel-default">
        <div class="panel-body">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="i-consent-confirmation" value="1" onclick="if(this.checked) { document.getElementById('consent-div').style.display='';document.getElementById('consent-alt-text').style.display=''; } else { document.getElementById('consent-div').style.display='none';document.getElementById('consent-alt-text').style.display='none'; }"> I CONSENT <span id="consent-alt-text" style="display:none;">(Please scroll down to click Submit)</span>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row" id="consent-div" style="display:none;">
    <div class="col-xs-10 col-xs-offset-1 panel panel-default">
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt>Name: </dt>
                <dd>{{ $data->name }}</dd>
                <dt>Contract #: </dt>
                <dd>{{ $data->contract_number }}</dd>
                <dt>Email Address: </dt>
                <dd>{{ $data->email }}</dd>
                <dt>Date: </dt>
                <dd>{{ date('m/d/Y g:i:s a') }}</dd>
                <dt>Computer IP: </dt>
                <dd>{{ Request::getClientIp() }}</dd>
            </dl>
            <div class="text-right">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
</form>

@stop
