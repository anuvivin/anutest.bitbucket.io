@component('mail::message')
# Home Listing Requested

## User Info
- **Name:** {{ $name }}
- **Email Address:** {{ $email }}
- **Phone Number:** {{ $phone }}
- **Location:** {{ $city }}, {{ $state }}
@endcomponent
