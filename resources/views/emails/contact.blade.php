@component('mail::message')
# Contact Requested

## User Info
- **Name:** {{ $name }}
- **Email Address:** {{ $email }}
- **Phone Number:** {{ $phone }}
- **Reason:** {{ $reason }}

@if (!empty($comments))
## Comments
@component('mail::panel')
{{ $comments }}
@endcomponent
@endif
@endcomponent
