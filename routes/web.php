<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 * EConsent
 */
Route::get('/econsent', 'EConsentController@getEConsent')->name('econsent.create');
Route::post('/econsent', 'EConsentController@postEConsent')->name('econsent.store');

/*
 * Forms
 */
Route::post('/free-consultation', 'FreeConsultationController@store')->name('free-consultation.store');
Route::post('/contact-us', 'ContactUsController@store')->name('contact-us.store');
Route::post('/list-your-home', 'ListYourHomeController@store')->name('list-your-home.store');

/*
 * Pages
 */
Route::view('/', 'pages.home')->name('home');
Route::view('/community-leader-reviews', 'pages.endorsements')->name('endorsements');
Route::view('/islamic-home-finance-co-ownership-program', 'pages.coOwnership')->name('coOwnership');
Route::view('/message-from-ceo', 'pages.ceoMessage')->name('ceoMessage');
Route::view('/shariah-compliant-faq-video-library', 'pages.videoLibrary')->name('videoLibrary');
Route::view('/islamic-home-finance-awards-recognition', 'pages.awards')->name('awards');
Route::view('/community-involvement', 'pages.givingBack')->name('givingBack');
Route::view('/islamic-home-finance-white-papers', 'pages.whitePapers')->name('whitePapers');
Route::view('/islamic-home-finance-webinars', 'pages.webinars')->name('webinars');
Route::view('/podcasts', 'pages.podcasts')->name('podcasts');
Route::view('/leadership-and-management', 'pages.ourTeam')->name('ourTeam');
Route::view('/why-guidance', 'pages.whyGuidance')->name('whyGuidance');
Route::view('/customer-reviews', 'pages.testimonials')->name('testimonials');
Route::view('/our-history', 'pages.ourHistory')->name('ourHistory');
Route::view('/us-islamic-home-finance', 'pages.ourCompany')->name('ourCompany');
Route::view('/guidance-in-the-news', 'pages.guidanceInNews')->name('guidanceInNews');
Route::view('/home-buying-reasons', 'pages.homeBuyingReasons')->name('homeBuyingReasons');
Route::view('/refinancing-reasons', 'pages.refinancingReasons')->name('refinancingReasons');
Route::view('/home-buying-questions', 'pages.homeBuyingQuestions')->name('homeBuyingQuestions');
Route::view('/home-buying-process', 'pages.homeBuyingProcess')->name('homeBuyingProcess');
Route::view('/refinancing-process', 'pages.refinancingProcess')->name('refinancingProcess');
Route::view('/islamic-home-financing-checklist', 'pages.buyingApplicationChecklist')->name('buyingApplicationChecklist');
Route::view('/islamic-home-refinancing-checklist', 'pages.refinancingApplicationChecklist')->name('refinancingApplicationChecklist');
Route::view('/e-books', 'pages.eBooks')->name('eBooks');
Route::view('/privacy-and-security', 'pages.privacySecurity')->name('privacySecurity');
Route::view('/licensing-and-registrations', 'pages.licensingRegistrations')->name('licensingRegistrations');
Route::view('/assistance-and-repayment', 'pages.assistanceRepayment')->name('assistanceRepayment');
Route::view('/islamic-finance-scholars-ruling-fatwa', 'pages.scholarsRulings')->name('scholarsRulings');
Route::view('/glossary-of-terms', 'pages.glossaryOfTerms')->name('glossaryOfTerms');
Route::view('/home-finance-calculators', 'pages.estimationCalculators')->name('estimationCalculators');
Route::view('/shariah-board', 'pages.shariahBoard')->name('shariahBoard');
Route::view('/islamic-home-finance-options', 'pages.financingOptions')->name('financingOptions');
Route::view('/islamic-home-refinance-options', 'pages.refinancingOptions')->name('refinancingOptions');
Route::view('/refinancing-questions', 'pages.refinancingQuestions')->name('refinancingQuestions');
Route::view('/events-photo-gallery', 'pages.mediaGallery')->name('mediaGallery');
Route::view('/careers', 'pages.careers')->name('careers');
Route::view('/download-gios-app', 'pages.appDownload')->name('appDownload');
Route::view('/licensed-us-locations', 'pages.operationalStates', [
    'offices' => [], //GiosService::getOperationStatesData(),
])->name('operationalStates');
Route::view('/contact-us', 'pages.contactUs')->name('contactUs');
Route::view('/my-account', 'pages.myAccount')->name('myAccount');

/*
 * Rates
 */
Route::get('/home-financing-rates', 'RatesController@getPurchaseRates')->name('purchaseRates');
Route::get('/home-refinancing-rates', 'RatesController@getRefinanceRates')->name('refinanceRates');
Route::redirect('/rates', '/home-financing-rates', 301)->name('rates');
Route::get('/rates/json', 'RatesController@getCurrentRates')->name('ratesJson');
Route::get('/rates/edit', 'RatesController@edit')->name('rates.edit');
Route::post('/rates/update', 'RatesController@update')->name('rates.update');

/*
 * Guidance Realty
 */
Route::view('/buyers-save-at-closing', 'pages.guidanceRealty')->name('guidanceRealty');
Route::view('/sell-your-home-realty-program', 'pages.sellWithGuidance')->name('sellWithGuidance');
Route::view('/sign-up-for-realty-partner-program', 'pages.signUpGetStarted')->name('signUpGetStarted');
Route::view('/realty-program', 'pages.getPreQualifiedRefferals')->name('getPreQualifiedRefferals');

/*
 * Press Releases
 */
Route::view('/press-releases', 'pages.pressReleases.index')->name('pressReleases');
Route::view('/press-releases/guidance-residential-launches-new-website-to-simplify-islamic-home-financing', 'pages.pressReleases.launchesNewWebsite')->name('pressReleases.launchesNewWebsite');
Route::view('/press-releases/leading-us-islamic-home-financing-provider-launches-new-mobile-app', 'pages.pressReleases.launchesNewMobileApp')->name('pressReleases.launchesNewMobileApp');
Route::view('/press-releases/guidance-residential-simplifies-islamic-home-financing-for-consumers', 'pages.pressReleases.grSimplifiesHomeFinancing')->name('pressReleases.grSimplifiesHomeFinancing');
Route::view('/press-releases/best-islamic-home-financier-usa-by-world-finance', 'pages.pressReleases.awardedByWorldFinance')->name('pressReleases.awardedByWorldFinance');
Route::view('/press-releases/finance-operations-in-southeast', 'pages.pressReleases.arkansasAlabamaOperations')->name('pressReleases.arkansasAlabamaOperations');
Route::view('/press-releases/islamic-bank-morocco', 'pages.pressReleases.islamicBankMorocco')->name('pressReleases.islamicBankMorocco');
Route::view('/press-releases/finance-operations-in-delaware', 'pages.pressReleases.delawareOperations')->name('pressReleases.delawareOperations');
Route::view('/press-releases/finance-operations-in-kentucky', 'pages.pressReleases.southeastOperations')->name('pressReleases.southeastOperations');
Route::view('/press-releases/university-bank-of-ann-arbor-liable-for-trade-secret-misappropriation', 'pages.pressReleases.tradeSecretMisappropriation')->name('pressReleases.tradeSecretMisappropriation');
Route::view('/press-releases/new-york-metro-office', 'pages.pressReleases.newYorkMetroOffice')->name('pressReleases.newYorkMetroOffice');
Route::view('/press-releases/encouraged-by-amja', 'pages.pressReleases.amjaResolution')->name('pressReleases.amjaResolution');
Route::view('/press-releases/new-jersey-office', 'pages.pressReleases.newJerseyOffice')->name('pressReleases.newJerseyOffice');
Route::view('/press-releases/chicago-office', 'pages.pressReleases.chicagoOffice')->name('pressReleases.chicagoOffice');
Route::view('/press-releases/southern-california-office', 'pages.pressReleases.southernCaliOffice')->name('pressReleases.southernCaliOffice');
Route::view('/press-releases/historic-islamic-finance-training', 'pages.pressReleases.historicIslamicFinance')->name('pressReleases.historicIslamicFinance');
Route::view('/press-releases/landmark-islamic-finance-training', 'pages.pressReleases.landmarkIslamicTraining')->name('pressReleases.landmarkIslamicTraining');
Route::view('/press-releases/pioneering-event', 'pages.pressReleases.pioneeringEvent')->name('pressReleases.pioneeringEvent');
Route::view('/press-releases/corporate-headquarters-moves-to-new-home', 'pages.pressReleases.corporateHeadquartersMovesToNewHome')->name('pressReleases.corporateHeadquartersMovesToNewHome');
Route::view('/press-releases/two-billion-in-sharia-compliant-financing', 'pages.pressReleases.twoBillionInShariaCompliantFinancing')->name('pressReleases.twoBillionInShariaCompliantFinancing');
Route::view('/press-releases/nominated-for-best-islamic-home-finance-provider-award', 'pages.pressReleases.nominatedForBestIslamicHomeFinanceProviderAward')->name('pressReleases.nominatedForBestIslamicHomeFinanceProviderAward');
Route::view('/press-releases/corporate-headquarters-moves-to-reston-va', 'pages.pressReleases.corporateHeadquartersMovesToRestonVa')->name('pressReleases.corporateHeadquartersMovesToRestonVa');

/*
 * Account Executives
 */
Route::get('/connect-with-account-executive/{state?}', 'AccountExecutiveController@index')->name('account-executive.index');
Route::get('/account-executive-profile/{ae}', 'AccountExecutiveController@show')->name('account-executive.show');

/*
 * MUST BE LAST ROUTE
 * This is a catch-all route for any special handling of missing pages.
 */
Route::get('{url}', 'MissingPageController@catch')->name('missing-page.catch');
