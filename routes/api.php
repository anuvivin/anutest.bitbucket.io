<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('free-consultation', 'FreeConsultationController@store')->name('free-consultation.store');
Route::post('contact-us', 'ContactUsController@store')->name('contact-us.store');
Route::post('list-your-home', 'ListYourHomeController@store')->name('list-your-home.store');
